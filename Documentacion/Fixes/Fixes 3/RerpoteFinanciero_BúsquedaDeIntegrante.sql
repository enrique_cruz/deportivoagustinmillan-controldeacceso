
--   1. Módulo Reporte Financiero. Reporte financiero no muestra ninguna interfaz. No entregado

		UPDATE Permisos SET Ruta='/ReporteFinanciero' WHERE permisosId=1031


--2  Módulo Alumnos
--    Submódulo Disciplinas
--        - Corregir función de Actualizar horarios, días ó actividades, no guarda los cambios y elimina la actualización. No entregado




--3. Módulo Rentas 
--    Submódulo Integrantes 
--        - La barra de búsqueda no funciona bien, busca los integrantes de todos los equipos y no muestra a integrantes nuevos ingresados. No entregado
GO
			ALTER PROC [dbo].[IntegrantesEquipoSearchBy]
				@equipoId INT,
				@strBusqueda VARCHAR(50)
			AS
			BEGIN
				SELECT 
					IE.equipoId, E.equipoNombre ,IE.integranteId, I.integranteNombre, I.integranteApellidoPaterno, 
					I.integranteApellidoMaterno, I.integranteCURP, I.integranteTelefono, IE.integrantesEquipoActivo integranteActivo
				FROM IntegrantesEquipo IE 
					JOIN Integrantes I ON IE.integranteId = I.integranteId
					JOIN Equipos E ON IE.equipoId = E.equipoId
				WHERE IE.equipoId = @equipoId AND
							 (I.integranteNombre LIKE CONCAT(@strBusqueda, '%') OR					
							 I.integranteCURP LIKE CONCAT(@strBusqueda, '%'))
			END

-- 4. Motor de Base de datos
--    - Insertar pago de mensualidad de febrero a usuarios para evitar que les cobre un monto extra al pagar Marzo (adjunto archivo). No entregado

--    - Verificar SP de validación de tiempo de tolerancia de Entrada, ya que, permite el acceso antes y después del tiempo de tolerancia. No entregado

--    - Usuarios duplicados porque no tenían CURP y se le asignó una aleatoria (eliminarlos). No entregado
		SELECT * FROM Alumno WHERE alumnoCURP LIKE 'CURP%' 
		SELECT * FROM TutorAlumno

-- 5. Observaciones
--    - ¿Cómo es el cobro de anualidad?, es decir, ¿cuándo le cobraría el sistema, el día que se inscribe ó desde el día 1ro del mes en que se inscribe?. Estatus: Resuelto
--    - Se abre sesión master en otros usuarios intermitentemente (error esporádico hasta ahora). Estatus: En revisión



