
ALTER PROC [dbo].[ReporteinscritosPorMesPorDisciplina]
	@disciplinaId INT,
	@fechaInicio DATE,
	@fechafin DATE
AS
BEGIN

	SELECT DISTINCT 
			APH.alumnoIdFk,A.alumnoNombre, A.alumnoApellidoPaterno, A.alumnoApellidoMaterno, APH.planIdFk,
			(SELECT disciplinaNombre FROM Disciplina WHERE disciplinaId=@disciplinaId) disciplinaNombre, 
			CONVERT(VARCHAR(10),APH.alumnoplanhorarioFechaDeCreacion,103) FechaInscripcion,		P.planDescripcion
	FROM AlumnoPlanHorario APH
		JOIN Alumno A ON APH.alumnoIdFk = A.alumnoId
		JOIN PlanDeInscripcion P ON APH.planIdFk = P.planId
	WHERE 
			horarioIdFk IN (SELECT horarioId FROM Horario WHERE disciplinaIdFk=@disciplinaId) AND
			alumnoplanhorarioFechaDeCreacion 
				BETWEEN DATEADD(DAY,-1,@fechaInicio) AND DATEADD(DAY,1,@fechafin) AND
			APH.alumnoplanhorarioActivo='SI' AND
			A.alumnoActivo = 'SI' AND
			P.planActivo = 'SI'

END
