
CREATE PROC [dbo].[CargarMensualidad]
	@disciplinaId INT,
	@folio VARCHAR(10),
	@horarioInicio TIME,
	@horarioFin TIME,
	@mensualidadImporte DECIMAL(10,2),	
	@concepto VARCHAR(500),
	@observaciones VARCHAR(500)
AS
BEGIN

	DECLARE @alumnoId INT
	DECLARE @planId INT
	DECLARE @horarioId INT
	DECLARE @result INT
	DECLARE @counter INT 
	DECLARE @planCounter INT

	SET @alumnoId = 0
	SET @planId = 0
	SET @horarioId =0
	SET @result=0
	SET @counter=0
	SET @planCounter=0

	SET @counter=(SELECT COUNT(alumnoId) FROM Alumno WHERE Folio=@folio)
	
	IF(@counter>1)
		BEGIN
			--HAY MAS DE UN ALUMNO CON ESE FOLIO
			SET @result=3
		END
	ELSE
		BEGIN
	
				SET @alumnoId =  (SELECT alumnoId FROM Alumno WHERE Folio=@folio)

				IF(@alumnoId>0)
					BEGIN		
						
						SET @planCounter =(
																SELECT COUNT(planIdFk) FROM
																(
																	SELECT  DISTINCT planIdFk FROM AlumnoPlanHorario WHERE alumnoIdFk = @alumnoId AND alumnoplanhorarioActivo='SI' AND horarioIdFk IN 
																	(
																		SELECT horarioId FROM Horario WHERE disciplinaIdFk=@disciplinaId AND horarioInicio=@horarioInicio AND horarioFin=@horarioFin AND horarioActivo='SI'
																	)
																) T																
														)		
						IF(@planCounter>1)
							BEGIN
								SET @result=4
							END
						ELSE
							BEGIN										
										SET @planId = (SELECT  DISTINCT planIdFk FROM AlumnoPlanHorario WHERE alumnoIdFk = @alumnoId AND alumnoplanhorarioActivo='SI' AND horarioIdFk IN 
													(
														SELECT horarioId FROM Horario WHERE disciplinaIdFk=@disciplinaId AND horarioInicio=@horarioInicio AND horarioFin=@horarioFin AND horarioActivo='SI'
													)
												)			
												
										IF(@planId>0)
											BEGIN								
												INSERT INTO Mensualidades 
													(alumnoIdFk, mensualidadImporte, mensualidadFechaDePago, mensualidadFechaRealDepago, mensualidadConRetraso, mensualidadImportePorRetraso, mensualidadesActivo, mensualidadesPorcentajePago, planIdFk, mensualidaSaldoAFavor, cuentaBeneficiaria, conceptoDePago, observaciones)
												VALUES
													(@alumnoId, @mensualidadImporte, '2022-02-01', '2022-02-01', 'NO', 0.00, 'SI', 100, @planId, 0.00, 2, @concepto, @observaciones)
												SET @result = 1
											END
										ELSE
											BEGIN
												--NO EXISTE PLAN U HORARIO
												SET @result=2
											END
							END													
					END
				ELSE
					BEGIN
						--NO EXISTE EL ALUMNO
						SET @result=0
					END
		END

	SELECT @result

END


GO

CREATE PROC cargaParcialidadDePago
	@folio VARCHAR(15),
	@parcialidadImporte DECIMAL,
	--@parcialidadFechaDeRegistro DATE,
	@tipoDePago VARCHAR(150),
	@folioDePago VARCHAR(150),
	@conceptoDePago VARCHAR(150),
	@observaciones VARCHAR(150)
AS
BEGIN
	DECLARE @resullt	INT
	DECLARE @alumnoId INT
	SET @resullt = 0

	SET @alumnoId = (SELECT alumnoId FROM Alumno WHERE Folio=@folio)

	INSERT INTO ParcialidadDePago 
		(alumnoId, parcialidadImporte, parcialidadFechaRegistro, TipoDePago, folioDePago, cuentaBeneficiaria, conceptoDePago, observaciones, fechaDeReciboDePago)
	VALUES
		(@alumnoId, @parcialidadImporte, '2022-02-01', @tipoDePago, @folioDePago, '2', @conceptoDePago, @observaciones,'2022-02-01')

	SELECT SCOPE_IDENTITY() 

END



SELECT * FROM Alumno WHERE alumnoId IN
(
		SELECT alumnoIdFk FROM AlumnoPlanHorario WHERE horarioIdFk IN
	(
		SELECT horarioId FROM Horario WHERE horarioDias='Miercoles' AND horarioInicio='12:00'
	)
)
