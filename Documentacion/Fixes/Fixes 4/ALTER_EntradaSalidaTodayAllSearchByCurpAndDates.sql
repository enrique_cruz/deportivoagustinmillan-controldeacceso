

ALTER PROC [dbo].[EntradaSalidaTodayAllSearchByCurpAndDates]
	@CURP VARCHAR(18),
	@fechaInicio DATE,
	@fechaFin DATE
AS
BEGIN

	DECLARE @dia VARCHAR(25)
	DECLARE @auxDia VARCHAR(35)
	SET @dia = (SELECT DATENAME(WEEKDAY, GETDATE()) )
	 
	SET @auxDia = CASE @dia
								WHEN 'Monday' THEN 'Lunes'
								WHEN 'Tuesday' THEN 'Martes'
								WHEN 'Wednesday' THEN 'Miercoles'
								WHEN 'Thursday' THEN 'Jueves'
								WHEN 'Friday' THEN 'Viernes'
								WHEN 'Saturday' THEN 'Sábado'
								WHEN 'Sunday' THEN 'Domingo'
						  END

	SELECT * FROM (
		SELECT  DISTINCT
		 APH.alumnoIdFk, A.alumnoNombre, A.alumnoApellidoPaterno, A.alumnoApellidoMaterno, D.disciplinaNombre,--H.horarioId, H.horarioInicio, H.horarioFin, H.horarioDias,
			'' horarioDias, '' horarioInicio, '' horarioFin, 0 horarioId,	T.tutorNombre, T.tutorApellidoPaterno, T.tutorApellidoMaterno, 
			ES.entradasalidaFechaEntrada, ES.entradasalidaFechaSalida, 0 isExit
		FROM AlumnoPlanHorario APH
			JOIN Alumno A ON APH.alumnoIdFk = A.alumnoId
			JOIN Horario H ON APH.horarioIdFk = H.horarioId
			JOIN Disciplina D ON H.disciplinaIdFk = D.disciplinaId
			JOIN TutorAlumno TA ON TA.alumnoIdFk = A.alumnoId
			JOIN Tutor T ON TA.tutorIdFk = T.tutorId
			JOIN EntradaSalida ES ON ES.alumnoIdFk = A.alumnoId
		WHERE 
			 CONVERT(VARCHAR, ES.entradasalidaFechaEntrada,23)
			 BETWEEN DATEADD(DAY, -1, CONVERT(VARCHAR,@fechaInicio,23)) AND	DATEADD(DAY, 1, CONVERT(VARCHAR,@fechaFin,23)) AND
			a.alumnoCURP = @CURP
			
		UNION

		SELECT 					
			I.instructorId alumnoIdFk, I.instructorNombre alumnoNombre, I.instructorApellidoPaterno alumnoApellidoPaterno,
			I.instructorApellidoMaterno alumnoApellidoMaterno, '' disciplinaNombre, '' horarioId, '' horarioInicio, 
			'' horarioFin, '' horarioDias, '' tutorNombre, '' tutorApellidoPaterno, '' tutorApellidoMaterno,
			ESI.fechaEntrada entradasalidaFechaEntrada, ESI.fechaSalida entradasalidaFechaSalida, 0 isExit
		FROM Instructor I
			JOIN EntradaSalidaInstructores ESI ON ESI.instructorId = I.instructorId
		WHERE 			
			 CONVERT(VARCHAR, ESI.fechaEntrada,23)
			 BETWEEN DATEADD(DAY, -1, CONVERT(VARCHAR,@fechaInicio,23)) AND	DATEADD(DAY, 1, CONVERT(VARCHAR,@fechaFin,23)) AND	
			I.instructorCURP = @CURP
	) Q
	ORDER BY Q.entradasalidaFechaEntrada DESC	
END




	