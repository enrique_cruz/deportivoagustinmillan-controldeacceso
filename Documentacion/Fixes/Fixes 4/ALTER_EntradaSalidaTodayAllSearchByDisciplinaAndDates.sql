
ALTER PROC [dbo].[EntradaSalidaTodayAllSearchByDisciplinaAndDates]
	@disciplinaId INT,
	@fechaInicio DATE,
	@fechaFin DATE
AS
BEGIN

	DECLARE @dia VARCHAR(25)
	DECLARE @auxDia VARCHAR(35)
	SET @dia = (SELECT DATENAME(WEEKDAY, GETDATE()) )
	

	SET @auxDia = CASE @dia
								WHEN 'Monday' THEN 'Lunes'
								WHEN 'Tuesday' THEN 'Martes'
								WHEN 'Wednesday' THEN 'Miercoles'
								WHEN 'Thursday' THEN 'Jueves'
								WHEN 'Friday' THEN 'Viernes'
								WHEN 'Saturday' THEN 'Sábado'
								WHEN 'Sunday' THEN 'Domingo'
						  END

	SELECT * FROM (
		SELECT  DISTINCT		 
			APH.alumnoIdFk, A.alumnoNombre, A.alumnoApellidoPaterno, A.alumnoApellidoMaterno, D.disciplinaNombre,
			'' horarioId, '' horarioInicio, '' horarioFin, '' horarioDias, --H.horarioId, H.horarioInicio, H.horarioFin, H.horarioDias,
			T.tutorNombre, T.tutorApellidoPaterno, T.tutorApellidoMaterno, ES.entradasalidaFechaEntrada, ES.entradasalidaFechaSalida,
			0 isExit
		FROM AlumnoPlanHorario APH
			JOIN Alumno A ON APH.alumnoIdFk = A.alumnoId
			JOIN Horario H ON APH.horarioIdFk = H.horarioId
			JOIN Disciplina D ON H.disciplinaIdFk = D.disciplinaId
			JOIN TutorAlumno TA ON TA.alumnoIdFk = A.alumnoId
			JOIN Tutor T ON TA.tutorIdFk = T.tutorId
			JOIN EntradaSalida ES ON ES.alumnoIdFk = A.alumnoId
		WHERE 
			 CONVERT(VARCHAR, ES.entradasalidaFechaEntrada,23)
			 BETWEEN DATEADD(DAY, -1, CONVERT(VARCHAR,@fechaInicio,23)) AND	DATEADD(DAY, 1, CONVERT(VARCHAR,@fechaFin,23)) AND
			D.disciplinaId=@disciplinaId  AND
			APH.alumnoplanhorarioActivo='Si'
		
		-- EN LA BUSQUEDA DE INSTRUCTORES NO LA METEMOS PORQUE NO EXISTE EL CAMPO DISCIPLINA

	) Q
	ORDER BY Q.entradasalidaFechaEntrada DESC	
END

