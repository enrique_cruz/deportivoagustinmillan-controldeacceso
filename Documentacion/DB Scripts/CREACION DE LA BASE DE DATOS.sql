
CREATE DATABASE CADAM
GO 

USE CADAM

CREATE  TABLE Roles
(
	rolesId INT PRIMARY KEY IDENTITY,
	rolesNombre VARCHAR(35),
	rolesFechaDeCreacion DATE DEFAULT GETDATE(),
	rolesActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Permisos
(
	permisosId INT PRIMARY KEY IDENTITY,
	permisosNombre VARCHAR(50),
	permisosFechaDeCracion DATE DEFAULT GETDATE(),
	permisosActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Usuario
(
	usuarioId INT PRIMARY KEY IDENTITY,
	usuarioNombre VARCHAR(75),
	usuarioApellidoPaterno VARCHAR(50),
	usuarioApellidoMaterno VARCHAR(50),
	usuarioCorreoElectronico VARCHAR(100),
	usuarioPassword VARCHAR(50),
	rolRolIdFk INT FOREIGN KEY REFERENCES Roles (rolesId),
	usuarioFechaDeCreacion DATE DEFAULT GETDATE(),
	usuarioActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE RolPermiso
(
	rolpermisoId INT PRIMARY KEY IDENTITY,
	rolIdFk INT FOREIGN KEY REFERENCES Roles(rolesId),
	permisoIdFk INT FOREIGN KEY REFERENCES Permisos(permisosId),
	rolpermisoFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioId INT FOREIGN KEY REFERENCES Usuario(usuarioId)
)
GO


CREATE TABLE Alumno2
(
	alumnoId INT PRIMARY KEY IDENTITY,
	alumnoNombre VARCHAR(75) NOT NULL,
	alumnoApellidoPaterno VARCHAR(50) NOT NULL,
	alumnoApellidoMaterno VARCHAR(50) NOT NULL,
	alumnoCURP VARCHAR(18) NOT NULL,
	alumnoFechaDeNacimiento DATE NOT NULL,
	alumnoSexo VARCHAR(9),
	alumnoEstatura DECIMAL(3,2),
	alumnoPeso DECIMAL (7,2),
	alumnoRutaFoto VARCHAR(500),
	alumnoTelefonoCasa VARCHAR(10),
	alumnoTelefonoCelular VARCHAR(10),
	alumnoCorreoElectronico VARCHAR(100),
	alumnoRedesSociales VARCHAR(500),
	alumnoPassword VARCHAR(50),
	alumnoCodigoPostal INT,
	alumnoEstado VARCHAR(50),
	alumnoMunicipio VARCHAR(75),
	alumnoColonia VARCHAR(75),
	alumnoCalle VARCHAR(150),
	alumnoAfiliacionMedica VARCHAR(50) NOT NULL,
	alumnoAlergias VARCHAR(350) NOT NULL,
	alumnoPadecimientos VARCHAR(350) NOT NULL,
	alumnoTipoDeSangre VARCHAR(6), 
	alumnoFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	alumnoActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Tutor
(
	tutorId INT PRIMARY KEY IDENTITY,
	tutorNombre VARCHAR(75) NOT NULL,
	tutorApellidoPAterno VARCHAR(50) NOT NULL,
	tutorApellidoMaterno VARCHAR(50) NOT NULL,
	tutorParentesco VARCHAR(50),
	tutorTelefonoCasa VARCHAR(10),
	tutorTelefonoCelular	VARCHAR(10),
	tutorCorreoElectronico VARCHAR(100),
	tutorFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	tutorActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE TutorAlumno
(
	tutoralumnoId INT PRIMARY KEY IDENTITY,
	tutorIdFk INT FOREIGN KEY REFERENCES Tutor(tutorId) NOT NULL,
	alumnoIdFk INT FOREIGN KEY REFERENCES Alumno(alumnoId) NOT NULL,
	tutoralumnoFechaDeregistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	tutoralumnoActivo VARCHAR(2) DEFAULT 'SI'
)
GO

--################################################################
CREATE TABLE Categoria
(
	categoriaId INT PRIMARY KEY IDENTITY,
	categoriaNombre VARCHAR(75) NOT NULL,
	categoriaEdadRango1 INT NOT NULL,
	categoriaEdadRango2 INT NOT NULL,
	categoriaFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	categoriaActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Disciplina
(
	disciplinaId INT PRIMARY KEY IDENTITY,
	disciplinaNombre VARCHAR(50) NOT NULL,
	disciplinaFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	disciplinaActivo VARCHAR(2) DEFAULT 'SI'
)

CREATE TABLE Instructor
(
	instructorId INT PRIMARY KEY IDENTITY,
	instructorNombre VARCHAR(75) NOT NULL,
	instructorApellidoPaterno VARCHAR(50) NOT NULL,
	instructorApellidoMaterno VARCHAR(50) NOT NULL,
	instructorCURP VARCHAR(18),
	instructorRFC VARCHAR(13) NOT NULL,
	instructorCodigoPostal INT,
	instructorEstado VARCHAR(75),
	instructorMunicipio VARCHAR(75),
	instructorColonia VARCHAR(50),
	instructorCalle VARCHAR(150),
	instructorNumero INT,
	instructorFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	instructorActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Gimnasio 
(
	gimnasioId INT PRIMARY KEY IDENTITY,
	gimnasioNombre VARCHAR(75) NOT NULL,
	gimnasioFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	gimnasioActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE PlanDeInscripcion
(
	planId INT PRIMARY KEY IDENTITY,
	disciplinaId INT FOREIGN KEY REFERENCES Disciplina(disciplinaId),
	planDescripcion VARCHAR (150) NOT NULL,
	planRecurrencia VARCHAR (25) NOT NULL, -- LISTA 1 clase a la semana, 2 clases a la semana etc...
	planDias VARCHAR(100) NOT NULL, --LUNES, MARTES, MIERCOLES, JUEVES, VIERNES ETC
	planImporte DECIMAL(6,2) NOT NULL,
	planFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	planActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Mensualidades
(
	mensualidadId INT PRIMARY KEY IDENTITY,
	alumnoIdFk INT FOREIGN KEY REFERENCES Alumno(alumnoId) NOT NULL,
	mensualidadImporte DECIMAL(6,2) NOT NULL,
	mensualidadFechaDePago VARCHAR(75),
	mensualidadFechaRealDepago DATE DEFAULT GETDATE(),
	mensualidadConRetraso VARCHAR(2) DEFAULT 'NO',
	mensualidadImportePorRetraso DECIMAL (5,2),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId)
)
GO

CREATE TABLE Inscripciones 
(
	inscrpcionId INT PRIMARY KEY IDENTITY,
	alumnoIdFk INT FOREIGN KEY REFERENCES Alumno(alumnoId),
	inscripcionImporteAnual DECIMAL (6,2),
	inscripcionFechaDeVigencia DATETIME DEFAULT GETDATE(),
	inscripcionFechaDeVencimiento DATETIME DEFAULT DATEADD(YEAR, 1, GETDATE()),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	horarioActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE Horario
(
	horarioId INT PRIMARY KEY IDENTITY,
	horarioNombre VARCHAR(250), -- BOXEO INFANTIL 3 DIAS 
	disciplinaIdFk INT FOREIGN KEY REFERENCES Disciplina(disciplinaId), --DEPORTE
	categoriaIdFk INT FOREIGN KEY REFERENCES Categoria(categoriaId),
	gimnasioIdFk INT FOREIGN KEY REFERENCES  Gimnasio(gimnasioId),
	instructorIdFk INT FOREIGN KEY REFERENCES Instructor(instructorId),
	horarioInicio TIME,
	horarioFin TIME,
	horarioDias VARCHAR(250),
	horarioFechaDeRegistro DATE DEFAULT GETDATE(),
	usuarioIdFk INT FOREIGN KEY REFERENCES Usuario(usuarioId),
	horarioActivo VARCHAR(2) DEFAULT 'SI'
)
GO

CREATE TABLE AlumnoPlanHorario
(
	alumnoplanhorarioId INT PRIMARY KEY IDENTITY,
	alumnoIdFk INT FOREIGN KEY REFERENCES Alumno(alumnoId),
	planIdFk INT FOREIGN KEY REFERENCES PlanDeInscripcion(planId),
	horarioIdFk INT FOREIGN KEY REFERENCES Horario(horarioId),	
)
GO

CREATE TABLE EntradaSalida
(
	entradasalidaId INT PRIMARY KEY IDENTITY,
	alumnoIdFk INT FOREIGN KEY REFERENCES Alumno(alumnoId),
	entradasalidaFechaEntrada DATETIME,
	entradasalidaFechaSalida DATETIME,
)
GO



/**  INSERT ROLES Y PERMISOS  **/
	INSERT INTO Roles (rolesNombre) VALUES
		('Administrador'),
		('Colaborador')
	GO

	INSERT INTO Permisos (permisosNombre) VALUES
	('SuperUsuario'),
	('Solo Lectura')
	GO

	--SELECT * FROM Roles
	--SELECT * FROM Permisos

	INSERT INTO  RolPermiso (rolIdFk, permisoIdFk) VALUES
	(1,1)

	--SELECT * FROM RolPermiso

/**  INSERT Usuario Administrador **/
INSERT INTO Usuario 
	(usuarioNombre, usuarioApellidoPaterno, usuarioApellidoMaterno, usuarioCorreoElectronico, usuarioPassword, rolRolIdFk)
VALUES
('WebMaster','*','*','webmaster@dam.com','123',1)

--SELECT * FROM Usuario


/**  ########################### CREACION DE STORED PROCDURES  ###########################  **/

	/** ROLES Y PERMISOS **/
	SELECT RP.rolpermisoId, R.rolesNombre, P.permisosNombre FROM RolPermiso RP
	JOIN Roles R ON
		RP.rolIdFk = R.rolesId
	JOIN Permisos P ON 
		RP.permisoIdFk = P.permisosId
	GO


	/** USUARIOS **/



	/** Alumno **/
	--Datos Dummy
	INSERT INTO Alumno 	(
		alumnoNombre, alumnoApellidoPaterno, alumnoApellidoMaterno, alumnoCURP, alumnoFechaDeNacimiento, alumnoSexo, alumnoEstatura,
		alumnoPeso, alumnoRutaFoto, alumnoTelefonoCasa, alumnoTelefonoCelular, alumnoCorreoElectronico, alumnoRedesSociales, alumnoCodigoPostal,
		alumnoEstado, alumnoMunicipio, alumnoColonia, alumnoCalle, alumnoAfiliacionMedica, alumnoAlergias, alumnoPadecimientos, alumnoTipoDeSangre
		) VALUES
		('Jos� Enrique','Cruz','Perales','CUPE871007HMCRRN00','1987/10/07','Masculino',1.70,75.0,'C:\Users\Enrique Cruz\Pictures\Personas\persona2','7225137660','7225137660','enrique.cruz@grupopissa.com.mx','',50220,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto Gomez 26','IMSS','NO','Migra�a','O+'),
		('VINCENT AMAURI','ABUNDIS',' MEJIA','AUMV950427HGRBJN07','1994/04/27','Masculino',1.70,85.0,'C:\Users\Enrique Cruz\Pictures\Personas\persona3','7225137661','7225137661','correodummy@grupopissa.com.mx','https://www.facebook.com/profile?', 502201,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto Gomez 26','IMSS','NO','Migra�a','O+' ),
		('ANA ALICIA','ACU�A','SERRANO','AUSA941203MCCCRN07','1994/12/03','Femenino', 1.56,50,'C:\Users\Enrique Cruz\Pictures\Personas\persona4','7225137662','7225137662','correodummy2@grupopissa.com.mx','https://www.facebook.com/profile?', 50202 ,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto Gomez 26','IMSS','NO','NO','O+' ),
		('SANTOS JONATAN','AGUILAR','RUIZ','AURS931106HSPGZN00','1993/11/06','Masculino',1.80, 85,'C:\Users\Enrique Cruz\Pictures\Personas\persona5','7225137663','7225137663','correodummy3@grupopissa.com.mx','https://www.facebook.com/profile?', 50203,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto Gomez 26','IMSS','NO','NO','O+' )
		
	SELECT * FROM Alumno


	/** Categoria **/
	INSERT INTO Categoria (categoriaNombre, categoriaEdadRango1, categoriaEdadRango2) VALUES
	('Infantil / Juvenil',6,14),
	('Adultos',15,65),
	('Adultos de tercera edad',66,100)

	SELECT * FROM Categoria

	/** Disciplina **/
	INSERT INTO Disciplina (disciplinaNombre) VALUES
	('Boxeo'),
	('Yoga'),
	('Zumba'),
	('Aquafitness'),
	('Maternal')

	SELECT * FROM Disciplina

	/** Gimnasio **/
	INSERT INTO Gimnasio (gimnasioNombre) VALUES 
	('Gimnasio de Boxeo'),
	('Gimansio E'),
	('Gimnasio D'),
	('Gimnasio B')

	SELECT * FROM Gimnasio

	/** Inscripciones **/
	SELECT * FROM Alumno
	INSERT INTO Inscripciones (alumnoIdFk,inscripcionImporteAnual,inscripcionFechaDeVigencia, inscripcionFechaDeVencimiento) VALUES
	(1, 500, '2021/03/10', '2022/03/10'),
	(2, 500, '2021/09/07', '2022/09/07'),
	(3, 500, '2019/01/01', '2020/01/01'),
	(4, 500, '2020/08/09', '2021/08/09')

	SELECT * FROM Inscripciones
	--UPDATE Inscripciones SET inscripcionActivo ='NO' WHERE alumnoIdFk=3

	/** Insctructor **/
	INSERT INTO Instructor 
		(instructorNombre, instructorApellidoPaterno, instructorApellidoMaterno, instructorCURP, instructorRFC, instructorCodigoPostal,
		 instructorEstado, instructorMunicipio, instructorColonia, instructorCalle, instructorNumero) VALUES
		 ('LINDA STEPHANIE','ANDRADE','DIAZ','AADL650608MNLNZN07','AADL650608MNL', 50040, 'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto G�mez', 18),
		 ('LAURA CECILIA','ARAGON','MORALES','AAML650429MCHRRR09','AAML650429MCH', 52000,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto G�mez', 26),
		 ('JACQUELINE','BALDERAS','PLASCENCIA','BAPJ650212MSRLLC08','BAPJ650212MSR', 52001,'Estado de M�xico','Toluca','San Mateo Otzacatipan','Primera Privada de Filiberto G�mez', 50 )
		 
		 SELECT * FROM Instructor
		 ALTER TABLE Instructor ADD   instructorFechaDeNacimiento DATE
		 ALTER TABLE Instructor ADD   instructorSexo VARCHAR(15)

		 /** Mensualidades **/		 
		 ALTER TABLE Mensualidades
		 ALTER COLUMN mensualidadFechaDePago DATE

		 SELECT * FROM Alumno
		 SELECT * FROM Inscripciones
		 
		 INSERT INTO Mensualidades 
		 (alumnoIdFk, mensualidadImporte, mensualidadFechaDePago, mensualidadFechaRealDepago, mensualidadConRetraso, mensualidadImportePorRetraso) VALUES
		 ( 1, 187, '2021-11-03',  '2021-09-08', 'NO', 0),
		 ( 2, 187, '2021-08-09',  '2021-09-08', 'SI', 50)

		 SELECT * FROM Mensualidades

		 /** PLAN DE INSCRIPCION **/
		 SELECT * FROM PlanDeInscripcion
		 SELECT * FROM Disciplina

		 INSERT INTO PlanDeInscripcion (disciplinaId, planDescripcion, planRecurrencia, planDias, planImporte) VALUES
		 (1, 'Publico en general','2 Clases a la semana','Martes y Jueves,S�bados y Domingos', 187 ),
		 (1, 'Pensionado - Jubilado - Adulto Mayor','2 Clases a la semana','Martes y Jueves, S�bado y Domingo', 187 ),
		 (2, 'Publico en general','1 Clase a la semana','S�bados o Domingos', 187),
		 (2, 'Pensionado - Jubilado - Adulto Mayor','1 Clase a la semana','Martes o Jueves o Domingo', 122 ),
		 (3, 'Publico en general','1 Clase a la semana','S�bados o Domingos', 187),
		 (3, 'Pensionado - Jubilado - Adulto Mayor','1 Clase a la semana','Martes o Jueves o Domingo', 122 ),
		 (1, 'Publico en general','4 Clases a la semana','Martes a Viernes', 187 ),
		 (1, 'Pensionado - Jubilado - Adulto Mayor','4 Clases a la semana','Martes a Viernes', 187 )
		
		 SELECT * FROM PlanDeInscripcion
		 UPDATE PlanDeInscripcion SET planDescripcion ='Publico en general', planRecurrencia = '2 Clases a la semana', planDias='Martes y Jueves,S�bados y Domingos' WHERE planId=1 
		 UPDATE PlanDeInscripcion SET planDescripcion ='Pensionado - Jubilado - Adulto Mayor', planRecurrencia = '2 Clases a la semana', planDias='Martes y Jueves,S�bados y Domingos', planImporte=122 WHERE planId=2
		 UPDATE PlanDeInscripcion SET planDescripcion ='Publico en general', planRecurrencia = '4 Clases a la semana'  WHERE planId=7
		 UPDATE PlanDeInscripcion SET planDescripcion ='Pensionado - Jubilado - Adulto Mayor', planRecurrencia = '4 Clases a la semana', planImporte=122 WHERE planId=8


		 /** Tutor **/
		 SELECT * FROM Alumno
		 SELECT * FROM Tutor

		 INSERT INTO Tutor (tutorNombre, tutorApellidoPAterno, tutorApellidoMaterno, tutorParentesco, tutorTelefonoCasa, tutorTelefonoCelular, tutorCorreoElectronico) VALUES
		 ('Yasm�n','Sol�s','Perales','Mam�','7224567891','7224567891','ysols@gamil.com'),
		 ('Yvette Carol','S�nchez','G�mez','Tia','7224567891','7224567891','ycarol16@outlook.com')
		 SELECT * FROM Tutor

		 INSERT INTO TutorAlumno (tutorIdFk, alumnoIdFk) VALUES
		 (1,1),
		 (1,4),
		 (2,2)


		 /** HOARIO **/
		 SELECT * FROM Horario
		 ALTER TABLE  Horario ADD horarioTurno VARCHAR(30)

		 SELECT * FROM Disciplina
		 SELECT * FROM Categoria
		 SELECT * FROM Gimnasio
		 SELECT * FROM Instructor

		 INSERT INTO Horario (horarioNombre, disciplinaIdFk, categoriaIdFk, gimnasioIdFk, instructorIdFk, horarioInicio, horarioFin, horarioDias, horarioTurno) VALUES
		 ('BOX mayores de 10 a�os', 1,  1,  1,  1,'07:00','09:00','Martes,Miercoles,Jueves,Viernes','Matutino'),
		 ('BOX mayores de 10 a�os', 1,  1,  1,  1,'10:00','12:00','Martes,Miercoles,Jueves,Viernes','Matutino'),
		 ('BOX mayores de 10 a�os', 1,  1,  1,  2,'15:00','17:00','Martes,Miercoles,Jueves,Viernes','Vespertino'),
		 ('BOX mayores de 10 a�os', 1,  1,  1,  2,'17:00','19:00','Martes,Miercoles,Jueves,Viernes','Vespertino'),
		 ('Yoga mayores de 16 a�os', 2,  2,  2,  3,'07:00','09:00','Martes,Miercoles,Jueves,Viernes','Matutino'),
		 ('Yoga mayores de 16 a�os', 2,  2,  2,  3,'10:00','12:00','Martes,Miercoles,Jueves,Viernes','Matutino')
		 
		 UPDATE Horario SET horarioDias= 'Miercoles,Viernes' WHERE  horarioId=5 OR horarioId=6
		 SELECT * FROM Horario

		 /** ALUMNOPLANHORARIO **/
		 SELECT * FROM AlumnoPlanHorario
		 
		 SELECT * FROM Alumno
		 SELECT * FROM Disciplina
		 SELECT * FROM PlanDeInscripcion
		 SELECT * FROM Horario
		 
		 INSERT INTO  AlumnoPlanHorario (alumnoIdFk, planIdFk, horarioIdFk) VALUES
		 (1,7,1),
		 (1,7,3),
		 (2,3, 6)
		 GO


		 /** STORED PROCEDURE DE USUARIOS, ROLES Y PERMISOS **/
		 
		 SELECT * FROM Usuario
		 SELECT * FROM Roles
		 SELECT * FROM Permisos
		 SELECT * FROM RolPermiso
		 GO
		 
		 CREATE PROCEDURE UsuarioLogin
			@usuarioCorreoElectronico VARCHAR(150),
			@usuarioPassword VARCHAR(150)
		AS
		BEGIN
			SELECT 
				U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno, U.usuarioCorreoElectronico, 
				R.rolesNombre, P.permisosNombre --RP.permisoIdFk      U.usuarioPassword,
			FROM Usuario U
			JOIN Roles R
				ON U.rolRolIdFk = R.rolesId
			JOIN RolPermiso RP
				ON U.rolRolIdFk = RP.rolIdFk
			JOIN Permisos P
				ON RP.permisoIdFk = P.permisosId
			WHERE 
				U.usuarioActivo='SI' AND
				U.usuarioCorreoElectronico = @usuarioCorreoElectronico AND
				U.usuarioPassword = @usuarioPassword
		END
		GO


		CREATE PROC UsuarioRecuperarPassword
			@usuarioCorreoElectronico VARCHAR(150)
		AS
		BEGIN
			IF EXISTS(SELECT * FROM Usuario WHERE usuarioCorreoElectronico= @usuarioCorreoElectronico) 
				BEGIN
					-- Si existe la cuenta
					RETURN SELECT 1
				END
			ELSE
				BEGIN
					-- No existe la cuenta
					RETURN SELECT 0
				END
		END
		GO

		CREATE PROC UsuarioNuevaContrase�a
			@usuarioCorreoElectronico VARCHAR(150),
			@nuevoPassword VARCHAR(150)
		AS
		BEGIN
			UPDATE Usuario SET usuarioPassword = @nuevoPassword WHERE usuarioCorreoElectronico= @usuarioCorreoElectronico
		END
		GO


		CREATE PROC UsuariosGetAll
		AS
		BEGIN
			--SELECT 
			--	U.usuarioId, U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno, U.usuarioCorreoElectronico, 
			--	R.rolesNombre, P.permisosNombre, U.usuarioActivo, U.usuarioPassword --RP.permisoIdFk      U.usuarioPassword,
			--FROM Usuario U
			--JOIN Roles R
			--	ON U.rolRolIdFk = R.rolesId
			--JOIN RolPermiso RP
			--	ON U.rolRolIdFk = RP.rolIdFk
			--JOIN Permisos P
			--	ON RP.permisoIdFk = P.permisosId	

			SELECT 
				U.usuarioId, U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno, U.usuarioCorreoElectronico, 
				R.rolesNombre,  U.usuarioActivo, U.usuarioPassword
			FROM Usuario U
			JOIN Roles R
				ON U.rolRolIdFk = R.rolesId			
			ORDER BY U.UsuarioNombre
		END
		GO
		
		CREATE PROC UsuariosGetAllActive
		AS
		BEGIN
			SELECT 
				U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno, U.usuarioCorreoElectronico, 
				R.rolesNombre, P.permisosNombre --RP.permisoIdFk      U.usuarioPassword,
			FROM Usuario U
			JOIN Roles R
				ON U.rolRolIdFk = R.rolesId
			JOIN RolPermiso RP
				ON U.rolRolIdFk = RP.rolIdFk
			JOIN Permisos P
				ON RP.permisoIdFk = P.permisosId
			WHERE 
				U.usuarioActivo='SI' 				
		END
		GO

		CREATE PROC UsuariosGetAllInactive
		AS
		BEGIN
			SELECT 
				U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno, U.usuarioCorreoElectronico, 
				R.rolesNombre, P.permisosNombre --RP.permisoIdFk      U.usuarioPassword,
			FROM Usuario U
			JOIN Roles R
				ON U.rolRolIdFk = R.rolesId
			JOIN RolPermiso RP
				ON U.rolRolIdFk = RP.rolIdFk
			JOIN Permisos P
				ON RP.permisoIdFk = P.permisosId
			WHERE 
				U.usuarioActivo='NO' 				
		END
		GO

		CREATE PROC UsuarioCreate
			@usuarioNombre VARCHAR(75),
			@usuarioApellidoPaterno VARCHAR(75),
			@usuarioApellidoMaterno VARCHAR(75),
			@usuarioCorreoElectronico VARCHAR(150),
			@usuarioPassword VARCHAR(50),
			@rolIdFk INT
		AS
		BEGIN
			DECLARE @result INT
			SET @result=0

			IF NOT EXISTS(SELECT * FROM Usuario WHERE  usuarioCorreoElectronico=@usuarioCorreoElectronico)
				BEGIN
					INSERT INTO Usuario (usuarioNombre, usuarioApellidoPaterno, usuarioApellidoMaterno, usuarioCorreoElectronico, usuarioPassword, rolRolIdFk)
					VALUES(@usuarioNombre, @usuarioApellidoPaterno, @usuarioApellidoMaterno, @usuarioCorreoElectronico, @usuarioPassword, @rolIdFk)
				
					SET @result=1
				END

			SELECT @result
		END


		/** PERMISOS **/
		SELECT * FROM Permisos
		GO

		CREATE  PROC PermisosGetAll
		AS
		BEGIN 
			SELECT * FROM Permisos
		END
		GO

		CREATE PROC PermisosGetAllActive
		AS
		BEGIN
			SELECT * FROM Permisos WHERE permisosActivo = 'SI'
		END
		GO

		CREATE PROC PrmisosGetAllInactive
		AS
		BEGIN
			SELECT * FROM Permisos WHERE permisosActivo='NO'
		END
		GO

		/** Roles **/

		SELECT * FROM Roles
		GO

		CREATE PROC RolesGetAll
		AS
		BEGIN 		
			SELECT * FROM Roles
		END
		GO

		CREATE PROC RolesGetAllActive
		AS
		BEGIN
			SELECT * FROM Roles WHERE rolesActivo='SI'
		END
		GO

		CREATE PROC RolesGetAllInactive
		AS
		BEGIN
			SELECT * FROM Roles WHERE rolesActivo='NO'
		END
		GO


		CREATE PROC RolesCreate
			@rolNombre VARCHAR(50)	
		AS
		BEGIN
		
			DECLARE @result INT
			SET @result=0
			
			IF  NOT EXISTS( SELECT * FROM Roles WHERE rolesNombre= @rolNombre)
				BEGIN
					INSERT INTO Roles (rolesNombre) VALUES (@rolNombre)
					SET @result= (SELECT SCOPE_IDENTITY())
				END			
				
			SELECT @result
		END
		GO


		/** ROLES PERMISO**/		
		SELECT * FROM Roles
		SELECT * FROM Permisos
		SELECT * FROM RolPermiso
		GO

		CREATE PROC RolesPermisoGetById
			@rolIdFk INT
		AS
		BEGIN
			SELECT R.rolesId, R.rolesNombre, P.permisosId, P.permisosNombre FROM RolPermiso RP
			JOIN Roles R
				ON RP.rolIdFk = R.rolesId
			JOIN Permisos P
				ON Rp.permisoIdFk = P.permisosId
			WHERE RP.rolIdFk = @rolIdFk
		END
		GO



		--#
		CREATE PROC RolPermisoDelete
			@rolIdFk INT
		AS
		BEGIN
			DELETE FROM RolPermiso WHERE rolIdFk=@rolIdFk
		END
		GO
		--#

		--#
		CREATE PROC RolesPermisoCreate
			@rolIdFk INT,
			@permisoIdFk INT
		AS
		BEGIN
								
			IF NOT EXISTS( SELECT * FROM RolPermiso WHERE rolIdFk=@rolIdFk AND permisoIdFk = @permisoIdFk)
				BEGIN
					INSERT INTO RolPermiso(rolIdFk, permisoIdFk) VALUES (@rolIdFk, @permisoIdFk)
				END
			
		END	
		Go
		--#

		CREATE PROC ActivarDesactivarRol
			@rolId INT,
			@status VARCHAR(2)
		AS
		BEGIN 
						
			UPDATE Roles SET rolesActivo=@status

			SELECT 'Actualizaci�n correcta!'
		END

		SELECT * FROM Roles
		SELECT * FROM RolPermiso
GO


--####################################################################
CREATE FUNCTION dbo.splitstring ( @stringToSplit VARCHAR(MAX) )
RETURNS
 @returnList TABLE ([Name] [nvarchar] (500))
AS
BEGIN

 DECLARE @name NVARCHAR(255)
 DECLARE @pos INT

 WHILE CHARINDEX(',', @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(',', @stringToSplit)  
  SELECT @name = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT INTO @returnList 
  SELECT @name

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT INTO @returnList
 SELECT @stringToSplit

 RETURN
END
GO


INSERT INTO PERMISOS (permisosNombre) VALUES
('ALTA'),
('BAJA'),
('UPDATE'),
('DELETE')
GO

CREATE PROC UsuarioDelete
	@usuarioId INT,
	@usuarioActivo VARCHAR(2)
AS
BEGIN 
	DECLARE @result INT
	SET @result=0
	IF EXISTS(SELECT * FROM Usuario WHERE usuarioId=@usuarioId)
	BEGIN
		UPDATE Usuario SET usuarioActivo=@usuarioActivo WHERE usuarioId = @usuarioId
		SET @result=1
	END
	
	SELECT @result
END
GO


CREATE PROC RolesDelete
	@rolesId INT,
	@rolesActivo VARCHAR(2)	
AS
BEGIN

	DECLARE @result INT
	SET @result = 0
	
	IF EXISTS(SELECT * FROM Roles WHERE rolesId=@rolesId)
	BEGIN
		UPDATE Roles SET rolesActivo= @rolesActivo WHERE rolesId = @rolesId
		SET @result=1
	END

	SELECT @result
	
END
GO
SELECT * FROM Roles



/** 15/09/2021 **/
CREATE PROC UsuarioUpdate
	@usuarioId INT,
	@usuarioNombre VARCHAR(75),
	@usuarioApellidoPaterno VARCHAR(50),
	@usuarioApellidoMaterno VARCHAR(50),
	@usuarioCorreoElectronico VARCHAR(150),
	@usuarioPassword VARCHAR(50),
	@rolIdFk INT
AS
BEGIN
	
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Usuario WHERE usuarioId=@usuarioId)
		BEGIN
			UPDATE Usuario SET 
				usuarioNombre=@usuarioNombre,
				usuarioApellidoPaterno = @usuarioApellidoPaterno,
				usuarioApellidoMaterno = @usuarioApellidoMaterno,
				usuarioCorreoElectronico = @usuarioCorreoElectronico,
				usuarioPassword = @usuarioPassword,
				rolRolIdFk = @rolIdFk
			WHERE usuarioId=@usuarioId

				SET @result = 1

		END

	SELECT @result
	
END
GO

/** Categorias **/

CREATE PROC CategoriaGetActive
AS
BEGIN
	SELECT * FROM Categoria WHERE categoriaActivo='SI'
END
GO

CREATE PROC CategoriaGetInactive
AS 
BEGIN
	SELECT * FROM Categoria WHERE categoriaActivo='NO'
END
GO

CREATE PROC CategoriaGetAll
AS
BEGIN 
	SELECT * FROM Categoria
END
GO


CREATE PROC CategoriaCreate
    @categoriaNombre VARCHAR(75),
	@categoriaEdadRango1 INT,
	@categoriaEdadRango2 INT
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF NOT EXISTS(SELECT * FROM Categoria WHERE categoriaNombre=@categoriaNombre)
	BEGIN
		INSERT INTO Categoria (categoriaNombre, categoriaEdadRango1, categoriaEdadRango2)
		VALUES (@categoriaNombre, @categoriaEdadRango1, @categoriaEdadRango2)

		SET @result=1
	END

	SELECT @result
	
END
GO

CREATE PROC CategoriaUpdate
	@categoriaId INT,
    @categoriaNombre VARCHAR(75),
	@categoriaEdadRango1 INT,
	@categoriaEdadRango2 INT
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Categoria WHERE categoriaId=@categoriaId)
	BEGIN

		UPDATE Categoria SET 
			categoriaNombre=@categoriaNombre,
			categoriaEdadRango1 = @categoriaEdadRango1,
			categoriaEdadRango2 = @categoriaEdadRango2
		WHERE categoriaId=@categoriaId
	
		SET @result=1

	END

	SELECT @result
	
END
GO


CREATE PROC CategoriaDelete
	@categoriaId INT,
	@categoriaActivo VARCHAR(2)
AS
BEGIN 
	DECLARE @result INT
	SET @result = 0

	IF EXISTS (SELECT * FROM Categoria WHERE categoriaId= @categoriaId)
	BEGIN 
		UPDATE Categoria SET categoriaActivo=@categoriaActivo
	END
	
	SELECT @result

END


/** DISCIPLINA **/
CREATE PROC DisciplinaGetActive
AS
BEGIN
	SELECT * FROM Disciplina WHERE disciplinaActivo='SI'
END
GO

CREATE PROC DisciplinaGetInactive
AS 
BEGIN
	SELECT * FROM Disciplina WHERE disciplinaActivo='NO'
END
GO

CREATE PROC DisciplinaGetAll
AS
BEGIN 
	SELECT * FROM Disciplina
END
GO


CREATE PROC DisciplinaCreate
    @disciplinaNombre VARCHAR(100)	
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF NOT EXISTS(SELECT * FROM Disciplina WHERE disciplinaNombre=@disciplinaNombre)
	BEGIN
		INSERT INTO Disciplina (disciplinaNombre)
		VALUES (@disciplinaNombre)

		SET @result=1
	END

	SELECT @result
	
END
GO

CREATE PROC DisciplinaUpdate
	@disciplinaId INT,
    @disciplinaNombre VARCHAR(100)	
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Disciplina WHERE disciplinaId=@disciplinaId)
	BEGIN

		UPDATE Disciplina SET 
			disciplinaNombre=@disciplinaNombre			
		WHERE disciplinaId=@disciplinaId
	
		SET @result=1

	END

	SELECT @result
	
END
GO


CREATE PROC DisciplinaDelete
	@disciplinaId INT,
	@disciplinaActivo VARCHAR(2)
AS
BEGIN 
	DECLARE @result INT
	SET @result = 0

	IF EXISTS (SELECT * FROM Disciplina WHERE disciplinaId= @disciplinaId)
	BEGIN 
		UPDATE Disciplina SET disciplinaActivo=@disciplinaActivo
	END
	
	SELECT @result

END



/** Gimnasio **/
CREATE PROC GimnasioGetActive
AS
BEGIN
	SELECT * FROM Gimnasio WHERE gimnasioActivo='SI'
END
GO

CREATE PROC GimnasioGetInactive
AS 
BEGIN
	SELECT * FROM Gimnasio WHERE gimnasioActivo='NO'
END
GO

CREATE PROC GimnasioGetAll
AS
BEGIN 
	SELECT * FROM Gimnasio
END
GO


CREATE PROC GimnasioCreate
    @gimnasioNombre VARCHAR(100)	
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF NOT EXISTS(SELECT * FROM Gimnasio WHERE gimnasioNombre=@gimnasioNombre)
	BEGIN
		INSERT INTO Gimnasio (gimnasioNombre)
		VALUES (@gimnasioNombre)

		SET @result=1
	END

	SELECT @result
	
END
GO

CREATE PROC GimnasioUpdate
	@gimnasioId INT,
    @gimnasioNombre VARCHAR(100)	
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Gimnasio WHERE GimnasioId=@gimnasioId)
	BEGIN

		UPDATE Disciplina SET 
			disciplinaNombre=@gimnasioNombre			
		WHERE disciplinaId=@gimnasioId
	
		SET @result=1

	END

	SELECT @result
	
END
GO


CREATE PROC GimnasioDelete
	@gimnasioId INT,
	@gimnasioActivo VARCHAR(2)
AS
BEGIN 
	DECLARE @result INT
	SET @result = 0

	IF EXISTS (SELECT * FROM Gimnasio WHERE gimnasioId= @gimnasioId)
	BEGIN 
		UPDATE Gimnasio SET gimnasioActivo=@gimnasioActivo
	END
	
	SELECT @result

END
GO

/**  INSTRUCTOR **/
CREATE PROC InstructorGetAll
 AS
 BEGIN
	SELECT * FROM Instructor
 END
 GO

 CREATE PROC InstructorGetAllActive
 AS
 BEGIN
	SELECT * FROM Instructor WHERE instructorActivo='SI'
 END
 GO

 CREATE PROC InstructorGetAllInactive
 AS
 BEGIN
	SELECT * FROM Instructor WHERE instructorActivo='NO'
 END
 GO

 CREATE PROC InstructorCreate
	@instructorNombre VARCHAR(75),
	@instructorApellidoPaterno VARCHAR(75),
	@instructorApellidoMaterno VARCHAR(75),
	@instructorCURP VARCHAR(35),
	@instructorRFC VARCHAR(30),
	@instructorCodigoPostal INT,
	@instructorEstado VARCHAR(75),
	@instructorMunicipio VARCHAR(75),
	@instructorColonia VARCHAR(75),
	@instructorCalle VARCHAR(150),
	@instructorNumero INT,
	@instructorFechaDeRegistro DATE ,
	@instructorActivo VARCHAR(2),
	@instructorFechaDeNacimiento DATE,
	@instructorSexo VARCHAR(15)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF NOT EXISTS (SELECT * FROM Instructor WHERE instructorCURP=@instructorCURP AND instructorRFC=@instructorRFC)
	BEGIN 
		INSERT INTO Instructor 
			(instructorNombre, instructorApellidoPaterno, instructorApellidoMaterno, instructorCURP, instructorRFC, instructorCodigoPostal, instructorEstado, instructorMunicipio,
			 instructorColonia, instructorCalle, instructorNumero, instructorFechaDeNacimiento, instructorSexo) VALUES
			 (
				@instructorNombre, @instructorApellidoPaterno, @instructorApellidoMaterno, @instructorCURP, @instructorRFC, @instructorCodigoPostal, @instructorEstado,
				@instructorMunicipio, @instructorColonia, @instructorCalle, @instructorNumero, @instructorFechaDeNacimiento,
				@instructorSexo
			 )

			 SET @result=1
	END

	SELECT @result

END
GO


CREATE PROC InstructorUdate
	@instructorId INT,
	@instructorNombre VARCHAR(75),
	@instructorApellidoPaterno VARCHAR(75),
	@instructorApellidoMaterno VARCHAR(75),
	@instructorCURP VARCHAR(35),
	@instructorRFC VARCHAR(30),
	@instructorCodigoPostal INT,
	@instructorEstado VARCHAR(75),
	@instructorMunicipio VARCHAR(75),
	@instructorColonia VARCHAR(75),
	@instructorCalle VARCHAR(150),
	@instructorNumero INT,
	@instructorFechaDeRegistro DATE ,
	@instructorActivo VARCHAR(2),
	@instructorFechaDeNacimiento DATE,
	@instructorSexo VARCHAR(15)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF  EXISTS (SELECT * FROM Instructor WHERE instructorId=@instructorId)
	BEGIN 
		UPDATE Instructor SET
			instructorNombre= @instructorNombre, instructorApellidoPaterno = @instructorApellidoPaterno, instructorApellidoMaterno = @instructorApellidoMaterno,
			instructorCURP = @instructorCURP, instructorRFC = @instructorRFC, instructorCodigoPostal= @instructorCodigoPostal, instructorEstado= @instructorEstado, 
			instructorMunicipio= @instructorMunicipio, instructorColonia= @instructorColonia, instructorCalle= @instructorCalle, instructorNumero= @instructorNumero,
			instructorActivo= @instructorActivo, instructorFechaDeNacimiento= @instructorFechaDeNacimiento,
			instructorSexo = @instructorSexo WHERE instructorId= @instructorId
			 				     		
			 SET @result=1
	END

	SELECT @result

END
GO

CREATE PROC InstructorDelete
	@instructorId INT,
	@instructorActivo VARCHAR(2)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Instructor WHERE instructorId=@instructorId)
	BEGIN
		UPDATE Instructor SET instructorActivo = @instructorActivo WHERE instructorId= @instructorId

		SET @result=1
	END

	SELECT @result
END
GO

/** ALUMNO **/

CREATE PROC AlumnoGetAll
AS
BEGIN
	SELECT * FROM Alumno
END
GO

CREATE PROC AlumnoGetAllActive
AS
BEGIN
	SELECT * FROM Alumno WHERE alumnoActivo='SI'
END
GO


CREATE PROC AlumnoGetAllInactive
AS
BEGIN
	SELECT * FROM Alumno WHERE alumnoActivo='NO'
END
GO

CREATE PROC AlumnoCreate
	@alumnoNombre VARCHAR(75),
	@alumnoApellidoPaterno VARCHAR(75),
	@alumnoApellidoMaterno VARCHAR(75),
	@alumnoCURP VARCHAR(35),
	@alumnoFechaDeNacimiento DATE,
	@alumnoSexo VARCHAR(15),
	@alumnoEstatura Decimal(3,2),
	@alumnoPeso Decimal(5,2),
	@alumnoRutaFoto VARCHAR(500),
	@alumnoTelefonoCasa VARCHAR(10),
	@alumnoTelefonoCelular VARCHAR(10),
	@alumnoCorreoElectronico VARCHAR(150),
	@alumnoRedesSociales VARCHAR(150),
	@alumnoPassword VARCHAR(50),
	@alumnoCodigoPostal INT,
	@alumnoEstado VARCHAR(50),
	@alumnoMunicipio VARCHAR(75),
	@alumnoColonia VARCHAR(75),
	@alumnoCalle VARCHAR(150),
	@alumnoAfiliacionMedica VARCHAR(50),
	@alumnoAlergias VARCHAR(350),
	@alumnoPadecimientos VARCHAR(350),
	@alumnoTipoDeSangre VARCHAR(6)
AS
BEGIN
	DECLARE @result INT
	SET @result  = 0

	IF NOT EXISTS(SELECT * FROM Alumno WHERE alumnoCURP=@alumnoCURP)
	BEGIN
		INSERT INTO Alumno
		(alumnoNombre, alumnoApellidoPaterno, alumnoApellidoMaterno, alumnoCURP, alumnoFechaDeNacimiento, alumnoSexo, alumnoEstatura, alumnoPeso,
		alumnoRutaFoto, alumnoTelefonoCasa, alumnoTelefonoCelular, alumnoCorreoElectronico, alumnoRedesSociales, alumnoPassword, alumnoCodigoPostal, alumnoEstado,
		alumnoMunicipio, alumnoColonia, alumnoCalle, alumnoAfiliacionMedica, alumnoAlergias, alumnoPadecimientos, alumnoTipoDeSangre) 
		VALUES
		(@alumnoNombre, @alumnoApellidoPaterno, @alumnoApellidoMaterno, @alumnoCURP, @alumnoFechaDeNacimiento, @alumnoSexo, @alumnoEstatura,
		@alumnoPeso, @alumnoRutaFoto, @alumnoTelefonoCasa, @alumnoTelefonoCelular, @alumnoCorreoElectronico, @alumnoRedesSociales, @alumnoPassword,
		@alumnoCodigoPostal, @alumnoEstado, @alumnoMunicipio, @alumnoColonia, @alumnoCalle, @alumnoAfiliacionMedica, @alumnoAlergias, @alumnoPadecimientos,
		@alumnoTipoDeSangre)

		SET @result=1
	END

	SELECT @result
	
END
GO


CREATE PROC AlumnoUpdate
	@alumnoId INT,
	@alumnoNombre VARCHAR(75),
	@alumnoApellidoPaterno VARCHAR(75),
	@alumnoApellidoMaterno VARCHAR(75),
	@alumnoCURP VARCHAR(35),
	@alumnoFechaDeNacimiento DATE,
	@alumnoSexo VARCHAR(15),
	@alumnoEstatura Decimal(3,2),
	@alumnoPeso Decimal(5,2),
	@alumnoRutaFoto VARCHAR(500),
	@alumnoTelefonoCasa VARCHAR(10),
	@alumnoTelefonoCelular VARCHAR(10),
	@alumnoCorreoElectronico VARCHAR(150),
	@alumnoRedesSociales VARCHAR(150),
	@alumnoPassword VARCHAR(50),
	@alumnoCodigoPostal INT,
	@alumnoEstado VARCHAR(50),
	@alumnoMunicipio VARCHAR(75),
	@alumnoColonia VARCHAR(75),
	@alumnoCalle VARCHAR(150),
	@alumnoAfiliacionMedica VARCHAR(50),
	@alumnoAlergias VARCHAR(350),
	@alumnoPadecimientos VARCHAR(350),
	@alumnoTipoDeSangre VARCHAR(6),
	@alumnoActivo VARCHAR(2)
AS
BEGIN
	DECLARE @result INT
	SET @result  = 0

	IF  EXISTS(SELECT * FROM Alumno WHERE alumnoId=@alumnoId)
	BEGIN
		UPDATE Alumno  SET
		alumnoNombre=@alumnoNombre, alumnoApellidoPaterno=@alumnoApellidoPaterno, alumnoApellidoMaterno=@alumnoApellidoMaterno, alumnoCURP=@alumnoCURP,
		alumnoFechaDeNacimiento=@alumnoFechaDeNacimiento, alumnoSexo=@alumnoSexo, alumnoEstatura=@alumnoEstatura, alumnoPeso=@alumnoPeso,
		alumnoRutaFoto=@alumnoRutaFoto, alumnoTelefonoCasa=@alumnoTelefonoCasa, alumnoTelefonoCelular=@alumnoTelefonoCelular, alumnoCorreoElectronico=@alumnoCorreoElectronico,
		alumnoRedesSociales=@alumnoRedesSociales, alumnoPassword=@alumnoPassword, alumnoCodigoPostal=@alumnoCodigoPostal, alumnoEstado=@alumnoEstado,
		alumnoMunicipio=@alumnoMunicipio, alumnoColonia=@alumnoColonia, alumnoCalle=@alumnoCalle, alumnoAfiliacionMedica=@alumnoAfiliacionMedica, 
		alumnoAlergias=@alumnoAlergias, alumnoPadecimientos=@alumnoPadecimientos, alumnoTipoDeSangre=@alumnoTipoDeSangre, alumnoActivo=@alumnoActivo
		WHERE alumnoId=@alumnoId
				   	
		SET @result=1
	END

	SELECT @result
	
END
GO

CREATE PROC AlumnoDelete
	@alumnoId INT,
	@alumnoActivo VARCHAR(2)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Alumno WHERE alumnoId=@alumnoId)
	BEGIN
		UPDATE Alumno SET alumnoActivo = @alumnoActivo WHERE alumnoId=@alumnoId

		SET @result=1
	END

	SELECT @result
END
GO

/** TUTOR **/
CREATE PROC TutorGetAll
AS
BEGIN
	SELECT * FROM Tutor
END
GO

CREATE PROC TutorGetAllActive
AS
BEGIN
	SELECT * FROM Tutor WHERE tutorActivo='SI'
END
GO


CREATE PROC TutorGetAllInactve
AS
BEGIN
	SELECT * FROM Tutor WHERE tutorActivo='NO'
END
GO

CREATE PROC TutorCreate
	@tutorNombre VARCHAR(75),	
	@tutorApellidoPAterno VARCHAR(50),
	@tutorApellidoMaterno VARCHAR(50),
	@tutorParentesco VARCHAR(50),
	@tutorTelefonoCasa VARCHAR(10),
	@tutortelefonoCelular VARCHAR(10),
	@tutorCorreoElectronico VARCHAR(100)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM Tutor WHERE  tutorNombre=@tutorNombre AND tutorApellidoPAterno=@tutorApellidoPAterno AND tutorApellidoMaterno=@tutorApellidoMaterno)
	BEGIN
		INSERT INTO Tutor (tutorNombre, tutorApellidoPAterno, tutorApellidoMaterno, tutorParentesco, tutorTelefonoCasa, tutorTelefonoCelular, tutorCorreoElectronico) VALUES
		(@tutorNombre, @tutorApellidoPAterno, @tutorApellidoMaterno, @tutorParentesco, @tutorTelefonoCasa, @tutortelefonoCelular, @tutorCorreoElectronico)

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC TutorUpdate
	@tutorId INT,
	@tutorNombre VARCHAR(75),	
	@tutorApellidoPAterno VARCHAR(50),
	@tutorApellidoMaterno VARCHAR(50),
	@tutorParentesco VARCHAR(50),
	@tutorTelefonoCasa VARCHAR(10),
	@tutortelefonoCelular VARCHAR(10),
	@tutorCorreoElectronico VARCHAR(100),
	@tutorActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Tutor WHERE  tutorId=@tutorId)
	BEGIN
		UPDATE Tutor SET tutorNombre=@tutorNombre, tutorApellidoPAterno=@tutorApellidoPAterno, tutorApellidoMaterno=@tutorApellidoMaterno, tutorParentesco=@tutorParentesco,
		                              tutorTelefonoCasa=@tutorTelefonoCasa, tutorTelefonoCelular=@tutortelefonoCelular, tutorCorreoElectronico=@tutorCorreoElectronico, tutorActivo=@tutorActivo
		WHERE tutorId=@tutorId

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC TutorDelete
	@tutorId INT,
	@tutorActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Tutor WHERE  tutorId=@tutorId)
	BEGIN
		UPDATE Tutor SET tutorActivo= @tutorActivo WHERE tutorId=@tutorId
		SET @result=1
	END

	SELECT @result

END
GO

/** TutorAlumno **/
CREATE PROC TutorAlumnoGetAll
AS
BEGIN
	SELECT * FROM TutorAlumno
END
GO

CREATE PROC TutorAlumnoGetAllActive
AS
BEGIN
	SELECT * FROM TutorAlumno WHERE tutoralumnoActivo='SI'
END
GO


CREATE PROC TutorAlumnoGetAllInactve
AS
BEGIN
	SELECT * FROM TutorAlumno WHERE tutoralumnoActivo='NO'
END
GO

CREATE PROC TutorAlumnoCreate
	@tutorIdFk INT,
	@alumnoIdFk INT
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0
	
	IF NOT EXISTS (SELECT * FROM TutorAlumno WHERE alumnoIdFk=@alumnoIdFk)
	BEGIN
		INSERT INTO TutorAlumno(tutorIdFk, alumnoIdFk) VALUES
		(@tutorIdFk, @alumnoIdFk)

		SET @result=1
	END	

	SELECT @result
END
GO


CREATE PROC TutorAlumnoUpdate
	@tutoralumnoId INT,
	@tutorIdFk INT,
	@alumnoIdFk INT,
	@tutoralumnoActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM TutorAlumno WHERE  tutoralumnoId =@tutoralumnoId )
	BEGIN
		UPDATE TutorAlumno SET tutorIdFk=@tutorIdFk, alumnoIdFk=@alumnoIdFk, tutoralumnoActivo=@tutoralumnoActivo		                            
		WHERE tutoralumnoId=@tutoralumnoId

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC TutorAlumnoDelete
	@tutoralumnoId INT,
	@tutoralumnoActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM TutorAlumno WHERE  tutoralumnoId=@tutoralumnoId)
	BEGIN
		UPDATE TutorAlumno SET tutoralumnoActivo= @tutoralumnoActivo WHERE tutoralumnoId=@tutoralumnoId
		SET @result=1
	END

	SELECT @result

END









--HORARIO
USE CADAM
GO

CREATE PROC HorarioGetAll
AS
BEGIN
	SELECT * FROM Horario
END
GO

CREATE PROC HorarioGetAllActive
AS
BEGIN
	SELECT * FROM Horario WHERE horarioActivo='SI'
END
GO


CREATE PROC HorarioGetAllInactve
AS
BEGIN
	SELECT * FROM Horario WHERE horarioActivo='NO'
END
GO

CREATE PROC HorarioCreate
	@horarioNombre VARCHAR(75),	
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME,
	@horarioDias VARCHAR(300),
	@horarioTurno VARCHAR(30)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM Horario WHERE  horarioNombre=@horarioNombre)
	BEGIN
		INSERT INTO Horario(horarioNombre, disciplinaIdFk, categoriaIdFk, gimnasioIdFk, instructorIdFk, horarioInicio, horarioFin, horarioDias, horarioTurno) VALUES
		(@horarioNombre, @disciplinaIdFk, @categoriaIdFk, @gimnasioIdFk, @instructorIdFk, @horarioInicio, @horarioFin, @horarioDias, @horarioTurno)

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC HorarioUpdate
	@horarioId INT,
	@horarioNombre VARCHAR(75),	
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME,
	@horarioDias VARCHAR(300),
	@horarioTurno VARCHAR(30),
	@horarioActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Horario WHERE  horarioId=@horarioId)
	BEGIN
		UPDATE Horario SET horarioNombre=@horarioNombre, disciplinaIdFk=@disciplinaIdFk, categoriaIdFk=@categoriaIdFk, gimnasioIdFk=@gimnasioIdFk, instructorIdFk=@instructorIdFk,
		                              horarioInicio=@horarioInicio, horarioFin=@horarioFin, horarioDias=@horarioDias, horarioTurno=@horarioTurno, horarioActivo=@horarioActivo
		WHERE horarioId=@horarioId

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC HorarioDelete
	@horarioId INT,
	@horarioActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Horario WHERE  horarioId=@horarioId)
	BEGIN
		UPDATE Horario SET horarioActivo= @horarioActivo WHERE horarioId=@horarioId
		SET @result=1
	END

	SELECT @result
END
GO

--MENSUALIDADES
CREATE PROC MensualidadesGetAll
AS
BEGIN
	SELECT * FROM Mensualidades
END
GO

ALTER TABLE Mensualidades ADD mensualidadesActivo VARCHAR(2) DEFAULT 'SI'
GO
ALTER TABLE Mensualidades ADD mensualidadesPorcentajePago INT DEFAULT 100
GO

UPDATE Mensualidades SET mensualidadesActivo='SI'
Go
UPDATE Mensualidades SET mensualidadesPorcentajePago=100
GO
SELECT * FROM Mensualidades
GO

CREATE PROC MensualidadesGetAllActive
AS
BEGIN
	SELECT * FROM Mensualidades WHERE mensualidadesActivo='SI'
END
GO

CREATE PROC MensualidadesGetAllInactve
AS
BEGIN
	SELECT * FROM Mensualidades WHERE mensualidadesActivo='NO'
END
GO


CREATE PROC MensualidadesCreate
	@alumnoIdFk INT,
	@mensualidadImporte DECIMAL(6,2),
	@mensualidadFechaDePago DATE,
	@mensualidadFechaRealDePago DATE,
	@mensualidadConRetraso VARCHAR(2),
	@mensualidadImportePorRetraso DECIMAL(5,2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	INSERT INTO Mensualidades (alumnoIdFk, mensualidadImporte, mensualidadFechaDePago, mensualidadFechaRealDepago, mensualidadConRetraso, mensualidadImportePorRetraso)
	VALUES
	(@alumnoIdFk, @mensualidadImporte, @mensualidadFechaDePago, @mensualidadFechaRealDePago, @mensualidadConRetraso, @mensualidadImportePorRetraso)

	SET @result=1
	SELECT @result
END
GO


CREATE PROC MensualidadesUpdate
	@mensualidadId INT,
	@alumnoIdFk INT,
	@mensualidadImporte DECIMAL(6,2),
	@mensualidadFechaDePago DATE,
	@mensualidadFechaRealDePago DATE,
	@mensualidadConRetraso VARCHAR(2),
	@mensualidadImportePorRetraso DECIMAL(5,2),
	@mensualidadActivo VARCHAR(2),
	@mensualidadPorcentajePago INT
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Mensualidades WHERE  mensualidadId=@mensualidadId)
	BEGIN
		UPDATE Mensualidades SET 
			mensualidadImporte= @mensualidadImporte, mensualidadFechaDePago = @mensualidadFechaDePago, mensualidadFechaRealDePago= @mensualidadFechaRealDePago,
			mensualidadConRetraso = @mensualidadConRetraso, mensualidadImportePorRetraso= @mensualidadImportePorRetraso, mensualidadesActivo = @mensualidadActivo,
			mensualidadesPorcentajePago = @mensualidadPorcentajePago
		WHERE mensualidadId = @mensualidadId

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC MensualidadDelete
	@mensualidadId INT,
	@mensualidadesActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Mensualidades WHERE  mensualidadId=@mensualidadId)
	BEGIN
		UPDATE Mensualidades SET @mensualidadesActivo= @mensualidadesActivo WHERE mensualidadId=@mensualidadId
		SET @result=1
	END

	SELECT @result
END
GO




--INSCRIPCION
CREATE PROC InscripcionesGetAll
AS
BEGIN
	SELECT * FROM Inscripciones
END
GO

CREATE PROC InscripcionesGetAllActive
AS
BEGIN
	SELECT * FROM Inscripciones WHERE InscripcionesActivo='SI'
END
GO

CREATE PROC InscripcionesGetAllInactve
AS
BEGIN
	SELECT * FROM Inscripciones WHERE InscripcionesActivo='NO'
END
GO


ALTER TABLE Inscripciones ADD  inscripcionesActivo VARCHAR(2) DEFAULT 'SI'
GO
UPDATE Inscripciones SET inscripcionesActivo= 'SI'
GO

--SELECT * FROM Inscripciones WHERE  inscripcionFechaDeVencimiento>=GETDATE() AND inscripcionesActivo='SI'

CREATE PROC InscripcionesCreate
	@alumnoIdFk INT,
	@inscripcionImporteAnual DECIMAL(6,2),
	@inscripcionFechaDeVigencia DATETIME
	--@inscripcionFechaDeVencimineto DATETIME
AS 
BEGIN 
	DECLARE @result INT
	DECLARE @inscripcionFechaDeVencimineto DATETIME
	SET @result=0

	SET @inscripcionFechaDeVencimineto = DATEADD(YEAR, 1,GETDATE())	

	IF NOT EXISTS(SELECT * FROM Inscripciones WHERE alumnoIdFk = @alumnoIdFk AND inscripcionFechaDeVencimiento>=GETDATE() AND inscripcionesActivo='SI' )
	BEGIN
		INSERT INTO Inscripciones(alumnoIdFk, inscripcionImporteAnual, inscripcionFechaDeVigencia, inscripcionFechaDeVencimiento )
		VALUES
		(@alumnoIdFk, @inscripcionImporteAnual, @inscripcionFechaDeVigencia, @inscripcionFechaDeVencimineto)

		SET @result=1
	END
	
	SELECT @result

END
GO

CREATE PROC InscripcionesUpdate
	@inscripcionId INT,
	@alumnoIdFk INT,
	@inscripcionImporteAnual DECIMAL(6,2),
	@inscripcionFechaDeVigencia DATETIME,
	@inscripcionActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	DECLARE @inscripcionFechaDeVencimineto DATETIME
	SET @result=0

	SET @inscripcionFechaDeVencimineto = DATEADD(YEAR, 1,GETDATE())	

	IF EXISTS (SELECT * FROM Inscripciones WHERE  inscrpcionId =@inscripcionId)
	BEGIN
		UPDATE Inscripciones SET inscripcionImporteAnual= @inscripcionImporteAnual, inscripcionFechaDeVigencia=@inscripcionFechaDeVigencia, inscripcionFechaDeVencimiento= @inscripcionFechaDeVencimineto,
		inscripcionesActivo=@inscripcionActivo		                                           
		WHERE inscrpcionId= @inscripcionId

		SET @result=1
	END

	SELECT @result
END
GO

CREATE PROC InscripcionesDelete
	@inscripcionId INT,
	@inscripcionesActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM Inscripciones WHERE  inscrpcionId=@inscripcionId)
	BEGIN
		UPDATE Inscripciones SET inscripcionesActivo= @inscripcionesActivo WHERE inscrpcionId=@inscripcionId
		SET @result=1
	END

	SELECT @result
END
GO

--PLANES DE INSCRIPCION
CREATE PROC PlanDeInscripcionGetAll
AS
BEGIN
	SELECT * FROM PlanDeInscripcion
END
GO

CREATE PROC PlanDeInscripcionGetAllActive
AS
BEGIN
	SELECT * FROM PlanDeInscripcion WHERE planActivo='SI'
END
GO

CREATE PROC PlanDeInscripcionGetAllInactve
AS
BEGIN
	SELECT * FROM PlanDeInscripcion WHERE planActivo='NO'
END
GO

CREATE PROC PlanDeInscripcionCreate
	@disciplinaId INT,
	@planDescripcion VARCHAR(400),
	@planRecurrencia VARCHAR(400),
	@planDias VARCHAR(400),
	@planImporte DECIMAL(6,2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS(SELECT * FROM PlanDeInscripcion WHERE planDescripcion = @planDescripcion)
	BEGIN
		INSERT INTO PlanDeInscripcion (disciplinaId, planDescripcion, planRecurrencia, planDias, planImporte)
		VALUES
		(@disciplinaId, @planDescripcion, @planRecurrencia, @planDias, @planImporte)

		SET @result=1
	END
	
	SELECT @result

END
GO

CREATE PROC PlanDeInscripcionUpdate
	@planId INT,
	@disciplinaId INT,
	@planDescripcion VARCHAR(400),
	@planRecurrencia VARCHAR(400),
	@planDias VARCHAR(400),
	@planImporte DECIMAL(6,2),
	@planActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM PlanDeInscripcion WHERE  planId =@planId)
	BEGIN
		UPDATE PlanDeInscripcion SET 
			disciplinaId= @disciplinaId, planDescripcion = @planDescripcion, planRecurrencia= @planRecurrencia, planDias = @planDias, planImporte= @planImporte, planActivo = @planActivo
		WHERE planId = @planId

		SET @result=1
	END

	SELECT @result
END
GO

CREATE PROC PlanDeInscripcionDelete
	@planId INT,
	@planActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM PlanDeInscripcion WHERE  @planId=@planId)
	BEGIN
		UPDATE PlanDeInscripcion SET planActivo= @planActivo WHERE planId=@planId
		SET @result=1
	END

	SELECT @result
END
GO


--ALUMNOPLANHORARIO
ALTER TABLE AlumnoPlanHorario ADD alumnoplanhorarioActivo VARCHAR(2) DEFAULT 'SI'
ALTER TABLE AlumnoPlanHorario ADD alumnoplanhorarioFechaDeCreacion DATETIME DEFAULT GETDATE()

UPDATE AlumnoPlanHorario SET alumnoplanhorarioActivo = 'SI'
UPDATE AlumnoPlanHorario SET alumnoplanhorarioFechaDeCreacion = GETDATE()

GO


CREATE PROC AlumnoPlanHorarioAll
AS
BEGIN
	SELECT * FROM AlumnoPlanHorario
END
GO

CREATE PROC AlumnoPlanHorarioActive
AS
BEGIN
	SELECT * FROM AlumnoPlanHorario WHERE alumnoplanhorarioActivo='SI'
END
GO


CREATE PROC AlumnoPlanHorarioGetAllInactve
AS
BEGIN
	SELECT * FROM AlumnoPlanHorario WHERE alumnoplanhorarioActivo='NO'
END
GO

CREATE PROC AlumnoPlanHorarioCreate
	@alumnoIdFk INT,
	@planIdFk INT,
	@horarioIdFk INT
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM AlumnoPlanHorario WHERE  alumnoIdFk=@alumnoIdFk AND planIdFk=@planIdFk AND horarioIdFk = @horarioIdFk )
	BEGIN
		INSERT INTO AlumnoPlanHorario (alumnoIdFk, planIdFk, horarioIdFk) VALUES
		(@alumnoIdFk, @planIdFk, @horarioIdFk)

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC AlumnoPlanHorarioUpdate
	@alumnoplanhorarioId INT,
	@alumnoIdFk INT,
	@planIdFk INT,
	@horarioIdFk INT,
	@alumnoplanhorarioActivo VARCHAR(2)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM AlumnoPlanHorario WHERE  alumnoplanhorarioId=@alumnoplanhorarioId)
	BEGIN
		UPDATE AlumnoPlanHorario SET alumnoIdFk=@alumnoIdFk, planIdFk=@planIdFk, horarioIdFk=@horarioIdFk, alumnoplanhorarioActivo=@alumnoplanhorarioActivo			
		WHERE alumnoplanhorarioId=@alumnoplanhorarioId

		SET @result=1
	END

	SELECT @result
END
GO


CREATE PROC AlumnoPlanHorarioDelete
	@alumnoplanhorarioId INT,
	@alumnoplanhorarioActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	SET @result=0

	IF EXISTS (SELECT * FROM AlumnoPlanHorario WHERE  alumnoplanhorarioId=@alumnoplanhorarioId)
	BEGIN
		UPDATE AlumnoPlanHorario SET alumnoplanhorarioActivo= @alumnoplanhorarioActivo WHERE alumnoplanhorarioId=@alumnoplanhorarioId
		SET @result=1
	END

	SELECT @result

END
GO

CREATE PROC [dbo].[AlumnoGetById]
	@alumnoId INT
AS
BEGIN
	SELECT * FROM Alumno WHERE alumnoId=@alumnoId AND alumnoActivo='SI' 
END
GO

CREATE PROC [dbo].[HorarioGetById]
	@horarioId INT
AS
BEGIN
	SELECT * FROM Horario WHERE horarioActivo='SI' AND horarioId=@horarioId
END
GO


 CREATE PROC [dbo].[InstructorGetById]
	@instructorId INT 
 AS
 BEGIN
	SELECT * FROM Instructor WHERE instructorActivo='SI' AND instructorId = @instructorId
 END
 GO

 ALTER PROC [dbo].[InstructorCreate]
	@instructorNombre VARCHAR(75),
	@instructorApellidoPaterno VARCHAR(75),
	@instructorApellidoMaterno VARCHAR(75),
	@instructorCURP VARCHAR(35),
	@instructorRFC VARCHAR(30),
	@instructorCodigoPostal INT,
	@instructorEstado VARCHAR(75),
	@instructorMunicipio VARCHAR(75),
	@instructorColonia VARCHAR(75),
	@instructorCalle VARCHAR(150),
	@instructorNumero INT,
	@instructorFechaDeNacimiento DATE,
	@instructorSexo VARCHAR(15)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF NOT EXISTS (SELECT * FROM Instructor WHERE instructorCURP=@instructorCURP )
	BEGIN 
		INSERT INTO Instructor 
			(instructorNombre, instructorApellidoPaterno, instructorApellidoMaterno, instructorCURP, instructorRFC, instructorCodigoPostal, instructorEstado, instructorMunicipio,
			 instructorColonia, instructorCalle, instructorNumero, instructorFechaDeNacimiento, instructorSexo) VALUES
			 (
				@instructorNombre, @instructorApellidoPaterno, @instructorApellidoMaterno, @instructorCURP, @instructorRFC, @instructorCodigoPostal, @instructorEstado,
				@instructorMunicipio, @instructorColonia, @instructorCalle, @instructorNumero, @instructorFechaDeNacimiento,
				@instructorSexo
			 )
			 SET @result=1
	END

	SELECT @result

END
GO

CREATE PROC [dbo].[InstructorUpdate]
	@instructorId INT,
	@instructorNombre VARCHAR(75),
	@instructorApellidoPaterno VARCHAR(75),
	@instructorApellidoMaterno VARCHAR(75),
	@instructorCURP VARCHAR(35),
	@instructorRFC VARCHAR(30),
	@instructorCodigoPostal INT,
	@instructorEstado VARCHAR(75),
	@instructorMunicipio VARCHAR(75),
	@instructorColonia VARCHAR(75),
	@instructorCalle VARCHAR(150),
	@instructorNumero INT,
	@instructorFechaDeNacimiento DATE,
	@instructorSexo VARCHAR(15),
	@instructorActivo VARCHAR(2)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF  EXISTS (SELECT * FROM Instructor WHERE instructorId=@instructorId )
	BEGIN 
		UPDATE Instructor SET
		 	instructorNombre= @instructorNombre, instructorApellidoPaterno=@instructorApellidoPaterno, instructorApellidoMaterno= @instructorApellidoMaterno,
			instructorCURP= @instructorCURP, instructorRFC= @instructorRFC, instructorCodigoPostal= @instructorCodigoPostal, instructorEstado=@instructorEstado,
			instructorMunicipio=@instructorMunicipio, instructorColonia=@instructorColonia, instructorCalle=@instructorCalle, instructorNumero=@instructorNumero,
			instructorFechaDeNacimiento=@instructorFechaDeNacimiento, instructorSexo=@instructorSexo, instructorActivo= @instructorActivo
		WHERE 
			instructorId=@instructorId

		 SET @result=1
	END

	SELECT @result

END


ALTER PROC [dbo].[InstructorDelete]
	@instructorId INT,
	@instructorActivo VARCHAR(2)
AS
BEGIN
	DECLARE @result INT
	SET @result = 0

	IF EXISTS(SELECT * FROM Instructor WHERE instructorId=@instructorId)
	BEGIN
		UPDATE Instructor SET instructorActivo = @instructorActivo WHERE instructorId= @instructorId

		SET @result=1
		
	END

	SELECT @result
END
GO

CREATE PROC [dbo].[PlanDeInscripcionGetById]
	@planId INT
AS
BEGIN
	SELECT * FROM PlanDeInscripcion WHERE planActivo='SI' AND  planId=@planId
END
GO





CREATE PROC [dbo].[InscripcionesGetByAlumnoId]
@alumnoIdFk INT
AS
BEGIN
SELECT * FROM Inscripciones WHERE InscripcionesActivo='SI' AND alumnoIdFk = @alumnoIdFk
END
GO

ALTER PROC [dbo].[InscripcionesUpdate]
@inscripcionId INT,
@alumnoIdFk INT,
@inscripcionImporteAnual DECIMAL(6,2),
@inscripcionFechaDeVigencia DATETIME,
@inscripcionesActivo VARCHAR(2)
AS 
BEGIN 
DECLARE @result INT
DECLARE @inscripcionFechaDeVencimineto DATETIME
SET @result=0

SET @inscripcionFechaDeVencimineto = DATEADD(YEAR, 1,GETDATE()) 

IF EXISTS (SELECT * FROM Inscripciones WHERE  inscrpcionId =@inscripcionId)
BEGIN
UPDATE Inscripciones SET inscripcionImporteAnual= @inscripcionImporteAnual, inscripcionFechaDeVigencia=@inscripcionFechaDeVigencia, inscripcionFechaDeVencimiento= @inscripcionFechaDeVencimineto,
inscripcionesActivo=@inscripcionesActivo                                           
WHERE inscrpcionId= @inscripcionId

SET @result=1
END

SELECT @result
END
GO


CREATE PROC [dbo].[MensualidadesGetAll]
AS
BEGIN
SELECT * FROM Mensualidades 
END
GO

CREATE PROC [dbo].[MensualidadGetById]
@mensualidadId INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadesActivo='SI' AND mensualidadId = @mensualidadId
END
GO

CREATE PROC [dbo].[MensualidadGetByAlumnoId]
@alumnoId INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadesActivo='SI' AND mensualidadId = @alumnoId
END
GO



CREATE PROC [dbo].[MensualidadGetById]
@mensualidadId INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadesActivo='SI' AND mensualidadId = @mensualidadId
END
GO

CREATE PROC [dbo].[MensualidadGetByAlumnoId]
@alumnoId INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadesActivo='SI' AND mensualidadId = @alumnoId
END
GO

ALTER PROC [dbo].[MensualidadGetByAlumnoId]
@alumnoIdFK INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadesActivo='SI' AND mensualidadId = @alumnoIdFk
END
Go

ALTER PROC [dbo].[MensualidadDelete]
@mensualidadId INT,
@mensualidadesActivo VARCHAR(2)
AS BEGIN
DECLARE @result INT
SET @result=0

IF EXISTS (SELECT * FROM Mensualidades WHERE  mensualidadId=@mensualidadId)
BEGIN
UPDATE Mensualidades SET mensualidadesActivo= @mensualidadesActivo WHERE mensualidadId=@mensualidadId
SET @result=1
END

SELECT @result
END
GO

ALTER PROC [dbo].[MensualidadGetByAlumnoId]
@alumnoIdFK INT
AS
BEGIN
SELECT * FROM Mensualidades 
WHERE mensualidadId = @alumnoIdFk
END


SELECT * FROM AlumnoPlanHorario
SELECT * FROM Alumno
SELECT * FROM PlanDeInscripcion
SELECT * FROM Horario

SELECT 
	A.alumnoId, A.alumnoNombre, A.alumnoCURP, P.planDescripcion, P.planRecurrencia,  P.planImporte,
	D.disciplinaNombre, H.horarioNombre, C.categoriaNombre, G.gimnasioNombre, I.instructorNombre, H.horarioInicio, H.horarioFin, H.horarioDias, H.horarioTurno,
	M.mensualidadesActivo, Ins.inscripcionesActivo
FROM AlumnoPlanHorario APH
JOIN Alumno A 
	ON APH.alumnoIdFk = A.alumnoId
JOIN PlanDeInscripcion P
	ON APH.planIdFk = P.planId
JOIN Horario H
	ON APH.horarioIdFk = H.horarioId
JOIN Disciplina D
	ON	H.disciplinaIdFk = D.disciplinaId
JOIN Categoria C
	ON H.categoriaIdFk = C.categoriaId
JOIN Gimnasio G
	ON H.gimnasioIdFk = G.gimnasioId
JOIN  Instructor I
	ON H.instructorIdFk = instructorId
JOIN Inscripciones Ins
	ON APH.alumnoIdFk = Ins.alumnoIdFk
JOIN Mensualidades M
	ON APH.alumnoIdFk = M.alumnoIdFk


ALTER PROC [dbo].[AlumnoPlanHorarioCreate]
	@alumnoIdFk INT,
	@planIdFk INT,
	@horarioIdFk INT
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM AlumnoPlanHorario WHERE  alumnoIdFk=@alumnoIdFk AND planIdFk=@planIdFk AND horarioIdFk = @horarioIdFk )
	BEGIN
		INSERT INTO AlumnoPlanHorario (alumnoIdFk, planIdFk, horarioIdFk) VALUES
		(@alumnoIdFk, @planIdFk, @horarioIdFk)

		SET @result=1
	END

	SELECT @result
END
GO

CREATE PROC [dbo].[HorarioGetByDisciplinaId]
	@disciplinaIdFk INT
AS
BEGIN
	SELECT * FROM Horario WHERE horarioActivo='SI' AND disciplinaIdFk=@disciplinaIdFk
END
GO

ALTER PROC [dbo].[HorarioCreate]
	@horarioNombre VARCHAR(75),	
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME,
	@horarioDias VARCHAR(300),
	@horarioTurno VARCHAR(30)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM Horario 
							WHERE  horarioNombre=@horarioNombre AND 
										  disciplinaIdFk=@disciplinaIdFk AND 
										  gimnasioIdFk=@gimnasioIdFk AND 
										  instructorIdFk=@instructorIdFk AND
										  horarioDias=@horarioDias)
	BEGIN
		INSERT INTO Horario(horarioNombre, disciplinaIdFk, categoriaIdFk, gimnasioIdFk, instructorIdFk, horarioInicio, horarioFin, horarioDias, horarioTurno) VALUES
		(@horarioNombre, @disciplinaIdFk, @categoriaIdFk, @gimnasioIdFk, @instructorIdFk, @horarioInicio, @horarioFin, @horarioDias, @horarioTurno)

		SET @result=1
	END

	SELECT @result
END
	

	USE [CADAM]
GO
/****** Object:  StoredProcedure [dbo].[TutorCreate]    Script Date: 04/10/2021 03:52:33 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[TutorCreate]
	@tutorNombre VARCHAR(75),	
	@tutorApellidoPAterno VARCHAR(50),
	@tutorApellidoMaterno VARCHAR(50),
	@tutorParentesco VARCHAR(50),
	@tutorTelefonoCasa VARCHAR(10),
	@tutortelefonoCelular VARCHAR(10),
	@tutorCorreoElectronico VARCHAR(100)
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM Tutor WHERE  tutorNombre=@tutorNombre AND tutorApellidoPAterno=@tutorApellidoPAterno AND tutorApellidoMaterno=@tutorApellidoMaterno)
	BEGIN
		INSERT INTO Tutor (tutorNombre, tutorApellidoPAterno, tutorApellidoMaterno, tutorParentesco, tutorTelefonoCasa, tutorTelefonoCelular, tutorCorreoElectronico) VALUES
		(@tutorNombre, @tutorApellidoPAterno, @tutorApellidoMaterno, @tutorParentesco, @tutorTelefonoCasa, @tutortelefonoCelular, @tutorCorreoElectronico)

		SET @result=(SELECT SCOPE_IDENTITY() AS insertedId);
	END

	SELECT @result
END
GO

CREATE PROC AlumnoPlanHorarioCreate
	@alumnoIdFk INT,
	@planIdFk INT,
	@horarioIdFk INT
AS
BEGIN
	DECLARE @result INT
	SET @result  = 0

	INSERT INTO AlumnoPlanHorario (alumnoIdFk, planIdFk, horarioIdFk) VALUES
	(@alumnoIdFk, @planIdFk, @horarioIdFk)
	
	SET @result=(SELECT SCOPE_IDENTITY() AS insertedId);

END

DELETE FROM AlumnoPlanHorario
DELETE FROM Horario WHERE horarioId<10
GO


ALTER TABLE Horario ADD horarioAforo INT
GO

ALTER PROC [dbo].[HorarioCreate]
	@horarioNombre VARCHAR(75),	
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME,
	@horarioDias VARCHAR(300),
	@horarioTurno VARCHAR(30),
	@horarioAforo INT
AS 
BEGIN 
	DECLARE @result INT
	SET @result=0

	IF NOT EXISTS (SELECT * FROM Horario 
							WHERE  horarioNombre=@horarioNombre AND 
										  disciplinaIdFk=@disciplinaIdFk AND 
										  gimnasioIdFk=@gimnasioIdFk AND 
										  instructorIdFk=@instructorIdFk AND
										  horarioDias=@horarioDias AND
										  horarioActivo='SI')
	BEGIN
		INSERT INTO Horario(horarioNombre, disciplinaIdFk, categoriaIdFk, gimnasioIdFk, instructorIdFk, horarioInicio, horarioFin, horarioDias, horarioTurno, horarioAforo) VALUES
		(@horarioNombre, @disciplinaIdFk, @categoriaIdFk, @gimnasioIdFk, @instructorIdFk, @horarioInicio, @horarioFin, @horarioDias, @horarioTurno, @horarioAforo)

		SET @result=1
	END

	SELECT @result
END
GO

UPDATE Horario SET horarioAforo=5

ALTER PROC [dbo].[HorarioUpdate]
	@horarioId INT,
	@horarioNombre VARCHAR(75),	
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME,
	@horarioDias VARCHAR(300),
	@horarioTurno VARCHAR(30),
	@horarioActivo VARCHAR(2),
	@horarioAforo INT
AS 
BEGIN 
	DECLARE @result INT
	DECLARE @disciplinaIdOld INT
	DECLARE @categoriaIdOld INT
	DECLARE @gimnasioIdOld INT
	DECLARE @instructorIdOld INT
	DECLARE @horarioInicioOld TIME
	DECLARE @horarioFinOld TIME
	SET @result=0

	IF EXISTS (SELECT * FROM Horario WHERE  horarioId=@horarioId)
	BEGIN
		
		SELECT 
			@disciplinaIdOld=disciplinaIdFk, @categoriaIdOld=categoriaIdFk, @gimnasioIdOld=gimnasioIdFk, @instructorIdOld=instructorIdFk, @horarioInicioOld=horarioInicio, @horarioFinOld=horarioFin
		FROM Horario WHERE horarioId=@horarioId

		UPDATE Horario SET horarioNombre=@horarioNombre, disciplinaIdFk=@disciplinaIdFk, categoriaIdFk=@categoriaIdFk, gimnasioIdFk=@gimnasioIdFk, instructorIdFk=@instructorIdFk,
		                              horarioInicio=@horarioInicio, horarioFin=@horarioFin, horarioDias=@horarioDias, horarioTurno=@horarioTurno, horarioActivo=@horarioActivo, horarioAforo=@horarioAforo
		WHERE disciplinaIdFk=@disciplinaIdOld AND
		             categoriaIdFk=@categoriaIdOld AND
					 gimnasioIdFk=@gimnasioIdOld AND
					 instructorIdFk=@instructorIdOld AND
		             horarioInicio=@horarioInicioOld AND
					 horarioFin=@horarioFinOld 

		SET @result=1
	END

	SELECT @result
END



ALTER PROC [dbo].[HorarioDelete]
	@horarioId INT,
	@horarioActivo VARCHAR(2)
AS BEGIN
	DECLARE @result INT
	DECLARE @disciplinaIdOld INT
	DECLARE @categoriaIdOld INT
	DECLARE @gimnasioIdOld INT
	DECLARE @instructorIdOld INT
	DECLARE @horarioInicioOld TIME
	DECLARE @horarioFinOld TIME
	SET @result=0

	IF EXISTS (SELECT * FROM Horario WHERE  horarioId=@horarioId)
	BEGIN		

		UPDATE Horario SET horarioActivo= @horarioActivo 		
		WHERE horarioId=@horarioId

		SET @result=1

	END

	SELECT @result

END
GO
		

CREATE PROC HorarioGetGrouped
	@horarioId INT,
	@disciplinaIdFk INT,
	@categoriaIdFk INT,
	@gimnasioIdFk INT,
	@instructorIdFk INT,
	@horarioInicio TIME,
	@horarioFin TIME
AS
BEGIN
	IF EXISTS(SELECT * FROM Horario WHERE horarioId=@horarioId)
	BEGIN
		SELECT * FROM Horario WHERE
		disciplinaIdFk=@disciplinaIdFk   AND
		categoriaIdFk= @categoriaIdFk AND
		gimnasioIdFk = @gimnasioIdFk  AND
		instructorIdFk = @instructorIdFk AND
		horarioInicio = @horarioInicio  AND
		horarioFin = @horarioFin 

		--SELECT * FROM Horario WHERE
		--disciplinaIdFk=2   AND
		--categoriaIdFk= 2 AND
		--gimnasioIdFk = 2  AND
		--instructorIdFk = 2 AND
		--horarioInicio = '17:00'  AND
		--horarioFin = '19:00' 
	END
END

SELECT * FROM Horario

SELECT * FROM PlanDeInscripcion
DELETE FROM PlanDeInscripcion

ALTER TABLE PlanDeInscripcion
ALTER COLUMN planRecurrencia INT
GO
ALTER PROC [dbo].[PlanDeInscripcionGetAll]
AS
BEGIN
	SELECT D.disciplinaNombre, P.planDescripcion, P.planRecurrencia, P.planDias, P.planImporte, P.planActivo
	FROM PlanDeInscripcion P
	JOIN Disciplina D ON P.disciplinaId = D.disciplinaId
END
GO
		


SELECT * FROM PlanDeInscripcion
SELECT * FROM Horario
GO
CREATE PROC GetPlanesHorariosByDisciplina
	@disciplinaId INT
AS
BEGIN

	--SELECT * FROM PlanDeInscripcion
	--SELECT * FROM Horario

	SELECT 
		D.disciplinaId, D.disciplinaNombre, 
		P.planId, P.planDescripcion, P.planRecurrencia, P.planImporte, 
		H.horarioId, H.categoriaIdFk, 
		C.categoriaNombre,
		H.gimnasioIdFk, 
		G.gimnasioNombre,
		H.instructorIdFk, 
		I.instructorNombre,
		H.horarioInicio, 
		H.horarioFin, H.horarioDias, H.horarioTurno, H.horarioAforo
	FROM PlanDeInscripcion P 
		JOIN Horario H ON P.disciplinaId=H.disciplinaIdFk
		JOIN Disciplina D ON P.disciplinaId = D.disciplinaId
		JOIN Categoria C ON H.categoriaIdFk = C.categoriaId
		JOIN Gimnasio G ON H.gimnasioIdFk = G.gimnasioId
		JOIN Instructor I ON H.instructorIdFk = I.instructorId
	WHERE P.disciplinaId = 2 AND 
		         H.horarioActivo = 'SI'
END
GO 

CREATE PROC PlanDeInscripcionGetByDisciplina
	@disciplinaId INT
AS
BEGIN
		SELECT * FROM PlanDeInscripcion WHERE disciplinaId=@disciplinaId
END
GO

CREATE PROC HorarioGetByDisciplina
	@disciplinaId INT
AS
BEGIN
	SELECT * FROM Horario WHERE disciplinaIdFk = @disciplinaId
END


		


			





		 


		 
