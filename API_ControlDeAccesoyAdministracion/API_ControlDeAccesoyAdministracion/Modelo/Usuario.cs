﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Usuario
    {
        public int usuarioId { get; set; }
        public string usuarioNombre { get; set; }
        public string usuarioApellidoPaterno { get; set; }
        public string usuarioApellidoMaterno { get; set; }
        public string usuarioCorreoElectronico { get; set; }
        public string usuarioPassword { get; set; }
        public int rolRolIdFk { get; set; }
        public DateTime usuarioFechaDeCreacion { get; set; }
        public string usuarioActivo { get; set; }

    }
}
