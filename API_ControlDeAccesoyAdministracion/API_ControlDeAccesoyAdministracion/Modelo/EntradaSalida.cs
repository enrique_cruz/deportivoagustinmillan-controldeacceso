﻿using API_ControlDeAccesoyAdministracion.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Models
{
    public class EntradaSalida
    {
        [Key]
        public int entradasalidaId { get; set; }

        //ALUMNO
        [Key]
        [ForeignKey("alumnoId")]
        public int alumnoIdFk {get; set;}
        public string CURP { get; set; }
        public int disciplinaId { get; set; }


        //ENTRADA SALIDA
        public DateTime entradasalidaFechaEntrada { get; set; }
        public DateTime entradasalidaFechaSalida { get; set; }

        public virtual List<Alumno> alumno { get; set; }
    }
}
