﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class TutorAlumno
    {
        //TUTOR ALUMNO
        [Key]
        public int tutoralumnoId { get; set; }

        //TUTOR
        [Key]
        [ForeignKey("tutorId")]
        public int tutorIdFk { get; set; }

        //ALUMNO
        [Key]
        [ForeignKey("alumnoId")]
        public int alumnoIdFk { get; set; }

        //TUTOR ALUMNO
        public DateTime tutoralumnoFechaDeRegistro { get; set; }
        public string tutoralumnoActivo { get; set; }

        //USUARIO
        public int usuarioIdFk { get; set; }        

        public virtual List<Alumno> alumno { get; set; }
        public virtual List<Tutor> tutor { get; set; }


        
    }
}
