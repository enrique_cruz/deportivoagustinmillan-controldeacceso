﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class ReposicionDeClase
    {
        public int reposicionId { get; set; }
        public int alumnoIdFk { get; set; }
        public int horarioIdFk { get; set; }
        public DateTime fechaDeReposicion { get; set; }
        //public virtual Alumno alumno { get; set; }
        //public virtual Horario horario { get; set; }
    }
}
