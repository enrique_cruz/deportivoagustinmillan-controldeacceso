﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class PlanDeInscripcion
    {
        [Key]
        public int planid { get; set; }

        [ForeignKey("disciplinaId")]
        public int disciplinaIdFk { get; set; }
        public virtual Disciplina disciplina { get; set; }
        public string planDescripcion { get; set; }
        public int  planRecurrencia { get; set; }
        public string planDias { get; set; }
        public Decimal planImporte { get; set; }
        public DateTime planFechaDeRegistro { get; set; }
        public string  planActivo { get; set; }

    }
}
