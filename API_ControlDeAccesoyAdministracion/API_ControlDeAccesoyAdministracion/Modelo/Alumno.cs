﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Alumno
    {
        public int alumnoId { get; set; }
        public string alumnoNombre { get; set; }
        public string alumnoApellidoPaterno { get; set; }
        public string alumnoApellidoMaterno { get; set; }
        public string alumnoCURP { get; set; }
        public DateTime alumnoFechaDeNacimiento { get; set; }
        public string alumnoSexo { get; set; }
        public decimal alumnoEstatura { get; set; }
        public decimal alumnoPeso { get; set; }
        public string alumnoRutaFoto { get; set; }
        public string alumnoTelefonoCasa { get; set; }
        public string alumnoTelefonoCelular { get; set; }
        public string alumnoCorreoElectronico { get; set; }
        public string alumnoRedesSociales { get; set; }
        public string alumnoPassword { get; set; }
        public int alumnoCodigoPostal { get; set; }
        public string alumnoEstado { get; set; }
        public string alumnoMunicipio { get; set; }
        public string alumnoColonia { get; set; }
        public string alumnoCalle { get; set; }
        public string alumnoNumero { get; set; }
        public string alumnoAfiliacionMedica { get; set; }
        public string alumnoAlergias { get; set; }
        public string alumnoPadecimientos { get; set; }
        public string alumnoTipoDeSangre { get; set; }
        public DateTime FechaDeRegistro { get; set; }
        public string alumnoActivo { get; set; }
        public string alumnoBase64Image { get; set; }
        public string alumnoFolio { get; set; }

    }
}
