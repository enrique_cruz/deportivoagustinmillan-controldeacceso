﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class AlumnoPLanHorarioRecreado
    {
        public int alumnoId { get; set; }
        public int disciplinaId { get; set; }
        public string disciplinaNombre { get; set; }

        public virtual List<horarioUpdate> horario {get; set;}
        public string planDescripcion { get; set; }

        public int planId { get; set; }
        public decimal planImporte { get; set; }
        public int planRecurrencia { get; set; }
    }
}
