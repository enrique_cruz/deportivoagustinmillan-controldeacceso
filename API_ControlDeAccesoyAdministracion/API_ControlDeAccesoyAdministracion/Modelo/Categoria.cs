﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Categoria
    {
        public int categoriaId { get; set; }
        public string categoriaNombre { get; set; }
        public int categoriaEdadRango1 { get; set; }
        public int categoriaEdadrango2 { get; set; }
        public DateTime categoriaFechaDeRegistro { get; set; }
        public string categoriaActivo { get; set; }
        public string categoriaTipo { get; set; }
    }
}
