﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Parcialidad
    {
        public int parcilidadId { get; set; }
        public int alumnoId { get; set; }
        public Decimal parcialidadImporte { get; set; }
        public DateTime parcialidadMesCorrespondiente { get; set; }
        public string TipoDePago { get; set; }
        public string folioDePago { get; set; }
        public string cuentaBeneficiaria { get; set; }
        public string  conceptoDePago { get; set; }
        public string observaciones { get; set; }
        public DateTime fechaDeReciboDePago { get; set; }

        public int equipoId { get; set; }
    }   
}
