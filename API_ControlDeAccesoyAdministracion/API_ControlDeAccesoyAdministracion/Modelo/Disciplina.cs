﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Disciplina
    {
        public int disciplinaId { get; set; }
        public string disciplinaNombre { get; set; }
        public DateTime disciplinaFechaDeRegistro { get; set; }
        public string disciplinaActivo { get; set; }
    }
}
