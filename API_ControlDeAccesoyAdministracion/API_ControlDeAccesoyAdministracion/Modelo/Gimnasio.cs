﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Gimnasio
    {
        public int gimnasioId { get; set; }
        public string gimnasioNombre { get; set; }
        public DateTime gimnasioFechaDeRegistro { get; set; }
        public string gimnasioActivo { get; set; }
    }
}
