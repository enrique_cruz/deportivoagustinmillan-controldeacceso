﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class horarioUpdate
    {
        public int horarioId { get; set; }
        public string horarioNombre { get; set; }
        public string horarioInicio { get; set; }
        public string horarioFin { get; set; }
        public string horarioDias { get; set; }
        public int horarioDisponibilidad { get; set; }
    }
}
