﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Inscripciones
    {
        [Key]
        public int inscripcionerId { get; set; }
        [Key]
        [ForeignKey("alumnoId")]
        public int alumnoIdFk { get; set; }
        public virtual List<Alumno> alumno { get; set; }
        public Decimal inscripcionImporteAnual { get; set; }
        public DateTime inscripcionFechaDeVigencia { get; set; }
        public DateTime inscripcionFechaDeVencimiento { get; set; }
        public string inscripcionesActivo { get; set; }




        //[Key]
        //public int inscripcionerId { get; set; }
        //[Key]
        //[ForeignKey("alumnoId")]
        //public int alumnoIdFk { get; set; }
        //public virtual List<Alumno> alumno { get; set; }
        //public Decimal inscripcionImporteAnual { get; set; }
        //public DateTime inscripcionFechaDeVigencia { get; set; }
        //public DateTime inscripcionFechaDeVencimiento { get; set; }
        //public string inscripcionesActivo { get; set; }

    }
}
