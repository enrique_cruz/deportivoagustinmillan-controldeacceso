﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Horario
    {
        [Key]
        public int horarioId { get; set; }
        public string horarioNombre { get; set; }

        [ForeignKey("disciplinaId")]
        public int disciplinaIdFk { get; set; }
        public virtual Disciplina disciplina { get; set; }


        [ForeignKey("categoriaId")]
        public int categoriaIdFk { get; set; }
        public virtual Categoria categoria { get; set; }

        [ForeignKey("gimnasioId")]
        public int gimnasioIdFk { get; set; }
        public virtual Gimnasio gimnasio { get; set; }

        [ForeignKey("instructorId")]
        public int instructorIdFk { get; set; }
        public virtual Instructor instructor { get; set; }

        public DateTime horarioInicio { get; set; }
        public DateTime horarioFin { get; set; }
        public string horarioDias { get; set; }
        public DateTime horarioFechaDeRegistro { get; set; }
        [Key]
        public int usuarioIdFk { get; set; }
        public string horarioActivo { get; set; }
        public string horarioTurno { get; set; }

        public int horarioAforo { get; set; }
                
    }
}
