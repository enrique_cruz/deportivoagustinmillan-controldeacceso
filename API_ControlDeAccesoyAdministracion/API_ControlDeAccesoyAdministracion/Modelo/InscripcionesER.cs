﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class InscripcionesER
    {
        [Key]
        public int inscripcionesId { get; set; }
        public virtual Alumno alumno { get; set; }
        public virtual List<Tutor> tutor { get; set; }
        public virtual List<PlanHorario> PlanHorario { get; set; }        
        public Decimal inscripcionImporteAnual { get; set; }
        public DateTime inscripcionFechaDeVigencia { get; set; }
        public DateTime inscripcionFechaDeVencimiento { get; set; }
        public string inscripcionesActivo { get; set; }
        public int porcentajeDePago { get; set; }
        public string cuentaBenefiaria { get; set; }
        public string conceptoDePago { get; set; }
        public string observaciones { get; set; }
        public DateTime fechaDeReciboDePago { get; set; }
        //public Decimal importeTotal { get; set; }
        public virtual Parcialidad parcialidad { get; set; }
    }
}