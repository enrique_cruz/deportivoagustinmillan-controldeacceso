﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Tutor
    {
        public int tutorId { get; set; }
        public string tutorNombre { get; set; }
        public string tutorApellidoPaterno { get; set; }
        public string tutorApellidoMaterno { get; set; }
        public string tutorParentesco { get; set; }
        public string tutorTelefonoCasa { get; set; }
        public string tutorTelefonoCelular { get; set; }
        public string tutorCorreoElectronico { get; set; }
        public DateTime tutorFechaDeRegistro { get; set; }
        public string tutorActivo { get; set; }
        public string tutorBase64Image { get; set;}

    }
}
