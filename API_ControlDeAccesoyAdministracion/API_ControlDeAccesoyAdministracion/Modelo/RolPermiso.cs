﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class RolPermiso
    {
        public int rolpermisoId { get; set; }
        public int rolIdFk { get; set; }
        public string rolNombre { get; set; }
        public virtual List<Permisos> permiso { get; set; }

        public int usuarioIdFk { get; set; }

    }
}
