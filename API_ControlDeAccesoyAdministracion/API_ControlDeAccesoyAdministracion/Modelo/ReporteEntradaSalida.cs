﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class ReporteEntradaSalida
    {
        public int alumnoIdFk { get; set; }
        public string alumnoNombre { get; set; }
        public string alumnoApellidoPaterno { get; set; }
        public string alumnoApellidoMaterno { get; set; }
        public string fechaEntrada { get; set; }
        public string fechaSalida { get; set; }
    }
}
