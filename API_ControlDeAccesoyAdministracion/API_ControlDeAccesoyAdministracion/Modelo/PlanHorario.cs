﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class PlanHorario
    {        
        public int  planId { get; set; }
        public int planRecurrencia { get; set; }
        public Decimal planImporte  { get; set; }
        public string planActivo { get; set; }
        //public int planPorcentajeDePago { get; set; }
        public virtual List<Horario> Horario { get; set; }

    }
}
