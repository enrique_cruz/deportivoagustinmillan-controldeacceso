﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class DiaHorario
    {
        public string dia { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }

        public int gimnasioId { get; set; }
        public string gimnasioNombre { get; set; }

        public int disciplinaId { get; set; }
        public string  disciplinaNombre { get; set; }


    }
}
