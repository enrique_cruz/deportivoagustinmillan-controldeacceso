﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Permisos
    {
        public int permisosId { get; set; }
        public string permisosNombre { get; set; }
        public DateTime permisosFechaDeCreacion { get; set; }
        public string permisosActivo { get; set; }
    }
}
