﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Mensualidad
    {
        [Key]
        public int mensualidadId { get; set; }
        [ForeignKey("alumnoId")]
        public int alumnoIdFk { get; set; }
        public virtual Alumno alumno { get; set; }
        public Decimal mensualidadImporte { get; set; }
        public DateTime mensualidadFechaDePago { get; set; }
        public DateTime mensualidadFechaRealDePago { get; set; }
        public string mensualidadConRetraso { get; set; }
        public Decimal mensualidadImportePorRetraso { get; set; }
        public string mensualidadesActivo { get; set; }
        public int mensualidadesPorcentajePago { get; set; }
        public List<PlanHorario> planHorario { get; set; }
        public Decimal mensualidadSaldoAFavor { get; set; }

        //Atributos para inscripcion
        public Decimal inscripcionImporteAnual { get; set; }
        //public DateTime inscripcionFechaDeVigencia { get; set; }
        public string cuentaBenefiaria { get; set; }
        public string conceptoDePago { get; set; }
        public string  observaciones { get; set; }

        public virtual List<Parcialidad> parcialidades { get; set; }        
        
    }
}
