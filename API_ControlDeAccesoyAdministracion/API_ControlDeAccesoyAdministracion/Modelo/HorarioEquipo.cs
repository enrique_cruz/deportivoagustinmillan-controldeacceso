﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class HorarioEquipo
    {
        public int horarioEquipoId { get; set; }
        public virtual Disciplina disciplina { get; set; }
        public virtual Equipos equipo { get; set; }

        public virtual Gimnasio gimnasio { get; set; }

        public virtual List<DiaHorario> horario { get; set; }

        public string horarioEquipoActivo { get; set; }

        public int horarioId { get; set; }
        public int gimnasioId { get; set; }
        public string horarioEquipoDia { get; set; }
        public TimeSpan horarioEquipoInicio { get; set; }
        public TimeSpan horarioEquipoFin { get; set; }
    }
}
