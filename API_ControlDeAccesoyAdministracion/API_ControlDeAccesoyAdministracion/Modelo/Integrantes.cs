﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Integrantes
    {
        public int integranteId { get; set; }
        public string integranteNombre { get; set; }
        public string integranteApellidoPaterno { get; set; }
        public string integranteApellidoMaterno { get; set; }
        public string integranteCURP { get; set; }
        public DateTime integranteFechaDeRegistro { get; set; }
        public string integranteActivo { get; set; }
        public string integranteTelefono { get; set; }
        public int equipoId { get; set; }
    }
}
