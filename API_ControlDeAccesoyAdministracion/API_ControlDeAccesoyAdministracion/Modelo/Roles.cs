﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Roles
    {
        public int rolesId { get; set; }
        public string rolesNombre { get; set; }
        public DateTime rolesFechaDeRegistro { get; set; }
        public string rolesActivo  { get; set; }
        
    }
}
