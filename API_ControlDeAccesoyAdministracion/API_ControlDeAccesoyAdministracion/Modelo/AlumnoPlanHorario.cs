﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class AlumnoPlanHorario
    {
        public int actual { get; set; }
        public int alumnoId { get; set; }
        public int disciplinaId { get; set; }
        public int planid { get; set; }
        public virtual List<horarioUpdate> horario { get; set; }

        public int alumnoplanhorarioId { get; set; }

        //ALUMNO
        
        public string alumnoNombre { get; set; }
        public string alumnoApellidoPaterno { get; set; }
        public string alumnoApellidoMaterno { get; set; }
        public string alumnoCURP { get; set; }
        public DateTime alumnoFechaDeNacimiento { get; set; }
        public string alumnoSexo { get; set; }
        public decimal alumnoEstatura { get; set; }
        public decimal alumnoPeso { get; set; }
        public string alumnoRutaFoto { get; set; }
        public string alumnoTelefonoCasa { get; set; }
        public string alumnoTelefonoCelular { get; set; }
        public string alumnoCorreoElectronico { get; set; }
        public string alumnoRedesSociales { get; set; }
        public string alumnoPassword { get; set; }
        public int alumnoCodigoPostal { get; set; }
        public string alumnoEstado { get; set; }
        public string alumnoMunicipio { get; set; }
        public string alumnoColonia { get; set; }
        public string alumnoCalle { get; set; }
        public string alumnoAfiliacionMedica { get; set; }
        public string alumnoAlergias { get; set; }
        public string alumnoPadecimientos { get; set; }
        public string alumnoTipoDeSangre { get; set; }
        public DateTime FechaDeRegistro { get; set; }
        public string alumnoActivo { get; set; }

        //PLAN
        

        //DISCIPLINA
        
        public string disciplinaNombre { get; set; }
        public DateTime disciplinaFechaDeRegistro { get; set; }
        public string disciplinaActivo { get; set; }

        //PLAN
        public string planDescripcion { get; set; }
        public string planRecurrencia { get; set; }
        public string planDias { get; set; }
        public Decimal planImporte { get; set; }
        public DateTime planFechaDeRegistro { get; set; }
        public string planActivo { get; set; }

        //HORARIO
        public int horarioId { get; set; }
        public string horarioNombre { get; set; }        
        public DateTime horarioInicio { get; set; }
        public DateTime horarioFin { get; set; }
        public string horariosDias { get; set; }
        public DateTime horarioFechaDeRegistro { get; set; }
        public string horarioActivo { get; set; }

        //CATEGORIA
        public int categoriaId { get; }
        public string categoriaNombre { get; set; }
        public int categoriaEdadRango1 { get; set; }
        public int categoriaEdadrango2 { get; set; }
        public DateTime categoriaFechaDeRegistro { get; set; }
        public string categoriaActivo { get; set; }

        //GIMNASIO
        public int gimnasioId { get; set; }
        public string gimnasioNombre { get; set; }
        public DateTime gimnasioFechaDeRegistro { get; set; }
        public string gimnasioActivo { get; set; }

        //INSTRUCTOR
        public int instructorId { get; set; }
        public string instructorNombre { get; set; }
        public string instructorApellidoPaterno { get; set; }
        public string instructorApellidoMaterno { get; set; }
        public string instructorCURP { get; set; }
        public string instructorRFC { get; set; }
        public int instructorCodigoPostal { get; set; }
        public string instructorEstado { get; set; }
        public string instructorMunicipio { get; set; }
        public string instructorColonia { get; set; }
        public string instructorCalle { get; set; }
        public int instructorNumero { get; set; }
        public DateTime instructorFechaDeRegistro { get; set; }
        public string instructorActivo { get; set; }

       

    }
}
