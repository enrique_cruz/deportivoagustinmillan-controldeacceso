﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Instructor
    {
        public int instructorId { get; set; }
        public string instructorNombre { get; set; }
        public string instructorApellidoPaterno { get; set; }
        public string instructorApellidoMaterno { get; set; }
        public string instructorCURP { get; set; }
        public string instructorRFC { get; set; }
        public int instructorCodigoPostal { get; set; }
        public string instructorEstado { get; set; }
        public string instructorMunicipio { get; set; }
        public string instructorColonia { get; set; }
        public string instructorCalle { get; set; }
        public int instructorNumero { get; set; }
        public DateTime instructorFechaDeRegistro { get; set; }
        public string instructorActivo{ get; set; }
        public DateTime instructorFechaDeNacimiento { get; set; }
        public string instructorSexo { get; set; }

        public string instructorBase64Image { get; set; }

        public string instructorTelefono { get; set; }
    }
}
