﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class Equipos
    {
        public int equipoId { get; set; }
        public string equipoNombre { get; set; }

        public DateTime equipoFechaDeRegistro { get; set; }

        public virtual List<Integrantes> integrantes { get; set; }

        public string equipoActivo { get; set; }

        public string equipoRepresentante { get; set; }

        public string equipoRepresentanteTelefono { get; set; }
        public Decimal equipoImporte { get; set; }

    }
}
