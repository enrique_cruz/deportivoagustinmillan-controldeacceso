﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Modelo
{
    public class QrMaestro
    {
        public int qrId { get; set; }
        public string qrString { get; set; }
        public DateTime fechaDeCreacion { get; set; }

        public string qrActivo { get; set; }
    }
}
