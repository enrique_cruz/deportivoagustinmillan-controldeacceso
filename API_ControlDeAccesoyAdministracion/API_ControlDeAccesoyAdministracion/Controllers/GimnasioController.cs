﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GimnasioController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public GimnasioController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GimnasioGetActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactiveActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GimnasioGetInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GimnasioGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }           

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("AllData")]
        public JsonResult allData()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GimnasioDataAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Gimnasio gimnasio)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "GimnasioCreate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@gimnasioNombre", gimnasio.gimnasioNombre);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Gimnasio gimnasio)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "GimnasioUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@gimnasioId", gimnasio.gimnasioId);
                cmd.Parameters.AddWithValue("@gimnasioNombre", gimnasio.gimnasioNombre);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Gimnasio gimnasio)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "GimnasioDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@gimnasioId", gimnasio.gimnasioId);
                cmd.Parameters.AddWithValue("@gimnasioActivo", gimnasio.gimnasioActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }
    }
}
