﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolPermisoController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public RolPermisoController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{id}")]
        public JsonResult RolPermisoGetById(int id)
        {

            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "RolesPermisoGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rolIdFk", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Usuario/ID/{UsuarioId}")]
        public JsonResult RolPermisoGetByUsuarioId(int UsuarioId)
        {

            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuarioRolPermisoGetByUserID";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuarioIdFk", UsuarioId);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Usuario/CorreoElectronico/{correo}")]
        public JsonResult RolPermisoGetByCorreoElectronico(string correo)
        {

            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuarioRolPermisoGetByCorreo";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuarioCorreoElectronico", correo);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult RolPermisoCreate(RolPermiso rp)
        {
            int result = 0;
            int rolId;

            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "RolesCreate";
            string SP_NAME_ROL_PERMISO_CREATE = "RolesPermisoCreate";

            SqlCommand cmd;      

            try
            {                
                if (rp.rolIdFk == 0)
                {
                    //ES NUEVO ROL
                    con.Open();
                    cmd = new SqlCommand(SP_NAME, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Nombre", rp.rolNombre);
                                     

                    //Obtenemos el último id insertado
                    rolId = (int)cmd.ExecuteScalar();

                    if (rolId != 0)
                    {
                        foreach (var p in rp.permiso)
                        {
                            cmd = new SqlCommand(SP_NAME_ROL_PERMISO_CREATE, con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@rolIdFk", rolId);
                            cmd.Parameters.AddWithValue("@permisoIdFk", p.permisosId);

                            cmd.ExecuteNonQuery();

                            result = 1;
                        }
                    }

                }
                else
                {
                    //Llamamos elimninar Rol
                    JsonResult r= UpdateStatus(rp);
                    con.Open();
                    foreach (var p in rp.permiso)
                    {
                        cmd = new SqlCommand(SP_NAME_ROL_PERMISO_CREATE, con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@rolIdFk", rp.rolIdFk);
                        cmd.Parameters.AddWithValue("@permisoIdFk", p.permisosId);

                        cmd.ExecuteNonQuery();

                        result = 1;
                    }
                }

                con.Close();
                //transaction.Commit();
            }
            catch (Exception ex)
            {
                //transaction.Rollback(); 
                return new JsonResult("Commit Exception Type: " + ex.GetType() + ". Error: " + ex.Message);
            }
            finally
            {
                con.Close();
                //transaction.Rollback();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowOnyOrigin")]
        [HttpPost("UpdateStatus")]
        public JsonResult UpdateStatus(RolPermiso rp)
        {
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            int result = 0;
            string DELETE_ROL = "RolPermisoDelete";

            try
            {

                //Eliminamos  primero todos los IDs
                cmd = new SqlCommand(DELETE_ROL, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@rolIdFk", rp.rolIdFk);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                result = 1;

            }
            catch (Exception ex)
            {

                return new JsonResult("Error: " + ex.Message.ToString());
            }
            finally 
            {
                con.Close();
            }            

            return new JsonResult(result);
        }
    }
}
