﻿using API_ControlDeAccesoyAdministracion.Modelo;
using API_ControlDeAccesoyAdministracion.Utilerias;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class InscripcionController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public InscripcionController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InscripcionesGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InscripcionesGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InscripcionesGetAllInactve";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InscripcionesGetByAlumnoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Inscripciones inscripcion)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InscripcionesCreate";


            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", inscripcion.alumnoIdFk);
                cmd.Parameters.AddWithValue("@inscripcionimporteAnual", inscripcion.inscripcionImporteAnual);                
                cmd.Parameters.AddWithValue("@inscripcionFechaDeVigencia", inscripcion.inscripcionFechaDeVigencia);
                //cmd.Parameters.AddWithValue("@inscripcionFechaDeVencimiento", inscripcion.inscripcionFechaDeVencimiento);
                


                con.Open();
                result = (int)cmd.ExecuteScalar();


            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("NuevaInscripcion")]
        public JsonResult NuevaInscripcion(InscripcionesER inscripcioner)
        {
            int result = 0;
            int idAlumno = 0;
            int idTutor = 0;
            decimal importe;

            DateTime now = DateTime.Now;

            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_Alumno = "AlumnoCreate";
            string SP_NAME_Tutor = "TutorCreate";
            string SP_NAME_TutorAlumno = "TutorAlumnoCreate";
            string SP_NAME_AlumnoPlanHorario = "AlumnoPlanHorarioCreate";
            string SP_NAME_Mensualidad = "MensualidadesCreate";
            string SP_NAME_Inscripcion = "InscripcionesCreate";

            string SP_NAME_VirificaFolio = "ParcialidadVerificarFolio";
            string SP_NAME_ParcialidadDePagoCreate = "ParcialidadDePagoCreateSinVerificarFolio";

            SqlCommand cmd;

            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();

            SqlDataReader rd2;
            SqlDataReader rd3;
            SqlDataReader rd4;

            try
            {
                con.Open();

                //PASO 0. VERIFICAMOS QUE EL FOLIO NO ESTË REPETIDO
                if (inscripcioner.parcialidad.folioDePago != "")
                {
                    cmd = new SqlCommand(SP_NAME_VirificaFolio, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@folio", inscripcioner.parcialidad.folioDePago);
                    cmd.Parameters.AddWithValue("@tipoDePago", inscripcioner.parcialidad.TipoDePago);

                    rd2 = cmd.ExecuteReader();
                    dt2.Load(rd2);

                    if (dt2.Rows[0].ItemArray[2].ToString().Equals("0"))
                    {
                        //2. Si el folio está repetido retornamos el mensaje y el estatus
                        var estatus = dt2.Rows[0].ItemArray[0].ToString();
                        return new JsonResult(estatus);
                    }
                                                                                 
                }
             
                //1. Insertamos Alumno
                cmd = new SqlCommand(SP_NAME_Alumno, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoNombre", inscripcioner.alumno.alumnoNombre);
                cmd.Parameters.AddWithValue("@alumnoApellidoPaterno", inscripcioner.alumno.alumnoApellidoPaterno);
                cmd.Parameters.AddWithValue("@alumnoApellidoMaterno", inscripcioner.alumno.alumnoApellidoMaterno);
                cmd.Parameters.AddWithValue("@alumnoCURP", inscripcioner.alumno.alumnoCURP);
                cmd.Parameters.AddWithValue("@alumnoFechaDeNacimiento", inscripcioner.alumno.alumnoFechaDeNacimiento);
                cmd.Parameters.AddWithValue("@alumnoSexo", inscripcioner.alumno.alumnoSexo);
                cmd.Parameters.AddWithValue("@alumnoEstatura", inscripcioner.alumno.alumnoEstatura);
                cmd.Parameters.AddWithValue("@alumnoPeso", inscripcioner.alumno.alumnoPeso);
                cmd.Parameters.AddWithValue("@alumnoRutaFoto", inscripcioner.alumno.alumnoRutaFoto);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCasa", inscripcioner.alumno.alumnoTelefonoCasa);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCelular", inscripcioner.alumno.alumnoTelefonoCelular);
                cmd.Parameters.AddWithValue("@alumnoCorreoElectronico", inscripcioner.alumno.alumnoCorreoElectronico);
                cmd.Parameters.AddWithValue("@alumnoRedesSociales", inscripcioner.alumno.alumnoRedesSociales);
                cmd.Parameters.AddWithValue("@alumnoPassword", inscripcioner.alumno.alumnoPassword);
                cmd.Parameters.AddWithValue("@alumnoCodigoPostal", inscripcioner.alumno.alumnoCodigoPostal);
                cmd.Parameters.AddWithValue("@alumnoEstado", inscripcioner.alumno.alumnoEstado);
                cmd.Parameters.AddWithValue("@alumnoMunicipio", inscripcioner.alumno.alumnoMunicipio);
                cmd.Parameters.AddWithValue("@alumnoColonia", inscripcioner.alumno.alumnoColonia);
                cmd.Parameters.AddWithValue("@alumnoCalle", inscripcioner.alumno.alumnoCalle);
                cmd.Parameters.AddWithValue("@alumnoNumero", inscripcioner.alumno.alumnoNumero);
                cmd.Parameters.AddWithValue("@alumnoAfiliacionMedica", inscripcioner.alumno.alumnoAfiliacionMedica);
                cmd.Parameters.AddWithValue("@alumnoAlergias", inscripcioner.alumno.alumnoAlergias);
                cmd.Parameters.AddWithValue("@alumnoPadecimientos", inscripcioner.alumno.alumnoPadecimientos);
                cmd.Parameters.AddWithValue("@alumnoTipoDeSangre", inscripcioner.alumno.alumnoTipoDeSangre);
                cmd.Parameters.AddWithValue("@alumnoFolio", inscripcioner.alumno.alumnoFolio);

                
                idAlumno = (int)cmd.ExecuteScalar();

                if (idAlumno > 0)
                {
                    result = 1;
                }

                string path = @"C:\imagenes";

                if (Directory.Exists(path) && result == 1)
                {
                    Imagen.Base64ToImage(inscripcioner.alumno.alumnoBase64Image, inscripcioner.alumno.alumnoCURP, path);
                }
                else if (result == 1)
                {
                    //Si no existe el directorio lo creamos
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    Imagen.Base64ToImage(inscripcioner.alumno.alumnoBase64Image, inscripcioner.alumno.alumnoCURP, path);
                }

                if (idAlumno > 0 && inscripcioner.tutor.Count>0) 
                {

                    //2. INSERTAMOS TUTOR EN LA TABLA TUTOR
                    foreach (var t in inscripcioner.tutor) 
                    {
                        
                        cmd = new SqlCommand(SP_NAME_Tutor, con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@tutorNombre", t.tutorNombre);
                        cmd.Parameters.AddWithValue("@tutorApellidoPAterno", t.tutorApellidoPaterno);
                        cmd.Parameters.AddWithValue("@tutorApellidoMaterno", t.tutorApellidoMaterno);
                        cmd.Parameters.AddWithValue("@tutorParentesco", t.tutorParentesco);
                        cmd.Parameters.AddWithValue("@tutorTelefonoCasa", t.tutorTelefonoCasa);
                        cmd.Parameters.AddWithValue("@tutortelefonoCelular", t.tutorTelefonoCelular);
                        cmd.Parameters.AddWithValue("@tutorCorreoElectronico", t.tutorCorreoElectronico);

                        idTutor = (int)cmd.ExecuteScalar();

                        if (idTutor > 0)
                        {
                            //3. INSERTAMOS TUTOR y ALUMNO EN LA TABLA TutorAlumno
                            cmd = new SqlCommand(SP_NAME_TutorAlumno, con);                            
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@tutorIdFk", idTutor);
                            cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);

                            result = (int)cmd.ExecuteScalar();

                        }

                    }

                    //4. INSERTAMOS AlumnoId, PLanId, HorarioId EN AlumnoPlanHOrario
                    foreach (var ph in inscripcioner.PlanHorario)
                    {
                        var planId = ph.planId;
                        foreach (var h in ph.Horario)
                        {
                            var horarioId = h.horarioId;

                            cmd = new SqlCommand(SP_NAME_AlumnoPlanHorario, con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);
                            cmd.Parameters.AddWithValue("@planIdFk",ph.planId);
                            cmd.Parameters.AddWithValue("@horarioIdFk",h.horarioId);

                            result = (int)cmd.ExecuteScalar();

                        }

                        //5. INSERTAMOS LA MENSUALIDAD EN Mensualidad                                        
                        //importe = Convert.ToDecimal(ph.planImporte) * (Convert.ToDecimal(inscripcioner.porcentajeDePago) / 100);
                        
                        cmd = new SqlCommand(SP_NAME_Mensualidad, con);

                        //MensualidadesCreate
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);
                        //cmd.Parameters.AddWithValue("@mensualidadImporte", importe);
                        cmd.Parameters.AddWithValue("@mensualidadImporte", ph.planImporte);
                        cmd.Parameters.AddWithValue("@mensualidadFechaDePago", new DateTime(now.Year, now.Month, 1));
                        cmd.Parameters.AddWithValue("@mensualidadFechaRealDePago", DateTime.Now);
                        cmd.Parameters.AddWithValue("@mensualidadConRetraso", "NO");
                        cmd.Parameters.AddWithValue("@mensualidadImportePorRetraso", 0);
                        cmd.Parameters.AddWithValue("@mensualidadPorcentajeDePago", inscripcioner.porcentajeDePago);
                        cmd.Parameters.AddWithValue("@planIdFk", ph.planId);
                        cmd.Parameters.AddWithValue("@mensualidadSaldoAFavor", 0);
                        cmd.Parameters.AddWithValue("@cuentaBeneficiaria", "");
                        cmd.Parameters.AddWithValue("@conceptoDePago", "Mensualidad");
                        cmd.Parameters.AddWithValue("@observaciones", "");

                        result = (int)cmd.ExecuteScalar();

                        
                        //Paso 6 INSERTAMOS LA PARCIALIDAD
                        cmd = new SqlCommand(SP_NAME_ParcialidadDePagoCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);
                        cmd.Parameters.AddWithValue("@parcialidadImporte", ph.planImporte);
                        cmd.Parameters.AddWithValue("@tipoDePago", inscripcioner.parcialidad.TipoDePago);
                        cmd.Parameters.AddWithValue("@folioDePago", inscripcioner.parcialidad.folioDePago);
                        cmd.Parameters.AddWithValue("@cuentaBeneficiaria", inscripcioner.parcialidad.cuentaBeneficiaria);
                        cmd.Parameters.AddWithValue("@conceptoDePago", "Mensualidad");
                        cmd.Parameters.AddWithValue("@observaciones", inscripcioner.parcialidad.observaciones);
                        cmd.Parameters.AddWithValue("@fechaDeReciboDePago", inscripcioner.parcialidad.fechaDeReciboDePago);

                        rd3 = cmd.ExecuteReader();
                        dt3.Load(rd3);

                    }

                    importe = Convert.ToDecimal(inscripcioner.inscripcionImporteAnual) * (Convert.ToDecimal(inscripcioner.porcentajeDePago) / 100);

                    //Paso 6.1 INSERTAMOS LOS DATOS DE LA INSCRIPCION EN LA PARCIALIDAD
                    cmd = new SqlCommand(SP_NAME_ParcialidadDePagoCreate, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);
                    cmd.Parameters.AddWithValue("@parcialidadImporte", importe);
                    cmd.Parameters.AddWithValue("@tipoDePago", inscripcioner.parcialidad.TipoDePago);
                    cmd.Parameters.AddWithValue("@folioDePago", inscripcioner.parcialidad.folioDePago);
                    cmd.Parameters.AddWithValue("@cuentaBeneficiaria", inscripcioner.parcialidad.cuentaBeneficiaria);
                    cmd.Parameters.AddWithValue("@conceptoDePago", "Inscripcion");
                    cmd.Parameters.AddWithValue("@observaciones", inscripcioner.parcialidad.observaciones);
                    cmd.Parameters.AddWithValue("@fechaDeReciboDePago", inscripcioner.parcialidad.fechaDeReciboDePago);

                    rd4 = cmd.ExecuteReader();
                    dt4.Load(rd4);

                    //7. INSERTAMOS DATOS EXTRA DE INSCRIPCION


                    //8. SP_NAME => InscripcionesCreate
                    cmd = new SqlCommand(SP_NAME_Inscripcion, con); 
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@alumnoIdFk", idAlumno);
                    cmd.Parameters.AddWithValue("@inscripcionimporteAnual", importe);
                    cmd.Parameters.AddWithValue("@inscripcionFechaDeVigencia", new DateTime(now.Year, now.Month, 1));
                    cmd.Parameters.AddWithValue("@PorcentajeDePago", inscripcioner.porcentajeDePago);
                    cmd.Parameters.AddWithValue("@cuentaBeneficiaria", "");
                    cmd.Parameters.AddWithValue("@conceptoDePago", "Inscripcion");
                    cmd.Parameters.AddWithValue("@observaciones", "Inscripcion");
                    cmd.Parameters.AddWithValue("@fechaDeReciboDePago", DateTime.Now);

                    result = (int)cmd.ExecuteScalar();

                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Inscripciones inscripcion)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InscripcionesUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@inscripcionId", inscripcion.inscripcionerId);
                cmd.Parameters.AddWithValue("@alumnoIdFk", inscripcion.alumnoIdFk);
                cmd.Parameters.AddWithValue("@inscripcionimporteAnual", inscripcion.inscripcionImporteAnual);
                cmd.Parameters.AddWithValue("@inscripcionFechaDeVigencia", inscripcion.inscripcionFechaDeVigencia);
                cmd.Parameters.AddWithValue("@inscripcionesActivo", inscripcion.inscripcionesActivo);
                //cmd.Parameters.AddWithValue("@inscripcionFechaDeVencimiento", inscripcion.inscripcionFechaDeVencimiento);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Inscripciones inscripcion)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InscripcionesDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@inscripcionId", inscripcion.inscripcionerId);
                cmd.Parameters.AddWithValue("@inscripcionesActivo", inscripcion.inscripcionesActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("PlaneHorario")]
        public JsonResult PlanesHorariosByDiciplina(int id) 
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GetPlanesHorariosByDisciplina";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

        }

    }
}
