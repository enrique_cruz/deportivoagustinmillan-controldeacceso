﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class MensualidadController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public MensualidadController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "MensualidadesGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "MensualidadesGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "MensualidadesGetAllInactve";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "MensualidadGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mensualidadId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Alumno/{id}")]
        public JsonResult GetMensulaidadByAlumnoId(int id, DateTime fecha)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "MensualidadGetByAlumnoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", id);
                cmd.Parameters.AddWithValue("@fechaDeReciboDePago", fecha);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        //Pagos para otros conceptos que no sean Mensualidad
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("PagoOtroConcepto")]
        public JsonResult RegistrarPago(Mensualidad m)
        {
            DateTime now = DateTime.Now;
            DataTable dt = new DataTable();
            SqlDataReader rd;

            DataTable dt2 = new DataTable();
            SqlDataReader rd2;

            DataTable dt3 = new DataTable();
            SqlDataReader rd3;

            SqlConnection con = new SqlConnection(CON_STR);

            SqlCommand cmd;

            string SP_NAME_RegistrarPago= "RegistrarPago";
            string SP_NAME_VirificaFolio = "ParcialidadVerificarFolio";
            string SP_NAME_ParcialidadDePagoCreate = "ParcialidadDePagoCreate";

            try
            {
                con.Open();

                if (m.parcialidades.Count > 0)
                {
                    //1. Verificamos que por lo menos haya una parcialidad y que no esté repetida
                    foreach (var p in m.parcialidades) 
                    {
                        cmd = new SqlCommand(SP_NAME_VirificaFolio, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@folio", p.folioDePago);
                        cmd.Parameters.AddWithValue("@tipoDePago", p.TipoDePago);

                        rd2 = cmd.ExecuteReader();
                        dt2.Load(rd2);

                        if (dt2.Rows[0].ItemArray[2].ToString().Equals("0"))
                        {
                            //2. Si el folio está repetido retornamos el mensaje y el estatus
                            return new JsonResult(dt2);
                        }                        
                    }

                    //3. Si no hay repetidos continumaos con la insercion de las parcialidades
                    foreach (var p in m.parcialidades) 
                    {
                        cmd = new SqlCommand(SP_NAME_ParcialidadDePagoCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@alumnoIdFk", m.alumnoIdFk);
                        cmd.Parameters.AddWithValue("@parcialidadImporte", p.parcialidadImporte);
                        cmd.Parameters.AddWithValue("@tipoDePago", p.TipoDePago);
                        cmd.Parameters.AddWithValue("@folioDePago", p.folioDePago);
                        cmd.Parameters.AddWithValue("@cuentaBeneficiaria",p.cuentaBeneficiaria);
                        cmd.Parameters.AddWithValue("@conceptoDePago",p.conceptoDePago);
                        cmd.Parameters.AddWithValue("@observaciones",p.observaciones);
                        cmd.Parameters.AddWithValue("@fechaDeReciboDePago",p.fechaDeReciboDePago);

                        rd3 = cmd.ExecuteReader();
                        dt3.Load(rd3);
                        
                    }


                    //4. registrar Pago en Mensualidades
                    if (dt3.Rows[0].ItemArray[0].ToString().Equals("1")) 
                    {
                        cmd = new SqlCommand(SP_NAME_RegistrarPago, con);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@alumnoIdFk", m.alumnoIdFk);
                        cmd.Parameters.AddWithValue("@importe", m.mensualidadImporte);                        
                        cmd.Parameters.AddWithValue("@fechaRealDePago", m.mensualidadFechaRealDePago);
                        cmd.Parameters.AddWithValue("@porcentajeDePago", m.mensualidadesPorcentajePago);
                        cmd.Parameters.AddWithValue("@cuentaBeneficiaria", m.cuentaBenefiaria);
                        cmd.Parameters.AddWithValue("@conceptoDePago", m.conceptoDePago);
                        cmd.Parameters.AddWithValue("@observaciones", m.observaciones);
                        cmd.Parameters.AddWithValue("@folio", m.parcialidades[0].folioDePago);
                        cmd.Parameters.AddWithValue("@saldoAFavor", m.mensualidadSaldoAFavor);
                        cmd.Parameters.AddWithValue("@mensualidadFechaDePago", new DateTime(now.Year, now.Month, 1));

                        rd = cmd.ExecuteReader();
                        dt.Load(rd);
                    }
                }
                
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        //Pago para las mensualidades
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Pago")]
        public JsonResult Nuevo(Mensualidad mensualidad)
        {
            int result = 0;
            DateTime now = DateTime.Now;

            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_NAME_MENSUALIDAD = "MensualidadesCreate";
            string SP_NAME_INSCRIPCION = "InscripcionesCreate";

            string SP_NAME_inactivateAllByAlumnoId = "AlumnoPlanHorario_InactivateAllByAlumnoIdFk";
            string SP_NAME_NuevoAlumnoPlanHorario = "AlumnoPlanHorarioCreate";

            string SP_NAME_ParcialidadesCreate = "ParcialidadDePagoCreate";
            string SP_NAME_ParcialidadVerificarFolio = "ParcialidadVerificarFolio";

            List<int> foliosResultLst = new List<int>();
            DataTable dt;
            SqlDataReader rd;

            try
            {
                con.Open();

                //Si hay un folio repetido que sea de Transferencia Electrónica o Depósito Bancario 
                //terminamos el proceso y mandamos el mensaje
                foreach (var p in mensualidad.parcialidades)
                {
                    cmd = new SqlCommand(SP_NAME_ParcialidadVerificarFolio, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@folio", p.folioDePago);
                    cmd.Parameters.AddWithValue("@tipoDePago", p.TipoDePago);

                    rd = cmd.ExecuteReader();
                    dt = new DataTable();
                    dt.Load(rd);
                   
                    if (dt.Rows[0].ItemArray[2].ToString().Equals("0"))
                    {
                        return new JsonResult(dt);
                    }
                   
                }
                
                //Si no hay ningún Folio repetido los insertamos en las parcialidades     
                foreach (var p in mensualidad.parcialidades)
                {

                    cmd = new SqlCommand(SP_NAME_ParcialidadesCreate, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);
                    cmd.Parameters.AddWithValue("@parcialidadImporte", p.parcialidadImporte);
                    cmd.Parameters.AddWithValue("@tipoDePago", p.TipoDePago);
                    cmd.Parameters.AddWithValue("@folioDePago", p.folioDePago);
                    cmd.Parameters.AddWithValue("@cuentaBeneficiaria", p.cuentaBeneficiaria);
                    cmd.Parameters.AddWithValue("@conceptoDePago", p.conceptoDePago);
                    cmd.Parameters.AddWithValue("@observaciones", p.observaciones);
                    cmd.Parameters.AddWithValue("@fechaDeReciboDePago", p.fechaDeReciboDePago);

                    result = (int)cmd.ExecuteScalar();
                    
                }              

                //Aqui actualizamos sus horarios - Primero borramos todos
                cmd = new SqlCommand(SP_NAME_inactivateAllByAlumnoId, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);

                result = (int)cmd.ExecuteScalar();

                foreach (var p in mensualidad.planHorario) 
                {
                    
                    foreach (var h in p.Horario)
                    {
                        int horarioId = h.horarioId;

                        cmd = new SqlCommand(SP_NAME_NuevoAlumnoPlanHorario, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);
                        cmd.Parameters.AddWithValue("@planIdFk", p.planId);
                        cmd.Parameters.AddWithValue("@horarioIdFk", horarioId);

                        result = (int)cmd.ExecuteScalar();
                    }

                }

                //Verificamos si se tiene que registrar el pago de inscripcion
                if (mensualidad.inscripcionImporteAnual > 0)
                {
                    cmd = new SqlCommand(SP_NAME_INSCRIPCION, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);
                    cmd.Parameters.AddWithValue("@inscripcionimporteAnual", mensualidad.inscripcionImporteAnual);
                    cmd.Parameters.AddWithValue("@inscripcionFechaDeVigencia", new DateTime(now.Year, now.Month, 1));
                    cmd.Parameters.AddWithValue("@PorcentajeDePago", mensualidad.mensualidadesPorcentajePago);
                    cmd.Parameters.AddWithValue("@cuentaBeneficiaria", mensualidad.cuentaBenefiaria);
                    cmd.Parameters.AddWithValue("@conceptoDePago", mensualidad.conceptoDePago);
                    cmd.Parameters.AddWithValue("@observaciones", mensualidad.observaciones);
                    cmd.Parameters.AddWithValue("@fechaDeReciboDePago", mensualidad.parcialidades[0].fechaDeReciboDePago);


                    result = (int)cmd.ExecuteScalar();

                }
                             
                //Mensualidades
                foreach (var p in mensualidad.planHorario) 
                {
                    cmd = new SqlCommand(SP_NAME_MENSUALIDAD, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);
                    cmd.Parameters.AddWithValue("@mensualidadImporte", p.planImporte);
                    cmd.Parameters.AddWithValue("@mensualidadFechaDePago", new DateTime(now.Year, now.Month, 1));
                    //cmd.Parameters.AddWithValue("@mensualidadFechaRealDePago", DateTime.Now);
                    cmd.Parameters.AddWithValue("@mensualidadFechaRealDePago", mensualidad.parcialidades[0].fechaDeReciboDePago);
                    cmd.Parameters.AddWithValue("@mensualidadConRetraso", mensualidad.mensualidadConRetraso);
                    cmd.Parameters.AddWithValue("@mensualidadImportePorRetraso", mensualidad.mensualidadImportePorRetraso);
                    cmd.Parameters.AddWithValue("@mensualidadPorcentajeDePago", mensualidad.mensualidadesPorcentajePago);
                    cmd.Parameters.AddWithValue("@planIdFk", p.planId);
                    cmd.Parameters.AddWithValue("@mensualidadSaldoAFavor", mensualidad.mensualidadSaldoAFavor);
                    cmd.Parameters.AddWithValue("@cuentaBeneficiaria", mensualidad.cuentaBenefiaria);
                    cmd.Parameters.AddWithValue("@conceptoDePago", mensualidad.conceptoDePago);
                    cmd.Parameters.AddWithValue("@observaciones", mensualidad.observaciones);
                    //cmd.Parameters.AddWithValue("@fechaDeReciboDePago", mensualidad.mensualidadesPorcentajePago);

                    result = (int)cmd.ExecuteScalar();

                }                
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Mensualidad mensualidad)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "MensualidadesUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mensualidadId", mensualidad.mensualidadId);
                cmd.Parameters.AddWithValue("@alumnoIdFk", mensualidad.alumnoIdFk);
                cmd.Parameters.AddWithValue("@mensualidadImporte", mensualidad.mensualidadImporte);
                cmd.Parameters.AddWithValue("@mensualidadFechaDePago", mensualidad.mensualidadFechaDePago);
                cmd.Parameters.AddWithValue("@mensualidadFechaRealDePago", mensualidad.mensualidadFechaRealDePago);
                cmd.Parameters.AddWithValue("@mensualidadConRetraso", mensualidad.mensualidadConRetraso);
                cmd.Parameters.AddWithValue("@mensualidadImportePorRetraso", mensualidad.mensualidadImportePorRetraso);
                cmd.Parameters.AddWithValue("@mensualidadesPorcentajePago", mensualidad.mensualidadesPorcentajePago);
                cmd.Parameters.AddWithValue("@mensualidadesActivo", mensualidad.mensualidadesActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Mensualidad mensualidad)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "MensualidadDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mensualidadId", mensualidad.mensualidadId);
                cmd.Parameters.AddWithValue("@mensualidadesActivo", mensualidad.mensualidadesActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }

        //PARCIALIDADES
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Parcialidades")]
        public JsonResult guadarParcialdiad(Parcialidad p)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "MensualidadDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", p.alumnoId);
                cmd.Parameters.AddWithValue("@parcialidadImporte", p.parcialidadImporte);
                cmd.Parameters.AddWithValue("@parcialidadMesCorrespondiente", p.parcialidadMesCorrespondiente);
                cmd.Parameters.AddWithValue("@TipoDePago", p.TipoDePago);
                cmd.Parameters.AddWithValue("@folioDePago", p.folioDePago);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }

    }
}
