﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class HorarioController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public HorarioController(IConfiguration configuration) 
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@horarioId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);               
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Disciplina/{id}")]
        public JsonResult GetHorarioByDisciplinaId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetByDisciplinaId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaIdFk", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Gimnasio/{id}")]
        public JsonResult GetHorarioByGimnasioId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GetHorarioByGimnasioId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@gimnasioId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }        


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Reposicion")]
        public JsonResult Reposicion(ReposicionDeClase reposicion)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "ReposicionDeClaseNuevo";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", reposicion.alumnoIdFk);
                cmd.Parameters.AddWithValue("@horarioIdFk", reposicion.horarioIdFk);
                cmd.Parameters.AddWithValue("@fechaDeReposicion", reposicion.fechaDeReposicion);
               
                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Horario horario)
        {
            int result = 0;
            
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;
            SP_NAME = "HorarioCreate";
           
            try
            {              
                con.Open();

                foreach (var dia in horario.horarioDias.Split(","))
                {
                    cmd = new SqlCommand(SP_NAME, con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@horarioNombre", horario.horarioNombre);
                    cmd.Parameters.AddWithValue("@disciplinaIdFk", horario.disciplinaIdFk);
                    cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
                    cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
                    cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);
                    cmd.Parameters.AddWithValue("@horarioInicio", horario.horarioInicio);
                    cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin);
                    cmd.Parameters.AddWithValue("@horarioDias", dia);
                    cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioTurno);
                    cmd.Parameters.AddWithValue("@horarioAforo", horario.horarioAforo);

                    result = (int)cmd.ExecuteScalar();
                }
                                                                            
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        //[EnableCors("AllowAnyOrigin")]
        //[HttpPost("Update")]
        //public JsonResult Update(Horario horario)
        //{
        //    int result = 0;
        //    SqlConnection con = new SqlConnection(CON_STR);
        //    string SP_NAME_UPDATE = "HorarioUpdate";
        //    SqlCommand cmd;

        //    DataTable dt = new DataTable();
        //    SqlDataReader rd;
        //    string SP_HORARIO_GET_GROUPED = "HorarioGetGrouped";

        //    var t1 = horario.horarioInicio.ToShortTimeString();
        //    var t2 = horario.horarioInicio.ToLocalTime();
        //    var t3 = horario.horarioInicio.ToString().Substring(11, 5);
        //    var t4 = horario.ToString().Substring(11, 5);

        //    try
        //    {
        //        //1.- OBETENEMOS LOS DÏAS QUE SE VAN A ELIMINAR




        //        //Obtenemos el número de registros que hay con ese horario
        //        cmd = new SqlCommand(SP_HORARIO_GET_GROUPED, con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@horarioId", horario.horarioId);
        //        cmd.Parameters.AddWithValue("@disciplinaIdFk", horario.disciplinaIdFk);
        //        cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
        //        cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
        //        cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);                
        //        cmd.Parameters.AddWithValue("@horarioInicio",horario.horarioInicio.ToString().Substring(11, 5));
        //        cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin.ToString().Substring(11, 5));

        //        con.Open();

        //        rd = cmd.ExecuteReader();
        //        dt.Load(rd);

        //        //Paso 1 : Insertamos todos los días que no estén en la base de datos
        //        foreach (var dia in horario.horarioDias.Split(","))
        //        {
        //            //Buscamos los nuevos dias en el result set
        //            if (dt.AsEnumerable().Any(row => dia == row.Field<String>("horarioDias")))
        //            {
        //                var HORARIO_ROW = dt
        //                                .AsEnumerable()
        //                                .Where(row => row.Field<String>("horarioDias") == dia);

        //                //Si existe solo actualizamos los demas datos
        //                cmd = new SqlCommand(SP_NAME_UPDATE, con);

        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@horarioId", horario.horarioId);
        //                cmd.Parameters.AddWithValue("@horarioNombre", horario.horarioNombre);
        //                cmd.Parameters.AddWithValue("@disciplinaIdFk", horario.disciplinaIdFk);
        //                cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
        //                cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
        //                cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);
        //                cmd.Parameters.AddWithValue("@horarioInicio", horario.horarioInicio);
        //                cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin);
        //                cmd.Parameters.AddWithValue("@horarioDias", dia);
        //                cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioTurno);
        //                cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioActivo);
        //                cmd.Parameters.AddWithValue("@horarioAforo", horario.horarioAforo);

        //                result = (int)cmd.ExecuteScalar();
        //            }
        //            else 
        //            {
        //                //SI NO EXISTE LO INSERTAMOS
        //                string SP_HORARIO_CREATE = "HorarioCreate";

        //                cmd = new SqlCommand(SP_HORARIO_CREATE, con);

        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@horarioNombre", horario.horarioNombre);
        //                cmd.Parameters.AddWithValue("@disciplinaIdFk", horario.disciplinaIdFk);
        //                cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
        //                cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
        //                cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);
        //                cmd.Parameters.AddWithValue("@horarioInicio", horario.horarioInicio);
        //                cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin);
        //                cmd.Parameters.AddWithValue("@horarioDias", dia);
        //                cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioTurno);
        //                cmd.Parameters.AddWithValue("@horarioAforo", horario.horarioAforo);

        //                result = (int)cmd.ExecuteScalar();
        //            }                   
        //        }

        //        foreach (var row in dt.Rows)
        //        {
        //            if (true) 
        //            { 

        //            }
        //        }

        //        //new SqlCommand(SP_NAME, con);

        //        //cmd.CommandType = CommandType.StoredProcedure;
        //        //cmd.Parameters.AddWithValue("@horarioId", horario.horarioId);
        //        //cmd.Parameters.AddWithValue("@horarioNombre", horario.horarioNombre);
        //        //cmd.Parameters.AddWithValue("@disciplinaIdFk", horario.disciplinaIdFk);
        //        //cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
        //        //cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
        //        //cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);
        //        //cmd.Parameters.AddWithValue("@horarioInicio", horario.horarioInicio);
        //        //cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin);
        //        //cmd.Parameters.AddWithValue("@horarioDias", horario.horarioDias);
        //        //cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioTurno);
        //        //cmd.Parameters.AddWithValue("@horarioActivo", horario.horarioActivo);
        //        //cmd.Parameters.AddWithValue("@horarioAforo", horario.horarioAforo);

        //        //result = (int)cmd.ExecuteScalar();

        //    }
        //    catch (Exception ex)
        //    {
        //        return new JsonResult(ex.Message);
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }

        //    return new JsonResult(result);
        //}
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Horario horario)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_UPDATE = "HorarioUpdate";
            SqlCommand cmd;

            
            var t1 = horario.horarioInicio.ToShortTimeString();
            var t2 = horario.horarioInicio.ToLocalTime();
            var t3 = horario.horarioInicio.ToString().Substring(11, 5);
            var t4 = horario.ToString().Substring(11, 5);

            try
            {
                cmd = new SqlCommand(SP_NAME_UPDATE, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@horarioId",horario.horarioId);
                cmd.Parameters.AddWithValue("@horarioNombre",horario.horarioNombre);
                cmd.Parameters.AddWithValue("@disciplinaIdFk",horario.disciplinaIdFk);
                cmd.Parameters.AddWithValue("@categoriaIdFk", horario.categoriaIdFk);
                cmd.Parameters.AddWithValue("@gimnasioIdFk", horario.gimnasioIdFk);
                cmd.Parameters.AddWithValue("@instructorIdFk", horario.instructorIdFk);
                cmd.Parameters.AddWithValue("@horarioInicio", horario.horarioInicio);
                cmd.Parameters.AddWithValue("@horarioFin", horario.horarioFin);
                cmd.Parameters.AddWithValue("@horarioDias", horario.horarioDias);
                cmd.Parameters.AddWithValue("@horarioTurno", horario.horarioTurno);
                cmd.Parameters.AddWithValue("@horarioActivo", horario.horarioActivo);
                cmd.Parameters.AddWithValue("@horarioAforo", horario.horarioAforo);

                con.Open();

                result = (int)cmd.ExecuteScalar();


            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Horario horario)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "HorarioDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@horarioId", horario.horarioId);
                cmd.Parameters.AddWithValue("@horarioActivo", horario.horarioActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("HorarioDisciplina/")]
        public JsonResult GetByDisciplina(int id, int edad, string sexo)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioGetByDisciplinayAforo";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaIdFk", id);
                cmd.Parameters.AddWithValue("@edad", edad);
                cmd.Parameters.AddWithValue("@sexo", sexo);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }



    }
}
