using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportesController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;
        public ReportesController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet ("AlumnoByFecha")]
        public JsonResult Permisos(DateTime R1 , DateTime R2)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "reportesAlumnos";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fechaRango1", R1);
                cmd.Parameters.AddWithValue("@fechaRango2", R2);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ReporteEntradaSalida")]
        public JsonResult ReporteEntradaSalida(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteDeEntradaSalida";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaIncio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);
                

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("PagosPorAlumno")]
        public JsonResult PagosPorAlumno(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "PagosByAlumnoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaSalida", r.fechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("AlumnosPorDisciplina")]
        public JsonResult AlumnosPorDisciplina(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReportePorUsuariosPorDisciplina";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", r.alumnoIdFk);              

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("InscritosPorDisciplina")]
        public JsonResult InscritosPorDisciplina(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteinscritosPorMesPorDisciplina";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);
                
                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("AforoPorDisciplina")]
        public JsonResult AforoPorDisciplina(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteAforoPordisciplina";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", r.alumnoIdFk);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("InscritosMensualesPorDisciplina")]
        public JsonResult InscritosMensualesPorDisciplina(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteinscritosPorMesPorDisciplina";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("AsistenciaDeProfesores")]
        public JsonResult AsistenciaDeProfesores(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteAsistenciaDeProfesores";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ReporteReposicionDeClase")]
        public JsonResult ReporteReposicionDeClase(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ReporteReposicionDeClasePorAlumno";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", r.alumnoIdFk);
                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ReporteFinanciero")]
        public JsonResult ReporteFinanciero(ReporteFinanciero r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            var fa1 = r.fi.ToString("yyyy-MM-dd");
            var fa2 = r.ff.ToString("yyyy-MM-dd");

            SP_NAME = "ReporteFinanciero";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", r.dId);
                cmd.Parameters.AddWithValue("@fechaInicial", fa1);
                cmd.Parameters.AddWithValue("@fechaFinal", fa2);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

    }
}
