﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class QrMaestroController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public QrMaestroController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("GenerarQrMaestro/{id}")]
        public JsonResult GenerarQrMaestro(string id)
        {
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            int result = 0;
            SP_NAME = "QrMaestroCreate";

            try
            {
                con.Open();
               
                cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@qrString", id);

                
                result = (int)cmd.ExecuteScalar();
              
            }
            catch (Exception ex)
            {

                return new JsonResult("Error: " + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }
    }
}
