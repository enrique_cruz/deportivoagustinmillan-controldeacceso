﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class AlumnoPlanHorarioController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public AlumnoPlanHorarioController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoPlanHorarioAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoPlanHorarioActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoPlanHorarioGetAllInactve";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Alumno/{id}")]
        public JsonResult GetByAlumnoId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoPlanHorarioGetByAlumnoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Create")]
        public JsonResult create(List<AlumnoPlanHorario> alumnoPlanHorario)
        {
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;
            int result = 0;

            string SP_NAME_AlumnoPlanHorarioCreate = "AlumnoPlanHorarioCreate";            

            try
            {

                con.Open();

                foreach (var aph in alumnoPlanHorario)
                {
                    cmd = new SqlCommand(SP_NAME_AlumnoPlanHorarioCreate, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (aph.actual == 0) 
                    {
                        foreach (var h in aph.horario)
                        {
                            cmd = new SqlCommand(SP_NAME_AlumnoPlanHorarioCreate, con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@alumnoIdFk", aph.alumnoId);
                            cmd.Parameters.AddWithValue("@planIdFk", aph.planid);
                            cmd.Parameters.AddWithValue("@horarioIdFk", h.horarioId);

                            result = (int)cmd.ExecuteScalar();

                            if (result == 0) 
                            {
                                return new JsonResult(result);
                            }
                        }
                    }                                     
                }
                                                           
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("RecreateHorario")]
        public JsonResult recreateHorarioByAlumnoId(List<AlumnoPLanHorarioRecreado> ph)
        {            
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;
            int result = 0;

            string SP_NAME_inactivateAllByAlumnoId = "AlumnoPlanHorario_InactivateAllByAlumnoIdFk";
            string SP_NAME_NuevoAlumnoPlanHorario = "AlumnoPlanHorarioCreate";

            try
            {
                cmd = new SqlCommand(SP_NAME_inactivateAllByAlumnoId, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoIdFk", ph[0].alumnoId);

                con.Open();

                result = (int)cmd.ExecuteScalar();

                foreach (var p in ph) 
                {
                    foreach (var h in p.horario)
                    {

                        cmd = new SqlCommand(SP_NAME_NuevoAlumnoPlanHorario, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@alumnoIdFk", p.alumnoId);
                        cmd.Parameters.AddWithValue("@planIdFk", p.planId);
                        cmd.Parameters.AddWithValue("horarioIdFk", h.horarioId);

                        result = (int)cmd.ExecuteScalar();

                    }

                }               

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(List<AlumnoPlanHorario> aph)
        {
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;
            int result = 0;

            string SP_NAME_AlumnoPlanHorarioDeleteByAlumnoIdPlanIdHorarioId = "AlumnoPlanHorarioDeleteByAlumnoIdPlanIdHorarioId";
            
            try
            {
                con.Open();

                foreach (var h in aph)
                {
                    cmd = new SqlCommand(SP_NAME_AlumnoPlanHorarioDeleteByAlumnoIdPlanIdHorarioId, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@alumnoIdFk", h.alumnoId);
                    cmd.Parameters.AddWithValue("@planIdFk", h.planid);
                    cmd.Parameters.AddWithValue("@horarioIdFk", h.horarioId);

                    result = (int)cmd.ExecuteScalar();
                }                                                                            
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }
    }
}
