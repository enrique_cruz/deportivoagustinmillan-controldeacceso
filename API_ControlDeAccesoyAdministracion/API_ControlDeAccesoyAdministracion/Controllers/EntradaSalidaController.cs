﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_ControlDeAccesoyAdministracion.Utilerias;
using API_ControlDeAccesoyAdministracion.Modelo;
using API_ControlDeAccesoyAdministracion.Models;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class EntradaSalidaController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public EntradaSalidaController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("TodayAll")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EntradaSalidaTodayAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("RegistrarEntrada")]
        public JsonResult RegistrarEntrada(string criptedString)
        {
            int result = 0;
            //var val = "POEURNUSJDH99-10/10/2021";
            string tipoDeUsuario = criptedString.Split("-")[0];
            string spltedCurp = criptedString.Split("-")[1];
            string mensaje = "";

            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;
            DataTable dt = new DataTable();
            SqlDataReader rd;

            string SP_NAME_ENTRADA_INSTRUCTOR = "InstructorRegistrarEntrada";           
            SP_NAME = "EntradaSalidaCreate";

            try
            {
                con.Open();

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("A"))
                {
                    cmd = new SqlCommand(SP_NAME, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                   

                    result = (int)cmd.ExecuteScalar();

                    string SP_NAME_GET_ALUMNO = "GetAlumnoByCurp";


                    cmd = new SqlCommand(SP_NAME_GET_ALUMNO, con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    string path = dt.Rows[0].ItemArray[9].ToString();
                    string curp = spltedCurp;
                    string base64Image = Imagen.ImageToBase64(curp);

                    DataColumn nuevaColumna = new DataColumn("alumnoBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);

                    if (result == 1)
                    {
                        mensaje = "Success";
                    }

                    if (result == 2)
                    {
                        mensaje = "Anualidad vencida";
                        //return new JsonResult("Anualidad vencida"); 
                    }

                    if (result == 3)
                    {
                        mensaje = "Mensualidad vencida";
                        //return new JsonResult("Mensualidad vencida");
                    }

                    if (result == 4)
                    {
                        mensaje = "No ha registrado salida";
                        //return new JsonResult("No ha registrado salida");
                    }

                    if (result == 5)
                    {
                        mensaje = "Horario no válido.";
                    }

                    if (result == 0)
                    {
                        mensaje = "Ocurrío un error";
                        //return new JsonResult("Ocurrío un error");
                    }

                    nuevaColumna = new DataColumn("Estatus", typeof(System.String));
                    nuevaColumna.DefaultValue = mensaje;
                    dt.Columns.Add(nuevaColumna);
                }

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("I")) 
                {
                    cmd = new SqlCommand(SP_NAME_ENTRADA_INSTRUCTOR, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                    
                    result = (int)cmd.ExecuteScalar();

                    string SP_NAME_GET_InstructorByCurp = "InstructorGetByCurp";


                    cmd = new SqlCommand(SP_NAME_GET_InstructorByCurp, con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    string path = dt.Rows[0].ItemArray[9].ToString();
                    string curp = spltedCurp;
                    string base64Image = Imagen.ImageToBase64(curp);

                    DataColumn nuevaColumna = new DataColumn("instructorBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);

                    if (result == 1)
                    {
                        mensaje = "Success";
                    }

                    if (result == 6)
                    {
                        mensaje = "Primero debe registrar su salida.";
                    }

                    nuevaColumna = new DataColumn("Estatus", typeof(System.String));
                    nuevaColumna.DefaultValue = mensaje;
                    dt.Columns.Add(nuevaColumna);

                }

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("M"))
                {
                    string SP_QrGetCurrentActive = "QrGetCurrentActive";
                    string SP_QrMaestroInsertarEntrada = "QrMaestroInsertarEntrada";

                    cmd = new SqlCommand(SP_QrGetCurrentActive, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@qrString", criptedString);

                    result = (int)cmd.ExecuteScalar();

                    if (result == 1)
                    {
                        var res = 0;
                        cmd = new SqlCommand(SP_QrMaestroInsertarEntrada, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        res = (int)cmd.ExecuteScalar();

                        if (res > 0)
                        {                  

                            mensaje = "Success";
                        }
                        else
                        {
                            mensaje = "QR Maestro no ha registrado salida";
                        }
                        
                    }
                    else 
                    {
                        mensaje = "QR MAESTRO NO VALIDO";
                    }



                    dt.Columns.Add("Estatus");

                    DataRow r = dt.NewRow();
                    r["Estatus"] = mensaje;
                    dt.Rows.Add(r);

                    //dt.Columns.Add("alumnoBase64ImageString");

                    string base64Image = Imagen.ImageToBase64("AGUSTINMILLANVIVER");

                    DataColumn nuevaColumna = new DataColumn("alumnoBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);

                }


            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("RegistrarSalida")]
        public JsonResult RegistrarSalida(string criptedString)
        {
            int result = 0;
            //var val = "POEURNUSJDH99-10/10/2021"; A-TORH950408HMCVMG92-2021-10-29T20:55:45.589Z

            string tipoDeUsuario = criptedString.Split("-")[0];
            string spltedCurp = criptedString.Split("-")[1];
            string mensaje = "";

            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            DataTable dt = new DataTable();
            SqlDataReader rd;

            SP_NAME = "EntradaSalidaRegistrarSalida";
            string SP_NAME_SALIDA_INSTRUCTOR = "InstructorRegistrarSalida";


            try
            {
                con.Open();

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("A")) 
                {
                    cmd = new SqlCommand(SP_NAME, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                    result = (int)cmd.ExecuteScalar();


                    dt.Columns.Add("Estatus");

                    DataRow r = dt.NewRow();
                    r["Estatus"] = result;
                    dt.Rows.Add(r);

                    //dt.Columns.Add("alumnoBase64ImageString");

                    string base64Image = Imagen.ImageToBase64(spltedCurp);

                    DataColumn nuevaColumna = new DataColumn("alumnoBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);

                }

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("I")) 
                {                   
                    cmd = new SqlCommand(SP_NAME_SALIDA_INSTRUCTOR, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", spltedCurp);

                    result = (int)cmd.ExecuteScalar();



                    dt.Columns.Add("Estatus");

                    DataRow r = dt.NewRow();
                    r["Estatus"] = result;
                    dt.Rows.Add(r);

                    string base64Image = Imagen.ImageToBase64(spltedCurp);

                    DataColumn nuevaColumna = new DataColumn("alumnoBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);

                }

                if (tipoDeUsuario != "" && tipoDeUsuario.Equals("M"))
                {
                    cmd = new SqlCommand(SP_NAME, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CURP", "AGUSTINMILLANVIVER");

                    result = (int)cmd.ExecuteScalar();


                    dt.Columns.Add("Estatus");

                    DataRow r = dt.NewRow();
                    r["Estatus"] = result;
                    dt.Rows.Add(r);

                    string base64Image = Imagen.ImageToBase64("AGUSTINMILLANVIVER");

                    DataColumn nuevaColumna = new DataColumn("alumnoBase64ImageString", typeof(System.String));
                    nuevaColumna.DefaultValue = base64Image;
                    dt.Columns.Add(nuevaColumna);
                }
                                                            
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("SearchBy/{pattern}")]
        public JsonResult alumnoSearchBy(string pattern)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoTutorSearchBy";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pattern", pattern);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("EntradaSalidadTodayByusrDiciplinaFechaInicioFin")]
        public JsonResult GetAllEntradas(ReporteEntradaSalida r)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EntradaSalidaTodaySearchByUsrDisciplinaFechaInicioFin";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@fechaInicio", r.fechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", r.fechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("EntradaSalidaSearchByCURP")]
        public JsonResult EntradaSalidaSearchByCURP(EntradaSalida es)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EntradaSalidaTodayAllSearchByCurpAndDates";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CURP", es.CURP);
                cmd.Parameters.AddWithValue("@fechaInicio", es.entradasalidaFechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", es.entradasalidaFechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }



        [EnableCors("AllowAnyOrigin")]
        [HttpPost("EntradaSalidaSearchByDisciplina")]
        public JsonResult EntradaSalidaSearchByDisciplina(EntradaSalida es)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EntradaSalidaTodayAllSearchByDisciplinaAndDates";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@disciplinaId", es.disciplinaId);
                cmd.Parameters.AddWithValue("@fechaInicio", es.entradasalidaFechaEntrada);
                cmd.Parameters.AddWithValue("@fechaFin", es.entradasalidaFechaSalida);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


    }
}
