﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using API_ControlDeAccesoyAdministracion.Utilerias;
using static System.Net.Mime.MediaTypeNames;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnoController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public AlumnoController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }
       

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Get/{id}")]
        public JsonResult GetAlumnoById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
                string path = dt.Rows[0].ItemArray[9].ToString();
                string curp = dt.Rows[0].ItemArray[4].ToString();
                string base64Image = Imagen.ImageToBase64(curp);

                DataColumn nuevaColumna = new DataColumn("alumnoBase64ImgaeString", typeof(System.String));
                nuevaColumna.DefaultValue = base64Image;
                dt.Columns.Add(nuevaColumna);                

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("CURP/{curp}")]
        public JsonResult GetByCurp(string curp)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GetAlumnoByCurp";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CURP", curp);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
                string path = dt.Rows[0].ItemArray[9].ToString();
                //string curp = curp;
                string base64Image = Imagen.ImageToBase64(curp);

                DataColumn nuevaColumna = new DataColumn("alumnoBase64ImgaeString", typeof(System.String));
                nuevaColumna.DefaultValue = base64Image;
                dt.Columns.Add(nuevaColumna);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Search/{pattern}")]
        public JsonResult alumnoSearchBy(string pattern)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "AlumnoSearchBy";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pattern", pattern);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("VerificaCurp")]
        public JsonResult Delete(string curp)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "ConsultaCurp";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@curp", curp);
                
                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Alumno alumno)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "AlumnoCreate";

            string remotePath = "172.16.0.37/imagenes/"+ alumno.alumnoCURP;

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoNombre", alumno.alumnoNombre);
                cmd.Parameters.AddWithValue("@alumnoApellidoPaterno", alumno.alumnoApellidoPaterno);
                cmd.Parameters.AddWithValue("@alumnoApellidoMaterno", alumno.alumnoApellidoMaterno);
                cmd.Parameters.AddWithValue("@alumnoCURP", alumno.alumnoCURP);
                cmd.Parameters.AddWithValue("@alumnoFechaDeNacimiento", alumno.alumnoFechaDeNacimiento);
                cmd.Parameters.AddWithValue("@alumnoSexo", alumno.alumnoSexo);
                cmd.Parameters.AddWithValue("@alumnoEstatura", alumno.alumnoEstatura);
                cmd.Parameters.AddWithValue("@alumnoPeso", alumno.alumnoPeso);
                cmd.Parameters.AddWithValue("@alumnoRutaFoto", remotePath);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCasa", alumno.alumnoTelefonoCasa);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCelular", alumno.alumnoTelefonoCelular);
                cmd.Parameters.AddWithValue("@alumnoCorreoElectronico", alumno.alumnoCorreoElectronico);
                cmd.Parameters.AddWithValue("@alumnoRedesSociales", alumno.alumnoRedesSociales);
                cmd.Parameters.AddWithValue("@alumnoPassword", alumno.alumnoPassword);
                cmd.Parameters.AddWithValue("@alumnoCodigoPostal", alumno.alumnoCodigoPostal);
                cmd.Parameters.AddWithValue("@alumnoEstado", alumno.alumnoEstado);
                cmd.Parameters.AddWithValue("@alumnoMunicipio", alumno.alumnoMunicipio);
                cmd.Parameters.AddWithValue("@alumnoColonia", alumno.alumnoColonia);
                cmd.Parameters.AddWithValue("@alumnoCalle", alumno.alumnoCalle);
                cmd.Parameters.AddWithValue("@alumnoNumero", alumno.alumnoNumero);
                cmd.Parameters.AddWithValue("@alumnoAfiliacionMedica", alumno.alumnoAfiliacionMedica);
                cmd.Parameters.AddWithValue("@alumnoAlergias", alumno.alumnoAlergias);
                cmd.Parameters.AddWithValue("@alumnoPadecimientos", alumno.alumnoPadecimientos);
                cmd.Parameters.AddWithValue("@alumnoTipoDeSangre", alumno.alumnoTipoDeSangre);
                cmd.Parameters.AddWithValue("@alumnoFolio", alumno.alumnoTipoDeSangre);

                con.Open();
                result = (int)cmd.ExecuteScalar();

                string path = @"C:\imagenes";

                if (Directory.Exists(path) && result==1)
                {
                    Imagen.Base64ToImage(alumno.alumnoBase64Image, alumno.alumnoCURP, path);
                }
                else if(result==1)
                {
                    //Si no existe el directorio lo creamos
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    Imagen.Base64ToImage(alumno.alumnoBase64Image, alumno.alumnoCURP, path);
                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Alumno alumno)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "AlumnoUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", alumno.alumnoId);
                cmd.Parameters.AddWithValue("@alumnoNombre", alumno.alumnoNombre);
                cmd.Parameters.AddWithValue("@alumnoApellidoPaterno", alumno.alumnoApellidoPaterno);
                cmd.Parameters.AddWithValue("@alumnoApellidoMaterno", alumno.alumnoApellidoMaterno);
                cmd.Parameters.AddWithValue("@alumnoCURP", alumno.alumnoCURP);
                cmd.Parameters.AddWithValue("@alumnoFechaDeNacimiento", alumno.alumnoFechaDeNacimiento);
                cmd.Parameters.AddWithValue("@alumnoSexo", alumno.alumnoSexo);
                cmd.Parameters.AddWithValue("@alumnoEstatura", alumno.alumnoEstatura);
                cmd.Parameters.AddWithValue("@alumnoPeso", alumno.alumnoPeso);
                cmd.Parameters.AddWithValue("@alumnoRutaFoto", alumno.alumnoRutaFoto);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCasa", alumno.alumnoTelefonoCasa);
                cmd.Parameters.AddWithValue("@alumnoTelefonoCelular", alumno.alumnoTelefonoCelular);
                cmd.Parameters.AddWithValue("@alumnoCorreoElectronico", alumno.alumnoCorreoElectronico);
                cmd.Parameters.AddWithValue("@alumnoRedesSociales", alumno.alumnoRedesSociales);
                cmd.Parameters.AddWithValue("@alumnoPassword", alumno.alumnoPassword);
                cmd.Parameters.AddWithValue("@alumnoCodigoPostal", alumno.alumnoCodigoPostal);
                cmd.Parameters.AddWithValue("@alumnoEstado", alumno.alumnoEstado);
                cmd.Parameters.AddWithValue("@alumnoMunicipio", alumno.alumnoMunicipio);
                cmd.Parameters.AddWithValue("@alumnoColonia", alumno.alumnoColonia);
                cmd.Parameters.AddWithValue("@alumnoCalle", alumno.alumnoCalle);
                cmd.Parameters.AddWithValue("@alumnoNumero", alumno.alumnoNumero);
                cmd.Parameters.AddWithValue("@alumnoAfiliacionMedica", alumno.alumnoAfiliacionMedica);
                cmd.Parameters.AddWithValue("@alumnoAlergias", alumno.alumnoAlergias);
                cmd.Parameters.AddWithValue("@alumnoPadecimientos", alumno.alumnoPadecimientos);
                cmd.Parameters.AddWithValue("@alumnoTipoDeSangre", alumno.alumnoTipoDeSangre);
                cmd.Parameters.AddWithValue("@alumnoActivo", alumno.alumnoActivo);
                cmd.Parameters.AddWithValue("@alumnoFolio", alumno.alumnoFolio);

                con.Open();
                result = (int)cmd.ExecuteScalar();

                string path = @"C:\imagenes";                

                if (Directory.Exists(path) && result == 1)
                {
                    Imagen.Base64ToImage(alumno.alumnoBase64Image, alumno.alumnoCURP, path);
                }
                else if (result == 1)
                {
                    //Si no existe el directorio lo creamos
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    Imagen.Base64ToImage(alumno.alumnoBase64Image, alumno.alumnoCURP, path);
                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Alumno alumno)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "AlumnoDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", alumno.alumnoId);
                cmd.Parameters.AddWithValue("@alumnoActivo", alumno.alumnoActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("PrimeroCadaMes")]
        public JsonResult getPrimeroCadaMes( )
        {

            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);


            return new JsonResult(startDate);

        }

    }
}
