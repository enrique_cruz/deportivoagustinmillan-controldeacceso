﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.AspNetCore.Cors;
using System.Data;
using API_ControlDeAccesoyAdministracion.Modelo;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public UsuarioController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        //LOGIN
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Auth")]
        public JsonResult Login(Usuario usuario)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuarioLogin";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuarioCorreoElectronico", usuario.usuarioCorreoElectronico);
                cmd.Parameters.AddWithValue("@usuarioPassword", usuario.usuarioPassword);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuariosGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

            //return new JsonResult("Esta vivo!");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuariosGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

            //return new JsonResult("Esta vivo!");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "UsuariosGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);

            //return new JsonResult("Esta vivo!");
        }


        //NUEVO USUARIO
        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult NuevoUsuario(Usuario usuario)
        {

            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "UsuarioCreate";
            string SP_NAME_UPDATE = "UsuarioUpdate";
            int result = 0;
           
            try
            {
                if (usuario.usuarioId == 0)
                {
                    SqlCommand cmd = new SqlCommand(SP_NAME, con);

                    cmd.CommandType = CommandType.StoredProcedure;                    
                    cmd.Parameters.AddWithValue("@usuarioNombre", usuario.usuarioNombre);
                    cmd.Parameters.AddWithValue("@usuarioApellidoPaterno", usuario.usuarioApellidoPaterno);
                    cmd.Parameters.AddWithValue("@usuarioApellidoMaterno", usuario.usuarioApellidoMaterno);
                    cmd.Parameters.AddWithValue("@usuarioCorreoElectronico", usuario.usuarioCorreoElectronico);
                    cmd.Parameters.AddWithValue("@usuarioPassword", usuario.usuarioPassword);
                    cmd.Parameters.AddWithValue("@rolIdFk", usuario.rolRolIdFk);

                    con.Open();
                    result = (int)cmd.ExecuteScalar();

                }
                else 
                {
                    SqlCommand cmd = new SqlCommand(SP_NAME_UPDATE, con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@usuarioId", usuario.usuarioId);
                    cmd.Parameters.AddWithValue("@usuarioNombre", usuario.usuarioNombre);
                    cmd.Parameters.AddWithValue("@usuarioApellidoPaterno", usuario.usuarioApellidoPaterno);
                    cmd.Parameters.AddWithValue("@usuarioApellidoMaterno", usuario.usuarioApellidoMaterno);
                    cmd.Parameters.AddWithValue("@usuarioCorreoElectronico", usuario.usuarioCorreoElectronico);
                    cmd.Parameters.AddWithValue("@usuarioPassword", usuario.usuarioPassword);
                    cmd.Parameters.AddWithValue("@rolIdFk", usuario.rolRolIdFk);

                    con.Open();

                    result = (int)cmd.ExecuteScalar();

                }
             
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("UpdateStatus")]
        public JsonResult UpdateStatus(Usuario usuario) 
        {
            int result = 0;
            SP_NAME = "UsuarioDelete";

            SqlConnection con = new SqlConnection(CON_STR);

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuarioId", usuario.usuarioId);
                cmd.Parameters.AddWithValue("@usuarioActivo", usuario.usuarioActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {

                return new JsonResult(result);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }
    }
}
