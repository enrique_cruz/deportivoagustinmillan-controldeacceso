﻿using API_ControlDeAccesoyAdministracion.Modelo;
using API_ControlDeAccesoyAdministracion.Utilerias;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorAlumnoController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;
        public TutorAlumnoController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorAlumnoGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorAlumnoAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorAlumnoGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("GetTutoresByAlumnoId/{Id}")]
        public JsonResult GetTutorByAlumnoId(int Id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorAlumnoGetByAlumnoId";

            try
            {

                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@alumnoId", Id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(TutorAlumno ta)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_Tutor = "TutorCreate";
            SP_NAME = "TutorAlumnoCreate";
            int idTutor = 0;

            SqlCommand cmd;

            try
            {
                con.Open();
                
                foreach (var t in ta.tutor)
                {
                    cmd = new SqlCommand(SP_NAME_Tutor, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tutorNombre", t.tutorNombre);
                    cmd.Parameters.AddWithValue("@tutorApellidoPAterno", t.tutorApellidoPaterno);
                    cmd.Parameters.AddWithValue("@tutorApellidoMaterno", t.tutorApellidoMaterno);
                    cmd.Parameters.AddWithValue("@tutorParentesco", t.tutorParentesco);
                    cmd.Parameters.AddWithValue("@tutorTelefonoCasa", t.tutorTelefonoCasa);
                    cmd.Parameters.AddWithValue("@tutortelefonoCelular", t.tutorTelefonoCelular);
                    cmd.Parameters.AddWithValue("@tutorCorreoElectronico", t.tutorCorreoElectronico);

                    idTutor = (int)cmd.ExecuteScalar();


                    if (idTutor > 0)
                    {
                        string path = @"C:\imagenes";

                        if (Directory.Exists(path) && idTutor >0)
                        {
                            Imagen.Base64ToImage(t.tutorBase64Image, idTutor.ToString(), path);
                        }
                        else if (idTutor > 0)
                        {
                            //Si no existe el directorio lo creamos
                            DirectoryInfo di = Directory.CreateDirectory(path);
                            Imagen.Base64ToImage(t.tutorBase64Image, idTutor.ToString(), path);
                        }
                    }


                    if (idTutor > 0) 
                    {
                        cmd = new SqlCommand(SP_NAME, con);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@tutorIdFk", idTutor);
                        cmd.Parameters.AddWithValue("@alumnoIdFk", ta.alumnoIdFk);

                        result = (int)cmd.ExecuteScalar();
                    }

                }
                                                                            
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(TutorAlumno ta)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "TutorAlumnoUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutorAlumnoId", ta.tutoralumnoId);
                cmd.Parameters.AddWithValue("@tutorIdFk", ta.tutorIdFk);
                cmd.Parameters.AddWithValue("@alumnoIdFk", ta.alumnoIdFk);                
                cmd.Parameters.AddWithValue("@tutoralumnoActivo", ta.tutoralumnoActivo);
               

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(TutorAlumno al)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "TutorAlumnoDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutoralumnoId", al.tutoralumnoId);
                cmd.Parameters.AddWithValue("@tutoralumnoActivo", al.tutoralumnoActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }
    }
}
