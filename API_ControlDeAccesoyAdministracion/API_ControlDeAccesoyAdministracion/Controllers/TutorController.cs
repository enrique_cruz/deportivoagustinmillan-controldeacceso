﻿using API_ControlDeAccesoyAdministracion.Modelo;
using API_ControlDeAccesoyAdministracion.Utilerias;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorController : Controller
    {
        
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;

        public TutorController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{Id}")]
        public JsonResult GetTutorById(int Id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "TutorGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutorId", Id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

                string base64Image = Imagen.ImageToBase64(Id.ToString());

                DataColumn nuevaColumna = new DataColumn("tutorBase64Image", typeof(System.String));
                nuevaColumna.DefaultValue = base64Image;
                dt.Columns.Add(nuevaColumna);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Tutor tutor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "TutorCreate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutorNombre", tutor.tutorNombre);
                cmd.Parameters.AddWithValue("@tutorApellidoPAterno", tutor.tutorApellidoPaterno);
                cmd.Parameters.AddWithValue("@tutorApellidoMaterno", tutor.tutorApellidoMaterno);
                cmd.Parameters.AddWithValue("@tutorParentesco", tutor.tutorParentesco);
                cmd.Parameters.AddWithValue("@tutorTelefonoCasa", tutor.tutorTelefonoCasa);
                cmd.Parameters.AddWithValue("@tutortelefonoCelular", tutor.tutorTelefonoCelular);
                cmd.Parameters.AddWithValue("@tutorCorreoElectronico", tutor.tutorCorreoElectronico);
                
                con.Open();
                result = (int)cmd.ExecuteScalar();


                if (result > 0)
                {
                    string path = @"C:\imagenes";

                    if (Directory.Exists(path) && result == 1)
                    {
                        Imagen.Base64ToImage(tutor.tutorBase64Image, tutor.tutorId.ToString(), path);
                    }
                    else if (result == 1)
                    {
                        //Si no existe el directorio lo creamos
                        DirectoryInfo di = Directory.CreateDirectory(path);
                        Imagen.Base64ToImage(tutor.tutorBase64Image, tutor.tutorId.ToString(), path);
                    }
                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Tutor tutor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "TutorUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutorId", tutor.tutorId);
                cmd.Parameters.AddWithValue("@tutorNombre", tutor.tutorNombre);
                cmd.Parameters.AddWithValue("@tutorApellidoPAterno", tutor.tutorApellidoPaterno);
                cmd.Parameters.AddWithValue("@tutorApellidoMaterno", tutor.tutorApellidoMaterno);
                cmd.Parameters.AddWithValue("@tutorParentesco", tutor.tutorParentesco);
                cmd.Parameters.AddWithValue("@tutorTelefonoCasa", tutor.tutorTelefonoCasa);
                cmd.Parameters.AddWithValue("@tutortelefonoCelular", tutor.tutorTelefonoCelular);
                cmd.Parameters.AddWithValue("@tutorCorreoElectronico", tutor.tutorCorreoElectronico);
                cmd.Parameters.AddWithValue("@tutorActivo", tutor.tutorActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();



                if (result > 0)
                {
                    string path = @"C:\imagenes";

                    if (Directory.Exists(path) && result == 1)
                    {
                        Imagen.Base64ToImage(tutor.tutorBase64Image, tutor.tutorId.ToString(), path);
                    }
                    else if (result == 1)
                    {
                        //Si no existe el directorio lo creamos
                        DirectoryInfo di = Directory.CreateDirectory(path);
                        Imagen.Base64ToImage(tutor.tutorBase64Image, tutor.tutorId.ToString(), path);
                    }
                }


            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Tutor tutor)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "TutorDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tutorId", tutor.tutorId);
                cmd.Parameters.AddWithValue("@tutorActivo", tutor.tutorActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }
    }
}
