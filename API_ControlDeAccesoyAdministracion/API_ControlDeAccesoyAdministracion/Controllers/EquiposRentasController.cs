﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class EquiposRentasController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;
        public EquiposRentasController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Equipo/{strBusqueda}")]
        public JsonResult EquipoSearchBy(string strBusqueda)
        {
            DataTable dt = new DataTable();


            SqlCommand cmd;
            SqlDataReader rd;

            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_EquipoSearchBy = "EquipoSearchBy";

            try
            {
                con.Open();

                cmd = new SqlCommand(SP_NAME_EquipoSearchBy, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@strBusqueda", @strBusqueda);

                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Equipo/{id}/Integrante/{strBusqueda}")]
        public JsonResult IntegranteEquipoSearchBy(int id, string strBusqueda)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd;
            SqlDataReader rd;
            
            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_IntegrantesEquipoSearchBy = "IntegrantesEquipoSearchBy";

            try
            {
                con.Open();

                cmd = new SqlCommand(SP_NAME_IntegrantesEquipoSearchBy, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", id);
                cmd.Parameters.AddWithValue("@strBusqueda", strBusqueda);

                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
           

            SqlCommand cmd;
            SqlDataReader rd;
          
                ;
            SqlConnection con = new SqlConnection(CON_STR);
            string SP_NAME_Equipos_GET_ALL = "Equipos_GET_ALL";
           
            try
            {
                con.Open();

                cmd = new SqlCommand(SP_NAME_Equipos_GET_ALL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                rd = cmd.ExecuteReader();
                dt.Load(rd);
           
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();

            SqlCommand cmd;
            SqlDataReader rd;
            SqlDataReader rd2;
            SqlDataReader rd3
                ;
            SqlConnection con = new SqlConnection(CON_STR);

            HorarioEquipo he = new HorarioEquipo();
            Integrantes i;
            Equipos e;
            DiaHorario dh;


            List<DiaHorario> DiaHorarioLst = new List<DiaHorario>();
            List<Integrantes> integrantesLst = new List<Integrantes>();

            List<HorarioEquipo> horarioEquiposList= new List<HorarioEquipo>();
            int equipoId;

            string SP_NAME_GetEquiposDistinct = "GetEquiposDistinct";
            string SP_NAME_GetEquipoIntegrantes = "GetEquipoIntegrantes";
            string SP_NAME_GetHorarioEquipoByEquipoId = "GetHorarioEquipoByEquipoId";

            try
            {
                con.Open();

                cmd = new SqlCommand(SP_NAME_GetEquiposDistinct, con);
                cmd.CommandType = CommandType.StoredProcedure;

                rd = cmd.ExecuteReader();
                dt.Load(rd);

                if (dt.Rows.Count > 0) 
                {
                    
                    // POR CADA EQUIPO OBTENEMOS SU HORARIO Y SUS INTEGRANTES
                    foreach (DataRow r in dt.Rows) 
                    {
                        equipoId = (int)r.ItemArray[0];

                        //Integrantes
                        cmd = new SqlCommand(SP_NAME_GetEquipoIntegrantes, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@equipoId", r.ItemArray[0]);

                        rd2 = cmd.ExecuteReader();
                        dt2.Load(rd2);


                        //Horario
                        cmd = new SqlCommand(SP_NAME_GetHorarioEquipoByEquipoId, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@equipoId", r.ItemArray[0]);

                        rd3 = cmd.ExecuteReader();
                        dt3.Load(rd3);


                        //Llenamos la lista de integrantes
                        foreach (DataRow r2 in dt2.Rows)
                        {
                            i = new Integrantes();
                            i.integranteId = (int)r2.ItemArray[0];
                            i.integranteNombre = r2.ItemArray[1].ToString();
                            i.integranteApellidoPaterno = r2.ItemArray[2].ToString();
                            i.integranteApellidoMaterno = r2.ItemArray[3].ToString();

                            integrantesLst.Add(i);

                        }

                        foreach (DataRow r3 in dt3.Rows) 
                        {
                            dh = new DiaHorario();

                            dh.dia = r3.ItemArray[2].ToString();
                            dh.HoraInicio =TimeSpan.Parse( r3.ItemArray[3].ToString());
                            dh.HoraFin =TimeSpan.Parse( r3.ItemArray[4].ToString());
                            dh.gimnasioId = (int)r3.ItemArray[0];
                            dh.gimnasioNombre = r3.ItemArray[1].ToString();
                            dh.disciplinaId = (int)r3.ItemArray[5];
                            dh.disciplinaNombre = r3.ItemArray[6].ToString();

                            DiaHorarioLst.Add(dh);
                        }

                        //Llenamos el equipo
                        e = new Equipos();

                        e.equipoId = equipoId;
                        e.equipoNombre = r.ItemArray[1].ToString();
                        e.equipoRepresentante = r.ItemArray[2].ToString();
                        e.equipoRepresentanteTelefono = r.ItemArray[3].ToString();
                        e.integrantes = integrantesLst;

                        //llenamos horario equipo
                        he.equipo = e;
                        he.horario = DiaHorarioLst;

                        horarioEquiposList.Add(he);

                        dt2.Clear();
                        dt3.Clear();

                        integrantesLst.Clear();
                        DiaHorarioLst.Clear();

                    }

                }

                return  new JsonResult(horarioEquiposList);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(HorarioEquipo e)
        {
            int result = 0;
            DateTime now = DateTime.Now;

            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_EquipoCreate = "EquipoCreate";
            string SP_IntegranteCreate = "IntegranteCreate";
            string SP_integranteEquipoCreate = "IntegranteEquipoCreate";
            string SP_HorarioEquipoCreate = "HorarioEquipoCreate";

            int equipoId = 0;
            int integranteId = 0;

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo
                
                cmd = new SqlCommand(SP_EquipoCreate, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoNombre", e.equipo.equipoNombre);
                cmd.Parameters.AddWithValue("@equipoRepresentante", e.equipo.equipoRepresentante);
                cmd.Parameters.AddWithValue("@equipoRepresentanteTelefono", e.equipo.equipoRepresentanteTelefono);
                cmd.Parameters.AddWithValue("@equipoImporte", e.equipo.equipoImporte);

                result = (int)cmd.ExecuteScalar();

                /*
                 * 2. INTEGRANTES  Si result > 0 quiere decir que si se insertó correctamente el quipo y
                 *      continuamos con la inserción de los Integrantes
                 */
                if (result > 0) 
                {
                    //Asignamos y limpiamos la variable
                    equipoId = result;
                    result = 0;

                    foreach (var i in e.equipo.integrantes)
                    {
                        cmd = new SqlCommand(SP_IntegranteCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@integranteId", 0);
                        cmd.Parameters.AddWithValue("@integranteNombre", i.integranteNombre);
                        cmd.Parameters.AddWithValue("@integranteApellidoPaterno", i.integranteApellidoPaterno);
                        cmd.Parameters.AddWithValue("@integranteApellidoMaterno",i.integranteApellidoMaterno);
                        cmd.Parameters.AddWithValue("@integranteCURP",i.integranteCURP);
                        cmd.Parameters.AddWithValue("@integranteTelefono",i.integranteTelefono);

                        result = (int)cmd.ExecuteScalar();

                        /*
                         * 3.   Si result > 0 quiere decir que si se insertó correctamente el integrante y
                         *      continuamos con la inserción de la tabla integrante equipo
                         */
                        if (result>0) 
                        {
                            //Asignamos y limpiamos
                            integranteId = result;
                            result = 0;

                            cmd = new SqlCommand(SP_integranteEquipoCreate, con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@equipoId", equipoId);
                            cmd.Parameters.AddWithValue("@integranteId", integranteId);

                            result = (int)cmd.ExecuteScalar();
                        }

                    }

                    /*
                     * 4.   Por ultimo agregamos el horario del equipo
                     *      
                     */

                    result = 0;

                    foreach (var h in e.horario)
                    {
                        cmd = new SqlCommand(SP_HorarioEquipoCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@equipoId", equipoId);
                        cmd.Parameters.AddWithValue("@gimnasioId", e.gimnasio.gimnasioId);
                        cmd.Parameters.AddWithValue("@horarioEquipoDia", h.dia);
                        cmd.Parameters.AddWithValue("@horarioEquipoInicio", h.HoraInicio);
                        cmd.Parameters.AddWithValue("@horarioEquipoFin", h.HoraFin);
                        cmd.Parameters.AddWithValue("@disciplinaIdFk", e.disciplina.disciplinaId);

                        result = (int)cmd.ExecuteScalar();
                    }
                  
                }                

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("AddHorario")]
        public JsonResult AddHorario(HorarioEquipo e)
        {
            int result = 0;


            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_HorarioEquipoCreateSinValidaciones = "HorarioEquipoCreateSinValidaciones";

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo

                cmd = new SqlCommand(SP_HorarioEquipoCreateSinValidaciones, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", e.equipo.equipoId);
                cmd.Parameters.AddWithValue("@gimnasioId", e.gimnasio.gimnasioId);
                cmd.Parameters.AddWithValue("@horarioEquipoDia", e.horario[0].dia);
                cmd.Parameters.AddWithValue("@horarioEquipoInicio", e.horario[0].HoraInicio);
                cmd.Parameters.AddWithValue("@horarioEquipoFin", e.horario[0].HoraFin);
                cmd.Parameters.AddWithValue("@disciplinaIdFk", e.disciplina.disciplinaId);

                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ActivarDesactivarIntegrantes")]
        public JsonResult ActivarDesactivarIntrgantes(Integrantes i)
        {
            int result = 0;
           

            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_Equipo_ACTIVARDesactivar = "IntegranteEquipo_ACTIVARDesactivar";

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo

                cmd = new SqlCommand(SP_Equipo_ACTIVARDesactivar, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", i.equipoId);
                cmd.Parameters.AddWithValue("@integranteId", i.integranteId);
                cmd.Parameters.AddWithValue("@activo", i.integranteActivo);

                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ActivarDesactivarEquipo")]
        public JsonResult ActivarDesactivarEquipo(Equipos e)
        {
            int result = 0;


            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_Equipo_ACTIVARDesactivar = "Equipos_ActivarDesactivar";

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo

                cmd = new SqlCommand(SP_Equipo_ACTIVARDesactivar, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", e.equipoId);
                cmd.Parameters.AddWithValue("@activo", e.equipoActivo);

                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("IntegrantesByEquipoId")]
        public JsonResult getIntegrantesByEquipoId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "Integrates_GET_BY_EQUIPOID";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("EquipoById/{id}")]
        public JsonResult getEquipoById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EquiposGetEquipoById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }



        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Horario/{id}")]
        public JsonResult getHorarioEquipoByEquipoId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioEquipo_Get_Horario_By_EquipoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(HorarioEquipo e)
        {
            int result = 0;
            int equipoUpdate = 0;
            int integrantesEquipoDelete = 0;
            int integranteCreate = 0;
            int integranteEquipoCReate = 0;
            int horarioEquioDelete = 0;
            int horarioEquipoCreate = 0;


            
            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_EquipoUpdate = "EquipoUpdate";
            string SP_IntegrantesEquipo_Delete_By_EquipoId = "IntegrantesEquipo_Delete_By_EquipoId";
            string SP_IntegranteCreate = "IntegranteCreate";
            string SP_integranteEquipoCreate = "IntegranteEquipoCreate";
            string SP_HorarioEquipo_Delete_By_EquipoId = "HorarioEquipo_Delete_By_EquipoId";
            string SP_HorarioEquipoCreate = "HorarioEquipoCreate";

            int equipoId = 0;
            int integranteId = 0;

            try
            {
                con.Open();

                //1.EQUIPO Actualizamos los datos del equipo

                cmd = new SqlCommand(SP_EquipoUpdate, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", e.equipo.equipoId);
                cmd.Parameters.AddWithValue("@equipoNombre", e.equipo.equipoNombre);
                cmd.Parameters.AddWithValue("@equipoRepresentante", e.equipo.equipoRepresentante);
                cmd.Parameters.AddWithValue("@equipoRepresentatneTelefono", e.equipo.equipoRepresentanteTelefono);

                equipoUpdate = (int)cmd.ExecuteScalar();

                /*
                 * 2. Eliminamos todos los INTEGRANTES DE integrantes Equipo
                 *      
                 */
                if (equipoUpdate > 0)
                {
                    //Asignamos y limpiamos la variable
                    equipoId = e.equipo.equipoId;

                    cmd = new SqlCommand(SP_IntegrantesEquipo_Delete_By_EquipoId, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@equipoId", equipoId);

                    integrantesEquipoDelete = (int)cmd.ExecuteScalar();


                    /*
                     * 3. Insertamos el integrante en la tabla integrantes. SI el integrante ya existe solo obetnemos su Id
                     *      
                     */
                    foreach (var i in e.equipo.integrantes)
                    {
                        cmd = new SqlCommand(SP_IntegranteCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@integranteId", i.integranteId);
                        cmd.Parameters.AddWithValue("@integranteNombre", i.integranteNombre);
                        cmd.Parameters.AddWithValue("@integranteApellidoPaterno", i.integranteApellidoPaterno);
                        cmd.Parameters.AddWithValue("@integranteApellidoMaterno", i.integranteApellidoMaterno);
                        cmd.Parameters.AddWithValue("@integranteCURP", i.integranteCURP);
                        cmd.Parameters.AddWithValue("@integranteTelefono", i.integranteTelefono);

                        integranteCreate = (int)cmd.ExecuteScalar();

                        /*
                         * 3.   Si result > 0 quiere decir que si se insertó correctamente y
                         *      continuamos con la inserción de la tabla integrante equipo
                         */

                        if (integranteCreate > 0)
                        {
                            //Asignamos y limpiamos
                            integranteId = integranteCreate;


                            cmd = new SqlCommand(SP_integranteEquipoCreate, con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@equipoId", equipoId);
                            cmd.Parameters.AddWithValue("@integranteId", integranteId);

                            integranteEquipoCReate = (int)cmd.ExecuteScalar();
                        }

                    }
                   
                    /*
                     * 4.   Eliminamos todos los horario    
                     *      
                     */

                    cmd = new SqlCommand(SP_HorarioEquipo_Delete_By_EquipoId, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@equipoId", equipoId);

                    horarioEquioDelete = (int)cmd.ExecuteScalar();


                    /*
                     * 5.   Por ultimo agregamos el horario del equipo
                     *      
                     */

                    foreach (var h in e.horario)
                    {
                        cmd = new SqlCommand(SP_HorarioEquipoCreate, con);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@equipoId", equipoId);
                        cmd.Parameters.AddWithValue("@gimnasioId", h.gimnasioId);
                        cmd.Parameters.AddWithValue("@horarioEquipoDia", h.dia);
                        cmd.Parameters.AddWithValue("@horarioEquipoInicio", h.HoraInicio);
                        cmd.Parameters.AddWithValue("@horarioEquipoFin", h.HoraFin);
                        cmd.Parameters.AddWithValue("@disciplinaIdFk", h.disciplinaId);

                        horarioEquipoCreate = (int)cmd.ExecuteScalar();
                    }

                }
                
                if (equipoUpdate>0 && integrantesEquipoDelete>0 && integranteCreate>0 && integranteEquipoCReate>0 && horarioEquioDelete>0 && horarioEquipoCreate>0) 
                {
                    result = 1;
                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("ParcialidadEquipo")]
        public JsonResult ParcialidadEquipo(Parcialidad p)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "ParcialidadDePagoEquipoCreate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoid",p.equipoId);
                cmd.Parameters.AddWithValue("@parcialidadImporte",p.parcialidadImporte);
                cmd.Parameters.AddWithValue("@tipoDePago",p.TipoDePago);
                cmd.Parameters.AddWithValue("@folioDePago",p.folioDePago);
                cmd.Parameters.AddWithValue("@cuentaBeneficiaria",p.cuentaBeneficiaria);
                cmd.Parameters.AddWithValue("@conceptoDePago",p.conceptoDePago);
                cmd.Parameters.AddWithValue("@observaciones",p.observaciones);
                cmd.Parameters.AddWithValue("@fechaDeReciboDePago",p.fechaDeReciboDePago);


                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Gimnasios/{id}")]
        public JsonResult GimnasiosByEquipoId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EquiposRenta_Get_Gimnasios_By_equipoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoid", id);
                
                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("GimnasioIdByHorarioId/{id}")]
        public JsonResult GimnasiosIdByHorarioId(int id)
        {
            int result=0;
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GetGimnasioByhorarioId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@horarioId", id);

                con.Open();
                
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Gimnasio/{gid}/Equipo/{eid}")]
        public JsonResult GetHorarioByGimnasioIdAndEquipoId(int gid, int eid)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "GetHorarioByGimnasioIdAndEquipoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@gimnasioId", gid);
                cmd.Parameters.AddWithValue("@equipoId", eid);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Disciplina/{id}")]
        public JsonResult DisciplinaByEquipoId(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "EquiposRenta_Get_Disciplina_By_equipoId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoid", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("UdateHorario")]
        public JsonResult UpdateHorario(HorarioEquipo h)
        {
            int result = 0;


            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_HorarioEquipoUpdate = "HorarioEquipoUpdate";

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo

                cmd = new SqlCommand(SP_HorarioEquipoUpdate, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@equipoId", h.equipo.equipoId);
                cmd.Parameters.AddWithValue("@equipoNombre", h.equipo.equipoNombre);
                cmd.Parameters.AddWithValue("@equipoImporte", h.equipo.equipoImporte);
                cmd.Parameters.AddWithValue("@horarioEquipoId", h.horarioId);
                cmd.Parameters.AddWithValue("@gimnasioId", h.gimnasioId);
                cmd.Parameters.AddWithValue("@horarioEquipoDia", h.horarioEquipoDia);
                cmd.Parameters.AddWithValue("@horarioEquipoInicio",h.horarioEquipoInicio);
                cmd.Parameters.AddWithValue("@horarioEquipoFin", h.horarioEquipoFin);

                //cmd.Parameters.AddWithValue("@equipoId", e.equipo.equipoId);
                //cmd.Parameters.AddWithValue("@gimnasioId", e.gimnasio.gimnasioId);
                //cmd.Parameters.AddWithValue("@horarioEquipoDia", e.horario[0].dia);
                //cmd.Parameters.AddWithValue("@horarioEquipoInicio", e.horario[0].HoraInicio);
                //cmd.Parameters.AddWithValue("@horarioEquipoFin", e.horario[0].HoraFin);
                //cmd.Parameters.AddWithValue("@disciplinaIdFk", e.disciplina.disciplinaId);

                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("DeleteHorario/{id}")]
        public JsonResult DeleteHorario(int id)
        {
            int result = 0;
            
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "HorarioEquipoDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@horarioId", id);

                con.Open();

                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("IntegranteCreate")]
        public JsonResult IntegranteCreate(Integrantes i)
        {
            int integranteId = 0;
            int integranteEquipoId = 0;


            SqlConnection con = new SqlConnection(CON_STR);
            SqlCommand cmd;

            string SP_IntegranteCreate = "IntegranteCReate";
            string SP_integranteEquipoCreate = "integranteEquipoCreate";

            try
            {
                con.Open();

                //1.EQUIPO Insertamos el los datos del equipo 

                cmd = new SqlCommand(SP_IntegranteCreate, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@integranteId", i.integranteId);
                cmd.Parameters.AddWithValue("@integranteNombre", i.integranteNombre);
                cmd.Parameters.AddWithValue("@integranteApellidoPaterno", i.integranteApellidoPaterno);
                cmd.Parameters.AddWithValue("@integranteApellidoMaterno", i.integranteApellidoMaterno);
                cmd.Parameters.AddWithValue("@integranteCURP", i.integranteCURP);
                cmd.Parameters.AddWithValue("@integranteTelefono", i.integranteTelefono);

                integranteId = (int)cmd.ExecuteScalar();

                if (integranteId > 0)
                {
                    cmd = new SqlCommand(SP_integranteEquipoCreate, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@equipoId", i.equipoId);
                    cmd.Parameters.AddWithValue("@integranteId", integranteId);

                    integranteEquipoId = (int)cmd.ExecuteScalar();

                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(integranteId);
        }


    }
}
