﻿using API_ControlDeAccesoyAdministracion.Modelo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_ControlDeAccesoyAdministracion.Utilerias;
using System.IO;

namespace API_ControlDeAccesoyAdministracion.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class InstructorController : Controller
    {
        private readonly IConfiguration _configuration;
        private string SP_NAME;
        private string CON_STR;
        public InstructorController(IConfiguration configuration)
        {
            _configuration = configuration;
            CON_STR = _configuration.GetConnectionString("damDB");
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorGetAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Active")]
        public JsonResult GetAllActive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorGetAllActive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Inactive")]
        public JsonResult GetAllInactive()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorGetAllInactive";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorGetById";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);


                string instructorCURP = dt.Rows[0].ItemArray[4].ToString();
                string base64Image = Imagen.ImageToBase64(instructorCURP);

                DataColumn nuevaColumna = new DataColumn("instructorBase64ImageString", typeof(System.String));
                nuevaColumna.DefaultValue = base64Image;
                dt.Columns.Add(nuevaColumna);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("Search/{pattern}")]
        public JsonResult tutorSearchBy(string pattern)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorSearchBy";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pattern", pattern);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpGet("AllData")]
        public JsonResult allData()
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorHorasClaseAll";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;                

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpGet("AllData/{id}")]
        public JsonResult allDataByInstructorId(string id)
        {
            DataTable dt = new DataTable();
            SqlDataReader rd;
            SqlConnection con = new SqlConnection(CON_STR);

            SP_NAME = "InstructorHorasClaseByInstructorId";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", id);

                con.Open();
                rd = cmd.ExecuteReader();
                dt.Load(rd);

                string instructorCURP = dt.Rows[0].ItemArray[4].ToString();
                string base64Image = Imagen.ImageToBase64(instructorCURP);

                DataColumn nuevaColumna = new DataColumn("instructorBase64ImageString", typeof(System.String));
                nuevaColumna.DefaultValue = base64Image;
                dt.Columns.Add(nuevaColumna);

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(dt);
        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Entrada")]
        public JsonResult RegistrarEntrada(Instructor instructor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InstructorRegistrarEntrada";


            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", instructor.instructorId);                             

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Salida")]
        public JsonResult RegistrarSalida(Instructor instructor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InstructorRegistrarSalida";


            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", instructor.instructorId);

                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Nuevo(Instructor instructor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InstructorCreate";


            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorNombre", instructor.instructorNombre);
                cmd.Parameters.AddWithValue("@instructorApellidoPaterno", instructor.instructorApellidoPaterno);
                cmd.Parameters.AddWithValue("@instructorApellidoMaterno", instructor.instructorApellidoMaterno);
                cmd.Parameters.AddWithValue("@instructorCURP", instructor.instructorCURP);
                cmd.Parameters.AddWithValue("@instructorRFC", instructor.instructorRFC);
                cmd.Parameters.AddWithValue("@instructorCodigoPostal", instructor.instructorCodigoPostal);
                cmd.Parameters.AddWithValue("@instructorEstado", instructor.instructorEstado);
                cmd.Parameters.AddWithValue("@instructorMunicipio", instructor.instructorMunicipio);
                cmd.Parameters.AddWithValue("@instructorColonia", instructor.instructorColonia);
                cmd.Parameters.AddWithValue("@instructorCalle", instructor.instructorCalle);
                cmd.Parameters.AddWithValue("@instructorNumero", instructor.instructorNumero);
                cmd.Parameters.AddWithValue("@instructorFechaDeNacimiento", instructor.instructorFechaDeNacimiento);
                cmd.Parameters.AddWithValue("@instructorSexo", instructor.instructorSexo);
                cmd.Parameters.AddWithValue("@instructorTelefono", instructor.instructorTelefono);


                con.Open();
                result = (int)cmd.ExecuteScalar();


                if (result > 0)
                {
                    result = 1;
                }

                string path = @"C:\imagenes";

                if (Directory.Exists(path) && result == 1)
                {
                    Imagen.Base64ToImage(instructor.instructorBase64Image, instructor.instructorCURP, path);
                }
                else if (result == 1)
                {
                    //Si no existe el directorio lo creamos
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    Imagen.Base64ToImage(instructor.instructorBase64Image, instructor.instructorCURP, path);
                }

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }


        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Update")]
        public JsonResult Update(Instructor instructor)
        {
            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InstructorUpdate";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", instructor.instructorId);
                cmd.Parameters.AddWithValue("@instructorNombre", instructor.instructorNombre);
                cmd.Parameters.AddWithValue("@instructorApellidoPaterno", instructor.instructorApellidoPaterno);
                cmd.Parameters.AddWithValue("@instructorApellidoMaterno", instructor.instructorApellidoMaterno);
                cmd.Parameters.AddWithValue("@instructorCURP", instructor.instructorCURP);
                cmd.Parameters.AddWithValue("@instructorRFC", instructor.instructorRFC);
                cmd.Parameters.AddWithValue("@instructorCodigoPostal", instructor.instructorCodigoPostal);
                cmd.Parameters.AddWithValue("@instructorEstado", instructor.instructorEstado);
                cmd.Parameters.AddWithValue("@instructorMunicipio", instructor.instructorMunicipio);
                cmd.Parameters.AddWithValue("@instructorColonia", instructor.instructorColonia);
                cmd.Parameters.AddWithValue("@instructorCalle", instructor.instructorCalle);
                cmd.Parameters.AddWithValue("@instructorNumero", instructor.instructorNumero);
                cmd.Parameters.AddWithValue("@instructorFechaDeNacimiento", instructor.instructorFechaDeNacimiento);
                cmd.Parameters.AddWithValue("@instructorSexo", instructor.instructorSexo);
                cmd.Parameters.AddWithValue("@instructorActivo", instructor.instructorActivo);

                con.Open();
                result = (int)cmd.ExecuteScalar();


                if (result > 0)
                {
                    string path = @"C:\imagenes";

                    if (Directory.Exists(path) && result == 1)
                    {
                        Imagen.Base64ToImage(instructor.instructorBase64Image, instructor.instructorCURP, path);
                    }
                    else if (result == 1)
                    {
                        //Si no existe el directorio lo creamos
                        DirectoryInfo di = Directory.CreateDirectory(path);
                        Imagen.Base64ToImage(instructor.instructorBase64Image, instructor.instructorCURP, path);
                    }

                }
              
            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);
        }

        [EnableCors("AllowAnyOrigin")]
        [HttpPost("Delete")]
        public JsonResult Delete(Instructor instructor)
        {

            int result = 0;
            SqlConnection con = new SqlConnection(CON_STR);
            SP_NAME = "InstructorDelete";

            try
            {
                SqlCommand cmd = new SqlCommand(SP_NAME, con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@instructorId", instructor.instructorId);
                cmd.Parameters.AddWithValue("@instructorActivo", instructor.instructorActivo);


                con.Open();
                result = (int)cmd.ExecuteScalar();

            }
            catch (Exception ex)
            {
                return new JsonResult(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return new JsonResult(result);

        }
    }
}
