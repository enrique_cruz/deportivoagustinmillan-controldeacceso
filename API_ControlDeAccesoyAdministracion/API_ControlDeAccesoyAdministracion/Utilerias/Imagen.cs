﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace API_ControlDeAccesoyAdministracion.Utilerias
{
    public  static class Imagen
    {

        public static void Base64ToImage(string base64String, string ImgName, string path)
        {
            try
            {
                string imageName = ImgName + ".png";

                string imgPath = Path.Combine(path, imageName);
                byte[] imageBytes = Convert.FromBase64String(base64String);
                File.WriteAllBytes(imgPath, imageBytes);
            }
            catch (Exception)
            {
                throw;
            }
                       
        }

        public static string ImageToBase64(string ImgName)
        {
            string imageName = ImgName + ".png";
            string path = @"C:\imagenes";
            string base64ImageString = "";

            string imgPath = Path.Combine(path, imageName);            

            try
            {
                if (File.Exists(imgPath))
                {
                    byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);

                    base64ImageString =  Convert.ToBase64String(imageArray);
                }
                
            }
            catch (Exception ex)
            {

                throw;
            }
                     
            return base64ImageString;
        }

    }
}
