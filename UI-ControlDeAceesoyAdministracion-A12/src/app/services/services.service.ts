import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as _  from 'lodash';
import { isThisMonth } from 'date-fns';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  //urlApi = "https://monitor.grupopissa.com.mx/capi/api"
  urlApi="https://localhost:44354/api"

  //PRODUCCION
  //urlApi = "https://192.168.100.89:4433/api";
  
  constructor(private http: HttpClient,
  private cookieService: CookieService) { }
  
  getUsuarios(){
    return this.http.get(this.urlApi+"/Usuario/All");
  }
  
  activarDesactivarUsuarios(json: any){
    return this.http.post(this.urlApi+"/Usuario/UpdateStatus", json);
  }
  
  //ROLES
  getRoles(){
    return this.http.get(this.urlApi+"/Roles");
  }
  
  getRolesActivos(){
    return this.http.get(this.urlApi+"/Roles/Active");
  }

  getRolPermisoByUsuarioId(usuarioId: number)
  {
    return this.http.get(this.urlApi+"/RolPermiso/Usuario/" + usuarioId);
  }

  getRolPermisoByCorreoElectronico(usuarioCorreoElectronico: string)
  {
    return this.http.get(this.urlApi+"/RolPermiso/Usuario/CorreoElectronico/" + usuarioCorreoElectronico);
  }

  activeInactiveRol(json: any){
    return this.http.post(this.urlApi+"/Roles/UpdateStatus", json);
  }
  
  getPermisos(){
    return this.http.get(this.urlApi+"/Permisos");
  }
  
  login(json: any){
    return this.http.post(this.urlApi+"/Usuario/Auth",json);
  }
  
  getPermisosRol(idRol: number){
    return this.http.get(this.urlApi+"/RolPermiso/"+idRol);
  }
  
  
  setUpdateRol(json: any){
    return this.http.post(this.urlApi+"/RolPermiso/Nuevo",json);
  }
  
  setUsuario(json: any){
    return this.http.post(this.urlApi+"/Usuario/Nuevo",json);
  }
  
  
  
  //servicios de categoria
      
  getCategoriasAll(){
    return this.http.get(this.urlApi+"/Categoria/All");
  }
  
  setCategoria(json: any){
    return this.http.post(this.urlApi+"/Categoria/Nuevo",json);
  }
   
    
  updateCategoria(json: any){
    return this.http.post(this.urlApi+"/Categoria/Update",json);
  }
  
  
  activeInactiveCategoria(json: any){
    return this.http.post(this.urlApi+"/Categoria/Delete",json);
  }
  
  getCategoriasActive(){
    return this.http.get(this.urlApi+"/Categoria/Active");
  }
  
  
  //servicios de disciplina

getDisciplinasAll(){
  return this.http.get(this.urlApi+"/Disciplina/All");
}

setDisciplina(json: any){
  return this.http.post(this.urlApi+"/Disciplina/Nuevo",json);
}


  
updateDisciplina(json: any){
  return this.http.post(this.urlApi+"/Disciplina/Update",json);
}


activeInactiveDisciplina(json: any){
  return this.http.post(this.urlApi+"/Disciplina/Delete",json);
}
  
getDisciplinasActive(){
  return this.http.get(this.urlApi+"/Disciplina/Active");
}
  //servicios de Gimnacio

  getGimnaciosAll(){
    return this.http.get(this.urlApi+"/Gimnasio/All");
  }
  
  getGimnaciosAllData(){
    return this.http.get(this.urlApi+"/Gimnasio/AllData");
  }
  
  
  
  setGimnacio(json: any){
    return this.http.post(this.urlApi+"/Gimnasio/Nuevo",json);
  }
  
  
    
  updateGimnacio(json: any){
    return this.http.post(this.urlApi+"/Gimnasio/Update",json);
  }
  
  
  activeInactiveGimnacio(json: any){
    return this.http.post(this.urlApi+"/Gimnasio/Delete",json);
  }
    
 
  getGimnaciosActivos(){
    return this.http.get(this.urlApi+"/Gimnasio/Active");
  }
  
  
  //servicios de alumno
  //https://monitor.grupopissa.com.mx/Search/
  getAlumnos(){
    return this.http.get(this.urlApi+"/Alumno/All");
  }

  
  getAlumnoById(id: number){   
    return this.http.get(this.urlApi+"/Alumno/Get/"+id);
  }
  
  searchAlumnos(pattern: string){
    return this.http.get(this.urlApi+"/Alumno/Search/"+pattern);
  }

  
  setAlumno(json: any){
    return this.http.post(this.urlApi+"/Alumno/Nuevo",json);
  }
  
  
  activarDesactivarAlumno(json : any){
    return this.http.post(this.urlApi+"/Alumno/Delete",json);
  }
  
  updateAlumno(json: any){
    return this.http.post(this.urlApi+"/Alumno/Update",json);
  }
  
  
  setAlumnoTutor(json: any){
    return this.http.post(this.urlApi+"/TutorAlumno/Nuevo",json);
  }
  
  getTutoresByIdAlumno(json: number){
    return this.http.get(this.urlApi+"/TutorAlumno/GetTutoresByAlumnoId/"+json);
  }
  
  setTutorAlumno(json: any){
    return this.http.post(this.urlApi+"/TutorAlumno/Nuevo",json);
  }
  
  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   ALUMNO PLAN HORARIO    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  getPlanesByAlumno(json: number){
    return this.http.get(this.urlApi+"/AlumnoPlanHorario/Alumno/"+json);
  }

  recreateAlumnoPlanHorario(aph:any){
    return this.http.post(this.urlApi + "/AlumnoPlanHorario/RecreateHorario", aph);
  }

  alumnoPlanHorarioCreate(APH : any){
    return this.http.post(this.urlApi + "/AlumnoPlanHorario/Create", APH);
  }

  alumnoPlanHorarioDelete(APH : any){
    return this.http.post(this.urlApi + "/AlumnoPlanHorario/Delete", APH);
  }

  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   ALUMNO PLAN HORARIO    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  
  
  verificarCURP(curp : string){
    return this.http.get(this.urlApi+"/Alumno/VerificaCurp?curp="+curp);
  }
  
  //servicios de instructor
  
  setInstructor(json: any){
    return this.http.post(this.urlApi+"/Instructor/Nuevo",json);
  }
  
  updateInstructor(json: any){
    return this.http.post(this.urlApi+"/Instructor/Update",json);
  }
  
  
  getInstructoresAll(){
    return this.http.get(this.urlApi+"/Instructor/All");
  }
  
  
  getInstructoresAllData(){
    return this.http.get(this.urlApi+"/Instructor/AllData");
  }
  
  
  getInstructorbyId(id: number){
    return this.http.get(this.urlApi+"/Instructor/"+id);
  }
  
  
  getInstructoresActivos(){
    return this.http.get(this.urlApi+"/Instructor/Active");
  }
  
  activeInactiveInstructor(json: any){
    return this.http.post(this.urlApi+"/Instructor/Delete",json);
  }
  
  //servicios de horarios
  
  
  setHorario(json: any){
    return this.http.post(this.urlApi+"/Horario/Nuevo",json);
  }
  updateHorario(json: any){
    return this.http.post(this.urlApi+"/Horario/Update",json);
  }
  
  
  getHorariosAll(){
    return this.http.get(this.urlApi+"/Horario/All");
  }
  
  getHorariosActivos(){
    return this.http.get(this.urlApi+"/Horario/Active");
  }
  
  getHorariosByGinmnasio(id: number){
    return this.http.get(this.urlApi+"/Horario/Gimnasio/"+id);
  }
  
  
  getHorarioById(idDisiplina: number){
    return this.http.get(this.urlApi+ "/Disciplina/"+idDisiplina);
  }
  
  getHorarioAforoById(json: any){
    return this.http.get(this.urlApi+"/Horario/HorarioDisciplina?id="+
      json.disciplinaId+"&edad="+json.edad+"&sexo="+json.sexo);
  }
  
  activeInactiveHorarios(json: any){
    return this.http.post(this.urlApi+"/Horario/Delete",json);
  }
  //Servicios de planes
  
  setPlan(json: any){
    return this.http.post(this.urlApi+"/PlanDeInscripcion/Nuevo",json);
  }

  updatePlan(json: any){
    return this.http.post(this.urlApi+"/PlanDeInscripcion/Update",json);
  }
  
  getPlanAll(){
    return this.http.get(this.urlApi+"/PlanDeInscripcion/All");
  }
  
  getPlanActivos(){
    return this.http.get(this.urlApi+"/PlanDeInscripcion/Active");
  }
  
  activeInactivePlan(json: any){
    return this.http.post(this.urlApi+"/PlanDeInscripcion/Delete",json);
  }  
  
  getPlanesByDisciplina(idDisciplina: number){
    return this.http.get(this.urlApi + "/PlanDeInscripcion/PlanDisciplina/"+idDisciplina);
  }
  
  //servicios de inscripcion
  setInscripcion(json: any){
    return this.http.post(this.urlApi+"/Inscripcion/NuevaInscripcion",json);
  }
  
  //mensualidades
  getMensualidadesByAlumno(idAlumno: number, fecha: string){
    return this.http.get(this.urlApi+"/Mensualidad/Alumno/"+idAlumno+"?fecha="+fecha);
  }
  
  
  setMensualidad(json: any){
    return this.http.post(this.urlApi+"/Mensualidad/Pago",json);
  }
  
  setPagoOtroConcepto(json : any){
    return this.http.post(this.urlApi+"/Mensualidad/PagoOtroConcepto",json);
  }
  
  //********************************************* ENTRADA SALIDA
  
  getRegistrosToday(){
    return this.http.get(this.urlApi+"/EntradaSalida/TodayAll");
  }
  
  
  getRegistroEntrada(curp: string){
    console.log(this.urlApi+"/EntradaSalida/RegistrarEntrada?criptedString="+curp);
    return this.http.get(this.urlApi+"/EntradaSalida/RegistrarEntrada?criptedString="+curp);
  }
  
  getRegistroSalida(curp: string){
    console.log(this.urlApi+"/EntradaSalida/RegistrarSalida?criptedString="+curp);
    return this.http.get(this.urlApi+"/EntradaSalida/RegistrarSalida?criptedString="+curp);
  }

  getEntradaSalidaByCURP(es : any)
  {
    return this.http.post(this.urlApi + "/EntradaSalida/EntradaSalidaSearchByCURP/", es)
  }

  getEntradaSalidaByDisciplina(es : any)
  {
    return this.http.post(this.urlApi + "/EntradaSalida/EntradaSalidaSearchByDisciplina/", es);
  }
  //********************************************* ENTRADA SALIDA

  //servicos de tutores
  
  getTutorById(id: number){
    //console.log(this.urlApi+"/Tutor/"+id);
    return this.http.get(this.urlApi+"/Tutor/"+id);
  }
  
   
  deleteTutorById(json: any){
    return this.http.post(this.urlApi+"/Tutor/Delete",json);
  }
  
  
  updateTutorById(json: any){
    return this.http.post(this.urlApi+"/Tutor/Update",json);
  }
    
  //reagendar clase
  
  reagendarClase(json : any){
    return this.http.post(this.urlApi+"/Horario/Reposicion",json);
  }
  
  
 //servicios de sesion
  checkSesion(){
    const cookieExists: boolean = this.cookieService.check('sesion_amAM_GP');
  }
  
  
  getSesion(){
    var cookieValue = this.cookieService.get('sesion_amAM_GP');
    return cookieValue;
  }
  
  setSesion(json: any){
    this.cookieService.set('sesion_amAM_GP', json);
  }
  
  
  deleteSesion(){
    this.cookieService.delete('sesion_amAM_GP');
  }
  
//###################################### EQUIPOS / RENTAS ##################################
getEquiposAll()
{
  return this.http.get(this.urlApi + "/EquiposRentas/All")
} 

getEquipoSearchBy(strBusqueda : string){
  return this.http.get(this.urlApi + "/EquiposRentas/Equipo/" +  strBusqueda)
}

getIntegranteEquipoSearchBy(id: number, strBusqueda : string){
  return this.http.get(this.urlApi + "/EquiposRentas/Equipo/" +  id + "/Integrante/"+ strBusqueda)
}
  
getEquipos(id: number){
  return this.http.get(this.urlApi+"/EquiposRentas/Active");
}

getEquipoById(id:number){
  return this.http.get(this.urlApi + "/EquiposRentas/EquipoById/" + id);
}
 
insertRentaEquipo(json: any){
  return this.http.post(this.urlApi+"/EquiposRentas/Nuevo",json);  
}

addNewHorario(h:any){
  return this.http.post(this.urlApi + "/EquiposRentas/AddHorario", h);
}

insertParcialidadEquipo(json:any){           //"    /EquiposRentas/ParcialidadEquipo"
  return this.http.post(this.urlApi + "/EquiposRentas/ParcialidadEquipo", json)
}

insertIntegranteEquipo(ie:any){
  return this.http.post(this.urlApi + "/EquiposRentas/IntegranteCreate", ie);
}

getGimnasiosByEquipoId(id: number)
{
  return this.http.get(this.urlApi + "/EquiposRentas/Gimnasios/"+id);

}

getDisciplinaByEquipoId(id: number)
{
  return this.http.get(this.urlApi +"/EquiposRentas/Disciplina/" + id);
}

getHorariosByEquipoId(id: number){

  return this.http.get(this.urlApi +"/EquiposRentas/Horario/" + id);
}

getGimnasioIdByHorarioId(id:number){
  return this.http.get(this.urlApi+ "/EquiposRentas/GimnasioIdByHorarioId/" + id);
}

getHorarioByGimnasioEquipoId(gimnasioId: number, equipoId : number){
  return this.http.get(this.urlApi +  "/EquiposRentas/Gimnasio/" + gimnasioId + "/Equipo/" + equipoId);
}

updateHorarioEquipo(horario:any){
  return this.http.post(this.urlApi + "/EquiposRentas/UdateHorario", horario);
}

deleteHorarioEquipo(id:number){
  return this.http.get(this.urlApi + "/EquiposRentas/DeleteHorario/" + id);
}
//###################################### EQUIPOS / RENTAS ##################################


//****************** QR MAESTRO */
createQrMaestro(qrString:any){
    return this.http.get(this.urlApi + "/QrMaestro/GenerarQrMaestro/" + qrString);
  }
//****************** QR MAESTRO */

  //servicio de codigo postal

  getInfoCodePostal(cp: string){
    return this.http.get('https://monitor.grupopissa.com.mx/vapi/api/CodigoPostal/GetLocalidadByCodigoPostal/'+cp);
   }
   
   //para descargar excel
   public exportAsExcelFile(json: any[], excelFileName: string): void {
    
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet',worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().toLocaleDateString()+ EXCEL_EXTENSION);
  }
  
  
  
  //reportes 
  getEntradaSalidaAlumno(json: any){
    return this.http.post(this.urlApi+"/Reportes/ReporteEntradaSalida",json);
  }
    
  getPagosAlumno(json: any){
    return this.http.post(this.urlApi+"/Reportes/PagosPorAlumno",json);
  }
  
  getAlumnosByDisciplina(json: any){
    return this.http.post(this.urlApi+"/Reportes/AlumnosPorDisciplina",json);
  }
  
  getAforoByDisciplina(json: any){
    return this.http.post(this.urlApi+"/Reportes/AforoPorDisciplina",json);
  }
  
  getAsistenciaInstructores(json: any){
    return this.http.post(this.urlApi+"/Reportes/AsistenciaDeProfesores",json);
  }
  
  getClasesReagendadasAlumno(json: any){
    return this.http.post(this.urlApi+"/Reportes/ReporteReposicionDeClase",json);
  }
  

  ///api/Reportes/InscritosMensualesPorDisciplina
  getInscritosDisciplinaFechas(json : any){
    return this.http.post(this.urlApi+"/Reportes/InscritosMensualesPorDisciplina",json);

  }
  

  getIntegrantesByEquipos(id: number){
    return this.http.get(this.urlApi+"/EquiposRentas/IntegrantesByEquipoId?id="+id);
  }

  getReporteFinanciero(json : any)
  {
    return this.http.post(this.urlApi + "/Reportes/ReporteFinanciero/",json)
  }

// REPORTES FIN
activarDesactivarEquipo(json: any){
  console.log(this.urlApi+"/EquiposRentas/ActivarDesactivarEquipo");
  return this.http.post(this.urlApi+"/EquiposRentas/ActivarDesactivarEquipo",json);
}

activeInactiveIntegrantes(json: any){

  return this.http.post(this.urlApi+"/EquiposRentas/ActivarDesactivarIntegrantes",json);
}



  
}


