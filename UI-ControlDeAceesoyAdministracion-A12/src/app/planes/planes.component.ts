import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.css']
})
export class PlanesComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  visibleCancelar = 1;
  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];

  plan = {
    planId: 0,
    disciplinaIdFk: 0,
    planDescripcion: "", 
    planRecurrencia: 0,// 2 dias
    stringRecurrencia: "",
    planImporte: 0,
    planActivo: ""
  }
  
  
  arrayPlanes:Array<{ 
    planId : number,
    planDescripcion: string, 
    planRecurrencia : string , 
    disciplinaId :number,
    planImporte: number, 
    planActivo: string,  
    isActive: boolean}>=[];
    
    arrayPlanesShow:Array<{ 
      planId : number,
      planDescripcion: string, 
      planRecurrencia : string , 
      disciplinaId :number,
      planImporte: number, 
      planActivo: string,  
      isActive: boolean}>=[];

    
  strBusqueda = "";
  
  constructor(private httpServices: ServicesService, private route: ActivatedRoute) {
  
    this.plan.planRecurrencia = this.permisos.value!;
    this.plan.planImporte = this.permisos.value!;
    
  }
 
  
  
  
  ngOnInit(): void {
    this.getDisciplinas();
    this.getPlanes();
  }
  
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: Number(jsonDatos[i].disciplinaId), 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      
      });
  }
  
  
  
  setPlan(){
   if(this.validarDatos()){
       // this.plan.planRecurrencia = this.plan.planRecurrencia+ "días"; 
    //
      if(this.plan.planRecurrencia === 1){
        this.plan.planRecurrencia = this.plan.planRecurrencia;
        this.plan.stringRecurrencia = this.plan.planRecurrencia+" clase a la semana"; 
      }else{
        this.plan.planRecurrencia = this.plan.planRecurrencia;
        this.plan.stringRecurrencia = this.plan.planRecurrencia+" clases a la semana"; 
      }
    
      this.plan.disciplinaIdFk = Number(this.plan.disciplinaIdFk);
    
    //this.plan.planRecurrencia = this.plan.planRecurrencia + ""
    console.log(this.plan);
      if(this.visibleCancelar == 1){
       
        this.httpServices.setPlan(this.plan).subscribe(
          datos => {
            console.log(datos);
            /*var jsonDatos = JSON.parse(JSON.stringify(datos));
            console.log(JSON.stringify(datos));*/
            if(datos == 1){
              this.getPlanes();
              this.reset();
            }else{
              alert("Error, intenta de nuevo");
            }
          });
      }else{
        this.httpServices.updatePlan(this.plan).subscribe(
          datos => {
            console.log(datos);
            /*var jsonDatos = JSON.parse(JSON.stringify(datos));
            console.log(JSON.stringify(datos));*/
            if(datos == 1){
              this.getPlanes();
              this.reset();
            }else{
              alert("Error, intenta de nuevo");
            }
          });
      }
    
   }
  
  }
  
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }

  getPlanes(){
    this.httpServices.getPlanAll().subscribe(
      datos => {
        console.log("planes");
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayPlanes = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayPlanes.push({
          planId: jsonDatos[i].planId, 
          planDescripcion: jsonDatos[i].planDescripcion, 
          planImporte : jsonDatos[i].planImporte,
           disciplinaId : jsonDatos[i].disciplinaId,
          planRecurrencia: jsonDatos[i].planRecurrencia, 
          planActivo: jsonDatos[i].planActivo,
        isActive: this.calcularBooleano(jsonDatos[i].planActivo)});   
        }
        
        this.arrayPlanesShow = this.arrayPlanes;
      });
  }
  
  
  btnEditar(plan: any){
    
    this.visibleCancelar = 0;
    this.plan.planId = plan.planId;
    this.plan.planDescripcion = plan.planDescripcion;
    this.plan.planRecurrencia = plan.planRecurrencia;
    this.plan.disciplinaIdFk = plan.disciplinaId;
    this.plan.planImporte = plan.planImporte;
    this.plan.planActivo = plan.planActivo;
  
  }

  desactivarActivarPlan(json: any){
    console.log(json);
    var send = {
      "planId" : json.planId,
      "planActivo": "SI" 
    };
    
    if(json.isActive){
      send.planActivo = "SI";
    }else{
      send.planActivo = "NO";
    }
    json.planActivo = send.planActivo;

    console.log(send);    
    this.httpServices.activeInactivePlan(send).subscribe(
      datos => {
        console.log(datos);
   
      },
      error => {
        console.log("hola desde el error"); 
        console.log(error); 
        if(json.isActive){
          json.planActivo = "NO";
          json.isActive = false;
          
        }else{
          json.planActivo = "SI";
          json.isActive = true;
        }
        
      });
    
}

  
  
reset(){
  this.visibleCancelar = 1;
  this.plan.planId = 0;
  this.plan.planDescripcion = "";

  this.plan.disciplinaIdFk = 0;

  this.plan.planActivo = "";
  this.plan.planRecurrencia = this.permisos.value!;
  this.plan.planImporte = this.permisos.value!;
}

validarDatos(){
  if(this.plan.disciplinaIdFk == 0){
    alert("Selecciona una disciplina para el plan");
    return false;
  }else if(this.plan.planDescripcion === ""){
    
    alert("Ingresa una descripción para el plan");
    return false;
  }else if(this.plan.planImporte == 0 || this.plan.planImporte== undefined){
    alert("Ingresa un importe para el plan");
    return false;
  }else if(this.plan.planRecurrencia === 0 || this.plan.planRecurrencia == undefined){
    alert("Ingresa una recurrencia para el plan");
    return false;
  }else{
    return true;
  }
}







buscarPlan(){
  this.arrayPlanesShow = [];
  if(this.strBusqueda.length >= 2){
    for(var i = 0 ; i < this.arrayPlanes.length ; i++){
      
      var nombre = this.arrayPlanes[i].planDescripcion;
     if(nombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
      this.arrayPlanesShow.push({
        planId: this.arrayPlanes[i].planId, 
        planDescripcion: this.arrayPlanes[i].planDescripcion, 
        planImporte : this.arrayPlanes[i].planImporte,
         disciplinaId : this.arrayPlanes[i].disciplinaId,
        planRecurrencia: this.arrayPlanes[i].planRecurrencia, 
        planActivo: this.arrayPlanes[i].planActivo,
      isActive: this.calcularBooleano(this.arrayPlanes[i].planActivo)
      }
      );
     }
    }
  }else{
    this.arrayPlanesShow = this.arrayPlanes;
  }
  
}    







}


  
  

