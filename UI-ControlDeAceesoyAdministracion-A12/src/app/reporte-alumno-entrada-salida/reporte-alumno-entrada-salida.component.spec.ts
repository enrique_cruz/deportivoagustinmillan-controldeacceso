import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAlumnoEntradaSalidaComponent } from './reporte-alumno-entrada-salida.component';

describe('ReporteAlumnoEntradaSalidaComponent', () => {
  let component: ReporteAlumnoEntradaSalidaComponent;
  let fixture: ComponentFixture<ReporteAlumnoEntradaSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAlumnoEntradaSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAlumnoEntradaSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
