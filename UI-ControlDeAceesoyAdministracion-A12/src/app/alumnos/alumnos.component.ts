import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ServicesService } from '../services/services.service';
import { ActivatedRoute } from '@angular/router';

import tarjeta from '../../assets/images/pdfimg/card.json';
//import cexoneración from '../../assets/images/pdfimg/carta-exoneración-22.json'
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import { DomSanitizer } from '@angular/platform-browser';
import iconpeople from '../../assets/images/pdfimg/iconpeople.json';
import { JsonpClientBackend } from '@angular/common/http';
import { JSDocTagName } from '@angular/compiler/src/output/output_ast';
import { result } from 'lodash';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css']
})
export class AlumnosComponent implements OnInit {
  
  @ViewChild("video",{ static: false })
  video!:  ElementRef;
  stream :any;
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  todaysdate : any;   
  contPorcentaje = 0;
  mesesPorPagar = 1;
  
  importeCobrar = 0;
  
  
  showImg : any;
  
  showImgtutor : any;
  saldoFavor = 0;
  auxImg = "";
  auxImgTutor = "";
  idCamara = "";
  arrayCamaras: Array<any>= [];

  isEditable = true;
  
  isCaptured = false;
  
  
  showTabs = false;
  value = '';
  
  planSelected = 0;
  strBusqueda = "";
  idPlanEdit = 0;
  showEditPlan = false;

  visibleCancelarTutor = 1;

  importeDisciplinas = 0;
  importeInscripcion = 0;
  importeMultas = 0;
  fechaUltimoPago = "";
  fechaProxima = "";
  diasAtrasados = 0;
  subTotal = 0;
  porcentajeDePago = 100;
  porcentajeAnualidad = 100;
  pagoTotal = 0;
  habilitarButtonMensualidad = true;
  showButtonMensualidad = 0;

  montoFaltante = 0;
          
  disciplinaSelected = 0;
  alumno = {
  alumnoId :0,
  alumnoBase64Image : "",
  alumnoNombre :  "",
  alumnoApellidoPaterno : "",
  alumnoApellidoMaterno : "",
  alumnoCURP : "",
  alumnoFechaDeNacimiento : "",
  alumnoSexo : "",
  alumnoRutaFoto : "",
  
  alumnoTelefonoCasa: "",
  alumnoTelefonoCelular : "",
  alumnoCorreoElectronico: "",
  alumnoRedesSociales: "",
  alumnoPassword: "",
  alumnoCodigoPostal : 0,
  alumnoEstado: "",
  alumnoMunicipio: "",
  alumnoColonia: "",
  alumnoNumero: "",
  alumnoCalle: "",
  
  alumnoAfiliacionMedica : "",
  alumnoAlergias : "",
  alumnoPadecimientos : "",
  alumnoTipoDeSangre : "",
  alumnoEstatura : 0,
  alumnoPeso : 0,
  alumnoActivo : "Si",
  isActive : false,
  alumnoFolio : ""
 }
  
 arrayTutores:Array<{
  tutorId: string, 
  tutorNombre: string,
  tutorApellidoPaterno: string,
  tutorApellidoMaterno: string,
  tutorParentesco: string,
  tutorBase64Image : ""
  }>=[];   
  /*
  arrayPlanesHorarios:Array<{
    plan: string,
    horarios : any
    }>=[];   */
 

  arrayAlumnosResultados:Array<{
    alumnoId: number,
    alumnoNombre: string, 
    alumnoFechaDeNacimiento: string,
    alumnoCURP: string
    }>=[];   
      
    arrayAsentamientos:Array<{nombre: string, tipo: string, cadena: string}>=[];
  
    
 tutor = {
   tutorId: 0,
  tutorNombre: "", 
  tutorApellidoPaterno: "", 
  tutorApellidoMaterno: "",
  tutorParentesco: "",
  tutorTelefonoCasa: "",
  tutorTelefonoCelular: "",
  tutorCorreoElectronico: "",
  tutorActivo:"",
  tutorBase64Image : ""
  }
        
  arrayHorariosLunes: Array<{
    horarioId: number,
    horarioNombre: string,
    horarioInicio: string,
    horarioFin: string,
    horarioDias: string,
    horarioDisponibilidad: number   
 }>=[];   
 arrayHorariosMartes: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosMiercoles: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[];
 arrayHorariosJueves: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosViernes: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosSabado: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[]  ;
 
 arrayHorariosDomingo: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[]   
 
 
 idHorarioLunesSelected = 0;
idHorarioMartesSelected = 0;
idHorarioMiercolesSelected = 0;
idHorarioJuevesSelected = 0;
idHorarioViernesSelected = 0;
idHorarioSabadoSelected = 0;
idHorarioDomingoSelected = 0;


enableLunes = true;
enableMartes = true;
enableMiercoles = true;
enableJueves = true;
enableViernes = true;
enableSabado = true;
enableDomingo = true;


conservarLunes = false;
conservarMartes = false;;
conservarMiercoles = false;
conservarJueves = false;
conservarViernes = false;
conservarSabado = false;
conservarDomingo = false;

habilitarLunes = true;
habilitarMartes = true;
habilitarMiercoles = true;
habilitarJueves = true;
habilitarViernes = true;
habilitarSabado = true;
habilitarDomingo = true;



planHorarioEditar = {
  planId:0 ,
  planDescripcion : "",
  planRecurrencia: 0,
  planImporte : 0,
  disciplinaId:0,
  disciplinaNombre:"",
  horario:[] as any
}

planHorario = {
  actual: 0,
  planId:0 ,
  planDescripcion : "",
  planRecurrencia: 0,
  planImporte : 0,
  disciplinaId:0,
  disciplinaNombre:"",
  horario:[] as any
}

arrayPlanesDisciplina : Array<{
  planId:number,
  disciplinaId:number,
  disciplinaNombre:string,
  planDescripcion: string,
  planRecurrencia: number,
  planImporte: number,
  }>= [];
  
  arrayhorariosSelected: Array<any>=[];

  arrayHorarios: Array<{horarioId: number; horarioNombre: string;}>= [];
  arrayDisciplinas: Array<{disciplinaId: number; disciplinaNombre: string;}>= [];
  
  montoParcialidad1 = 0;
  montoParcialidad2 = 0;
  montoParcialidad3 = 0;
  
  tipoParcialidad1 = "";
  tipoParcialidad2 = "";
  tipoParcialidad3 = "";

  folioParcialidad1 = "";
  folioParcialidad2 = "";
  folioParcialidad3 = "";
  
  fechaPagoParcialidad1 = "";
  fechaPagoParcialidad2 = "";
  fechaPagoParcialidad3 = "";
  
  cuentaParcialidad1 = "";
  cuentaParcialidad2 = "";
  cuentaParcialidad3 = "";
  
  conceptoParcialidad1 = "";
  conceptoParcialidad2 = "";
  conceptoParcialidad3 = "";
  
  comentariosParcialidad1 = "";
  comentariosParcialidad2 = "";
  comentariosParcialidad3 = "";
  
  
  constructor( config: NgbModalConfig, 

    private sanitizer: DomSanitizer,
    private httpServices: ServicesService,
    private modalService: NgbModal, private route: ActivatedRoute) { 
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    +iconpeople.people);
    
    this.showImgtutor = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    +iconpeople.people);
    
   config.backdrop = 'static';
   config.keyboard = false;
   this.getMediaDevices();
   this.montoParcialidad1 = this.permisos.value!
   this.montoParcialidad2 = this.permisos.value!
   this.montoParcialidad3 = this.permisos.value!
   this.importeCobrar = this.permisos.value!;
   
   this.todaysdate= new Date();
  }
    
   

  ngOnInit(): void {

  }

  validaCelulcar()
  {
    if(this.alumno.alumnoTelefonoCelular=="")
    {
      alert("El telefono celular es obligatorio")
    }
    else
    {
      if(this.alumno.alumnoTelefonoCelular.length<10)
      {
        alert("El telefono celular debe ser a 10 digitos")
      }
    }

  }

  validaCelulcarT()
  {
    if(this.tutor.tutorTelefonoCelular=="")
    {
      alert("El telefono celular es obligatorio")
    }
    else
    {
      if(this.tutor.tutorTelefonoCelular.length<10)
      {
        alert("El telefono celular debe ser a 10 digitos")
      }
    }

  }

  validaCorreoT()
  {
    if(this.tutor.tutorCorreoElectronico !="")
    {
      if(!this.isEmail(this.tutor.tutorCorreoElectronico))
      {
        alert("El correo no tiene un formato válido!");
      }
    }
    else
    {
      alert("El correo es un campo obligatorio");
    }
    
  } 

  validaCorreo()
  {
    if(this.alumno.alumnoCorreoElectronico !="")
    {
      if(!this.isEmail(this.alumno.alumnoCorreoElectronico))
      {
        alert("El correo no tiene un formato válido!");
      }
    }
    else
    {
      alert("El correo es un campo obligatorio");
    }
    
  } 

  isEmail(email:string)
  {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);  

  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }  

  
  /*
  getAlumno(){
    this.httpServices.getAlumnos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayAlumnos = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          if(!this.validarInsertAlumno(jsonDatos[i].alumnoId)){
            this.arrayAlumnos.push({ 
              alumnoId : jsonDatos[i].alumnoId,
            alumnoNombre: jsonDatos[i].alumnoNombre, 
            alumnoApellidoPaterno : jsonDatos[i].alumnoApellidoPaterno,
            alumnoApellidoMaterno : jsonDatos[i].alumnoApellidoMaterno,
            alumnoDisciplina :  jsonDatos[i].disciplinaNombre,
            alumnoCURP: jsonDatos[i].alumnoCURP,
          alumnoHorario : jsonDatos[i].horarioNombre});   
          } 
        }
      });
  }*/
  
  
  
  
  /*validarInsertAlumno(id: any){
    for(var i = 0 ; i < this.arrayAlumnos.length ; i++){
      if(this.arrayAlumnos[i].alumnoId === id){
        return true;
      }
    }
    return false;
  }*/
  
  
  
  
  
  
  busquedaAlumno(newObj: any){
    console.log(newObj);
  //console.log(this.strBusqueda);
    if(this.strBusqueda.length >= 3){
      this.httpServices.searchAlumnos(this.strBusqueda).subscribe(
        datos => {
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
           this.arrayAlumnosResultados = [];
           for(var i = 0 ; i < jsonDatos.length; i++ ){
              this.arrayAlumnosResultados.push({ 
              alumnoId : jsonDatos[i].alumnoId,
              alumnoNombre: jsonDatos[i].nombre, 
              alumnoFechaDeNacimiento :  jsonDatos[i].alumnoFechaDeNacimiento.toString().split("T")[0],
              alumnoCURP: jsonDatos[i].alumnoCURP});   
            } 
          
        });
    }else{
      this.arrayAlumnosResultados = [];
      
    }
   
  }
 
  
  selectedAlumno(){
    console.log("select");
    console.log(this.strBusqueda);
    
    for(var i = 0 ; i < this.arrayAlumnosResultados.length; i++){
      if(this.strBusqueda === this.arrayAlumnosResultados[i].alumnoCURP){
        this.alumno.alumnoId = this.arrayAlumnosResultados[i].alumnoId;
        this.alumno.alumnoCURP = this.strBusqueda;
        this.alumno.alumnoFechaDeNacimiento = this.arrayAlumnosResultados[i].alumnoFechaDeNacimiento;
        this.arrayAlumnosResultados = [];
        break;
      }
    }
    
    this.showTabs = true;
    this.getAlumnoById();
    this.getTutoresAlumno();
    this. getPlanAlumno();
    this.getDisciplinas();
    //this.getMensualidadesAlumno();
    this.strBusqueda = "";
    this.arrayAlumnosResultados = [];
  
  }
  
  
  getAlumnoById(){
    this.httpServices.getAlumnoById( this.alumno.alumnoId).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.alumno.alumnoAfiliacionMedica = jsonDatos[0].alumnoAfiliacionMedica;
         this.alumno.alumnoAlergias = jsonDatos[0].alumnoAlergias;
         this.alumno.alumnoNombre = jsonDatos[0].alumnoNombre;
         this.alumno.alumnoApellidoPaterno = jsonDatos[0].alumnoApellidoPaterno;
         this.alumno.alumnoApellidoMaterno = jsonDatos[0].alumnoApellidoMaterno;
         this.alumno.alumnoBase64Image = jsonDatos[0].alumnoBase64ImgaeString;
                  
          if(this.alumno.alumnoBase64Image != "" && this.alumno.alumnoBase64Image != null){
            this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
            +this.alumno.alumnoBase64Image);
          }else{
            console.log("no asigne la foto por que es "+this.alumno.alumnoBase64Image);
          }
         this.alumno.alumnoAfiliacionMedica = jsonDatos[0].alumnoAfiliacionMedica;
         this.alumno.alumnoCalle = jsonDatos[0].alumnoCalle;   
         this.alumno.alumnoCodigoPostal = jsonDatos[0].alumnoCodigoPostal;
         this.alumno.alumnoColonia = jsonDatos[0].alumnoColonia;
         this.alumno.alumnoCorreoElectronico = jsonDatos[0].alumnoCorreoElectronico;
         this.alumno.alumnoEstado = jsonDatos[0].alumnoEstado;
         this.alumno.alumnoEstatura = jsonDatos[0].alumnoEstatura;
         this.alumno.alumnoMunicipio = jsonDatos[0].alumnoMunicipio;
         this.alumno.alumnoNumero = jsonDatos[0].alumnoNumero;
         this.alumno.alumnoPadecimientos = jsonDatos[0].alumnoPadecimientos;
         this.alumno.alumnoPeso = jsonDatos[0].alumnoPeso;
         this.alumno.alumnoRedesSociales = jsonDatos[0].alumnoRedesSociales;
         this.alumno.alumnoSexo = jsonDatos[0].alumnoSexo;
         this.alumno.alumnoTelefonoCasa = jsonDatos[0].alumnoTelefonoCasa;
         this.alumno.alumnoTelefonoCelular = jsonDatos[0].alumnoTelefonoCelular;
         this.alumno.alumnoTipoDeSangre = jsonDatos[0].alumnoTipoDeSangre; 
         this.alumno.alumnoActivo = jsonDatos[0].alumnoActivo;
         this.alumno.alumnoFolio = jsonDatos[0].Folio;
         
         this.alumno.isActive = this.calcularBooleano(jsonDatos[0].alumnoActivo.toUpperCase());

      });
  }
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      
      return false;
    }
  }
  
   getTutoresAlumno(){
      this.httpServices.getTutoresByIdAlumno(this.alumno.alumnoId).subscribe(
        datos => {
          console.log("Tutores");
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
           this.arrayTutores = [];
           for(var i = 0 ; i < jsonDatos.length; i++ ){
              this.arrayTutores.push({ 
              tutorId : jsonDatos[i].tutorId,
              tutorNombre: jsonDatos[i].tutorNombre, 
              tutorApellidoMaterno :  jsonDatos[i].tutorApellidoMaterno,
              tutorParentesco :  jsonDatos[i].tutorParentesco,
              tutorApellidoPaterno :  jsonDatos[i].tutorApellidoPaterno,
              tutorBase64Image : "",
            });   
            } 
        });
  }
  
  getPlanAlumno(){
    this.httpServices.getPlanesByAlumno(this.alumno.alumnoId).subscribe(
      datos => {
        console.log("getPlanAlumno*#*#*#")
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayhorariosSelected = [];
        for(var i = 0 ; i < jsonDatos.length ;i++ ){

          if(jsonDatos[i].disciplinaNombre==="Natación" || jsonDatos[i].disciplinaNombre==="Natacion" || jsonDatos[i].disciplinaNombre==="natación" || jsonDatos[i].disciplinaNombre==="natacion")
          {   
            //FIX JOSE ENRIQUE CRUZ PERALES 15/02/2022  
            let results : any = this.arrayhorariosSelected.find
                          (
                            it => it.planDescripcion == jsonDatos[i].planDescripcion &&
                                 it.hi == jsonDatos[i].horarioInicio                            
                          );
            // console.log("Resultado del elemento buscado");
            // console.log(results);

            if(results=== undefined)
            {
              this.arrayhorariosSelected.push(
              {               
                actual: 1,
                alumnoId:this.alumno.alumnoId,
                planId: jsonDatos[i].planId ,
                planDescripcion : jsonDatos[i].planDescripcion,
                planRecurrencia: jsonDatos[i].planRecurrencia,
                planImporte : jsonDatos[i].planImporte,
                disciplinaId: jsonDatos[i].disciplinaId,
                disciplinaNombre: jsonDatos[i].disciplinaNombre,
                horario:[],
                hi:jsonDatos[i].horarioInicio
              });
            }
            
            // if(this.arrayhorariosSelected[i].planDescripcion === jsonDatos[i].planDescripcion)
            // {
            //   this.arrayhorariosSelected.push(
            //     {               
            //         actual: 1,
            //         alumnoId:this.alumno.alumnoId,
            //         planId: jsonDatos[i].planId ,
            //         planDescripcion : jsonDatos[i].planDescripcion,
            //         planRecurrencia: jsonDatos[i].planRecurrencia,
            //         planImporte : jsonDatos[i].planImporte,
            //         disciplinaId: jsonDatos[i].disciplinaId,
            //         disciplinaNombre: jsonDatos[i].disciplinaNombre,
            //         horario:[] 
            //     });
            // }                         

          }
          else
          {

            //INI ESTE ES EL PROCEDIMIENTO ORIGINAL ***********************************
            if(!this.insertarPlan(jsonDatos[i].planDescripcion))
            {
              this.arrayhorariosSelected.push(
              {               
                  actual: 1,
                  alumnoId:this.alumno.alumnoId,
                  planId: jsonDatos[i].planId ,
                  planDescripcion : jsonDatos[i].planDescripcion,
                  planRecurrencia: jsonDatos[i].planRecurrencia,
                  planImporte : jsonDatos[i].planImporte,
                  disciplinaId: jsonDatos[i].disciplinaId,
                  disciplinaNombre: jsonDatos[i].disciplinaNombre,
                  horario:[] 
              });
            }
            //FN |ESTE ES EL PROCEDIMIENTO ORIGINAL ***********************************
          }            
        }        

        //FIX. José Enrique Cruz Perales
        this.arrayhorariosSelected.forEach( function (plan){
          let horarios : any;
          if(plan.disciplinaNombre==="Natación" || plan.disciplinaNombre==="Natacion" || plan.disciplinaNombre==="natación" || plan.disciplinaNombre==="natacion")
          {            
              horarios = jsonDatos.filter(
                  (h : any) => h.planDescripcion === plan.planDescripcion &&
                              h.horarioInicio === plan.hi                            
              );
          }
          else
          {
              horarios = jsonDatos.filter(
                  (h : any) => h.planDescripcion === plan.planDescripcion                               
              );
          }           
        
          if(horarios!=undefined)
          {
            plan.horario = horarios;
          }

        });

        
        // for(var i= 0; i < this.arrayhorariosSelected.length; i++)
        // {
        //   for(var j = 0 ; j < jsonDatos.length ; j++)
        //   {

        //     // FIX 15-02-2022 José Enrique Cruz Pe4ales
        //     if(this.arrayhorariosSelected[i].disciplinaNombre==="Natación" || this.arrayhorariosSelected[i].disciplinaNombre==="Natacion" || this.arrayhorariosSelected[i].disciplinaNombre==="natación" || this.arrayhorariosSelected[i].disciplinaNombre==="natacion" )
        //     {
        //       this.arrayhorariosSelected.forEach( function (plan){

        //         let horarios : any = jsonDatos.filter(
        //           (h : any) => h.planDescripcion === plan.planDescripcion &&
        //                        h.horarioInicio === plan.hi
        //         );
      
        //         console.log("Horarios encontrados");
        //         console.log(horarios);
      
        //         if(horarios!=undefined)
        //         {
        //           plan.horario = horarios;
        //         }
      
        //       });               

        //     }  
        //     else            
        //     {
        //       if(this.arrayhorariosSelected[i].planDescripcion === jsonDatos[j].planDescripcion)
        //       {
        //         if(!this.insertarHorario(jsonDatos[j].horarioDias, i))
        //         {                  
        //           this.arrayhorariosSelected[i].horario.push
        //           (
        //             {
        //               horarioId: jsonDatos[j].horarioId,
        //               horarioDias: jsonDatos[j].horarioDias,
        //               horarioInicio : jsonDatos[j].horarioInicio,
        //               horarioFin : jsonDatos[j].horarioFin,
        //               horarioNombre : jsonDatos[j].horarioNombre,
        //             }
        //           );                  
        //         }
        //       }
        //     }

        //     //ORIGINAL
        //     // if(this.arrayhorariosSelected[i].planDescripcion === jsonDatos[j].planDescripcion)
        //     // {
        //     //     if(!this.insertarHorario(jsonDatos[j].horarioDias, i))
        //     //     {                  
        //     //       this.arrayhorariosSelected[i].horario.push
        //     //       (
        //     //         {
        //     //           horarioId: jsonDatos[j].horarioId,
        //     //           horarioDias: jsonDatos[j].horarioDias,
        //     //           horarioInicio : jsonDatos[j].horarioInicio,
        //     //           horarioFin : jsonDatos[j].horarioFin,
        //     //           horarioNombre : jsonDatos[j].horarioNombre,
        //     //         }
        //     //       );                  
        //     //     }
        //     // }

        //   }
        // }
        console.log("Format");
        console.log(  this.arrayhorariosSelected);                        
      });
  }
  
  
  getTutor(json: any){
    this.visibleCancelarTutor = 0;
    this.httpServices.getTutorById(json.tutorId).subscribe(
      datos => {

        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.tutor.tutorId = jsonDatos[0].tutorId;
         this.tutor.tutorNombre = jsonDatos[0].tutorNombre;
         this.tutor.tutorApellidoPaterno = jsonDatos[0].tutorApellidoPAterno;
         this.tutor.tutorApellidoMaterno = jsonDatos[0].tutorApellidoMaterno;
         this.tutor.tutorParentesco = jsonDatos[0].tutorParentesco;
         this.tutor.tutorTelefonoCasa = jsonDatos[0].tutorTelefonoCasa;
         this.tutor.tutorTelefonoCelular = jsonDatos[0].tutorTelefonoCelular;
         this.tutor.tutorCorreoElectronico = jsonDatos[0].tutorCorreoElectronico;
         //TODO validar foto y setearla
         if(jsonDatos[0].tutorBase64Image == ""){
          this.showImgtutor = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +iconpeople.people);
         }else{
          this.tutor.tutorBase64Image = jsonDatos[0].tutorBase64Image;
          this.showImgtutor = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +this.tutor.tutorBase64Image);
         }                                   
      });
}
  

deleteTutor(json: any){
  
  if(this.arrayTutores.length == 1){
    alert("Debes de dejar al menos un contacto de emergencia");
  }else{
    json ={
      "tutorId": json.tutorId,
      "tutorActivo": "No"
    }
    
    this.httpServices.deleteTutorById(json).subscribe(
      datos => {
  
        console.log(datos);
        if(datos == 1){
          this.getTutoresAlumno();
        }
        else{
          alert("Ocurrio un error");
        }  
      });
  
  }
 
}
  
  insertarPlan(des: string){
    for(var i  = 0; i < this.arrayhorariosSelected.length ; i++){
      if(this.arrayhorariosSelected[i].planDescripcion === des){
        return true;
      }
    }
    return false;
  }

  insertarPlanNatacion(des: string){
    for(var i  = 0; i < this.arrayhorariosSelected.length ; i++){
      if(this.arrayhorariosSelected[i].planDescripcion === des){
        return true;
      }
    }
    return false;
  }
    
  insertarHorario(dia: any, id: number){
    for(var i  = 0; i < this.arrayhorariosSelected[id].horario.length ; i++){
      if(this.arrayhorariosSelected[id].horario[i].horarioDias === dia){
        return true;
      }
    }
    return false;
  }
  
  
  getInfoCp(){
    //console.log("hola desde busqueda de cp con "+this.codePostal+" "+this.codePostal.toString().length);
    var strCP = this.alumno.alumnoCodigoPostal;
    if(strCP.toString().length >= 4){
      this.httpServices.getInfoCodePostal(strCP.toString()).subscribe(
        datos => {
          var json = JSON.parse(JSON.stringify(datos));
          this.arrayAsentamientos = [];
            console.log(json+" esto en json "+JSON.stringify(json));
            for(var i = 0 ; i < json.length; i++){
                let jsonAsentamiento = json[i];
                this.alumno.alumnoMunicipio = jsonAsentamiento.Municipio;
                this.alumno.alumnoEstado = jsonAsentamiento.Estado;
                //console.log(i+" "+jsonAsentamiento);
                this.arrayAsentamientos.push({nombre: jsonAsentamiento.Colonia,tipo: jsonAsentamiento.Tipo_Asentamiento, cadena: jsonAsentamiento.Colonia +" - "+jsonAsentamiento.Tipo_Asentamiento });
            }
        },
        error => {
         
            this.arrayAsentamientos = [];
          
        }
        
        
        );
    }else{
      this.arrayAsentamientos = [];
   
    }
 }
 
 insertDomingo(){
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioDomingoSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosDomingo.length ; i++){
        if(this.arrayHorariosDomingo[i].horarioId == this.idHorarioDomingoSelected){
 
      //   this.arrayhorariosSelected.push(this.arrayHorariosDomingo[i]);
         
          this.planHorario.horario.push(this.arrayHorariosDomingo[i]);
          
        }
      }
    }
  }else{
    //aqui inhabilitar el select
  
    this.idHorarioDomingoSelected = 0;
  }
  
}

insertLunes(){
  console.log("");
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioLunesSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosLunes.length ; i++){
        if(this.arrayHorariosLunes[i].horarioId == this.idHorarioLunesSelected){
       
             
          this.planHorario.horario.push(this.arrayHorariosLunes[i]);
        }
      }
    }
  }else{

    
    this.idHorarioLunesSelected = 0;
  }
 
}

insertMartes(){
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioMartesSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosMartes.length ; i++){
        if(this.arrayHorariosMartes[i].horarioId == this.idHorarioMartesSelected){
        
          this.planHorario.horario.push(this.arrayHorariosMartes[i]);
        }
      }
    }
  }else{

    
    this.idHorarioMartesSelected = 0;
  }
}

insertMiercoles(){
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioMiercolesSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosMiercoles.length ; i++){
        if(this.arrayHorariosMiercoles[i].horarioId == this.idHorarioMiercolesSelected){
       
          this.planHorario.horario.push(this.arrayHorariosMiercoles[i]);
        }
      }
    }
  }else{

    
    this.idHorarioMiercolesSelected = 0;
  }
 
}

insertJueves(){
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioJuevesSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosJueves.length ; i++){
        if(this.arrayHorariosJueves[i].horarioId == this.idHorarioJuevesSelected){
          this.planHorario.horario.push(this.arrayHorariosJueves[i]);

        }
      }
    } 
  }else{
   
    this.idHorarioJuevesSelected = 0;
  }
  
}

insertViernes(){

  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioViernesSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosViernes.length ; i++){
        if(this.arrayHorariosViernes[i].horarioId == this.idHorarioViernesSelected){
          this.planHorario.horario.push(this.arrayHorariosViernes[i]);
        }
      }
    }
  }else{

    
    this.idHorarioViernesSelected = 0;
  }
 
}
insertSabado(){
  if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
    if(this.idHorarioSabadoSelected != 0){
      for(var i = 0 ; i < this.arrayHorariosSabado.length ; i++){
        if(this.arrayHorariosSabado[i].horarioId == this.idHorarioSabadoSelected){
          this.planHorario.horario.push(this.arrayHorariosSabado[i]);
          
        } 
      }
    }
  }else{

    this.idHorarioSabadoSelected = 0;
  }
}
 
 
  
 validarClasesPlan()
 {
   this.enableDomingo = true;
   this.enableLunes = true;
   this.enableMartes = true;
   this.enableMiercoles = true;
   this.enableJueves = true;
   this.enableViernes = true;
   this.enableSabado = true;
   
  this.planHorario.horario = [];
  
  this.listenerLunes();
  this.listenerMartes();
  this.listenerMiercoles();
  this.listenerJueves();
  this.listenerViernes();
  this.listenerSabado();
  this.listenerDomingo();
  
  this.insertLunes();
  this.insertMartes();
  this.insertMiercoles();
  this.insertJueves();
  this.insertViernes();
  this.insertSabado();
  this.insertDomingo();
  
  console.log(this.planHorario.horario);
  if(this.planHorario.horario.length == this.planHorario.planRecurrencia){
    if(this.idHorarioLunesSelected == 0 || this.conservarLunes){
     this.enableLunes = false;
    }
    if(this.idHorarioMartesSelected == 0 || this.conservarMartes){
      this.enableMartes = false;
     }
    
    if(this.idHorarioMiercolesSelected == 0 || this.conservarMiercoles){
     this.enableMiercoles = false;
    }
    if(this.idHorarioJuevesSelected == 0 || this.conservarJueves){
     this.enableJueves = false;
    }
    if(this.idHorarioViernesSelected == 0 || this.conservarViernes){
     this.enableViernes = false;
    }
    if(this.idHorarioSabadoSelected == 0 || this.conservarSabado){
     this.enableSabado = false;
    }
    if(this.idHorarioDomingoSelected == 0 || this.conservarDomingo){
     this.enableDomingo = false;
    }
  }
 }
 
 
 getHorarioBydisciplina(){
  //this.arrayhorariosSelected = [];
  this.getPlanByIdJson();
  
  this.idHorarioDomingoSelected = 0 ;
  this.idHorarioLunesSelected = 0 ;
  this.idHorarioMartesSelected = 0 ;
  this.idHorarioMiercolesSelected = 0 ;
  this.idHorarioJuevesSelected = 0 ;
  this.idHorarioViernesSelected = 0 ;
  this.idHorarioSabadoSelected = 0 ;
  
  this.enableDomingo = true;
  this.enableLunes = true;
  this.enableMartes = true;
  this.enableMiercoles = true;
  this.enableJueves = true;
  this.enableViernes = true;
  this.enableSabado = true;
  
  this.arrayHorariosLunes = [];
  this.arrayHorariosMartes = [];
  this.arrayHorariosMiercoles = [];
  this.arrayHorariosJueves = [];
  this.arrayHorariosViernes = [];
  this.arrayHorariosSabado = [];
  this.arrayHorariosDomingo = [];
  
  console.log(this.planSelected);
  
  if(this.planSelected != 0){
    
    var json = {
      "disciplinaId" : this.disciplinaSelected,
      "edad" :  parseInt(this.calcularFdad()+"", 10) ,
      "sexo" : this.alumno.alumnoSexo
    }
    // var json = {
    //   "disciplinaId" : this.disciplinaSelected,
    //   "edad" :  12,
    //   "sexo" : "Masculino"
    // } 
    
    
    this.httpServices.getHorarioAforoById(json).subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
    
         
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
           
          var json = { horarioId: jsonDatos[i].horarioId,
                    horarioNombre: jsonDatos[i].horarioNombre,
                    horarioInicio: jsonDatos[i].horarioInicio,
                    horarioFin: jsonDatos[i].horarioFin,
                    horarioDias: jsonDatos[i].horarioDias,
                    horarioDisponibilidad: jsonDatos[i].Disponibilidad  
          }
          switch(json.horarioDias){
            case "Lunes": 
            this.arrayHorariosLunes.push(json);
            break;
            case "Martes": 
            this.arrayHorariosMartes.push(json);
            break;
            case "Miercoles": 
            this.arrayHorariosMiercoles.push(json);
            break;
            case "Jueves": 
            this.arrayHorariosJueves.push(json);
            break;
            case "Viernes": 
            this.arrayHorariosViernes.push(json);
            break;
            case "Sabado": 
            this.arrayHorariosSabado.push(json);
            break;
            case "Domingo": 
            this.arrayHorariosDomingo.push(json);
            break;
          }

          
          /*this.arrayHorarios.push({ 
          horarioId: jsonDatos[i].horarioId, 
          horarioNombre : jsonDatos[i].horarioNombre+" "+jsonDatos[i].horarioInicio+" - "+jsonDatos[i].horarioFin
        });   */
        }
      });
  }
  
}


getPlanByIdJson(){
  if(this.planSelected ==0){
    this.planHorario.planId = 0;
    this.planHorario.planImporte = 0;
    this.planHorario.planRecurrencia = 0;
    this.planHorario.disciplinaId = 0 ,
    this.planHorario.horario = [];
    this.planHorario.planDescripcion = "";
    
     
    
  }else{
    for(var i = 0 ; i < this.arrayPlanesDisciplina.length ; i++){
      if(this.arrayPlanesDisciplina[i].planId == this.planSelected){
        console.log(this.arrayPlanesDisciplina[i]);
        this.planHorario.planId = this.arrayPlanesDisciplina[i].planId;
        this.planHorario.planImporte = this.arrayPlanesDisciplina[i].planImporte;
        console.log(this.planHorario.planImporte+" "+this.arrayPlanesDisciplina[i].planImporte);
        this.planHorario.planRecurrencia = this.arrayPlanesDisciplina[i].planRecurrencia;          
        this.planHorario.planDescripcion = this.arrayPlanesDisciplina[i].planDescripcion;
        this.planHorario.disciplinaId = this.disciplinaSelected;
        
        this.planHorario.horario = [];
    }
  }
}
}

calcularFdad(){
  console.log(this.alumno.alumnoFechaDeNacimiento);
  var nacimeinto = new Date(this.alumno.alumnoFechaDeNacimiento+" 00:00");
  var hoy = new Date();
  console.log(hoy+" "+nacimeinto);
  var edad = hoy.getTime()- (nacimeinto.getTime() + 1000*3600);
  edad = edad/ (1000*60*60*24*365.25);
  console.log("la edad es "+edad );
  return edad
 /* if(edad < 18){
     this.tutores = true;
  }else{
   this.tutores = false;
  }*/
  
} 



getPlanesByDisciplina(){
  this.planSelected = 0;
  
  this.arrayHorarios = [];
  this.arrayHorariosLunes = [];
  this.arrayHorariosMartes = [];
  this.arrayHorariosMiercoles = [];
  this.arrayHorariosJueves = [];
  this.arrayHorariosViernes = [];
  this.arrayHorariosSabado = [];
  this.arrayHorariosDomingo = [];

  this.enableLunes = true;
  this.enableMartes = true;
  this.enableMiercoles = true;
  this.enableJueves = true;
  this.enableViernes = true;
  this.enableSabado = true;
  this.enableDomingo = true;

  
  
  
  this.httpServices.getPlanesByDisciplina(this.disciplinaSelected).subscribe(
    datos => {
      console.log(datos);
       var jsonDatos = JSON.parse(JSON.stringify(datos));
       this.arrayPlanesDisciplina = [];
       for(var i = 0 ; i < jsonDatos.length; i++ ){
        this.arrayPlanesDisciplina.push({ 
        planId: jsonDatos[i].planId, 
        disciplinaId: jsonDatos[i].disciplinaId, 
        disciplinaNombre: jsonDatos[i].disciplinaNombre, 
        planDescripcion : jsonDatos[i].planDescripcion,
        planRecurrencia: jsonDatos[i].planRecurrencia,
        planImporte : jsonDatos[i].planImporte 
      });
      }
    });
}


  
getDisciplinas(){
  this.httpServices.getDisciplinasActive().subscribe(
    datos => {
      console.log(datos);
       var jsonDatos = JSON.parse(JSON.stringify(datos));
       this.arrayDisciplinas = [];
       for(var i = 0 ; i < jsonDatos.length; i++ ){
        this.arrayDisciplinas.push({ 
        disciplinaId: jsonDatos[i].disciplinaId, 
        disciplinaNombre : jsonDatos[i].disciplinaNombre });   
      }
    });
}

  //array estaticos
  arrayAlergiaMedica = [
    "NINGUNA",
    "ÁCIDO ACETILSALICÍLICO(ASPIRINA)",
    "IBUPROFENO",
    "METAMIZOL SÓDICO",
    "PARACETAMOL",
    "BUPRENORFINA",
    "CAPSAICINA",
    "CLONIXINATO DE LISINA",
    "DEXMEDETOMIDINA",
    "DEXTROPROPOXIFENO",
    "ETOFENAMATO",
    "FENTANILO",
    "HIDROMORFONA",
    "KETOROLACO",
    "METADONA",
    "MORFINA",
    "NALBUFINA",
    "OXICODONA",
    "PARACETAMOL",
    "TAPENTADOL",
    "TRAMADOL",
    "PENICILINA",
    "DICLOFENACO",
    "SULFAS SULFONAMIDAS",
    "AMOXICILINA",
    "NAPROXENO",
    "ERITROMICINA", 
    "BETAMETASONA",
    "NEOMELUBRINA",
    "SALBUTAMOL",
    "AINES",
    "METFORMINA",
    "DEXAMETASONA",
    "CAPTOPRIL",
    "OMEPRAZOL",
    "SUBSALICILATO DE BISMUTO (PEPTOBISMOL)",
    "YODO",
    "CEFALOSPORINAS",
    "BICARBONATO DE SODIO" 
  ];
  arrayAfiliacionesMedicas = [
      "NINGUNA",
      "IMSS",
      "ISSEMYM",
      "ISSFAM",
      "ISSSTE",
      "SEGURO POPULAR",
      "PEMEX",
      "PRIVADA",
      "OTRO"
  ];
  arrayParentesco = [
        "ABUELO(A)",
        "CUÑADO(A)",
        "ESPOSO(A)",
        "HERMANO(A)",
        "HIJO(A)",
        "MAMÁ",
        "NIETO(A)",
        "NUERA",
        "PAPÁ",
        "PRIMO(A)",
        "SOBRINO(A)",
        "SUEGRO(A)",
        "TÍO (A)",
        "YERNO",
        "OTRO"
  ];
  arrayTipoSanguineo = [
    "O negativo",
    "O positivo",
    "A negativo",
    "A positivo",
    "B negativo",
    "B positivo",
    "AB negativo",
    "AB positivo"
  ];

  
  actualLunes = "";
  actualMartes = "";
  actualMiercoles = "";
  actualJueves = "";
  actualViernes = "";
  actualSabado = "";
  actualDomingo = "";

  
  
  btnCancelarEditar(){
    this.disciplinaSelected = 0;
    this.idPlanEdit = 0;
    this.planHorarioEditar.planId = 0;
    this.planHorarioEditar.horario = "";
    this.planHorarioEditar.planDescripcion = "";
    this.planHorarioEditar.planImporte = 0;
    this.planHorarioEditar.disciplinaId = 0;
    this.planHorarioEditar.planRecurrencia = 0;
    this.planHorarioEditar.disciplinaNombre = "";
    
    
    this.actualDomingo = "";
    this.actualLunes = "";
    this.actualMartes = "";
    this.actualMiercoles = "";
    this.actualJueves = "";
    this.actualViernes = "";
    this.actualSabado = "";
    this.showEditPlan = false;
  }
  
  
  btnEditar(json: any){
    console.log(json);
    let count = json.horario.length;
        
    this.disciplinaSelected = json.disciplinaId;;
    this.getPlanesByDisciplina();
    this.getHorarioBydisciplina();
    
    
    this.showEditPlan = true;

    this.planSelected = json.planId; 
    this.idPlanEdit = json.planId;
    this.planHorarioEditar.planId = json.planId;
    this.planHorarioEditar.horario = json.horario;
    this.planHorarioEditar.planDescripcion = json.planDescripcion;
    this.planHorarioEditar.planImporte = json.planImporte;
    this.planHorarioEditar.disciplinaId = json.disciplinaId;
    this.planHorarioEditar.planRecurrencia = json.planRecurrencia;
    this.planHorarioEditar.disciplinaNombre = json.disciplinaNombre;
    
    this.planHorario.planId = json.planId;
    this.planHorario.planDescripcion = json.planDescripcion;
    this.planHorario.planImporte = json.planImporte;
    this.planHorario.disciplinaId = json.disciplinaId;
    this.planHorario.planRecurrencia = json.planRecurrencia;
    this.planHorario.disciplinaNombre = json.disciplinaNombre;
    
   
    
    for(var i = 0 ; i <  this.planHorarioEditar.horario.length; i++ ){
      if(this.planHorarioEditar.horario[i].horarioDias === "Lunes"){
        this.actualLunes = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarLunes = false;
    
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Martes"){
        this.actualMartes = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarMartes = false;
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Miercoles"){
        this.actualMiercoles = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarMiercoles = false;
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Jueves"){
        this.actualJueves = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarJueves = false;
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Viernes"){
        this.actualViernes = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarViernes = false;
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Sabado"){
        this.actualSabado = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarSabado = false;
      }
      if(this.planHorarioEditar.horario[i].horarioDias === "Domingo"){
        this.actualDomingo = this.planHorarioEditar.horario[i].horarioNombre+ " "+this.planHorarioEditar.horario[i].horarioInicio
        this.habilitarDomingo = false;
      }
    }
  }
  
  listenerLunes(){
    if(this.conservarLunes){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Lunes"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioLunesSelected = 0;
            this.enableLunes =false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableLunes = true;
    }
}

  listenerMartes(){
    if(this.conservarMartes){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Martes"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioMartesSelected = 0;
            this.enableMartes = false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableMartes = true;
    }
}

  listenerMiercoles(){
    if(this.conservarMiercoles){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Miercoles"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioMiercolesSelected = 0;
            this.enableMiercoles = false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableMiercoles = true;
    }
}
  
  listenerJueves(){
    if(this.conservarJueves){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Jueves"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioJuevesSelected = 0;
            this.enableJueves = false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableJueves = true;
    }
}
  
  listenerViernes(){
    if(this.conservarViernes){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Viernes"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioViernesSelected = 0;
            this.enableViernes = false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableViernes = true;
    }
}
  
  
  listenerSabado(){
    if(this.conservarSabado){
      if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
        for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
          if(this.planHorarioEditar.horario[i].horarioDias == "Sabado"){
            this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
            this.idHorarioSabadoSelected = 0;
            this.enableSabado = false;
          }
        }
      }else{
        alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
      }
    }else{
      
      this.enableSabado = true;
    }
}

listenerDomingo(){
  if(this.conservarDomingo){
    if(this,this.planHorario.horario.length <= this.planHorario.planRecurrencia){
      for(var i =0 ; i < this.planHorarioEditar.horario.length; i++){
        if(this.planHorarioEditar.horario[i].horarioDias == "Domingo"){
          this.planHorario.horario.push(this.planHorarioEditar.horario[i]);
          this.idHorarioDomingoSelected = 0;
          this.enableDomingo = false;
        }
      }
    }else{
      alert("El horario solo puede tener "+this.planHorario.planRecurrencia);
    }
  }else{
    
    this.enableDomingo = true;
  }
}


btnAgregarEditado(){
  console.log("EDITANDO EL HORARIO DEL ALUMNO: ");
  console.log(this.alumno.alumnoId + " " + this.alumno.alumnoNombre + " " + this.alumno.alumnoApellidoPaterno );
  console.log("Plan horario =>")
  console.log(this.planHorario);

  console.log("arrayHorarioSelected");
  console.log(this.arrayhorariosSelected);

  if(this.planHorario.planRecurrencia ==  this.planHorario.horario.length)
  {
    //Eliminamos el plan editado
    for(var i = 0 ; i < this.arrayhorariosSelected.length ; i++)
    {
      if(this.arrayhorariosSelected[i].planId === this.planHorarioEditar.planId)
      {
        this.arrayhorariosSelected.splice(i,1);        
      }
    }

    this.arrayhorariosSelected.push({
      actual : 0,
      alumnoId: this.alumno.alumnoId,
      planId: this.planHorario.planId ,
      planDescripcion : this.planHorario.planDescripcion,
      planRecurrencia: this.planHorario.planRecurrencia ,
      planImporte : this.planHorario.planImporte ,
      disciplinaId: this.planHorario.disciplinaId,
      disciplinaNombre:  this.planHorario.disciplinaNombre,
      horario: this.planHorario.horario 
    });

    let arrNewSchedule : any = []

    for(var h of this.arrayhorariosSelected)
    {
      arrNewSchedule.push({
        alumnoId: h.alumnoId,
        planId: h.planId,
        //horario: h.horario.map(({horarioId: any}) => horarioId)
        horario: {
          horarioId: h.horario.map((h:any) => h.horarioId)
        }
      });      
    }

    console.log(arrNewSchedule);

    console.log("NUEVO HORARIO A ENVIAR");
    console.log(this.arrayhorariosSelected);

    this.httpServices.recreateAlumnoPlanHorario(this.arrayhorariosSelected).toPromise()
    .then(
      data => {
        console.log("***********    ESTATUS DEL HORARIO RECREADO...    *********");
        console.log(data);
        if(data>0)
        {
          alert("El horario fue actualizado con exito");
          this.actualLunes="";
          this.actualMartes="";
          this.actualMiercoles="";
          this.actualJueves="";
          this.actualViernes="";
          this.actualSabado="";
          this.actualDomingo="";

          console.log("Actualizacion exitosa: Array =>");
          console.log(this.arrayhorariosSelected);

          var planEdited = this.arrayhorariosSelected.find((x: any) => x.planId==this.planHorario.planId && x.horario.horarioDias==this.planHorario.horario.horarioDias && x.horario.horarioInicio==this.planHorario.horario.horarioInicio);

          if(planEdited != undefined)
          {
            planEdited.actual=1
          }
        }
        else
        {
          alert("El horario no se actualizo")
        }

      }
    )
    .catch(
      error => {
        alert("Hubo un error al actualizar el horario. ERROR:  " + JSON.stringify(error) );
      } 
    );
    
    this.planHorarioEditar.planId = 0;
    this.planHorarioEditar.horario = [];
    this.planHorarioEditar.planDescripcion = "";
    this.planHorarioEditar.planImporte = 0;
    this.planHorarioEditar.disciplinaId = 0;
    this.planHorarioEditar.planRecurrencia = 0;
    this.planHorarioEditar.disciplinaNombre = "";
    this.showEditPlan = false;
  }
  else{
    alert("Selecciona el numero de dias de clase");
  }
   
  
}

btnAgregarDisciplina(){
  if(this.planHorario.horario.length !=  this.planHorario.planRecurrencia){
    alert("Solo has agregado "+this.planHorario.horario.length+" clases a tu lpan")
  }else{
    console.log("#*#*#*#*#*#  ARGREGANDO NUEVA DISCIPLINA  #*#*#*#*#*#")
    console.log(this.planHorario);

    this.arrayhorariosSelected.push({      
      actual :0 ,
      alumnoId:this.alumno.alumnoId,
      planId: this.planHorario.planId ,
      planDescripcion : this.planHorario.planDescripcion,
      planRecurrencia: this.planHorario.planRecurrencia,
      planImporte : this.planHorario.planImporte,
      disciplinaId:this.planHorario.disciplinaId,
      horario: this.planHorario.horario        
    });

    console.log("Disciplina Seleccionada: *********************")
    console.log(this.arrayhorariosSelected);
    console.log("Para el alumno:");
    console.log(this.alumno.alumnoId +" - " + this.alumno.alumnoNombre + " " + this.alumno.alumnoApellidoPaterno +  " " + this.alumno.alumnoApellidoMaterno)

    this.httpServices.alumnoPlanHorarioCreate(this.arrayhorariosSelected).toPromise()
    .then(
      data => {
        if (data>0)
        {
          //GET THE LAST ITEM IN AN ARRAY
          let popped : any = this.arrayhorariosSelected.pop();          
          popped.actual=1;
          this.arrayhorariosSelected.push(popped);

          alert("Los nuevos horarios fueron insertados correctamente.");          
          this.resetDisciplinas();
        }
        else if(data==0)
        {
          alert("Todos o algunos de los horarios nuevos no se insertaron porque están repetidos. Revise e ingreselos hoarario que no se insertaron.");
        }
        else
        {
          alert(JSON.stringify(data));
        }
      }
    )
    .catch(
      error => {
        alert("Hubo un error al guardar los horarios. Favro conslta a tu administrador del sistema: " + JSON.stringify(error));
        this.resetDisciplinas();
      }
      
    );      
  }
  //this.resetDisciplinas();
}

btnQuitarPlan(plan: any, indice: number){
  console.log("Horario a eliminar... *#*#*#**#*#*#");
  console.log(plan + " " + indice);

  var arrAphDelete : any = [];
 
  for(let h of plan.horario)
  {
    arrAphDelete.push({
      alumnoId : plan.alumnoId,
      planId : plan.planId,
      horarioId : h?.horarioId
    })
  }

  console.log("Plan y horarios a eliminar*#*#*#*#*#*#");
  console.log(arrAphDelete);


  this.httpServices.alumnoPlanHorarioDelete(arrAphDelete).toPromise()
    .then
    (
      (data) => {
        if (data==1)
        {
          alert("Plan y horarios eliminados correctamente:");
          this.arrayhorariosSelected.splice(indice, 1);
        }
        else if(data == 2)
        {
          alert("El horario no pudo ser elimininado. Intenta de nuevo !!!");
        }
        else
        {
          alert(data);
        }
      }
    )
    .catch
    (
      (error) => {
        alert("ERROR : "  + JSON.stringify(error));
      }
    )
  
  
}

resetDisciplinas(){
  this.planSelected = 0;
  this.disciplinaSelected = 0;
  
  this.arrayHorarios = [];
  this.arrayHorariosLunes = [];
  this.arrayHorariosMartes = [];
  this.arrayHorariosMiercoles = [];
  this.arrayHorariosJueves = [];
  this.arrayHorariosViernes = [];
  this.arrayHorariosSabado = [];
  this.arrayHorariosDomingo = [];

  this.enableLunes = true;
  this.enableMartes = true;
  this.enableMiercoles = true;
  this.enableJueves = true;
  this.enableViernes = true;
  this.enableSabado = true;
  this.enableDomingo = true;

  this.planHorario.planId = 0;
    this.planHorario.planImporte = 0;
 
    this.planHorario.planRecurrencia = 0;
    this.planHorario.disciplinaId = 0 ;
    this.planHorario.planDescripcion = "";
    this.planHorario.horario = [];
}


btnguardartutor(){
  if(this.tutor.tutorNombre == ""){
   alert("Ingresa el nombre del tutor");
  }else if(this.tutor.tutorApellidoPaterno == ""){
   alert("Ingresa el apellido paterno");
  }else if(this.tutor.tutorApellidoMaterno == ""){
   alert("Ingresa el apellido materno");
  }else if(this.tutor.tutorParentesco == ""){
   alert("Ingresa parentesco");
  }else if(this.tutor.tutorTelefonoCelular.length != 10){
   alert("Ingresa un telefono celular válido(10 digitos)"); 
  }else if(this.tutor.tutorCorreoElectronico == ""){
   alert("Ingresa el correo electronico");
  }else{
    if(this.visibleCancelarTutor == 1){
      
      if(this.arrayTutores.length < 3){
          var json = {
            alumnoIdFk: this.alumno.alumnoId,
            tutor:[
              {
                tutorNombre: this.tutor.tutorNombre, 
                tutorApellidoPaterno: this.tutor.tutorApellidoPaterno, 
                tutorApellidoMaterno: this.tutor.tutorApellidoMaterno,
                tutorParentesco: this.tutor.tutorParentesco,
                tutorTelefonoCasa: this.tutor.tutorTelefonoCasa,
                tutorTelefonoCelular: this.tutor.tutorTelefonoCelular,
                tutorCorreoElectronico: this.tutor.tutorCorreoElectronico,
                tutorBase64Image: this.tutor.tutorBase64Image
                } 
            ]
          }
          
          console.log("Guardar esto")
          console.log(json);
          this.httpServices.setAlumnoTutor(json).subscribe(
            datos => {
              console.log("ghuardar tutor contesta");
              console.log(datos);
             if(datos == 0){
              alert("Ocurrio un error");
              console.log(datos);
             }else{
               this.resetTutor();
               this.getTutoresAlumno();
             }
              
            });
      }else{
        alert("No puedes ingresar mas de 3 contactos de emergencia");
      }
    
    }else{
     this.tutor.tutorActivo = "Si";
      this.httpServices.updateTutorById(this.tutor).subscribe(
        datos => {
          console.log(datos);
         if(datos == 1){
          this.resetTutor();
          this.getTutoresAlumno();
         }else{
       
         }
          
        });
      
      
    }
  }
}



resetTutor(){
  
  this.visibleCancelarTutor =1;
  this.tutor.tutorId = 0;
  this.tutor.tutorNombre = ""; 
  this.tutor.tutorApellidoPaterno = ""; 
  this.tutor.tutorApellidoMaterno = "";
  this.tutor.tutorParentesco = "";
  this.tutor.tutorTelefonoCasa = "";
  this.tutor.tutorTelefonoCelular = "";
  this.tutor.tutorCorreoElectronico = "";
  this.tutor.tutorBase64Image = "";
  
  this.showImgtutor = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
  +iconpeople.people);
  
}


pagarMensualidad(content : any){
  var json={
    "alumnoId" : this.alumno.alumnoId,
    "plan" : this.arrayhorariosSelected
  }
  console.log(json);
  this.calcularTotalDisciplinas();
  this.calcularPago();
  this.conceptoParcialidad1 = "Mensualidad";
  this.conceptoParcialidad2 = "Mensualidad";
  this.conceptoParcialidad3 = "Mensualidad";
  
  
  this.modalService.open(content, { size: 'xl' });
}


pagarOtrosServicios(content : any){
  // var json={
  //   "alumnoId" : this.alumno.alumnoId,
  //   "plan" : this.arrayhorariosSelected
  // }
  // console.log(json);
  // this.calcularTotalDisciplinas();
  // this.calcularPago();

  this.httpServices.getMensualidadesByAlumno(this.alumno.alumnoId, new Date().toISOString().split("T")[0]).subscribe(
    datos => {
      console.log("mensualidades");
      console.log(datos);
      var json = JSON.parse(JSON.stringify(datos));
      this.saldoFavor = json[0].SaldoAFavor;  
    });
    
  this.conceptoParcialidad1 = "";
  this.conceptoParcialidad2 = "";
  this.conceptoParcialidad3 = "";
  
  this.modalService.open(content, { size: 'xl' });
  
}



closeModal2(){
  this.modalService.dismissAll();
}


public openPDF():void {
      if(this.alumno.alumnoId != 0){
        this.value = crypto.AES.encrypt("A-"+this.alumno.alumnoCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();
        var nombre = this.alumno.alumnoNombre+" "+this.alumno.alumnoApellidoPaterno+" "+this.alumno.alumnoApellidoMaterno;
        var contexto = this;
        setTimeout(function()
        {                     
            console.log("entre al pdf "+contexto.value);                                    
            let PDF = new jsPDF('p', 'mm', 'letter');
            console.log("entre al pdf");
            
            
            var qrcode = document.getElementById("qrcode")?.innerHTML;
            var parser = new DOMParser();
            var divContenedor;
            console.log("entre al pdf");
            console.log(qrcode);
      
            if(qrcode != undefined){
              divContenedor = parser.parseFromString(qrcode,"text/html");  
            }
            var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 
      
            if(codeBase64 != undefined){
              PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
            }
            
                        
            
            //PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
            PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
            
            
            PDF.setFillColor(150,150,150);
            PDF.rect(53,90,110,7,'F');
            
            PDF.rect(63,33,35,35);
            
            PDF.line(55,80,106,80);
            
            
            PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
            PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
            PDF.setFontSize(6)
            
            PDF.setTextColor(255,255,255);
            PDF.text("Asociación Monarca de Triatlon",72, 93);
            PDF.text("del Estado de México",72, 96);
            
            PDF.text("Centro de Desarrollo del Deporte",129, 93);
            PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
            
            PDF.setTextColor(0,0,0);
            
            
            
            PDF.cell(53,12 , 55, 85," ",0, "center");
            PDF.cell(108,12 , 55, 85," ",0, "center");
                                              
            
            PDF.setFontSize(8);
            var w = PDF.internal.pageSize.getWidth();
            
            if(nombre.length >= 30 ){
              var char = nombre.split("");
              var espacio = 0;
              for(var i = 29 ; i > 0; i--){
                if(char[i] === " "){
                  espacio = i;
                  break;
                }
              }
              
              var nom1 = nombre.substring(0,espacio);
              var nom2 = nombre.substring(espacio);
                          
              PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
              PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
            
            }else{
              
            PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
            }
            
            console.log("entre al pdf");
            
            PDF.save(nombre+'_qr.pdf');
            
            
            contexto.value = "";
            console.log("entre al pdf");
                    
        }, 500);
      } 
 
  }



btnLimpiar(){
  this.resetMensualidad();
  this.showTabs = false;
  this.resetTutor();
  this.arrayTutores = [];
  this.arrayhorariosSelected = [];
  this.arrayAlumnosResultados = [];
  
  this.alumno.alumnoId  = 0;
  this.alumno.alumnoBase64Image = "";
  this.alumno.alumnoNombre =  "";
  this.alumno.alumnoApellidoPaterno = "";
  this.alumno.alumnoApellidoMaterno = "";
  this.alumno.alumnoCURP = "";
  this.alumno.alumnoFechaDeNacimiento = "";
  this.alumno.alumnoSexo = "";
  this.alumno.alumnoRutaFoto = "";
  
  this.alumno.alumnoTelefonoCasa = "";
  this.alumno.alumnoTelefonoCelular = "";
  this.alumno.alumnoCorreoElectronico = "";
  this.alumno.alumnoRedesSociales = "";
  this.alumno.alumnoPassword = "";
  this.alumno.alumnoCodigoPostal = 0;
  this.alumno.alumnoEstado = "";
  this.alumno.alumnoMunicipio = "";
  this.alumno.alumnoColonia = "";
  this.alumno.alumnoNumero = "";
  this.alumno.alumnoCalle = "";
  
  this.alumno.alumnoAfiliacionMedica = "";
  this.alumno.alumnoAlergias  = "";
  this.alumno.alumnoPadecimientos = "";
  this.alumno.alumnoTipoDeSangre = "";
  this.alumno.alumnoEstatura = 0;
  this.alumno.alumnoPeso = 0;
  this.alumno.alumnoActivo = "Si";
  
  
  this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
  +iconpeople.people);
  
}



updateAlumno(){
  console.log("mando al");
  console.log(this.alumno);
  this.httpServices.updateAlumno(this.alumno).subscribe(
    datos => {
      
      console.log(datos);
     if(datos == 1){

      alert("Alumno actualizado correctamente");
      
     }else{
      alert("Ocurrio un error");       
     }
    });
}







/**********************************/

////////inicio funciones camara 
open(content:any) {
  console.log(content);
 this.modalService.open(content, { size: 'xl' });
 
}
getMediaDevices(){
  this.arrayCamaras = [];
  var contexto = this;
  navigator.mediaDevices.enumerateDevices().then(function(devices) {
    devices.forEach(function(device) {
      console.log(device.kind + ": " + device.label +
                  " id = " + device.deviceId);
                  if(device.kind === "videoinput"){
                    if(contexto.idCamara === ""){
                      contexto.idCamara = device.deviceId;
                    }
                    contexto.arrayCamaras.push(device);
                  }
    });
  })
  .catch(function(err) {
    console.log(err.name + ": " + err.message);
  });
}

async iniciarCamara(video: any) {
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    try {
      this.stream = await navigator.mediaDevices.getUserMedia({
        video: {
          deviceId: { exact: this.idCamara },
          width: { ideal: 500 },
        height: { ideal: 500 } 
        }
      });
      
      if (this.stream) {
        video.srcObject = this.stream;
        video.play();
        //this.error = null;
        
      } else {
        //this.error = "You have no output video device";
      console.log("error desde ek id stream");
      } 
    } catch (e) {
      console.log("error catch "+e);
      //this.error = e;
    }
  }
}

public captures!: Array<any>;

public capture(canvas: any, video: any) {
  this.isCaptured = true;
  this.captures = [];
  var context = canvas.getContext("2d").drawImage(video, 0, 0, 500,500 );
  
  console.log(canvas.toDataURL("image/png"));
  this.captures.push(canvas.toDataURL("image/png"));
  this.auxImg = canvas.toDataURL("image/png");
  
}

public capture2(canvas: any, video: any) {
  this.isCaptured = true;
  this.captures = [];
  var context = canvas.getContext("2d").drawImage(video, 0, 0, 500,500 );
  
  console.log(canvas.toDataURL("image/png"));
  this.captures.push(canvas.toDataURL("image/png"));
  this.auxImgTutor = canvas.toDataURL("image/png");
  
}

tomarOtra(){
  this.isCaptured = false;
  this.auxImg = "";
}
tomarOtra2(){
  this.isCaptured = false;
  this.auxImgTutor = "";
}

closeModal(){
  this.modalService.dismissAll();
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });

  this.isCaptured = false;
}


btnGuardarfoto(){
  navigator.mediaDevices.getUserMedia().finally();
  if(this.auxImg != ""){
    this.alumno.alumnoBase64Image = this.auxImg.split(",")[1];
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.alumno.alumnoBase64Image);
    this.modalService.dismissAll()
    
    
  }else{
    alert("no se guardo ninguna foto");
    this.modalService.dismissAll()
  }
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.isCaptured = false;
}

cambioCamara(video:any){
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.iniciarCamara(video);
}

btnGuardarfoto2(){
  navigator.mediaDevices.getUserMedia().finally();
  if(this.auxImgTutor != ""){
    this.tutor.tutorBase64Image = this.auxImgTutor.split(",")[1];
    this.showImgtutor = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.tutor.tutorBase64Image);
    this.modalService.dismissAll()
    
    
  }else{
    alert("no se guardo ninguna foto");
    this.modalService.dismissAll()
  }
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.isCaptured = false;
}

calcularTotalDisciplinas(){
  
  if(new Date(this.fechaProxima).getTime() > new Date().getTime()){
    this.importeDisciplinas = 0;
    console.log("entre a desarrollo");
    for(var i = 0 ; i < this.arrayhorariosSelected.length; i++){
     console.log(this.arrayhorariosSelected[i]); 
      if(this.arrayhorariosSelected[i].actual == 0){
        console.log("agrego");   
        this.importeDisciplinas = this.importeDisciplinas + this.arrayhorariosSelected[i].planImporte;
      }
    }
    this.subTotal = Number(((this.importeInscripcion/100) * this.porcentajeAnualidad).toFixed(2))+Number((((this.importeDisciplinas*this.mesesPorPagar)/100)*this.porcentajeDePago).toFixed(2))+this.importeMultas;
    if(this.subTotal > 0){
      this.showButtonMensualidad = 1;
    }else{
      this.showButtonMensualidad = 0;
    }
    
  }else{
    
  this.importeDisciplinas = 0;
  for(var i = 0 ; i < this.arrayhorariosSelected.length; i++){
    this.importeDisciplinas = this.importeDisciplinas + this.arrayhorariosSelected[i].planImporte
  }
  this.subTotal = this.importeInscripcion+Number((((this.importeDisciplinas*this.mesesPorPagar)/100)*this.porcentajeDePago).toFixed(2))+this.importeMultas;
 
  }
  
}


calcularPago(){
  
  this.subTotal = Number(((this.importeInscripcion/100) * this.porcentajeAnualidad).toFixed(2))+Number((((this.importeDisciplinas*this.mesesPorPagar)/100)*this.porcentajeDePago).toFixed(2))+this.importeMultas;
  var p1 = 0;
  if(this.montoParcialidad1 != undefined){
    p1 = this.montoParcialidad1;
  }
  var p2 = 0;
  if(this.montoParcialidad2 != undefined){
    p2 = this.montoParcialidad2;
  }
  var p3 = 0;
  if(this.montoParcialidad3 != undefined){
    p3 = this.montoParcialidad3;
  }
  console.log(this.porcentajeDePago);
  this.subTotal = Number(this.subTotal.toFixed(2));
  this.pagoTotal = Number(this.subTotal.toFixed(2));
  this.montoFaltante = Number((this.pagoTotal-this.saldoFavor-Number(p1)-Number(p2)-Number(p3)).toFixed(2));
  if(this.montoFaltante === NaN){
    this.montoFaltante = 0;
  }
 
  if(this.montoFaltante <= 0){
    this.habilitarButtonMensualidad = false;
  }else{
    this.habilitarButtonMensualidad = true;
  }
  
  
}

////////fin funciones camara
calcularPagoParcialidades(){
  console.log("Entre a calcular parcialidad");
  var p1 = 0;
  if(this.montoParcialidad1 != undefined){
    p1 = this.montoParcialidad1;
  }
  var p2 = 0;
  if(this.montoParcialidad2 != undefined){
    p2 = this.montoParcialidad2;
  }
  var p3 = 0;
  if(this.montoParcialidad3 != undefined){
    p3 = this.montoParcialidad3;
  }
  
  
  this.montoFaltante = Number((this.pagoTotal-this.saldoFavor-Number(p1)-Number(p2)-Number(p3)).toFixed(2));
  console.log(this.montoFaltante);
  if(this.montoFaltante <= 0){
    this.habilitarButtonMensualidad = false;
  }else{
    this.habilitarButtonMensualidad = true;
  }
}

fechasPago(){  
}


getMensualidadesAlumno(){
  
  //todo calcular fecha mas actual  
  console.log("Aqui ando");
  var arrayFechas = [];
  
  if(this.fechaPagoParcialidad1 != ""){
    console.log(this.fechaPagoParcialidad1);
    arrayFechas.push(new Date(this.fechaPagoParcialidad1).getTime());
  }
  if(this.fechaPagoParcialidad2 != ""){
    console.log(this.fechaPagoParcialidad2);
    arrayFechas.push(new Date(this.fechaPagoParcialidad2).getTime());
  }
  if(this.fechaPagoParcialidad3 != ""){
    console.log(this.fechaPagoParcialidad3);
    arrayFechas.push(new Date(this.fechaPagoParcialidad3).getTime() );
  }

  
  if(arrayFechas.length != 0){
    var m = Math.max(...arrayFechas);
    
    
    
    console.log(m)
    console.log(new Date(m).toISOString());
    
  
  this.httpServices.getMensualidadesByAlumno(this.alumno.alumnoId, new Date(m).toISOString().split("T")[0]).subscribe(
    datos => {
      console.log("mensualidades");
      console.log(datos);
      var json = JSON.parse(JSON.stringify(datos));
     
     
     if(this.contPorcentaje == 0){
        this.porcentajeDePago = json[0].PorcentajeDePago;
        if(this.porcentajeDePago == null){
          this.porcentajeDePago = 100;
        }
     }
     this.contPorcentaje++;
      this.saldoFavor = json[0].SaldoAFavor;
      if(this.saldoFavor == null){
        this.saldoFavor =0;
      }
      
      
      this.importeInscripcion = json[0].ImporteAnual;
      if( this.importeInscripcion == null){
        this.importeInscripcion = 0;
      }
    
      this.importeMultas = json[0].ImporteMulta;
      if( this.importeMultas == null){
        this.importeMultas =0;
    }      
      
      

            
      var d = new Date(json[0].FechaUltimaMensualidad);
      if(json[0].FechaUltimaMensualidad != null){
        this.fechaUltimoPago = json[0].FechaUltimaMensualidad.split("T")[0];
      }else{
        d = new Date();
      }
      

      d.setMonth(d.getMonth() + 1);
      this.fechaProxima = d.getFullYear()+"-"+("0"+(d.getMonth()+1)).slice(-2)+"-"+("0"+d.getDate()).slice(-2);
    
      if(d.getTime() <= new Date().getTime()){
        this.showButtonMensualidad = 1;
      }
      
      
     // this.diasAtrasados = 89;
      
      if(json[0].DiasDeRetraso > 0){
        
        this.diasAtrasados = json[0].DiasDeRetraso;
        if(this.diasAtrasados < 30){
          this.mesesPorPagar = 1;
          console.log(this.mesesPorPagar);
        }else if(this.diasAtrasados < 60){ 
          this.mesesPorPagar = 2;
          console.log(this.mesesPorPagar);
        }else if(this.diasAtrasados < 90){
          this.mesesPorPagar = 3;
          console.log(this.mesesPorPagar);
        }


      }else{
        this.diasAtrasados = 0;
        console.log(this.diasAtrasados);
      }
      
      console.log(" antes de calcular" +this.diasAtrasados+" "+this.mesesPorPagar);
      this.calcularTotalDisciplinas();
      this.calcularPagoParcialidades();
      this.calcularPago();
      
    });
    
  }else{
    alert("Ingresa los datos de las parcialidades prueba");
  }
      
}


guardarMensualidd(){
    
  var arrayParcialidades = [];
  var arrayFechaActual = [];
  console.log(this.montoParcialidad1);
  if(this.montoParcialidad1 > 0){
    if(this.tipoParcialidad1 === "" || this.folioParcialidad1 === "" ||
    this.conceptoParcialidad1 === "" || this.cuentaParcialidad1 === "" || this.fechaPagoParcialidad1 === ""){
        alert("Llena todos los datos para la parcialidad 1");
      }else{
        var jsonp = {
          "parcialidadImporte": this.montoParcialidad1 ,
          "tipoDePago": this.tipoParcialidad1,
          "folioDePago": this.folioParcialidad1,
          "cuentaBeneficiaria": this.cuentaParcialidad1,
          "conceptoDePago" : this.conceptoParcialidad1,
          "observaciones" : this.comentariosParcialidad1,
          "fechaDeReciboDePago" : this.fechaPagoParcialidad1
        }
        arrayParcialidades.push(jsonp);
        arrayFechaActual.push(new Date(this.fechaPagoParcialidad1).getTime())
      }
    
    
 
  }
  console.log(this.montoParcialidad2);
  if(this.montoParcialidad2 > 0){
    if(this.tipoParcialidad2 === "" || this.folioParcialidad2 === "" ||
    this.conceptoParcialidad2 === "" || this.cuentaParcialidad2 === "" || this.fechaPagoParcialidad2 === ""){
       alert("Llena todos los datos para la parcialidad 2");

    }else{
      var jsonp = {
        "parcialidadImporte": this.montoParcialidad2 ,
        "tipoDePago": this.tipoParcialidad2,
        "folioDePago": this.folioParcialidad2,
        "cuentaBeneficiaria": this.cuentaParcialidad2,
        "conceptoDePago" : this.conceptoParcialidad2,
        "observaciones" : this.comentariosParcialidad2,
        "fechaDeReciboDePago" : this.fechaPagoParcialidad2
      }
      arrayParcialidades.push(jsonp); 
      arrayFechaActual.push(new Date(this.fechaPagoParcialidad2).getTime())
    }
}
console.log(this.montoParcialidad3);
if(this.montoParcialidad3 > 0){
  
  if(this.tipoParcialidad3 === "" || this.folioParcialidad3 === "" ||
  this.conceptoParcialidad3 === "" || this.cuentaParcialidad3 === "" || this.fechaPagoParcialidad3 === ""){
     alert("Llena todos los datos para la parcialidad 3");
  }else{
    var jsonp = {
      "parcialidadImporte": this.montoParcialidad3 ,
      "tipoDePago": this.tipoParcialidad3,
      "folioDePago": this.folioParcialidad3,
      "cuentaBeneficiaria": this.cuentaParcialidad3,
      "conceptoDePago" : this.conceptoParcialidad3,
      "observaciones" : this.comentariosParcialidad3,
      "fechaDeReciboDePago" : this.fechaPagoParcialidad3,
    }
    arrayParcialidades.push(jsonp);
    arrayFechaActual.push(new Date(this.fechaPagoParcialidad3).getTime())
  }

} 
  var conceptoGeneral = "";
  var cuentaGeneral = "";
  var observaciones = "";
  var fechaGeneral = "";
  var m = Math.max(...arrayFechaActual);
  
  for(var i = 0 ; i < arrayParcialidades.length ; i++){
    if(new Date(arrayParcialidades[i].fechaDeReciboDePago).getTime() == m){
      conceptoGeneral  = arrayParcialidades[i].conceptoDePago;
      cuentaGeneral = arrayParcialidades[i].cuentaBeneficiaria;
      observaciones = arrayParcialidades[i].observaciones;
      fechaGeneral = arrayParcialidades[i].fechaDeReciboDePago;
    }
  }  

  var saldoFavor = 0;
  if(this.montoFaltante < 0){
    saldoFavor = -1*this.montoFaltante;
  }
  var retraso = "No";
  if(this.diasAtrasados > 0){
    retraso = "Si";
  }
  
  if(this.arrayhorariosSelected.length == 0){
    alert("Debes asignar al menos una diciplina al alumno");
  }else{
  
    if(arrayParcialidades.length > 0){
      
      if(this.pagoTotal == 0){
        for(var i = 0 ; i < this.arrayhorariosSelected.length; i++){
          this.arrayhorariosSelected[i].planImporte = 0;
        }
      }
      
      
      var json = {
        "alumnoIdFk": this.alumno.alumnoId,
        "mensualidadImporte": this.pagoTotal,
        "mensualidadConRetraso": retraso,
        "mensualidadImportePorRetraso": this.importeMultas,
        "mensualidadesPorcentajePago": this.porcentajeDePago,
        "mensualidadSaldoAFavor": saldoFavor,
        "inscripcionImporteAnual": this.importeInscripcion,
        "parcialidades": arrayParcialidades,
        "planHorario": this.arrayhorariosSelected,
        "cuentaBenefiaria": cuentaGeneral,
        "conceptoDePago": conceptoGeneral,
        "observaciones": observaciones,
        "mensualidadFechaRealDePago": fechaGeneral,
      }
      
      this.httpServices.setMensualidad(json).subscribe(
        datos => {
          console.log("mensualidades pago");
          console.log(datos);
          //var json = JSON.parse(JSON.stringify(datos));
          if(datos == 1){
            this.resetMensualidad();
            this.closeModal2();
            //this.getMensualidadesAlumno();
            alert("Pago registrado correctamente");
          }else{
            var json = JSON.parse(JSON.stringify(datos))
            alert(json[0].mensaje+": "+json[0].Folio);
          }
        });
        console.log("Aqui esta el chido");
        console.log(json);
    }
    
    
  }
  
}


guardarMensualidadConcepto(){
  
  var arrayParcialidades = [];
  var arrayFechaActual = [];
  console.log(this.montoParcialidad1);

    if(this.tipoParcialidad1 == "Saldo a favor"){
      
      
      
      if(this.folioParcialidad1 === "" || this.importeCobrar == 0 ||
          this.importeCobrar == undefined || this.importeCobrar == null || this.conceptoParcialidad1 === ""  || this.fechaPagoParcialidad1 === ""){
         alert("Llena todos los datos para la parcialidad 1");
      }else{
        if(this.montoParcialidad1 == undefined || this.montoParcialidad1 == null){
          this.montoParcialidad1 = 0;
        }
        
        var jsonp = {
          "parcialidadImporte": this.montoParcialidad1 ,
          "tipoDePago": this.tipoParcialidad1,
          "folioDePago": this.folioParcialidad1,
          "cuentaBeneficiaria": this.cuentaParcialidad1,
          "conceptoDePago" : this.conceptoParcialidad1,
          "observaciones" : this.comentariosParcialidad1,
          "fechaDeReciboDePago" : this.fechaPagoParcialidad1
        }
        arrayParcialidades.push(jsonp);
        arrayFechaActual.push(new Date(this.fechaPagoParcialidad1).getTime())
      }
                              
    }else{
      
      if(this.montoParcialidad1 !=  0 && this.montoParcialidad1 != undefined){
            if(this.tipoParcialidad1 === "" || this.folioParcialidad1 === "" ||
            this.conceptoParcialidad1 === "" || this.cuentaParcialidad1 === "" || this.fechaPagoParcialidad1 === ""){
          alert("Llena todos los datos para la parcialidad 1");
        }else{
          var jsonp = {
            "parcialidadImporte": this.montoParcialidad1 ,
            "tipoDePago": this.tipoParcialidad1,
            "folioDePago": this.folioParcialidad1,
            "cuentaBeneficiaria": this.cuentaParcialidad1,
            "conceptoDePago" : this.conceptoParcialidad1,
            "observaciones" : this.comentariosParcialidad1,
            "fechaDeReciboDePago" : this.fechaPagoParcialidad1
          }
          arrayParcialidades.push(jsonp);
          arrayFechaActual.push(new Date(this.fechaPagoParcialidad1).getTime())
        }
      }
     
    }               

  console.log(this.montoParcialidad2);
  if(this.montoParcialidad2 > 0){
    if(this.tipoParcialidad2 === "" || this.folioParcialidad2 === "" ||
    this.conceptoParcialidad2 === "" || this.cuentaParcialidad2 === "" || this.fechaPagoParcialidad2 === ""){
      alert("Llena todos los datos para la parcialidad 2");

    }else{
      var jsonp = {
        "parcialidadImporte": this.montoParcialidad2 ,
        "tipoDePago": this.tipoParcialidad2,
        "folioDePago": this.folioParcialidad2,
        "cuentaBeneficiaria": this.cuentaParcialidad2,
        "conceptoDePago" : this.conceptoParcialidad2,
        "observaciones" : this.comentariosParcialidad2,
        "fechaDeReciboDePago" : this.fechaPagoParcialidad2
      }
      arrayParcialidades.push(jsonp); 
      arrayFechaActual.push(new Date(this.fechaPagoParcialidad2).getTime())
    }
}
console.log(this.montoParcialidad3);
if(this.montoParcialidad3 > 0){
  
  if(this.tipoParcialidad3 === "" || this.folioParcialidad3 === "" ||
  this.conceptoParcialidad3 === "" || this.cuentaParcialidad3 === "" || this.fechaPagoParcialidad3 === ""){
    alert("Llena todos los datos para la parcialidad 3");

  }else{
    var jsonp = {
      "parcialidadImporte": this.montoParcialidad3 ,
      "tipoDePago": this.tipoParcialidad3,
      "folioDePago": this.folioParcialidad3,
      "cuentaBeneficiaria": this.cuentaParcialidad3,
      "conceptoDePago" : this.conceptoParcialidad3,
      "observaciones" : this.comentariosParcialidad3,
      "fechaDeReciboDePago" : this.fechaPagoParcialidad3,
     
    }
    arrayParcialidades.push(jsonp);
    arrayFechaActual.push(new Date(this.fechaPagoParcialidad3).getTime())
  }

} 
  var conceptoGeneral = "";
  var cuentaGeneral = "";
  var observaciones = "";
  var fechaGeneral = "";
  var m = Math.max(...arrayFechaActual);
  
  for(var i = 0 ; i < arrayParcialidades.length ; i++){
    if(new Date(arrayParcialidades[i].fechaDeReciboDePago).getTime() == m){
      conceptoGeneral  = arrayParcialidades[i].conceptoDePago;
      cuentaGeneral = arrayParcialidades[i].cuentaBeneficiaria;
      observaciones = arrayParcialidades[i].observaciones;
      fechaGeneral = arrayParcialidades[i].fechaDeReciboDePago;
    }
  }

  var saldoFavor = 0;
  if(this.montoFaltante < 0){
    saldoFavor = -1*this.montoFaltante;
  }
  var retraso = "No";
  if(this.diasAtrasados > 0){
    retraso = "Si";
  }
  
  if(this.arrayhorariosSelected.length == 0){
    alert("Debes asignar al menos una diciplina al alumno");
  }else{
  
    if(arrayParcialidades.length > 0){
        
        for(var i = 0 ; i < this.arrayhorariosSelected.length; i++){
          this.arrayhorariosSelected[i].planImporte = 0;
        }
                
      var json = {
        "alumnoIdFk": this.alumno.alumnoId,
        "mensualidadImporte": this.importeCobrar,
        "mensualidadConRetraso": retraso,
        "mensualidadImportePorRetraso": this.importeMultas,
        "mensualidadesPorcentajePago": this.porcentajeDePago,
        "mensualidadSaldoAFavor": saldoFavor,
        "inscripcionImporteAnual": this.importeInscripcion,
        "parcialidades": arrayParcialidades,
        "planHorario": this.arrayhorariosSelected,
        "cuentaBenefiaria": cuentaGeneral,
        "conceptoDePago": conceptoGeneral,
        "observaciones": observaciones,
        "mensualidadFechaRealDePago": fechaGeneral,
      }

      this.httpServices.setPagoOtroConcepto(json).subscribe(
        datos => {
          console.log("mensualidades pago");
          console.log(datos);
          var json = JSON.parse(JSON.stringify(datos));
          if(json[0].result > 0){
            this.resetMensualidad();
            this.closeModal2();
          //  this.getMensualidadesAlumno();
            alert("Pago registrado correctamente");
          }else{
            var json = JSON.parse(JSON.stringify(datos))
            alert("Error, intenta de nuevo");
          }
        });
        console.log("antes de mandar");
        console.log(json);
    }
    
    
  }
  
}

resetMensualidad(){
  this.importeDisciplinas = 0;
  this.importeInscripcion = 0;
  this.importeMultas = 0;
  this.fechaUltimoPago = "";
  this.fechaProxima = "";
  this.diasAtrasados = 0;
  this.subTotal = 0;
  this.porcentajeDePago = 0;
  this.pagoTotal = 0;
  this.habilitarButtonMensualidad = true;
  this.montoFaltante = 0;
  this.showButtonMensualidad = 0;
  this.saldoFavor =0;
  

  this.montoParcialidad1 = this.permisos.value!;
  this.montoParcialidad2 = this.permisos.value!;;
  this.montoParcialidad3 = this.permisos.value!;;
  
  this.tipoParcialidad1 = "";
  this.tipoParcialidad2 = "";
  this.tipoParcialidad3 = "";

  this.folioParcialidad1 = "";
  this.folioParcialidad2 = "";
  this.folioParcialidad3 = "";
  
  this.fechaPagoParcialidad1 = "";
  this.fechaPagoParcialidad2 = "";
  this.fechaPagoParcialidad3 = "";
  
  this.cuentaParcialidad1 = "";
  this.cuentaParcialidad2 = "";
  this.cuentaParcialidad3 = "";
  
  this.conceptoParcialidad1 = "";
  this.conceptoParcialidad2 = "";
  this.conceptoParcialidad3 = "";
  
  this.contPorcentaje = 0;
  
  this.importeCobrar = 0;
}



  
activarDesactivar(){
  var send = {
    "alumnoId" : this.alumno.alumnoId,
    "alumnoActivo": "SI" 
  };
  if(this.alumno.isActive){
    send.alumnoActivo = "SI";
  }else{
    send.alumnoActivo = "NO";
  }
  //categoria.rolesActivo = send.categoriaActivo;
  
  console.log(send);
  this.httpServices.activarDesactivarAlumno(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){
        if(this.alumno.isActive){
          this.alumno.alumnoActivo = "NO";
          this.alumno.isActive = false;
          
        }else{
          this.alumno.alumnoActivo = "SI";
          this.alumno.isActive = true;
        }
        alert("error");
      }
      
      
      
    },
    error => {
      console.log("hola desde el error"); 
      console.log(error); 
      if(this.alumno.isActive){
        this.alumno.alumnoActivo = "NO";
        this.alumno.isActive = false;
        
      }else{
        this.alumno.alumnoActivo = "SI";
        this.alumno.isActive = true;
      }
      
    });
  
}


calcularPagoParcialidades2(){
  console.log("Entre a calcular parcialidad");
  var p1 = 0;
  if(this.montoParcialidad1 != undefined){
    p1 = this.montoParcialidad1;
  }
  var p2 = 0;
  if(this.montoParcialidad2 != undefined){
    p2 = this.montoParcialidad2;
  }
  var p3 = 0;
  if(this.montoParcialidad3 != undefined){
    p3 = this.montoParcialidad3;
  }
  
  if(this.importeCobrar == undefined){
    this.importeCobrar = 0;
  }
  
  
  this.pagoTotal =  this.importeCobrar;
  this.montoFaltante = Number((this.pagoTotal-(this.saldoFavor+Number(p1)+Number(p2)+Number(p3))).toFixed(2));
  console.log(this.montoFaltante);
  
}


changedSaldoAfavor(){
  if(this.tipoParcialidad1 == "Saldo a favor"){
    this.folioParcialidad1 = "SA"+new Date().getUTCFullYear()+(new Date().getUTCMonth()+1)+new Date().getDate()+new Date().getHours()+new Date().getUTCMinutes()+new Date().getUTCSeconds();
  }else{
    this.folioParcialidad1 = "";
  }
}





}
