import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteGimnaciosComponent } from './reporte-gimnacios.component';

describe('ReporteGimnaciosComponent', () => {
  let component: ReporteGimnaciosComponent;
  let fixture: ComponentFixture<ReporteGimnaciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteGimnaciosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteGimnaciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
