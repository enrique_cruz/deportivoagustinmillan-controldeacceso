import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporte-gimnacios',
  templateUrl: './reporte-gimnacios.component.html',
  styleUrls: ['./reporte-gimnacios.component.css']
})
export class ReporteGimnaciosComponent implements OnInit {

  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  idGimnacioFiltro=0;
  idDisciplinaFiltro=0;

  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }

  
  arrayGimnasiosActivos: Array<{
    gimnasioId: number;    
    gimnasioNombre: string;
  }>= [];
  
  
  arrayDisciplinasActivas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];

  
  
  
  arrayGimnacios:Array<{ 
    gimnasioId: number,
    gimnasioNombre: string,
    instructorIdFk: number,
    instructorNombre: string,
    instructorApellidoPaterno: string,
    instructorApellidoMaterno: string,
    instructorCURP: string,
    disciplinaId: number,
    disciplinaNombre: string,
    horarioId: number,
    horarioNombre: string,
    horarioinicio: string,
    horarioFin: string,
    horarioDias: string,
    CapacidadMaxima: number,
    CapacidadActual: number,
    AforoActual: number}>=[];
    
    
    arrayGimnaciosShow:Array<{ 
      gimnasioId: number,
      gimnasioNombre: string,
      instructorIdFk: number,
      instructorNombre: string,
      instructorApellidoPaterno: string,
      instructorApellidoMaterno: string,
      instructorCURP: string,
      disciplinaId: number,
      disciplinaNombre: string,
      horarioId: number,
      horarioNombre: string,
      horarioinicio: string,
      horarioFin: string,
      horarioDias: string,
      CapacidadMaxima: number,
      CapacidadActual: number,
      AforoActual: number}>=[];
  
  ngOnInit(): void {
    this.getAllGimnacios();
    this.getDisciplinas();
    this.getGimnacios();
  }

  
  getAllGimnacios(){
    this.httpServices.getGimnaciosAllData().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayGimnacios = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
           this.arrayGimnacios.push({ 
            gimnasioId: jsonDatos[i].gimnasioId,
            gimnasioNombre: jsonDatos[i].gimnasioNombre,
            instructorIdFk: jsonDatos[i].instructorIdFk,
            instructorNombre: jsonDatos[i].instructorNombre,
            instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno,
            instructorApellidoMaterno: jsonDatos[i].instructorApellidoMaterno,
            instructorCURP: jsonDatos[i].instructorCURP,
            disciplinaId: jsonDatos[i].disciplinaId,
            disciplinaNombre: jsonDatos[i].disciplinaNombre,
            horarioId: jsonDatos[i].horarioId,
            horarioNombre: jsonDatos[i].horarioNombre,
            horarioinicio: jsonDatos[i].horarioinicio,
            horarioFin: jsonDatos[i].horarioFin,
            horarioDias: jsonDatos[i].horarioDias,
            CapacidadMaxima: jsonDatos[i].CapacidadMaxima,
            CapacidadActual: jsonDatos[i].CapacidadActual,
            AforoActual: jsonDatos[i].AforoActual});   
      
          
         
           }
           this.arrayGimnaciosShow = this.arrayGimnacios;
          console.log(this.arrayGimnaciosShow);
      });
  }
 
   
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinasActivas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinasActivas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      });
  }
  
  getGimnacios(){
    this.httpServices.getGimnaciosActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayGimnasiosActivos= [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayGimnasiosActivos.push({ 
          gimnasioId: jsonDatos[i].gimnasioId, 
          gimnasioNombre : jsonDatos[i].gimnasioNombre });   
        }
      });
  }
  
  cambioFiltros(){
    this.arrayGimnaciosShow = [];
    
   //console.log(this.arrayInstructores.map(instructor=> instructor.horarioNombre == "Hugo"));
    if(this.idDisciplinaFiltro != 0 && this.idGimnacioFiltro != 0){
   
      for(var i = 0 ; i < this.arrayGimnacios.length; i++){
        if( this.arrayGimnacios[i].disciplinaId == this.idDisciplinaFiltro && this.arrayGimnacios[i].gimnasioId == this.idGimnacioFiltro ){
          this.arrayGimnaciosShow.push({ 
            gimnasioId: this.arrayGimnacios[i].gimnasioId,
            gimnasioNombre: this.arrayGimnacios[i].gimnasioNombre,
            instructorIdFk: this.arrayGimnacios[i].instructorIdFk,
            instructorNombre: this.arrayGimnacios[i].instructorNombre,
            instructorApellidoPaterno: this.arrayGimnacios[i].instructorApellidoPaterno,
            instructorApellidoMaterno: this.arrayGimnacios[i].instructorApellidoMaterno,
            instructorCURP: this.arrayGimnacios[i].instructorCURP,
            disciplinaId: this.arrayGimnacios[i].disciplinaId,
            disciplinaNombre: this.arrayGimnacios[i].disciplinaNombre,
            horarioId: this.arrayGimnacios[i].horarioId,
            horarioNombre: this.arrayGimnacios[i].horarioNombre,
            horarioinicio: this.arrayGimnacios[i].horarioinicio,
            horarioFin: this.arrayGimnacios[i].horarioFin,
            horarioDias: this.arrayGimnacios[i].horarioDias,
            CapacidadMaxima: this.arrayGimnacios[i].CapacidadMaxima,
            CapacidadActual: this.arrayGimnacios[i].CapacidadActual,
            AforoActual: this.arrayGimnacios[i].AforoActual});
        }
      } 
    }else{
      
      if(this.idDisciplinaFiltro != 0){
    
        for(var i = 0 ; i < this.arrayGimnacios.length; i++){
          if( this.arrayGimnacios[i].disciplinaId == this.idDisciplinaFiltro ){
            this.arrayGimnaciosShow.push({ 
              gimnasioId: this.arrayGimnacios[i].gimnasioId,
              gimnasioNombre: this.arrayGimnacios[i].gimnasioNombre,
              instructorIdFk: this.arrayGimnacios[i].instructorIdFk,
              instructorNombre: this.arrayGimnacios[i].instructorNombre,
              instructorApellidoPaterno: this.arrayGimnacios[i].instructorApellidoPaterno,
              instructorApellidoMaterno: this.arrayGimnacios[i].instructorApellidoMaterno,
              instructorCURP: this.arrayGimnacios[i].instructorCURP,
              disciplinaId: this.arrayGimnacios[i].disciplinaId,
              disciplinaNombre: this.arrayGimnacios[i].disciplinaNombre,
              horarioId: this.arrayGimnacios[i].horarioId,
              horarioNombre: this.arrayGimnacios[i].horarioNombre,
              horarioinicio: this.arrayGimnacios[i].horarioinicio,
              horarioFin: this.arrayGimnacios[i].horarioFin,
              horarioDias: this.arrayGimnacios[i].horarioDias,
              CapacidadMaxima: this.arrayGimnacios[i].CapacidadMaxima,
              CapacidadActual: this.arrayGimnacios[i].CapacidadActual,
              AforoActual: this.arrayGimnacios[i].AforoActual});
          }
        } 
        
        
      }else if(this.idGimnacioFiltro != 0){

        for(var i = 0 ; i < this.arrayGimnacios.length; i++){
          if( this.arrayGimnacios[i].gimnasioId == this.idGimnacioFiltro ){
            this.arrayGimnaciosShow.push({ 
              gimnasioId: this.arrayGimnacios[i].gimnasioId,
              gimnasioNombre: this.arrayGimnacios[i].gimnasioNombre,
              instructorIdFk: this.arrayGimnacios[i].instructorIdFk,
              instructorNombre: this.arrayGimnacios[i].instructorNombre,
              instructorApellidoPaterno: this.arrayGimnacios[i].instructorApellidoPaterno,
              instructorApellidoMaterno: this.arrayGimnacios[i].instructorApellidoMaterno,
              instructorCURP: this.arrayGimnacios[i].instructorCURP,
              disciplinaId: this.arrayGimnacios[i].disciplinaId,
              disciplinaNombre: this.arrayGimnacios[i].disciplinaNombre,
              horarioId: this.arrayGimnacios[i].horarioId,
              horarioNombre: this.arrayGimnacios[i].horarioNombre,
              horarioinicio: this.arrayGimnacios[i].horarioinicio,
              horarioFin: this.arrayGimnacios[i].horarioFin,
              horarioDias: this.arrayGimnacios[i].horarioDias,
              CapacidadMaxima: this.arrayGimnacios[i].CapacidadMaxima,
              CapacidadActual: this.arrayGimnacios[i].CapacidadActual,
              AforoActual: this.arrayGimnacios[i].AforoActual});
          }
        } 
      }else{
 
        this.getAllGimnacios();
      }
    }
  }
    
  
  
  downloadReporte(){
   
    var array = [];
    for(var i = 0 ; i < this.arrayGimnaciosShow.length; i++){
    
      array.push(
            {
             "Nombre" : this.arrayGimnaciosShow[i].gimnasioNombre,
            "Disciplina" : this.arrayGimnaciosShow[i].disciplinaNombre,
            "Horario" : this.arrayGimnaciosShow[i].horarioNombre,
            "Día" : this.arrayGimnaciosShow[i].horarioDias,
            "Hora_Inicio" : this.arrayGimnaciosShow[i].horarioinicio,
            "Hora_Fin" : this.arrayGimnaciosShow[i].horarioFin,
            "Aforo_Máximo" :  this.arrayGimnaciosShow[i].CapacidadMaxima,
            "Aforo_actual" :  this.arrayGimnaciosShow[i].CapacidadActual,
             "Aforo_disponible" : this.arrayGimnaciosShow[i].AforoActual 
            }
          );
      
      }
      this.httpServices.exportAsExcelFile(array, "reporte");
  }
    


}
