import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';
import tarjeta from '../../assets/images/pdfimg/card.json';
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

import iconpeople from '../../assets/images/pdfimg/iconpeople.json';


@Component({
  selector: 'app-instructores',
  templateUrl: './instructores.component.html',
  styleUrls: ['./instructores.component.css']
})
export class InstructoresComponent implements OnInit {
  
  @ViewChild("video",{ static: false })
  video!:  ElementRef;
  stream :any;
  auxImg = "";
  showImg : any;
  todaysdate : any;   
   
  idCamara = "";
   
  strBusqueda = "";
  
  
  isCaptured = false;
  arrayCamaras: Array<any>= [];
                     
  

  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  visibleCancelar = 1;
  
  value = '';
  
  instructor = {
    instructorId: 0,
    instructorNombre: "",
    instructorApellidoPaterno: "",
    instructorApellidoMaterno: "",
    instructorCURP: "",
    instructorRFC: "",
    instructorCodigoPostal: 0,
    instructorEstado: "",
    instructorMunicipio: "",
    instructorColonia: "",
    instructorCalle: "",
    instructorNumero: "",
    instructorActivo: "",
    instructorFechaDeNacimiento: "",
    instructorSexo: "",
    instructorBase64Image : "",
    instructorTelefono : ""
  }
  
  arrayInstructores: Array<{instructorId: number;
                            instructorBase64Image : "",    
                            instructorNombre: string;
                            instructorApellidoPaterno: string;
                            instructorApellidoMaterno: string;
                            instructorCURP: string;
                            instructorRFC: string;
                            instructorCodigoPostal: number,
                            instructorEstado: string,
                            instructorMunicipio: string,
                            instructorColonia: string,
                            instructorCalle: string,
                            instructorNumero: number,
                            instructorActivo: string,
                            instructorFechaDeNacimiento: string,
                            instructorSexo: string,
                            isActive: boolean,
                            instructorTelefono : string
                            }>= [];
                            
                            
   arrayInstructoresShow: Array<{instructorId: number;
                            instructorBase64Image : "",    
                            instructorNombre: string;
                            instructorApellidoPaterno: string;
                            instructorApellidoMaterno: string;
                            instructorCURP: string;
                            instructorRFC: string;
                            instructorCodigoPostal: number,
                            instructorEstado: string,
                            instructorMunicipio: string,
                            instructorColonia: string,
                            instructorCalle: string,
                            instructorNumero: number,
                            instructorActivo: string,
                            instructorFechaDeNacimiento: string,
                            instructorSexo: string,
                            isActive: boolean,
                            instructorTelefono : string
                            }>= [];                           
  
                            
                            
 arrayAsentamientos:Array<{nombre: string, tipo: string, cadena: string}>=[];
                            
                            
  constructor( private sanitizer: DomSanitizer,
    config: NgbModalConfig, 
    private modalService: NgbModal,private httpServices: ServicesService, private route: ActivatedRoute) { 
      this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      +iconpeople.people);
      this.instructor.instructorCodigoPostal = this.permisos.value!;
      this.getMediaDevices();
      this.todaysdate= new Date();
    }

  ngOnInit(): void {
    this.getInstructores();
  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  
  setInstructor(){
    
    if(this.validarDatos()){
      
      console.log("json instructor");
      console.log(this.instructor);
      if(this.visibleCancelar == 1){
        //nuevo
        this.instructor.instructorFechaDeNacimiento = new Date(this.instructor.instructorFechaDeNacimiento).toISOString();
        console.log(this.instructor.instructorFechaDeNacimiento);
      
        this.httpServices.setInstructor(this.instructor).subscribe(
          datos => {
            console.log(datos);
      
            if(datos == 1){
              this.getInstructores();
              this.openPDF(this.instructor);
              this.reset();
              
              alert("correcto");
            }else{
              alert("Error, intenta de nuevo");
            }
          });
      }else{
        //actualizo
        this.updateInstructor();
      }
    }
      
  }
  
  updateInstructor(){
    this.instructor.instructorFechaDeNacimiento = new Date(this.instructor.instructorFechaDeNacimiento).toISOString();
    console.log(this.instructor.instructorFechaDeNacimiento);
   console.log(this.instructor); 
    this.httpServices.updateInstructor(this.instructor).subscribe(
      datos => {
        console.log(datos);
   
        if(datos == 1){   
          this.getInstructores();
      
          this.reset();
          alert("correcto");
        }else{
          alert("Error, intenta de nuevo");
        }
      });
  }
  
  getInstructores(){
    this.httpServices.getInstructoresAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayInstructores = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayInstructores.push({
          instructorId: jsonDatos[i].instructorId, 
          instructorNombre: jsonDatos[i].instructorNombre,
          instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno, 
          instructorApellidoMaterno: jsonDatos[i].instructorApellidoMaterno, 
          instructorCURP: jsonDatos[i].instructorCURP, 
          instructorRFC: jsonDatos[i].instructorRFC, 
          instructorCodigoPostal: jsonDatos[i].instructorCodigoPostal, 
          instructorEstado: jsonDatos[i].instructorEstado, 
          instructorMunicipio: jsonDatos[i].instructorMunicipio, 
          instructorColonia: jsonDatos[i].instructorColonia,
          instructorCalle: jsonDatos[i].instructorCalle, 
          instructorNumero: jsonDatos[i].instructorNumero, 
          instructorFechaDeNacimiento: this.formatDate(jsonDatos[i].instructorFechaDeNacimiento), 
          instructorSexo: jsonDatos[i].instructorSexo,     
          instructorActivo: jsonDatos[i].instructorActivo,
          //TODO validar img
          instructorBase64Image : "",
          isActive: this.calcularBooleano(jsonDatos[i].instructorActivo),
          instructorTelefono :  jsonDatos[i].instructorTelefono
        
        
        })  
    
        }
        this.arrayInstructoresShow = this.arrayInstructores;
      });
  }
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
  
  btnEditar(json: any){
    
    this.visibleCancelar = 0;
    this.instructor.instructorId = json.instructorId;
    
    this.getPhotoInstructor();
    
    
    
    this.instructor.instructorNombre = json.instructorNombre;
    this.instructor.instructorApellidoMaterno = json.instructorApellidoMaterno;
    this.instructor.instructorApellidoPaterno = json.instructorApellidoPaterno;
    this.instructor.instructorCURP = json.instructorCURP;
    this.instructor.instructorRFC = json.instructorRFC;
    this.instructor.instructorCodigoPostal = json.instructorCodigoPostal;
    this.instructor.instructorEstado = json.instructorEstado;
    this.instructor.instructorMunicipio = json.instructorMunicipio;
    this.instructor.instructorColonia = json.instructorColonia;
    this.instructor.instructorCalle = json.instructorCalle;
    this.instructor.instructorNumero = json.instructorNumero;
    this.instructor.instructorFechaDeNacimiento = json.instructorFechaDeNacimiento;
    this.instructor.instructorSexo = json.instructorSexo;
    this.instructor.instructorActivo = json.instructorActivo;
    this.instructor.instructorBase64Image  = json.instructorBase64Image;
    if(this.instructor.instructorBase64Image === ""){
      this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      +iconpeople.people);
    }else{
      this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      +this.instructor.instructorBase64Image);
    }
    
    
    
  }
  
  
  reset(){
    this.visibleCancelar =1 ;
    this.instructor.instructorId = 0;
    this.instructor.instructorNombre = "";
    this.instructor.instructorApellidoMaterno = "";
    this.instructor.instructorApellidoPaterno = "";
    this.instructor.instructorCURP = "";
    this.instructor.instructorRFC = "";
    this.instructor.instructorCodigoPostal = 0;
    this.instructor.instructorEstado = "";
    this.instructor.instructorMunicipio = "";
    this.instructor.instructorColonia = "";
    this.instructor.instructorCalle = "";
    this.instructor.instructorNumero = "";
    this.instructor.instructorFechaDeNacimiento = "";
    this.instructor.instructorSexo = "";
    this.instructor.instructorActivo = "";
    this.instructor.instructorBase64Image  = "";
    this.instructor.instructorTelefono = "";
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    +iconpeople.people);
    
  }
  
  
  formatDate(fecha : string){
    var str = "";
    if(fecha != null){
      str = fecha.split("T")[0];
    }
    return str;
  }
  
  
  activarDesactivar(instructor : any){
    var send = {
      "instructorId" : instructor.instructorId,
      "instructorActivo": "SI" 
    };
    if(instructor.isActive){
      send.instructorActivo = "SI";
    }else{
      send.instructorActivo = "NO";
    }
    instructor.instructorActivo = send.instructorActivo;
    
    console.log(send);
    this.httpServices.activeInactiveInstructor(send).subscribe(
      datos => {
        console.log(datos);
        if(datos==0){
          if(instructor.isActive){
            instructor.instructorActivo = "NO";
            instructor.isActive = false;
            
          }else{
            instructor.instructorActivo = "SI";
            instructor.isActive = true;
          }
          alert("error");
        }
        
        
        
      },
      error => {
        console.log("hola desde el error"); 
        console.log(error); 
        if(instructor.isActive){
          instructor.instructorActivo = "NO";
          instructor.isActive = false;
          
        }else{
          instructor.instructorActivo = "SI";
          instructor.isActive = true;
        }
        
      });
    }
  
  
    getInfoCp(){
      //console.log("hola desde busqueda de cp con "+this.codePostal+" "+this.codePostal.toString().length);
      var strCP = this.instructor.instructorCodigoPostal;
      if(strCP.toString().length >= 4){
        this.httpServices.getInfoCodePostal(strCP.toString()).subscribe(
          datos => {
            var json = JSON.parse(JSON.stringify(datos));
            this.arrayAsentamientos = [];
              console.log(json+" esto en json "+JSON.stringify(json));
              for(var i = 0 ; i < json.length; i++){
                  let jsonAsentamiento = json[i];
                  this.instructor.instructorMunicipio = jsonAsentamiento.Municipio;
                  this.instructor.instructorEstado = jsonAsentamiento.Estado;
                  //console.log(i+" "+jsonAsentamiento);
                  this.arrayAsentamientos.push({nombre: jsonAsentamiento.Colonia,tipo: jsonAsentamiento.Tipo_Asentamiento, cadena: jsonAsentamiento.Colonia +" - "+jsonAsentamiento.Tipo_Asentamiento });
              }
          },
          error => {
              //this.openSnackBar("Código postal incorrecto");
              this.arrayAsentamientos = [];
              //this.estado = "";
              
              //this.municipio = "";
          }
          
          
          );
      }else{
        this.arrayAsentamientos = [];
        //this.estado = "";
        //this.municipio = "";
      }
   }
    
       
   validarDatos(){
    
    if(this.instructor.instructorBase64Image === ""){
      alert("Toma una foto al instructor")
      return false;
      
    }else if(this.instructor.instructorNombre === ""){
      alert("Ingresa el nombre del instructor")
      return false;
      
    }else if(this.instructor.instructorApellidoPaterno === ""){
      alert("Ingresa el apellido paterno del instructor")
      return false;
    }else if(this.instructor.instructorApellidoMaterno === ""){
      alert("Ingresa el apellido materno del instructor")
      return false;
    }else if(this.instructor.instructorCURP.length != 18){
      alert("Ingresa el una CURP valida")
      return false;
    }else if(this.instructor.instructorRFC.length != 13){
      alert("Ingresa el un RFC valida")
      return false;
    }else if(this.instructor.instructorCodigoPostal == 0 || this.instructor.instructorCodigoPostal == undefined){
      alert("Ingresa el un codigo postal")
      return false;
    }else if(this.instructor.instructorEstado === ""){
      alert("Ingresa el estado del instructor")
      return false;
    }else if(this.instructor.instructorMunicipio === ""){
      alert("Ingresa el municipio del instructor")
      return false;
    }else if(this.instructor.instructorColonia === ""){
      alert("Ingresa la colonia del instructor")
      return false;
    }else if(this.instructor.instructorCalle === ""){
      alert("Ingresa la calle del instructor")
      return false;
    }else if(this.instructor.instructorNumero == ""){
      alert("Ingresa el número del instructor")
      return false;
    }else if(this.instructor.instructorFechaDeNacimiento == ""){
      alert("Ingresa la fecha de nacimiento del instructor")
      return false;
    }else if(this.instructor.instructorSexo === ""){
      alert("Selecciona el genero del instructor")
      return false;
    }else{
      console.log("validar correcto");
      console.log(this.instructor);
      return true;
    }
   }
   
   
    public openPDF(json: any):void {
  
      this.value = crypto.AES.encrypt("I-"+json.instructorCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();
      var nombre = json.instructorNombre+" "+json.instructorApellidoPaterno+" "+json.instructorApellidoMaterno;
      var contexto = this;
      
      
      
      setTimeout(function()
      { 
                  
            console.log("entre al pdf "+contexto.value);
      
            
           

          let PDF = new jsPDF('p', 'mm', 'letter');
          console.log("entre al pdf");
          
          
          var qrcode = document.getElementById("qrcode")?.innerHTML;
          var parser = new DOMParser();
          var divContenedor;
          console.log("entre al pdf");
          console.log(qrcode);

          if(qrcode != undefined){
            divContenedor = parser.parseFromString(qrcode,"text/html");  
          }
          var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 

          if(codeBase64 != undefined){
            PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
          }
          
          
          //PDF.setDrawColor(0.5);
          
          PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
          PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
          
          
          PDF.setFillColor(150,150,150);
          PDF.rect(53,90,110,7,'F');
          
          PDF.rect(63,33,35,35);
          
          PDF.line(55,80,106,80);
          
          
          PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
          PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
          PDF.setFontSize(6)
          
          PDF.setTextColor(255,255,255);
          PDF.text("Asociación Monarca de Triatlon",72, 93);
          PDF.text("del Estado de México",72, 96);
          
          PDF.text("Centro de Desarrollo del Deporte",129, 93);
          PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
          
          PDF.setTextColor(0,0,0);
          
          
          
          PDF.cell(53,12 , 55, 85," ",0, "center");
          PDF.cell(108,12 , 55, 85," ",0, "center");
          
          
          
          
          
          PDF.setFontSize(8);
          var w = PDF.internal.pageSize.getWidth();
          
          if(nombre.length > 30 ){
            var char = nombre.split("");
            var espacio = 0;
            for(var i = 29 ; i > 0; i--){
              if(char[i] === " "){
                espacio = i;
                break;
              }
            }
            
            var nom1 = nombre.substring(0,espacio);
            var nom2 = nombre.substring(espacio);
                        
            PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
            PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
          
          }else{
            
            PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
          }
         // PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
          console.log("entre al pdf");
          PDF.save(nombre+'_qr.pdf');
          
          contexto.value = "";
          console.log("entre al pdf");
          
      
      }, 500);
      }
      
  
    
  
////////inicio funciones camara 
open(content:any) {
  console.log(content);
 this.modalService.open(content, { size: 'xl' });
 
}
getMediaDevices(){
  this.arrayCamaras = [];
  var contexto = this;
  navigator.mediaDevices.enumerateDevices().then(function(devices) {
    devices.forEach(function(device) {
      console.log(device.kind + ": " + device.label +
                  " id = " + device.deviceId);
                  if(device.kind === "videoinput"){
                    if(contexto.idCamara === ""){
                      contexto.idCamara = device.deviceId;
                    }
                    contexto.arrayCamaras.push(device);
                  }
    });
  })
  .catch(function(err) {
    console.log(err.name + ": " + err.message);
  });
}

async iniciarCamara(video: any) {
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    try {
      this.stream = await navigator.mediaDevices.getUserMedia({
        video: {
          deviceId: { exact: this.idCamara },
          width: { ideal: 500 },
        height: { ideal: 500 } 
        }
      });
      
      if (this.stream) {
        video.srcObject = this.stream;
        video.play();
        //this.error = null;
        
      } else {
        //this.error = "You have no output video device";
      console.log("error desde ek id stream");
      } 
    } catch (e) {
      console.log("error catch "+e);
      //this.error = e;
    }
  }
}

public captures!: Array<any>;

public capture(canvas: any, video: any) {
  this.isCaptured = true;
  this.captures = [];
  var context = canvas.getContext("2d").drawImage(video, 0, 0, 500,500 );
  
  console.log(canvas.toDataURL("image/png"));
  this.captures.push(canvas.toDataURL("image/png"));
  this.auxImg = canvas.toDataURL("image/png");
  
}

tomarOtra(){
  this.isCaptured = false;
  this.auxImg = "";
}

closeModal(){
  this.modalService.dismissAll();
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });

  this.isCaptured = false;
}


btnGuardarfoto(){

  if(this.auxImg != ""){
    this.instructor.instructorBase64Image = this.auxImg.split(",")[1];
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.instructor.instructorBase64Image);
    this.modalService.dismissAll()
    
    
  }else{
    alert("no se guardo ninguna foto");
    this.modalService.dismissAll()
  }
  //navigator.mediaDevices.getUserMedia().finally();
  
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.isCaptured = false;
}

cambioCamara(video:any){
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.iniciarCamara(video);
}
////////fin funcines camara

buscarInstructor(){
  this.arrayInstructoresShow = [];
  if(this.strBusqueda.length >= 2){
    for(var i = 0 ; i < this.arrayInstructores.length ; i++){
      
      var nombre = this.arrayInstructores[i].instructorNombre+" "+this.arrayInstructores[i].instructorApellidoPaterno+" "+this.arrayInstructores[i].instructorApellidoMaterno;
     if(nombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
      this.arrayInstructoresShow.push({
        instructorId: this.arrayInstructores[i].instructorId, 
          instructorNombre: this.arrayInstructores[i].instructorNombre,
          instructorApellidoPaterno: this.arrayInstructores[i].instructorApellidoPaterno, 
          instructorApellidoMaterno: this.arrayInstructores[i].instructorApellidoMaterno, 
          instructorCURP: this.arrayInstructores[i].instructorCURP, 
          instructorRFC: this.arrayInstructores[i].instructorRFC, 
          instructorCodigoPostal: this.arrayInstructores[i].instructorCodigoPostal, 
          instructorEstado: this.arrayInstructores[i].instructorEstado, 
          instructorMunicipio: this.arrayInstructores[i].instructorMunicipio, 
          instructorColonia: this.arrayInstructores[i].instructorColonia,
          instructorCalle: this.arrayInstructores[i].instructorCalle, 
          instructorNumero: this.arrayInstructores[i].instructorNumero, 
          instructorFechaDeNacimiento: this.formatDate(this.arrayInstructores[i].instructorFechaDeNacimiento), 
          instructorSexo: this.arrayInstructores[i].instructorSexo,     
          instructorActivo: this.arrayInstructores[i].instructorActivo,
          //TODO validar img
          instructorBase64Image : "",
          isActive: this.calcularBooleano(this.arrayInstructores[i].instructorActivo),
          instructorTelefono :  this.arrayInstructores[i].instructorTelefono
      }
      );
     }
    }
  }else{
    this.arrayInstructoresShow = this.arrayInstructores;
  }
  
}    



getPhotoInstructor(){
  
  console.log("el id del instructor es "+this.instructor.instructorId);
  this.httpServices.getInstructorbyId(this.instructor.instructorId).subscribe(
    datos => {
      console.log("foto de instructor");
      console.log(datos);
      if(datos === "There is no row at position 0."){
        this.instructor.instructorBase64Image = "";
        console.log("asigne por default");
      }else{
        
        var json = JSON.parse(JSON.stringify(datos))
        this.instructor.instructorBase64Image = json[0].instructorBase64ImageString;
      }
  
        if(this.instructor.instructorBase64Image === ""){
          this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +iconpeople.people); 
        }else{
          this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +this.instructor.instructorBase64Image);
        }
      
      
    });
}

validaCelulcar()
{
  if(this.instructor.instructorTelefono=="")
  {
    alert("El telefono celular es obligatorio")
  }
  else
  {
    if(this.instructor.instructorTelefono.length<10)
    {
      alert("El telefono celular debe ser a 10 digitos")
    }
  }

}



}
