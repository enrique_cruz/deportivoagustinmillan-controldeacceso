import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ServicesService } from '../services/services.service';
import * as crypto from 'crypto-js';
import { DomSanitizer } from '@angular/platform-browser';
import { BroadcastChannel } from 'broadcast-channel';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-salida',
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({      
        background: "linear-gradient(70deg, #a0d9ea, #29abe2)",
        width: "100%", 
        height: "100vh", 
        position: "absolute",
        zIndex: "100",
      })),
      state('closed', style({
        background: "linear-gradient(70deg, #a0d9ea, #29abe2)",
        width: "0%", 
        height: "0vh", 
        position: "absolute",
        zIndex: "-100", 
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ])
    ]),
  ],
  templateUrl: './salida.component.html',
  styleUrls: ['./salida.component.css']
})
export class SalidaComponent implements OnInit {
  splashShow = true;
  
  channel = new BroadcastChannel('foobar');
  showImg : any;
  
  idDevice1 = "";
  idDevice2 = "";
  
  //dispositivos de entrada
  availableDevices!: MediaDeviceInfo[];
  deviceCurrent!: MediaDeviceInfo;
  deviceSelected!: string;
  hasDevices!: boolean;
  hasPermission!: boolean;
  qrResultString: string = "";
  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;
  //dispositivo de salida
  availableDevices2!: MediaDeviceInfo[];
  deviceCurrent2!: MediaDeviceInfo;
  deviceSelected2!: string;
  hasDevices2!: boolean;
  hasPermission2!: boolean;
  qrResultString2: string = "";
  torchEnabled2 = false;
  torchAvailable$2 = new BehaviorSubject<boolean>(false);
  tryHarder2 = false;
  
  

  alumnoDatos = {
    acceso : false,
    alumnoNombre : "Nombre del alumno",
    alumnoApellidoPaterno: "",
    alumnoApellidoMaterno: "",
    disciplinas: [],
    tutores: [],
    foto: "",
  }
  
  
 arrayTutores:Array<{
    tutorId: string, 
    tutorNombre: string,
    tutorApellidoPaterno: string,
    tutorApellidoMaterno: string,
    tutorParentesco: string
  }>=[]; 
  
    
  arrayDisciplinas:Array<{
    disciplinaNombre: string
  }>=[]; 


  qrArrSplit : any;


  constructor( private sanitizer: DomSanitizer,private route: ActivatedRoute,private httpServices: ServicesService) { 
    
    var id1 = this.route.snapshot.paramMap.get("idEntrada");
    if(id1 != null){
      this.idDevice1 = id1;
    }
    var id2 =  this.route.snapshot.paramMap.get("idSalida");
    if(id2 != null){
      this.idDevice2 = id2;
    }
  
  }

  ngOnInit(): void {
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.baseImg);
  }
  
  ngAfterViewInit(){

    var contexto = this;
    setTimeout(function()
    { 
         contexto.onDeviceSelectChange2(contexto.idDevice2);
    // contexto.onDeviceSelectChange(contexto.idDevice1);
   }, 5000);
   

   
 }
  

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }
  
  onCamerasFound2(devices: MediaDeviceInfo[]): void {
    this.availableDevices2 = devices;
    this.hasDevices2 = Boolean(devices && devices.length);
  }
  onCodeResult(resultString: string) 
  {
    if(this.qrResultString === "")
    {

      // console.log("Escaneo de salida listo");
      // this.qrResultString = resultString;
      // console.log(this.qrResultString);


      this.qrArrSplit = this.qrResultString.split('-');
      console.log("Verificamos si es QR Maestro o cualquier otro");
      
      if(this.qrResultString[0]=="M")
      {
        console.log("Es QR Maestro")
        console.log(this.qrArrSplit);
        this.registrarEntrada(this.qrResultString);
      }
      else
      {

        console.log("Es Alumno o Instructor o Integrante de equipo");
        console.log(this.qrArrSplit);

        var descrip = crypto.AES.decrypt(this.qrResultString+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString(crypto.enc.Utf8);
        
        console.log("Decrypted")
        console.log(descrip); 
        
        this.registrarEntrada(descrip);
      }      
    }    
  }

  onCodeResult2(resultString: string) {
    console.log("Escaneo de Salida - OnCodeResult2(resultString)....");
    console.log("resultString: ");
    console.log(resultString);
    console.log("qrResultString2: ");    
    console.log(this.qrResultString2)


    if(this.qrResultString2 === ""){
      console.log("Cadena no vacia válida ***");
      this.qrResultString2 = resultString;

      console.log("Registrar salida de")
      console.log(this.qrResultString2);

      this.qrArrSplit = this.qrResultString2.split('-');
      console.log("Verificamos si es QR Maestro o cualquier otro");
      
      if(this.qrArrSplit[0]=="M")
      {
        console.log("Es QR Maestro")
        console.log(this.qrArrSplit);
        this.registrarSalida(this.qrResultString2);
      }
      else
      {

        console.log("Es Alumno o Instructor o Integrante de equipo");
        console.log(this.qrArrSplit);

        var descrip2 = crypto.AES.decrypt(this.qrResultString2,"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS");
      
        console.log(descrip2);
          
        if(descrip2.sigBytes > 0)
        {
          var descrip = descrip2.toString(crypto.enc.Utf8);
          this.registrarSalida(descrip);
        }
        else
        {
          this.qrResultString2 ="";
        }

      }
        
    }
  }


  onDeviceChange(device: MediaDeviceInfo) {
    const selectedStr = device?.deviceId || '';
    if (this.deviceSelected === selectedStr) { return; }
    this.deviceSelected = selectedStr;
    this.deviceCurrent = device || undefined;
  }
  onDeviceChange2(device: MediaDeviceInfo) {
    const selectedStr = device?.deviceId || '';
    if (this.deviceSelected2 === selectedStr) { return; }
    this.deviceSelected2 = selectedStr;
    this.deviceCurrent2 = device || undefined;
  }

  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }
  
  onHasPermission2(has: boolean) {
    this.hasPermission2 = has;
  }
  
  onDeviceSelectChange(selected: string) {
    const selectedStr = selected || '';
    if (this.deviceSelected === selectedStr) { return; }
    this.deviceSelected = selectedStr;
    const device = this.availableDevices.find(x => x.deviceId === selected);
    if(device != undefined){
      this.deviceCurrent = device ;
    }

    //this.currentDevice = device;
  }

  
  
  onDeviceSelectChange2(selected: string) {
    const selectedStr = selected || '';
    if (this.deviceSelected2 === selectedStr) { return; }
    this.deviceSelected2 = selectedStr;
    const device = this.availableDevices2.find(x => x.deviceId === selected);
    if(device != undefined){
      this.deviceCurrent2 = device ;
    }

    //this.currentDevice = device;
  }
  
registrarEntrada(string: string){
  var curp = string;
  console.log(curp);
  

  this.httpServices.getRegistroEntrada(curp).subscribe(
    datos => {
      
      this.splashShow = false;
      console.log(datos);
      var json = JSON.parse(JSON.stringify(datos));
      this.chanelSend(JSON.stringify(datos));

      if(json[0].Estatus === "Success")
      {
    
        this.alumnoDatos.alumnoNombre = json[0].alumnoNombre+" "+json[0].alumnoApellidoPaterno+" "+json[0].alumnoApellidoPaterno;
        this.alumnoDatos.acceso = true; 
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + json[0].alumnoBase64ImageString);
      }
      else
      {
  
        this.alumnoDatos.alumnoNombre = json[0].alumnoNombre+" "+json[0].alumnoApellidoPaterno+" "+json[0].alumnoApellidoPaterno;
        this.alumnoDatos.acceso = false; 
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + json[0].alumnoBase64ImageString);
       // alert(json[0].Estatus);
      }


      var contexto = this;
      setTimeout(function()
      { 
        contexto.splashShow = true;
        contexto.alumnoDatos.alumnoNombre = "";
        contexto.alumnoDatos.acceso = false;
        contexto.showImg = contexto.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + contexto.baseImg);
        contexto.qrResultString = "";
      }, 3000);
      
      /*  if(datos == 1){
      
        alert("Gracias por tu visita");
      }else if(datos == 2){
        alert("No se registro entrada");
      }else{ 
        alert("Error");
      }*/
   
    });
  
}

registrarSalida(string: string){
  
  var curp = string;
  console.log(curp);  

  var tipo = curp.split('-')[0];
  
  this.httpServices.getRegistroSalida(curp).subscribe(
    datos => {
      console.log("Respuesta del request de salida")
      console.log(datos);

      var json = JSON.parse(JSON.stringify(datos));

      this.splashShow = false;

      if(json[0].Estatus == 1)
      {
        var array = [];
      
        array.push({
          Estatus : "Salida"
        });

        //json[0].Estatus="Salida";
        //this.chanelSend(json)
        this.chanelSend(JSON.stringify(array));
        
        this.alumnoDatos.acceso = true; 
        this.alumnoDatos.alumnoNombre ="Gracias por tu visita";
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + json[0].alumnoBase64ImageString);
        console.log("Gracias por tu visita");
      
      }
      else if(json[0].Estatus == 2)
      {        
        this.alumnoDatos.acceso = false; 
        this.alumnoDatos.alumnoNombre ="No se registro entrada";
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + json[0].alumnoBase64ImageString);
     
        console.log("No se registro entrada");
      }
      else
      { 
        this.alumnoDatos.acceso = false; 
        this.alumnoDatos.alumnoNombre ="Error";
        console.log("error");
      }

      var contexto = this;
      setTimeout(function()
      { 
        contexto.splashShow = true;
        contexto.alumnoDatos.alumnoNombre = "";
        contexto.alumnoDatos.disciplinas = [];
        contexto.alumnoDatos.tutores = [];
        contexto.alumnoDatos.acceso = false;
        contexto.showImg = contexto.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        + contexto.baseImg);
        contexto.qrResultString2 = "";
      }, 3000);
      
    });
  
  
}


chanelSend(json: string){
  this.channel.postMessage(json);
}
  

  //base64 de foto por default
  baseImg ="/9j/4AAQSkZJRgABAQEBLAEsAAD/4QBWRXhpZgAATU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAAITAAMAAAABAAEAAAAAAAAAAAEsAAAAAQAAASwAAAAB/+0ALFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAPHAFaAAMbJUccAQAAAgAEAP/hDIFodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0n77u/JyBpZD0nVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkJz8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0nYWRvYmU6bnM6bWV0YS8nIHg6eG1wdGs9J0ltYWdlOjpFeGlmVG9vbCAxMS44OCc+CjxyZGY6UkRGIHhtbG5zOnJkZj0naHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyc+CgogPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9JycKICB4bWxuczp0aWZmPSdodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyc+CiAgPHRpZmY6UmVzb2x1dGlvblVuaXQ+MjwvdGlmZjpSZXNvbHV0aW9uVW5pdD4KICA8dGlmZjpYUmVzb2x1dGlvbj4zMDAvMTwvdGlmZjpYUmVzb2x1dGlvbj4KICA8dGlmZjpZUmVzb2x1dGlvbj4zMDAvMTwvdGlmZjpZUmVzb2x1dGlvbj4KIDwvcmRmOkRlc2NyaXB0aW9uPgoKIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PScnCiAgeG1sbnM6eG1wTU09J2h0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8nPgogIDx4bXBNTTpEb2N1bWVudElEPmFkb2JlOmRvY2lkOnN0b2NrOjllYzdmMDA5LTM4MTUtNGUzNy1iNTkwLTA4NDk5NGVmNGVlNDwveG1wTU06RG9jdW1lbnRJRD4KICA8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOmVjMzk5OWY2LTdmZjEtNDU5NC05MzkxLWM0OWRjNzg3YzZjNzwveG1wTU06SW5zdGFuY2VJRD4KIDwvcmRmOkRlc2NyaXB0aW9uPgo8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSd3Jz8+/9sAQwACAQEBAQECAQEBAgICAgIEAwICAgIFBAQDBAYFBgYGBQYGBgcJCAYHCQcGBggLCAkKCgoKCgYICwwLCgwJCgoK/9sAQwECAgICAgIFAwMFCgcGBwoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoK/8AAEQgB9AH0AwERAAIRAQMRAf/EAB4AAQACAgMBAQEAAAAAAAAAAAACAwEEBQkKCAYH/8QAThAAAQMCBAQEAwMJBQUGBQUAAQACAwQRBQYSIQcIMUEJE1FhFCJxCjKBFRkjQlJZkZahFmLB0eEkM3Kx8CVDU3SCkhc0RIOiY5OjstL/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIFBAMG/8QAMREBAAEDAQYGAQMEAwEAAAAAAAECAxEEEhQhMVHwBUFSkaHREyJxgTJCYfGxweEj/9oADAMBAAIRAxEAPwDv8QEBBh5sLINV87Y5A97bgdQgtNPTzvbUg3Fr2HQoKYcUpKuU0jorA7DUNj7IMuqZKOb4VzNbLXYb7genugy+aSoIbps30QXQx2G6CxAQEBAQEBAQEBAQEBAQEGH9PxQa8LmfEWeN/wBX6oE/wU8xppjok7G9r/T1QVw09Xh0hLB5sTuob94e9kE2z0LXmaGEaz1+SxQYY10z/Mf3QbMbA0IJICAgICAgICAgICAgIMEgBBB01u6Cmao2sCgxRODxJKG6i0bBBJ9UYqUGuY3W/pG1BUIpGkNkbY2QWth1WLje3S5QXRx6R0QTQEBAQEBAQEBAQEBBW+W3dBTJUDoEEW0vxDPMlk0tKDAgnwz9NDIZIju9h6geoQTNFRzzsxBjrfrG3Q+6CMlqicvHToEGxFEAEFgAHRAQEBAQEBAQEBAQEBAQEBBhwuEGpUMLXa27EdCgu/2eRra58d3Mad7bhBRHW1TpS8gaT0Zbogm2N8r/ADJOpQXxxho3CCaAgICAgICAgICAgICAgrmdZBWyOOojIDvm/wCSCHk09NF5lfIASel/8kGWsipWmto/mjI+dgPUeoQPOw6SUVQGp9tiQdkBrnzyeY4fQeiDYY2wugkgICAgICAgICAgICDDzYWQazp2Ml1SC4H9EEcQp3uj+Ipt9rlo7+4QYqInV+GMNM65ABt62FrIM4VHUQU721QIaPuh39UFVPE5rfLa46b3tdBtwxadkFwFtggICAgICAgICAgICAgICAgICCqaPUOiCEJMQc0i4PQIMQwAG5CC9rA0dEEkBAQEBAQEBAQEBAQEGCQAgg6YDugollLzpbvf0QQdSV7XCaDSCOjS7qglJHDisBjkbolZ1B6tP+SBSU8lDQPjmcLuJsAfwQYp6cW6bINqOINHRBNAQEBAQEBAQEBAQEEJJNPdBQ+pHZBXEWz1LWO33uQgVGKvpsQMMrP0YA7b/VBOaJ8H+10UgDXbuYeh9wgGWeoGl1gO4CC6GENCC0C2wQEBAQEBAQEBAQEBAQEBAQEBAQCAeqCJjB7/ANEGWtAQZQEBAQEBAQEBAQEBAQEFcz7BBWyJk8R0P+f37IKKFzvjzDMLOa02BQVVNdX01eQSS2+zSNiEGzXtLJo6mLZ+4PuEAebUO1SdugCDYijDR0QTQEBAQEBAQEBAQEBAQa07gCNQ2vuglUPpGRtEwAY7ZruwQa78PljkbVUEofbcNcev4oLXy0VQQayDS9v6r27j/NAklNRZjG6WjsgthiDRZBaBbYICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICASALlBW+YDugommDhYIMQuZTU766TfbYf9e6A10OJxippn6JozsfQ+h9kGW15voqKUh7SgfPUv1vG3YeiDYjjDR0QTQEBAQEBAQEBAQEBAQEFFQwkIIQNjqIHUkwuOw9kEZ6t8Ugp6RgaGdbj+iCT3SVVg5oAHp6oLYYQ0dEFoFhYICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgjKSAgphjZM8l5vb9VBXPRPe3XSSh390n/FBGklYYnYdXM0E3sHbaggnT0NLhzzN5riSLAEoMBrp5TI7ug2Y4w0dEE0BAQEBAQEBAQEBAQEBAQRkbqCCgMdFKHt/FAkYJZi8N/1QXRxADogmgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgjILhBqh5gnD+3f6ILBSx01S+tMxa0i5bfa/qgiK2CpDmTU509ri9/8AJBXFTtLiWssPRBtRRADogsQEBAQEBAQEBAQEBAQEBAQEGCwFBgRgIJICAgICAgICAgICAgICDDnBqCt04HdBgT3NgSgmyUHugmgICAgICAgICAgICAgICAgICAgIBFxZBr1EWrsgx8slKYperdh/ggjDT26hBsMjDR0QTQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQUzvsCUEWRxVENmO+Ydb9igrodYfLHKyzmj0QQhqD2KDbikuLoLEBAQEBAQEBAQEBAQEBAQEBAQEBBhzQ5BAw79EEms09UEkBAQEBAQEBAQEBAQEBBgvAQRMwv2QBKCgmCCLhAQEBAQEBAQEBAQEBAQEBAQRe8NCDWqH6jpAQQqWtw9gmZMfNJ+72d7INilqYquPz4mgPAsQe3sgh8aS4xy0xa4dr3QSi1OOo90F6AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMONhdBSSHyCPVZBRV66Z13XLT0cgsqHNgDA3uOvqgk2e4QbCAgICAgICAgICAgICAgXA6lBF8gA2KDXklc52hguUFT46mGRsz4Toa4FxBvYIJYjQSVb2zwOB+W1ie3qgwyj+AjbK2X9JfcdiPRBNjHzP8yQ7lBssZpF0EkBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQQldYdUGqY5KiTTGbe/ogmyZsrnYfWWLrbH9of5oJ1ApI2sbUg2As1xB/wQRtQW2O3sSg2UBAQEBAQEBAQEBAQEGHODUFMs4b3QVSmbS12g2d0sglQl2iVwZ87TYAoNfD8UqZaryKgXDjYbWLSgtlM9NUOZBIQw7hvUBBNkb5DrkcSfdBfHGG9kE0BAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAJA6oISShvdBrTT6ht0QSoX+ZBJ5ZAfcge22yDSioqmafS6NzSDu49vdBuySyCI00zQ53TV6j1+qDDKclu4QbaAgICAgICAgICAgIB2F0GvPKQghLA1kBlnm0E9LoM0NdA+FjJKhusm1j1QUyfHUVcZmsdKyU72CC4VVLrMraYiTvdoBCAxrpXmR/dBsMYAOiCSAgICAgICAgICAgICASB1QQMoCB53uEEmuDkGUBAQEBAQEBAQEBAQEBAQEBAQYc8NQUvn3sNyg1pajU7Tfcm3VBbV1seGBsbYtRd13sgyZmzU/x9Hs4D5gR19QUBtdLI2zYg0+t7oMxQknU7clBeGC24QSQEBAQEBAQEBAQEBBh5sLINaRwEzXHoDugzVUDK2Rsj5zpA+6EFbcPwqW8ERGoC92vuQgzT1k0QME7dZYSNd+qDLGOmkMr+pQbLIw0IJICAgICAgICAgICAgICCmaTTvdBXURyMi81rtQtcgII072yUz577g2+iCUNRfYlBsMffZBJAQEBAQEBAQEBAQEBAQEBBh7tIQa73ve7y4xugiwS09Wxj2gh9wDdBLEYSKR3w8ALr326j3CCpk9FitOGVJDXjrvax9kFgFPTU/w1MdV+puglBDbchBe1ob0QZQEBAQEBAQEBAQEBBh7w0INd8xLtLASfRBXMyVvzSMIHqgrdHr+UusD1QbEEEFAwvB1PcOvqgjHEZHF7u5uUGyxgaOiCSAgICAgICAgICDXxLFcNwagmxTFa+GmpqeMvnqKiVrI42jqXOcQGj3JTjMj5f46+Nx4UnLjic2CcUeeXIrK+BpM2H4DiD8XqGEfqujoGTFrvY2K96dNfr5UvGq/apjOXzjmf7Wt4ReX62WlwvH+I2OMjdZlRhWQZWsk92/ESRH+IC9o0Gol573axnP8Ax9uJwz7Xx4UddIY6zL/FyiAdYPqMkwPBHr+iq3H+imfDtRHREay1M/6+39V4V/aZPBs4otZDLzW/2YqpCAKbOGV8QoLX9ZTC6If+9Uq0WppjOF6dTaqqxEvrjhNzIcAuYfBG5j4Dcacq5zoHRh/xWV8wU9cwA9NXkvcWn2NiuaqiuicVRh7U10V8py/ZtmloWtdUEGNxsSDu0qqy5sNNDFI4f7uTcgbj+iCtrcP0aonbDsHFBZCSf4oLkBAQEBAQEBAQEBAQEBAQEFcxFjughTA+W9zAC++1/wCiDV/JVXUSGapqNJv23P8Aogmxs+FvGuUywvO5PVpQTnNI+PTDE0lxvcNsgzDBbchBssbpCDKAgICAgICAgICAgw52kIKn1AHUoKZKnbZBmheXxyyRAF42aCgS1L6aIQTOEsz/ANUD/rZBHyHxuDJLdOyC1kA62QXMjDQgkgICAgICAgICD81xc4xcK+AvD/EuKvGjiFg+V8t4RAZsSxvHcQjpqanYP2nvIFz0DRcuJAAJNlNNNVc4p4omqKYzLpU8RP7YJl/B6jEeG3hu8LGYvKxz4W8Sc70r46Q9R5lJh4LZJR3a+odGPWJwWnZ8OmeNyXBd11NPCnvvuHTlzW+Ihzt87+LPxPml5ls1ZtiMpkiwiqxAwYbASb/oqKAMp2fUMv7rSt2LVr+mGdXqLtznL+MMtG3RGA1v7LRYfwC9njMzVOZEQIMhzm/dcR9CiYiX6HhPnTNvDnPdHnLh/mzE8AxikeZKPFcExCSkqYngbFssTmuB/FUqppmMTD1iuqnGJdkHJ99qE8RLllzDS5f5gamDjPlBuhhgzC4U2MQs2uYq+Jl5Hddqhkt/2m9Vw3dBarj9PCXZb1tdM4q4999Xeh4cvjBcm3iM5dii4MZ7dh2ao6fzcT4fZk0U2MUdh8zmRhxbUxD/AMSFz2jbVoJssu9p7tif1Rw6tG3eouxwfUscQe8va0AE3AC8Hq2o2aQgmgICAgICAgICAgICAgICCEkgaNigoknB2CCtsj2OLo3WJQYcyurDp806e9th/RBdUtayBtKHFxFr3QIIO5CDYa0NCDKAgICAgICAgICAgIKp32BKCuEQVEZiJs/r7oIPNJh8bTVnU53tdBIGFkRr6EC1vnaNgR/gUBlbSvPnMpzrI6lov/FAjD5XmR/dBssbYXKDKAgICAgICAgIPmLxQfFX5bPCz4JO4mcZsTOI5gxJkkeTsjYdUNGIY7UNG4be/kwMJBkqHDSwEABzy1jvexYrv1Yh5Xb1NqnMvLR4j3in82fifcUf7dcwucfKwWhne7LGRsIkfHhOCRnp5cZP6WYj71RJeR3QFrbMG7Y09uxHDmxb2pruz/h839eq6HMICAgy5rm21NIuLi47ItFLmsDpaWiwx2NVMXmOFywW6AG234qszxW5NyixGix5pikhMcrPmbvcj3BUckRMS15szz0Ez6OqpA98bra2vsHe9rbKcZSpwXiBnHK+a6DPGUMyV2DYvhNUypwnE8JrH09RRTNN2yRSsIcx4IHzAgpNEVRiSK5onMO/TwOPtOVPxexnBuUjxHseoMOzJVGOjyvxTk0U9Li0xs1lPiLRZlPUO2DahumKQ2DhG4gvyNVoZp/Xb9mrp9XFc7NXPvvvLu3BDhcLMd4gICAgICAgICAgICAgi54agg+cDqUGtPPqNr9Tsgv1GCVlNDAXXF3vOwAQU1DonzltOL6R+k09AgzCHjdjiLoLGQfNc7lBe1ukIMoCAgICAgICAgICDDnaQgqfOB1KCmSR0rgxguT0CCL8MqriWKoDXjcbIJWjxKM0lZHomZ/1ceyDMNP8BQuge8OLyf67IFPAOtkGyyMNG4QSQEBAQEBAQEBB87+J34jfBnwxOVvFeYjiu746sL/gspZXgqAypx3E3tJjpoyfuMABfJJYiONrnWJ0tPrZs1X7kUw87tym1RmXkT5xucHjrz2cwWO8yfMPmt2KZgxuazIo9TabDaVpPlUVLGSfKgjBs1vUm7nFz3Ocfo7Vqi1RFNLAvXqr1WZfy9ejyEBAQ5t38h4jDSDEX04LG/MWE729SPRRmF4iH6OKnpauOKsko2axH8oIBsCOiolx2GV9PTmXCMShELS92lrjsAe1/wDFTPFCyGlwbBJHV/xmo6SGM1g/wt1QjEOBral1ZVyVThYvcTb0V4RMqkVCA4FrgCCLEEbEImJmJzDv0+zNePDi+ca7A/DW5xM1yVWI+UKPhNnPEqnVJVNY35MGqnuN3SBoIp5CbvDfJJLhHqx9dpNn/wClH8tfR6rb/RVzd6AIIuFltAQEBAQEBAQEBAQEGHGwugokcC8ML7XPVBCakm80CI/KepPZAfh9LKTE2pOsdbOFx+CCYnY5ppKmXRJaxINr+4QRaKakhMFLuT1N7oJwMsAgvAA6ICAgICAgICAgICAgIKp32BKCEbIaiItBs8dT3CCihL4sQdBOLO0HTfv9EFNQcRp8QL2lxu75bdCPRBtYiz9NFKw2e2+4QGMfK7XIST7oNmNmkdEEkBAQEBAQEBAQaeYcwYLlTAa3M+ZMVp6HDsOpJKqvrquUMip4Y2F8kj3HZrWtBcSegBSImZxCJmIji8hfjb+KDmbxQeczFeIWGYvVDhzleWbCuGWESAsbHQB/z1rmdpqpzRK4ncM8qO9o19FpNPFi3jz82Jq783K8RyfHa6nGICAg3cvQwz4tE2axAuQD3IGyieS1LlMXw/GcUrDA0tZTNtpJfsfcgbqsTELMx4NiWGNbUUdeZTGP9y5pAI7gbqMyjEwm7F8Aq4BWVLYy9rfuPZdw9vdTiUvzkrmPlc+OPS0uJa297D0V1ZlFFRAQX4XimJYHidNjWDYjUUdZR1DJ6SrpJjHLBKxwcyRj27se1wDg4bggEKJiJjErU1TTOYetTwBvFKg8TLktpMTz7jMEnFDIjosG4hU7QGuqpNJNPiQaNgypjaXG2wljmaAAAvntXY/Bd4cpb2nuxdt5fdK5XQICAgICAgICAgw54agrkmAHVBrSCWckRMJPsgzTVc9K8U9fGWgmzHnp9EFL8Jnp8RZPTOJaX3JJ3b63QXV0bZaoG24bZBZDABvZBsNbpCDKAgICAgICAgICAgE2F0FT5w3YlBRNOHBAje2koX1obdx6fxsgRyQYtAHsOiVhuCOrT/iEBtbVRkwzwt1D9YHYoJNa+Z/mSFBsMjDR0QSQEBAQEBAQEBAQdU/2sTn7n5bORyi5VsjYsYMzcaaiahrnwyWfTYDT6HVp23HnOfDT+7ZJfRd/h9rbu7U+Tj1l2bdvEebzJk3N1usMQEBAQxlmOR8TxLG8tc03BHYovEYhyLMfx2rIpYH3e7YaGC5UYhLk6Fs+CUEtZi1U5z3kHQX339PqVXhMj825xc4uPc3V1ZlhFRAQEBB9q+ARz9VHID4jmUc249iroMmZ3mZlTPDHPtGylqpWiCqd2vBU+VJfqGGUD7y5NZZ/LZnrDs0V2aLmPJ67mm4uV883GUBAQEBAQEBBhxsEFLna5BGXWugormvpvn3LT3QSmnkpcNbPALkgEm3S6DFBVDE4HwVUYJA3IHVBilqaiKLyHfMWmzXH0QWwxEnU7cnqg2Gt0hBlAQEBAQEBAQEBAQEEZTYIKqdscj3a9yOgPogqmo4qoF1JOAR1af8Aq4QRppDTxmixGKzTfS47tI+qCcEWH0RMlOdTnDazroDI3SvMj+pKDZYwNHRBJAQEBAQEBAQEBBhxs0kC9h0QeTn7S/zU4hzNeLLnvBYMRM2CcMooMnYJEH3ax1M3zKx222o1c07SfSNo7Lf0NuKNPE9eLE11e1dx33zfAS7XEICAiYjLmsDpaWkwx2MTwea+9mNte29hb3uqzPFZsY4yjlw5r6qmEdQ8DymM3dq9PcKIS/P/AKamm/WY9jvoQVdEzhZWYhWV7w+rnL7dB0A/BIjCuZUogQEBAQEC1/l1lt9tTTuPce6TxhMTicvZP4NfNVXc5/hn8I+PWP4iypxutytHh+Y5Q8lzsRoXuo6h7r9HPfAZP/uD1XzWpt/ivVUvorNe3biX06vB6iAgICAgIMOcGoK3zDpdBqyAzSCNp+8bBBsSVFKJBh877lzbHV3/ANUFQc7DR5EzC+An5HWvb2KCTZ6eNhbRxW1dwLIMwQW3IQbDWBqDKAgICAgICAgICAgICCMguEGq5xgnEnvv9EFjoKWlmfiDju4dv8PqghHiDpQRLALHoLoMRQXdq0gD0CDZjj09UE0BAQEBAQEBAQEBBo5nzBhmVMu12Z8ZnEdJh1HLVVUhP3Y42F7j+AaVMRmcImcRl4aOKvELF+LfFDMvFbH6l81dmfMNdi9ZNI7U58tTUSTuJPfd6+popimmIh85dq2q84cArPMQcsctGTC2VdHP5khGpzR0I9B7hVzxXiIW0mVaZsLX4lVOY9/RjXAW9t+pSakt2kYzAWCmqJrwOd+jldtpPof81HMRnGD0lS7Fqms8x/8A3bS8G3s0BOMj8/iNa7EKx9W5obqOwHYK8cIUlSiBAQEBAQEBB6PfsanF+szRyQcS+DFZVmX+yHEsVlIxx3hgxCiifpHo0y08zvq5yxPEaYi7EtvQzM2uM98v+ncOs52iAgICAgIKJ3kbeqDD6Mlm0vze/RBqOfPQ1DJaiM6Qd3DcWQXVuGMr3tqYZgLjc9QfdBbO4RwCm163WAcSgxBBbchBsNaGhBlAQEBAQEBAQEBAQEBAQOqCioiugiHNdTGGQbjYIEMFtygvawNQSQEBAQEBAQEBAQEBB/I+f7HRlfkU40Zl80sOH8J8xVDXg/dLMMqCCvSzGb1Mf5h53eFqr9peJSnbopomfsxNH8AF9PHJ89cnNyZ/ympUZa1z3BjGkkmwAHVFohy2EuxjBvnmopDTu3e233fceirOFm5jWHDFoG4hQyayG7NB2cPb3VUTGUMSdLDldsVcT5rtIAd1+9cf0UxzH59XRMiKiAgICAgICAg7yvsU+YXQZ75hcqmU6ajCctVYZf8AYlxCO/8A/IsnxTlTP7tXw6Zmanf0slpiAgICAgINepabIIFr6ukLGPIljPyuBtv2QWPqG0kLY6uTW8jew6oKZWU4saRzm6tyGEgfwQThgtuQg2WN0hBlAQEBAQEBAQEBAQEBAQEBBhzQ5BHye9ggk1oagygICAgICAgICAgICAg/k3PpgJzVyPcZMsNhMhxHhVmKmbG0XLi/DahoAH4r0tTi7T+8PO7GbVX7S8SNO7VTRP8A2omH+LQvp45PnrkYuTH+XLVGW5WYcytpphK7Tqe1vp7eqjPFERC/KFPG581UW3ewAM9r3SqVlmH5kqZK4U9dE0B79IAFi036KMIzxK+udl/FCIWaoZmanRXtY9Lj06JEZhLisTxSoxSfzZtmj7jAdmq0RhWZaylUQEBAQEBAQEBB3nfYpstST5x5hs3GE6IMOy1RCS213yYjIW3/APQD/BZPik8KY/dqeHR/U79lktQQEBAQEBBCVlwg14iYJ79jsUGKmBpqS8G5PX2QWQwAdkF7WhoQZQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAkDcoNHMGF4dmHBavAMVgEtLW00lPUxOH345Gljh/AlI4TlE8YeHDjVw0xjgvxjzbwezBTPhrsqZnxDBquGQWcySlqZISD/7F9TRVt0xPV87XTs3JhXg5Zg2Dtqa6d2mVwLWddN+lv+aTzQslg+ClOL4WwPjkF5omfrD9oe6IUvxjLZlFf5d5huB5Zvf/AJJiUuGxPEJMTqzUyCwtZrfQK0RhWZa6lUQEBAQEBAQEBAQekf7G9wbq8o8hnEDjNXUpiOdOJkkFG5w/3tNQUkMIcPbzpZ2/VpWH4jVm7EdG5oqaotce/P8A7dvqz3YICAgICAgEXFkFb4r9Qgw2Gx6fxQWAABBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEEJnaR1QVNgNQwudIW3uBbsg8q32nLk/xzlw8U7Nme6HCyMB4p0UGbMLmiFx572iCtYbfrCohdIfadpPXfe0F2K7ER0Y+tomm5teXf/r4Qhjp8fwaOASaZIwAf7rgLfwK63FwmEqKE5foZJK6sDm3u1g6X9B7lRzIjD85UzmpqH1BaG63E2A2C9ETKCKiAgICAgICAgICCUbJZJGsggdK8kBkTG3c93ZoHck7D6pK1NO1OHs28JXlTrOSjw6eE3LjjVK2HF8EynDPmFgj0kYnVOdV1YPqWzTvZf+4F8zfuflvVVPorNGxbiH0YvF6CAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCudtwgppnhshgePleOhQda/2nDkKq+ZzkPm4+ZVoXVOaODM0+NU7Io9UlRgsjWtxGLYXOhjI6gf+WcB95dmivfjvYnlLl1VqLlv9nmNqK/AKqF9bFUOilaNnRHS8/wD+luxliuCnqaipdrqJ3vPYvddXVmUEVEBAQEBAQEBAQEBB93/Z2PD+l58fEcy2/M+EOnyRwzdFmzOD3t/RzGCUfBUZvsTNUhhLe8cMvouPW3vxWeHOXborM3LmfKHrUAsLL59tsoCAgICAgICAgICAgICAgICAgICAgICCLXhyCSAgICAgICAgICAgICAgICAgICAgw8XCDVnjIdqb1G4KCvG8Nocewl9BiVLFPTzxujnpp4w+OVjgWuY5p2cCCQQeoJQeSzx4vCsx3wy+b+thyhgE44VZ6qJ8S4d4iGkx0zbh0+Fud2kp3Ps0E3dC6J25DrfQaPURet8ecMXV2Jt1Zjl33/t8OrscIgICAgICAgICAgIN/KuVsyZ4zPh2S8m4DV4ri+L10VFhWGUEJknq6mV4ZHDGwbue5zg0AdSVFVUUxmVqKJrqxD11+Bz4YeFeF9yVYZw3zBR0snEPND2YzxJxKneHh1e5lmUbHj70NNGRE3exd5rx/vCvnNTfm/dmfLyfQWLUWreH2Uud7CAgICAgICAgICAgICAgICAgICAgICAg04aj13QbTH6u6CSAgICAgICAgICAgICAgICAgICAgg+MO7IICGx6H+CD+K+ILyD8EfEd5Y8b5ZuOVC9tHiAbUYPjNIxpqsFxCMHya2Au21sJILTs9jnsds4r1s3a7Ne1S87lum5TiXkW5/eQfj/4cfMZi3LlzBYAYqykcZsFxumid8Dj1AXER1tK933mO6Ob96N4cx4BG/0Nm9Rfo2qWFfsVWav8P4ovZ4CAgICAgICAgIMsY6R4YwEkkAAC9z6JyTETM4h6Jfs13gMV3LzTYX4g/ORlB9PnutpDJw8ydiUGl+XKaVlvjqljhdtbIxxDIzvBG83/AEjyI8XW6vb/APnRPDzbOk0/442qubueAAFgFmu5guAQYMgCAHg9UEkBAQEBAQEBAQEBAQEBAQEBAQEBAQarZaFzbxxb9hayCyG5A+qC5AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQfwTxEPDk5bPEw4C1PA3mHyyX+UXz5czJQNa3EcBrC2wqaaQg2J2D43XZI0aXg7Eetm9XZq2qXnct03KcS8t/ik+DNzbeFjnqSLifgTsxZBrKksy9xKwWjf+T6sE/LFUDc0VTbrFIbON/LfIBcbtjVW78cOfRjX9LXanvvv+XyMQQbELqcggICAgICAg/Q8K+E/Ezjjn/DOFXB3IWLZnzLjNQIcKwPA6F9TU1Lz+yxgJsOpcbNaLlxAF1WuuminMr0W6rk4h6KPA8+zQ5W5QMTwnmt56qfDMzcTabRU5dyhEW1OGZVl2LZnv3bWVrezx+ihdcs1uDZBi6rWzd/TRwhsafS02+NXN29gBosAs92sPNggp+eVxaw2t1KCkyPbMIX7OJAQWSvEMugeiC2KUOGyCwG+4QEBAQEBAQEBAQEBAQEBAQEBAQEGpHGXvMjha5ug2WM0hBJAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEHGZxyZlLiFliuyVnvLOH4zg+J0zqfEsKxWiZUU1XC4WdHJFIC17T3DgQpiZicwiYiqMS6fPES+yIcv/FyoxDiPyA8QRw2xydz5v7FY75lVgEzzvohkF56EE32HnRjoGNC0bPiNdHCuMw4r2ipr408++/N02c3ng5eJFyP1E9Rx35WMxNweF1hmnLVP+V8KeOzjUUuvyrgXtK2N3stK3qrFzlLOuaW7R33D5kaWueY2OBc02c0G5B9COy6Mw55pqjnAQR1BCIEGS1zW63Cw9T0Qfr+C3L5x15j8zxZM5f+DmZ864pNIGMosrYHPXOBP7RiaWsHqXEAWNzsqVXKKIzVL2psXKpxEd/s7QuRP7IzzmcasQpM1c6eccP4TZbOmSXBqCaLFMeqGHfQGxuNNSkj9Z75HNP/AHZXBe8Rt08KOLstaCrnU7zuQnwu+TDw28kPylyvcJ4MPrauIMxnNeJv+KxjFbG/+0VTgHFtwCImBkTT0YFlXb929OapaVu1RajEQ+hOnReT0EFczgB1QarHVBl1U4uR1v0sgttBiDQ8fLJG4XB6g+hQSmgppZ7uns+1tIcEGNDIXaWPue/sgvZ0QZQEBAQEBAQEBAQEBAQEBAQEBAQQjjACCaAgICAgICAgICAgICAgICAgICAgICAgIBIAuSgICDBa072QfxDjt4anIFzNVMmIceOTvhzmatlv5mJ4hlSmFW6/X/aGNbL/APkvWm9eo4RVOHnNq3M5xxfOWb/sxXgv5rq5K2DlRqMIfIbluC53xeBjT/dYalzW/QCy9qddqY8/h5VaSxVOZj5lwuD/AGVzwbcMmMtXwKzLXjVcR1vEPEy0e3yStNlO/anr8I3OxPOH9c4V+Av4QnB2aGqyjyF5FqpoLFs+ZaWbGHkjuTXSSg/wXlVqtRV/c9KbFqmMY4PqXJuQ8k8OsCiyvkDKGF4HhkH+4w7B8PipYI/+GOJrWj+C8ZmZnMy9YiKYxDlgABYBQkQLgGxKCD5Q0bFBq1FQCOuyCQe9mGOmgHzEE7fVBo0Iq3VPm09y7q4k7H6oN2pfDUsafLIeDvfqPZBOniI3QbAFhZBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQCQOpQY1tQY8wdwgkCDuEBAQEBAQEAkAXJQfLXP94yHIT4b+GS0/MDxip5sziHzKPIWWg2uxqp2uLwNcBTtPaSd0bD2J6L3tae7en9McOrxuX7dqOMuk7nh+10863GuapytydZHwrhFgTnFseMVLY8WxyZvS+uVnw1Pf9lkT3DtItO14dbp418Wfd19XKnvv+H9z8Of7YHDFS0PDbxK+GcplbpiHEvItBqa8dNdZhwN2nu59MSD2hC8r3h0xxtz/D1s66mYxW7mOWrnH5XOcTJ7c98sPHbLOdsNLA6Z+A4oyWWmv0bPDtLA7+7Ixp9lm127lucVRh30101xwl/SwQ4XBuqLCAgICAgqqqymoqeSrqp2RxRMLpZXuAaxoFySTsAB3KD4I59vtIPhtckEFZlvCuJbeKGc6cOY3KvDuojrGxSj9Wprb/DU4vsRqfIP/DK67OjvXfLEOe5qbVvzdN/F37WH4o+dePcHFPhjieVMnZXoXOZR8O2YGyvoqiEkH/bKiUCeaWwA1xuhDd9LRc30qfDrEUYnjPVn1a+5NeaeXff07BOQT7W/yxcc6+g4e86/DufhXjlQWx/2mw6aTEMBmk9XkN+Iowf77ZGDvIFxXvD7lHGji67Wst18J77/AJdsuSeJXDXiRlehzpw8zdheYMFxKAS0GMYNWx1dLUsPRzJYyWuH0K4JiaZxLsiYqjMOae5tA0SQt1ROO4B+7fuPZQk+LZpIp4rE9yAEGIYTfU7qg2WN0hBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEFUjzew6nogpqHSQkayLHoUGQ7TTibVe7tkE4ptXdBcCCEGUBAQEH885nea3l75NeE1fxv5luKmFZTy3hwtJXYlPZ00hBLYYIm3kqJnWOmKNrnnsOpV6KK7lWzTGVa66aIzLz5+KH9q35jeYyTEuEnIVQV3C3JchfBLm6dzTmPFIrkao3NJZhrHDtGXzf/AKjN2rXseH0Uca+M/DMv66eVHffcupLFcWxTHcUqcbxvEqisrayZ01ZWVk7pZqiRxu58j3kue4ncucSStGIiIwzqqqqpzLXUqiDmcgcReIHCjNVPnvhdnrGctY5SOvS4zl/FJqKriP8Adlhc14+l7KtVFNUYmF6bldPKX3py0fagfFr5eIKXCMxcXcH4mYXTHak4jYG2pnc3uDWU7oZ3H3e55XHXoNPX5Y/Z10a67Tz77/d9s8Hvtp1B8Oyk4+8h1SyXbza/JedWSNPraCrgYR/+6Vy1eGTH9NTqp8QomeMd/P8Ay/vmUftiXhk4xRtfmvhZxhwWo/XjdlmhqWD6OirST/ALznw6/HnC9OutVdx9uarfteXhO0sQkp8M4sVTiL6IcixtI9ryVLR/VVjw/UT07/hedXaj/cfb+ZcRftnfJxhIkHCrlJ4nY89oPlflqsw7DGOPuWyzuA/9JPsvSPDbvnMPPf7Xf/mXyrx8+2Rc6udaafDeXvluyDkSOUkR1+M1NTjlXEOxbf4eEH6scPZe9HhluP6py8KvEZnlHf7/APjr55rfFM8QbnadUU3MpzW5tx/DKl5c/LsFd8DhQ9vgqURwkf8AE1x912W9NZtf0w5K9Tduc5fwOmpZqh7aakguf1WMFgP8l78njxqnMuXy7RRxVc9BX0RMhZuXC4Df9VWUrcRpaPAn0tTS0z7sk3eD1HcH39FGTk/vvJT4iXNxyE52Zn7lX4wVOF01RK12MZYr2mqwbFmj9WopHODddrjzWFkrb7PC8bti1ejFUPe1qLlucxL0E+Fr9or5UefaXDeB/FV9Lw04rVAZFBgGKV4OHY1Lb/6CqfYF5PSml0y72b5ti5ZF/R3LPGOMNWzqaLnDzdi1PE0i4XG6Ww1oagygICAgICAgICAgICAgEgdSgxqb6oMgg9CgICAgICAgICAgICAgICAgICDDzYINSd7i6zb37WQTZL5v+x1rLOcPlJ/W/wBUGfhY2UoppZrAO2de197oMGmZC3UJj7bdUFsR6ILEBAQfEPi8eOPy0eFZk92XcTezN3FTE6EzZd4e4fVBr2tdcMqq6UA/CU1xsSDJJYiNps5zenT6W5qJz5dXhev0WY483mA54uf7mk8RDjDPxn5oOJFRjFYHPbg+EQF0WG4JA4//AC9HTXLYWWsC7eR5F3vcd1u2bNFmnFMMW7qK7s5l/GF7PAQEBAQEBAQEBAQEH6HB4zh+BGupaYyzSC9gLk72H4BUnjL0UUuHZknnNe+fynltgZHdvS1jZTMwictuCqcz/sfHg27m/JITs8fX1UH7tDHY8MoGsiw2QiUm7yyQkW91MZOTimvex4ka4hwcHAg7gg3B+t97q0xlEVTE8HcN4Lf2n3iFy3T4Vy2eIdjmJ5t4ft0U2D8QX66rF8vM2DWVXV9dSt/a3njHTzWgNbl6rQRV+q3z6NLT63+2vvvvo9EnDziJkbizknC+JPDTNmH47gGN0UdZhGMYVVtnpqyB4uySORhIc0juP+ayJiaZxLTiYmMw5lQkQEBAQEBAQEBAQEGHO0hBS+U76QTbqggyd0j9DepQTEul5Y47hBa14cgkgICAgICAgICAgICAgICDDnBqCiaew6oIULmyTuJ/VCDSrZaiarIeSC11mj0QbzJHOgMGIR76dj+1/qghTw+g2QbcbdIQSQCQBclB1lePJ4/mT/Dey9Py78udRhuYON2K0Yc6OYCakyjTyNuyqq2dJKhwIdFTHqLSSWZpbJ26TSTfnaq5OXUaim1GI5vMZxM4mcQeMuf8X4qcV854jmHMmP1z6zGsbxeqdNU1k7ur3vPU9AALBoAAAAAG7TTTRTiOTErrquVZlwasoICAgICAgICAgICAiYiZb2GY/V4ZGYGta9l7hru30UTESu24cUx/GZwyk/RMvu5rdh9Sev0UYiBnN9RE50NKCC9l3O9r2SkcKrKTOREAJBuCg++fBV8dPjV4Wme4OHmcH12a+C2MV4fj+UTLqnwh7z89dhpcbRy/rPguI5rG+l9nri1WkovxmObu0uqm3OzVy77/AOf8ep7gVx14Tcy3CbA+OPA3PNBmTKuY6FtXg+MYdLqjnjOxBB3Y9rgWvY4BzHNc1wBBCwqqKqKtmqOLZpqiqMw/WqqRAQEBAQEBAQEBBXO6wQa8VY2B9pB8p6n0QTNKWVbKmAgsJ+YDtt1CDEtJUOmdI0t0k7b7oJsD4yGP6/VBc03CDKAgICAgICAgICAgIBIAuUFckwb3QUSVO2yAPh4KcVNVvq6BBmJtPM34ugtqGxA2B9igMrqWUiXyDrHctFx+KDB11D9bht2CDYijAHRBNAQdcvj7+N9lnwy+EZ4P8GMSo8R425uw5xwGjcGyx5cpHXacUqWHYm4Igid/vHtLiNDHX7NJpZv1Zn+mHNqNRTapx5vLPnLOWbOIebMSz5nvMldjON4zXS1uLYtidU6apramRxdJNLI4kve5xJJK36aYpjEMKuua5zLjVKogICAgICAgICAgILaKmdWVUdKw2L3Wv6e6LRD9DBQYJHUfkplD5j2svJI5t7fU+v0VMys4PGKSmpK58VHJqYDv/dP7N+6tAlRY7X0FM6lge3SfulwuW/RMRKsy1JJHyvMkjy5xNySdypRM5YRAgICD738DDxqs/wDhZ8am5Sz9W4hjPBfNVc3+1+XYnGR+FTOs38q0bO0rAB5sYsJ422++1hHHq9LF+nMc3dpdTNudmrl333w9WXDjiNkbi7kTCOJ3DPNVDjmX8ew6KuwbGMNnEsFZTStDmSscNi0gg/0NiCFgTE0ziWzExVGYc2oSICAgICAgICCLngdCg1qiYG4ugTSsoqIShgc6T173QauG174HeVJcxn2+7/og26nzIpQ+GpdZ25be6CULXOOt5JJQbAFhZAQEBAQEBAQEBAQEAmwugomm090FL3mN7Xzxu0k9LdUF1TS05Y+ZzHH5ejev4IKKZ0GKUIpnus9oH1HugnRUgw1j3SS31dAEEYIdRuQg2o4wAgmgIPmbxXvEr4V+F1yo4nx7zwIMRx6qc7D8jZVdPpkxrFHMJZGbbthYB5ksn6rGm3zOYD7WLFV+5FMPK7ci1RmXkF5guPnFfmi40Zj5gOOGbJsbzVmrE312MYjNsHSOsGsY3pHExobGyMbMYxrR0X0dFFNuiKaeTAu3Ju17Uvxyu8xAQEBAQEBAQEBBs4Vhr8Uqvh2yBgAu5x9PYd1EzhMRla3L2IOrzQ6ALbmQ/dt6pmMLYhyEOWajD5mVlNVCR0br6CzTcdxe6jOUuQkAxCld8BUiJzzZ7w35h6g+hVRxeLuw7CsNOEU4Ekjzd5O5B9T7q0ZmcolwqsoICAgICB06IO3v7MZ40z+VriRR8gHMpmoR8OM44rpyRjNfNaPLWMTvt8O5ztmUlS8/8MUxDtmyvIzNfpduPyU84aei1H9lT0kg3FwsZqiAgICAgIMFwHVBB84A6oKHSPleI2dSgy+hhf8Ao3VB126Aj/kgyfJ8sUNcRcD5XHYOHqPQoMMZR0cZbT2cXbdb3QYggt1CDZYzSEEkBAQEBAQEBAQEBAQRkNggogYJJyXC+noEFFWcQrJTFFE5rGnYnb8UEmS4lRvElbZ8ZsCW/q+6CySjoDqqW7FwuC13f2QQhic8/M4n3JQbUcYaOiCaAg47N+bct5CyriWd8441T4bhGD0E1diuI1cgZFS00THSSyvcejWsa5xPoFMRMziETMRGZeQfxpPFCzX4pfOFiHFSnqKqlyDl0SYVw1wOf5fh8OD7uqpGdBUVLgJX92t8uO5Ea+h0tiLFvHn5sPV35u148ofIi6nIICAgICAgICAgICCykNUycS0YfrYbgsFyEXjg/S4diYxSndTvJhqA2zhbce4uqSlp4TFilDjPwcznOY4EvJJIIt97+NlCsZiWhj8gZjMxp3lu41aTbewurxyTLR69VKszkRAgICAgICB7f4omJxL1GfZo/FvqOfnlnk5deNWYXVPFbhbh8ENbV1L7y4/g1xFTYgT1dKwhsE57v8uQn9NYYGt0/wCGvajlLd0t+LtGJ5uzhcTqEBAQEGHu0hBrzT22CCMMQqYnESfMDt7IIsDaOujZJODraRb0KCmuw6sNZ59Pchzrgg7tKDZr2B4ja+xcNygQwDqQg2GMDUEkBAQEBAQEBAQEBAQYc4NCCmWYdCUFLZXRyeYz+vdAmxCpI0wxAE9+qC0ec2hcKx13OBHRBVBALdEG1HGGjogmgICDpX+1weJzPww4WYX4b/CTHvLxrPFGzFeI1RTSjXS4KJCIKIlpuDUyxuc8bfooLbiVaXh9jbqm5PlycOsvbFOI5vPGSSblbTFEBAQEBAQEBAQEG1hWFzYrOYo3hoaLuce34KJnCYjKuqoaijqTSzss6+3ofcKcrRGHPVdTHlulhp6WmDi6+pxNrkWufqqTxJnDM0sOL4ccTpQY54QS13cEb29whnLWkzdeiAjgInIsSfuj3/0U7PFLhHuc9xe9xJJuSe5VlJnLCIEBAQEBAQEBB/ZvD+50+JPh9c2mUOarhi98tTl3EAMVwoSFrMWw2SzKqif2tJFexP3Xtjd1aF5XrVN63NMvfT3ZtXMvZnwN4z8PuYrg9lnjrwoxxmJZbzbgdNiuC1rCLyU88YezUATpeAdLm9Wua4HcL5qqmaKppnyfQUzFUZfq1VIgICCiofYFBEvio4PiZhdx6BBp09dUskklgodQf+y07INiWGmxmASsOmRvfu0+hQZJrKQNidUh+3Ut3CCUcbnu1yEk+6DYYwNCCSAgICAgICAgICAgEgdUETKAgonn2IBQRHkQQiepuS4/KB3QKuKKIB7DYuP3UEYHPY/Uz8R6oLHNkmdqkP0HogujjDR0QTQEBB+S49caci8ufBbNXHjidijaPL+UMAq8XxeoLgC2CCJ0jg25F3O06Wju5wHdWppmuqKY80VVRTGXi05xuabiLzsczudOaXipO44xnPHJa59MXlzaKDZlPSMv+pDC2OIezL9SV9NatxatxTD56/cm5czL+Zr0eIgICAgICAgICAg28Djq5MSjbRSaHdS49AO9/VROMLxwh+hxCkocYa+kMrfNi6EdWH/JV5J5qH1NHUxfk3HdMcrO7jYO/vApiRrV2I4ZheHvw/C5NbpL6nA3tfqb/T0UxEo5ODVlZnIiBAQEBAQEBAQEBB6AvsffiH1Oacj5q8OLiLjeuoyyyXMnDw1Eu5w+WUCuo23PSKeRk7QN7VMnZqxvEbOKouR/LZ0V3ap2Z8u+/wCXeEsx3iAgi94AQa9Q8EWCAKmnMQZUsvb1bdBhuJGSdsEFMSCdyTawQRq2mOs8yA6SWjVbugnFE57tchJPug2GMDQgkgICAgICAgICAgICCqV5HTv0QU1LJomh25B9PVBg0FRIz5pGtJ7dUFtMflbT1DQJIxt79rhBGOm8iR1XVza3n7thYD6IEDdRLrWBN7IL2sA6oJICAgIOmv7YNzy1PC/lnyhyNZNxcxYjxMrzi2amRSEObgtDI0xxOH7M1WWfUUrx3Wj4da27k1z5OHXXZot4jz77/Z5z+vVbbFEBAQEBAQEBAQTp4JaqZtPAwue42ACciIy5GqypXwxh8D2y7fM0bEH29VG1C8RBlmQUeKuhqWFjnsLBqFiDcGyiUtybBcThxU1tBK2z3lxc49L9QR3CqjE5QzdVUxjjpNLXSg6i79kf6q1KXBKykzkRAgICAgICAgICAgIP7FyAc3WauRHnGyBzW5TklLso4/HPilLE8j43DZLxVtMfUSU75Wj+9pPZeV+1F21NMvfT3Jt3YmHtQydmzL+fMqYZnbKeJx1uFYxh8FdhlbCbsqKeaNskcjT3DmOafxXzMxMTiX0ETExlyRIHUqEovkAGyDXe98jxGzqUEvhYyTGJwXgbhBruaWuLHjcdUF0EscUfyQ/N6+qAyNz3F8m5KDYY0NHRBJAQEBAQEBAQEBAQCQOpQQfILINaolB2ugxSYjaQU1TcX+449/ZBTirK+CpFXFM4s7Afq+yC/EGGWKJzxZ/U+226DEUDnbuJPuUG1HGGjogmgICAgw42F/4IPIB49HN/Pzn+KRxOz9Q4l8RgOWcVOUsq6HlzBQ4c50Dnsv2lqPiZv/uhfRaO1+KxEfywtZc27r47XU5BAQEBAQEBAQEEoZpKeVs8TrOYbgomnm/TTVNbVU0GI4S65dYPiduD9foVTktxWYh+S5AyDFHRiR4+W5sQfY9k4pcZjGIVmFvFJSYs6QEbhzQXMH1UxGRw73vkeZJHlzibkk3JVlJnLCIEBAQEBAQEBAQEBAQAbG9v4oPVL9lx5wZ+Zzwt8DyDmLEvPx7hLisuUqwyPJkfRRtbPQPN+wp5Wwj/AMuV8/rrX478z1b+lr27UOxhz3uJEbSSuN0qDNJI7QwEn0CBCJ6eqa6oiLWu21XuAUFs0MtOHGih1SSuJc8kbIIPpmwRNa595CbuPqguijuOiC1rA1BlAQEBAQEBAQEBAQEFUrzew6nogoqzJANT+h7j1QKV8baZ9a5urTewQYpq6nxTVTTwgEjYE3uglBUvpnupagl+j7ju5Ha6AS+ok1uFh2CDYjYAEE0BAQEBB/E/Ed5lm8nnIrxW5lo5Syqylkiuq8LNxvXOj8qlbv61EkQ/Felmj8l2KVLlWzRMvFVUT1dVO+pr6h008jy+eZ7rukeTdzie5JJP4r6eOEPnKqtqrKClUQEBAQEBAQEBAQc1lKv0vfh8jtnfNH9e4/69FWqF4cfjTZ2YnMyol1uDtne3b+imORnDVUqzORECAgICAgICAgICAgICAg7d/se3M9U8OOeHO3LFiVY8YdxKyWa6igDtjiWGPMjbD1NNPU3t/wCGPRZviVuJtxV0afh9fGae++b0fit+GfqcLtPULFarYe60D6mhY17nC490Gvhte+uL6apYCdN726j0KBHNVRPdCJbta4huob2QWRxPe7XISSg2WN0hBlAQEBAQEBAQEBAQCQNygg6QWsg1qmQHoguaW+Q2Ktc279gD39kFEX/Zj3U9RvC8/I+3T2KCUFNh1LIamGS57AOvb6IMNYZ5DIR1KDZjiAHRBNAQEBAQEHVL9r35gDwz8NPCOC9BWPZV8SuINDRzxNdYPoqJr66W/qPNiph/6l3+H0bV7PRya2rFnGO/94eZVbrCEBAQEBAQEBAQEBBKKWSCRs0Ly1zTdpHZBhznPcXvcSSbkk9UGEBAQEBAQEBAQEBAQEBAQTgpqipLhTwufpbqdpHQIPobwk+PcnLN4lfBTjGat0NNQ8QKGjxSRrrWoq1xoqi/t5VQ8/gvDU0bdmqHVpZmi9E994ey9/5Opg2OqOpx2JcL+11803mHM/JpFRTEugcfnZe9vcILTJSx3ngDS+QdR3QRgiJOpwQbLWhqDKAgICAgICAgICAgIK5n2QVeTPKzWDa/QHug1KmSSB4E0ZG/fuguxSjfiETJqZwdYbC/UFBbEJY6HRiADnWtb19PxQVU9OOwQbccYaOiCaAgICAgICDz5/bTeIWJ13GrgTwpdIRSYdlbG8XDA7Yyz1VNBcj2bT/1K1vC4jZqlmeIco75/wCnSOtZlCAgICAgICAgICAgICAgICAgICAgICAgICAgICDlMr11RDWfCRw62Sn5rdW+6iYXiODfxCjqMFrWZowXUyajeKgNjNiHMOsOb6G4CpM8F6I2q4jq9tfBnM0nFrgpk/iVJI0TY7lfD8Qk9CZ6aOU/1eV8xVGzXMPoKZzTEv1j4hT0LaNz9Rt/jdQsxBT7XIQbLGaUEkBAQEBAQEBAQEBAQEFNS24KCEL3ywOha8te0fKUGYXSSUhdiUTGjuD6IIGEU0fm0VTZh6MO4P09EEWiacgyOJQbMUYaEFiAgICAgICAg6C/tonALM4zZwU5oKOiklwc4diWVcRqGsOmmqfMZWUzXHoPMYam3r5RWr4ZXEbVLO8QpmaYnvvi6L1rsgQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEGWMfI8RxtLnONgAOqCyqoauhfoqoHMJ6X6H8UzlaKXLZNbHqnd+vZoH03ValnK8NMpZ74h8Q8L4UZUwWpxLGMy4rBhmGUUDC581VUSNijDbdbueP+fZUqmIpmZWtbX5Iw9sXCTh5Fwm4V5X4a0lWXjL+XaHDC+O4DxT08cOr8dF18zVO1VM9X0FMbNMQ/UQwlx1OULNhjNI6IJICAgICAgICAgICAgICCMjbhBq7wTCQDa+/0QSxGN8hYdfyenugjDT7C42QbMcQaOiCaAgICAgICAgIP5nze8pHA7nj5f8AH+WrmGyoMWy1mGnDJ2xv8uopJmHVFVU8liYp43gOY8A2IsQWlzTe3cqtV7VKtVMV04l53+c/7JT4gvBbM9biPKlX4Lxdyt5jn0DWYjDhWMxRX2bNT1DmwyOAsNUUvzWuGN+6ti14haqj9fBlXdDXHGjj331fJeL+CT4t2CVLqWt8PPig9zHFpdSYAKhpI9HRPcCPe66I1enn+6HjOjvR3P01PzMniw/u7uLf8oSqd6seqPdXdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/wCUJU3qx6o9zdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/AChKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv8AlCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o9zdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/wAoSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/AJQlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o9zdL3T4n6PzMniw/u7uLf8AKEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o907pe7ifps4N4Ovis4diDJ5/Dx4saT8ri7KEvyg91E6qxP90e6Y0t2PL4n6c3WeEN4p0dW2iqPDy4qyxTbNcMpSmx9CD/zVd5seqE7rd6f8/T9pwq+zweLhxPxyOHLfJ/jOW2l+maszliVJh1NG09S7zJTIR/wscfQKtWtsRHNaNHdme/+8O4nwbPs7uVOQPOtFzLcz2ZsMznxQooScBo8KjccIy697C180RlaH1VTpLmiZzWNYHO0s1fOs3U6yb0bNPCHfp9NFnjPN2fxRue7W9cTrbLG6R0QSQEBAQEBAQEBAQEBAQEAkNBc42A6lBoDM+X3VPwQxSLzb20XKDakiDhcbg90EfLJtqN7dEFrGW3IQSQEBAQEBAQEBAQEAgHYhBgMY3ZrAPoEGbD0CDB0DrZBjUz9n+iDILD2QZsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BBgtaRaw/gg1qqIEdEE6VwfDYsBczYXQarPPdOZydLj2btb2QbDIXOdrebkoL2Rho6IJICAgICAgICAgICAgICAgoxMkYbUEG36B+/4FB1YcqXG/OtRxawnH6rnPo8Tr8Sx7DcKr8uYrmiqxWgzL5ktQysraVppQMNe58lH8NTxlrG+RIyUkSBwDtKy8ZjgtMaj7/l/NvdBuICAgICAgICAgICAgICAgw91ggoL5HAljb2QVxSullEbXC5QTEwY8sJ6GyC9kgI/wAUEkBAQEBAQEBAQEBAQEBAQEBAQEBBGRmoIKWMfE8uYOoQZji+bU7ckoLmtDQgygICAgICAgICAgICAgICBceoQa2NNLsGq2tO5ppAP/aUHXxyhcUs65G4bZXwml5ouHGc/wAnVWD4dxDytWzUkdfgOJ11TFTTNpqrC2uimBrJ3NYyWE+Y+wNSLlyDsByzVCtwKnqQPvNI/g4j/BBvoCAgICAgICAgICAgICAgqndYHdBrxVT4XE6C5nV1h933QW/CtdVMradwsb6gO9x1QYkoZHSukEos43sR0QZY0xO0OcD9EF7TcIMoCAgICAgICAgICAgICAgICAgICDGlvogyAB0CAgICAgICAgICAgICAgIISSae6Cl1RbayDLZJHi7GkhBHHHFmC1jx1FLIf/xKDrn4EZdyrxN4K8D6XjRymY9g9Dh82AYrkDN2Tq38qQ0R86nqqelrSyNlTTxGQQiUSQvpxoD3TBzGyAOxXLkEdLglPBELNazYD6koN1AQEBAQEBAQEBAQEBAQQfIAOqDVqJwb+iCVXM6iomuhb8zjYm3sg1cLqpoCW6HOj6usPu+6DZqGxiUSU0hBdu4NOyCyCMjc9Sg2ALCyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMONhdBrVEmk/TsUEnCKvg/ROAcP6IJUAkbThkrbOa4gj8UFeONLsFrGgbmlkA/wDaUHXPyz50wfhNl3hPlfJfNNnLEJsVy/lmfNcOL4HWY7l+V+IsY2H/AGqoDKjC56t4kEDDII2uezVAAQCHYplqY1GB00xtuw9PqQg3kBAQEBAQEBAQEBAQYc4NQVSVAHUoKQ51RII2nqgk6HD3v+GdJ8//ABboMukigaKSs3aR8jiNiPf3QYElNDF5dGy1+4H+aBBBbchBssbpCDKAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgE23KCuWUAINeNoqJ9BOwFz9EFFbWNhqwaRga6PZx7O9kG7T11PPEJDI1p7tc7oUE6mMTU8kThs5hBv7hB1j8PMg8cMuccqfgzlHhDgFHguDZ1wypxuXJGEYSMBxySlr/Omr6zTJ59BPTUL8PfFTRtafiZASHxvYUHZZlenfS4BTQSdQw3/EkoN9AQEBAQEBAQEBAQRe/SEGvLMXHSwXPsgxTsiqonN1kPB6+iCGunw/EGRmU/OyztXb0P8ARBCrwiaSq8+CQWc65udwUF9cGvLIybkblBmGADqEGw1gagygICAgICAgICAgICAgIFx6oCAgICAgICAgICAgICAgICAgICAgw54agqlnA7oKmRSVNzqsPVAZTy0U3nl+thFn2G490CXC6aol+JbKQ125DTsUGJhR67Np2uAFr3sgxmTG6PLWX63MOIuc2noaWSeocyF0jgxjS5xDWAucbA7NBJ6AEoOvjDsq8mOWOaGo5msp8Uc50VdVYhV1lbhn9hMQmZPJOKEOYZ5KIziIfBl2jXsZjpLW6mvD744dZpos55Socx4W5zqSspIqike+B8TnRvaHNLmSAOYbEXa4AjoQCg5xAQEBAQEBAQEBAQUVDyBdBGWVtBTebp1Pd0+qDVgkxcF8sNMCH7m4tv7ILy2mxeHRKwskYdx3aUGHRyUpbDDVPItuCeiCyGE31O3KDYa0NCDKAgICAgICAgICAgICDDnaQgqfOB1KCLJjI8NYEEo5ge6C0EEIMoCAgICAgICAgICAgICAgICCEkoA6oNd0xe8Ma61za6DE1HUaj5bha1wSf6IMObJVYSG0zvmtuAevqEGMG+LDXxztdpHTUOh9EEIqcB5DOlzZBssgAG6DUzrjdXlrKGKZiocDrcTmoKCWoiw3DYmPqKtzGFwiia9zWukdbS0Oc0EkXIG6D4WxXnJ5lc1cehlyh4a5hy7kx2csIpMdxbGsGw1lZgU0rqVowqRja5zLVRmhf5zv0zI6ktjikL4XND7rynIZsv00pFi5hv8tu5QcigICAgICAgICAgg+QAINeeQOFgUGWVwjYGyRk26EIIxV9VUVIjihAZf5r77IMVjGmr8xhsQ0XIQWQwm+p3VBsNaGhBlAQEBAQEBAQEBAQEBAQVTvsCgrglhkBp5BYn+qDFNFLT1jmPJLSw6XfiEEGx1LHm8TuuxQXxOcNnAg+6C7qgICAgICAgICAgICAgICAgw82ag1iHTy+WDt3QUYnPSRN8hjLvb1IPRBZBilJWH4V7C0Obb5j19kEaegraGZ3wsjXRnfS8oJ/FTzt0FobfrZBbBCB2QXgACwQCQBc9kHXJzAUuI1/iQ1E8GCVRoXZxy/I7J0dNjJZmdzI4B/aEyRPFEwUFgC2RhLvyeNRDvJQdhOUCTlukuCP0Z6j3KDkkBAQEBAQEBBguA7oIvlACCgudPII2H6lBJsFLI50LJCXt+9Y9EGu4aXFhINjbZBfHM9kWhkYB9UCKE31O3JPVBsNaGhBlAQEBAQEBAQEBAQEBAQRc8NQa08uo2CCNcIKan8ssvI77rh1B9UEsOxDzx5M+zx0P7SDL56uCUxvc1w7G26CcIcfmcbkoLxsLICAgICAgICAgICAgICAgE23KCuV4sgoilbFOXOGx2QZe/DIHGfS0ucb9Lm/8AggOdBiVI97oiNN9JcNx7oK4p6mSPy3vvt1tugvhht2QXAABBlBVXmQUMxh+/5TtNvWxsg6uuF1SzMvEyh4mUfCXiNUUmLZ+y7XcQ8MzPxGx1sFDmWqqKamjpKWke1sdd+TjS09RK+fU1jWNYwhkTGsDs7yuJG4DTNlaQ4NIIJ3+8UG+gICAgICAgw91ggpJkkv5Y6IKA6ad5jjaSR19AgzHHUUlQJpmgsIsS030oLZ6eYR+XQ6Wa3EveTvughJBDAxkDN3DqfVBbFHtZBaGgIMoCAgICAgICAgICAgIBIHVBW+YAdUFLpHyv0R7oIy0FQRqZK243sQgnPTxYlGCXFj2Hcd2n0KCLqampYfL+88m9+4KDMURe7W83Pug2WMDQgygICAgICAgICAgICAgIBIHUoKppQAgqZHLUfMDYeqCM1PJD8x3B7hBWxkLnjzgbd7IL5ZA5vkQMs3vsglBCB2QXgACwQEBBh7mMaXyOAA6knZB8K8Zeanj3l/xD3cu2XqzCJ8NxdlHJgeGsmoQWULanDn1dZKXTio8zyBjMbYwzSDFSuAfqfpD7cyvI+bAaaV7rlzSSfxKDfQEBAQEBBguA6lBXNIAEGo6tdTP1jcd2+qDYklfPRumw5zdbhsbd/wDNBr4PWVNS59PVDVpHUjp2sUCLz43OijmdpDiGi/ZBfDAb6nblBsNbpCDKAgICAgICAgICAgICDDnhqCiWe3QoImCWWISMIJJ6X7IFE10VTJDKRq0gtt3CDUh/KNPiIY7Ubu+b0cPVBs10QNU2Rux072KCUMHcoNhjA3qgkgICAgICAgICAgICAgIISSae6CiSoLeyCqSSWRpLI3O/4W3QWwxtraFjPMIGr5wO/sgwPOq5fJbG6OFmxJFi5AjjGsgG4vsUF7Ih1AQTAA6IMoCAg4vO+GYjjeTsUwfCMySYPV1dBLDSYvDTslfRSuaQyZrJAWPLHEODXAtJFiCLhB1g5ny9juS+c7EsnZq4y1GZqT/4pZUmzDXVpwSixfEMULaI0clJRsoRUPomRshjmfFNEA34s6HBsuoOz/Jotlmk3/7s/wD9ig5NAQEBAQYe7SEFJMj7+WOiDWMplkEQdu42QW1FVQ0LxDJFqJFydIKDErG0lq6itodbzGDoR6j0QWOqodJdTtGp/U2QYp4bbkINgNAQZQEBAQEBAQEBAQEBAQOiDXnkIGyCMraelg11W5cOiDXo8Vp6ePy5Guvr69dkFlVQySTNr8Pl+Y7nfYoLDV1UZ8uWJuq3Vp2QI43Pf5khuSg2WMDQgygICAgICAgICAgICAgIMFwAQa1RJbYFBmOWnr2eS/ZzewP/ACQVVmKOoagU7ab5QOvS/wBEE6mV0bWVtKRZ9tQI2cLbH6oHxM8w02Db9bILYYtIsgu6ICAgICDjc54BT5ryjimWKvE66iixDD5qeSswyqdBUwNewtMkUjfmjkbe7XDdpAI6IOtTPOQqfhHztYRlvBePddjdXWZtwh2AYhjXE+or8VwzDtcDa3DZ8NbHJNKyd7ZgJ3FsLG1ZMjmiLcOyzKcL6fLtLDJa4jN7H3KDkUBAQEGC4DqUFczxYoKaaWXz9DBcHd1+yBW0ji8VtJYvY67mg/et/igxNTUuLNbNHJZw2O249iEEpGRwUwomO1W6/wAboMwQdyEGw1gagygICAgICAgICAgICAgIIvcALXQa07vmBHYoJy/A1IbJUEbdiUGIamhlk+EihuCD+psgrZ5lHM+GA/JfZp7ILI4nPfrkNyUF7WBqCSAgICAgICAgICAgICAghJJp7oKJKgt2sgjDH8US5zrNHUoMChpJ/wBJQ1JbI07Oa6+/ugk3ysRjNNWR6ZYz8wB/qEGZ3MfpghHys6WQWQxWAQXAAdEBAQEBAQRkjbLG6J/RzSD9EHy3k/wtuH3BLidgnEXlp4j4plSlwptRTT5YraaLEaGehqp6aWria+QCpbJIaSG0rpn6C24YQXNcH1Bh9KaOijpXEEsba46ILkBAQYcbC6CiR7i7QwXJQVVPmwi8jTb1HRBnC5WyGRoNnbIKMPhxKkrTEY3OY4/MT0+t0F1bBD8QHRts4j57ILIIO5CDYa0NCDKAgICAgICAgICAgICBcDqUEXyADYoNdz3yP8uMXKBJSTBuoEO9QEFBAcN0GxEaamjvAwlzvVBiKNz3a39Sg2WMDQgygICAgICAgICAgICAgIMOcAEGvUP09EEi1lfTdC09AbdCgpoAQyXDqkaX7/iCLXCCugwyqpKzzC8aADuD95BOeNstW6Rv0ugvhg09kFwFhZBlAQEBAQEBAQEBAQEGH9PxQa0bnNqQAeuxQWNme6sfTOsWhoI2QU1tLFTg1tOCx7f2eh/BBFmI1D4wSGgnuAglANTruNz6lBtsAAugygICAgICAgICAgICAgw42Fwgqe4goKpZH9L90GaAkvlJ7Wt/VBCN76fDBUMcS94Bc5xvuUEWtADDcnUwON/VBsMY30QWsaLXQSQEBAQEBAQEBAQEBAQEGHkjogpe9wvZBrzuJBPoEGcWqZaGKIUx0i52t1sgurIWTU3nOuHsbqa5psQUGtDVVErLSTEoNiBjR2QbDRYBBlAQEBAQEBB//9k="  

}
