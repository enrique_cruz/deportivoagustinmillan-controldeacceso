import { Component, ComponentFactoryResolver, ContentChild, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ServicesService } from '../services/services.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MatStepper } from '@angular/material/stepper';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import images from '../../assets/images/pdfimg/img1.json';
import cartaGeneral from '../../assets/images/pdfimg/general.json';
import cartaEexoneracion22 from '../../assets/images/pdfimg/carta-exoneracion-22.json'
import carta60 from '../../assets/images/pdfimg/carta60.json';
import { ActivatedRoute } from '@angular/router';
import tarjeta from '../../assets/images/pdfimg/card.json';
import { forEach, result } from 'lodash';

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.css']
})
export class NuevoUsuarioComponent implements OnInit {
  
  @ViewChild("video",{ static: false })
  video!:  ElementRef;
  stream :any;
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  
  title = 'app';
  elementType = 'url';
  value = '';
  
  
  planSelected = 0;

  importeDisciplinas = 0;
  importeInscripcion = 500;
  importeDisciplinasAux=0;
  importeInscripcionAux=0;

  subTotal = 0;
  porcentajeDePago = 100;
  porcentajeInscripcion = 100;
  porcentajeDisciplina = 100;
  pagoTotal = 0;
  
  idCamara = "";

  isEditable = true;
  
  isCaptured = false;
  
  step1Complete = false;
  step2Complete = false;
  step3Complete = false;
  step4Complete = false;
  step5Complete = false;
  tutores = true;
  titulotutores = "";

  disciplinaSelected = 0;
  auxImg = "";
  showImg : any;

  todaysdate:any;
  todaysdate2:any;

  parcialidad = {
    TipoDePago:"",
    folioDePago:"",
    cuentaBeneficiaria:"",
    fechaDeReciboDePago:"",
    observaciones:""
  }

 tutor = {
  tutorNombre: "", 
  tutorApellidoPaterno: "", 
  tutorApellidoMaterno: "",
  tutorParentesco: "",
  tutorTelefonoCasa: "",
  tutorTelefonoCelular: "",
  tutorCorreoElectronico: ""
  }
  
  
 alumno = {
  alumnoBase64Image : "",
  alumnoNombre :  "",
  alumnoApellidoPaterno : "",
  alumnoApellidoMaterno : "",
  alumnoCURP : "",
  alumnoFolio : "",
  alumnoFechaDeNacimiento : "",
  alumnoSexo : "",
  alumnoRutaFoto : "",
  
  alumnoTelefonoCasa: "",
  alumnoTelefonoCelular : "",
  alumnoCorreoElectronico: "",
  alumnoRedesSociales: "",
  alumnoPassword: "",
  alumnoCodigoPostal : 0,
  alumnoEstado: "",
  alumnoMunicipio: "",
  alumnoColonia: "",
  alumnoNumero: "",
  alumnoCalle: "",
  
  alumnoAfiliacionMedica : "",
  alumnoAlergias : "",
  alumnoPadecimientos : "",
  alumnoTipoDeSangre : "",
  alumnoEstatura : 0,
  alumnoPeso : 0
 }
  
 planHorario = {
   planId:0 ,
   planDescripcion : "",
   planRecurrencia: 0,
   planImporte : 0,
   disciplinaId:0,
   horario:[] as any
 }
 
 
 arrayPlanesHorariosAlumno : Array<any>=[];
 arrayhorariosSelected: Array<any>=[];
 
 
 arrayAsentamientos:Array<{nombre: string, tipo: string, cadena: string}>=[];
 arrayDisciplinas: Array<{disciplinaId: number; disciplinaNombre: string;}>= [];
 arrayHorarios: Array<{horarioId: number; horarioNombre: string;}>= [];
 arrayTutores:Array<{tutorNombre: string, 
                     tutorApellidoPaterno: string, 
                     tutorApellidoMaterno: string,
                     tutorParentesco: string,
                     tutorTelefonoCasa: string,
                     tutorTelefonoCelular: string,
                     tutorCorreoElectronico: string
                     }>=[];                      
arrayAlumnos:Array<{alumnoNombre: string, 
                      alumnoApellidoPaterno: string, 
                      alumnoApellidoMaterno: string,
                      alumnoDisciplina: string,
                      alumnoHorario: string
                      }>=[];             
arrayHorariosLunes: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
}>=[];   
arrayHorariosMartes: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[] ;
arrayHorariosMiercoles: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[];
arrayHorariosJueves: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[] ;
arrayHorariosViernes: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[] ;
arrayHorariosSabado: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[]  ;

arrayHorariosDomingo: Array<{
  horarioId: number,
  horarioNombre: string,
  horarioInicio: string,
  horarioFin: string,
  horarioDias: string,
  horarioDisponibilidad: number   
}>=[]   


idHorarioLunesSelected = 0;
idHorarioMartesSelected = 0;
idHorarioMiercolesSelected = 0;
idHorarioJuevesSelected = 0;
idHorarioViernesSelected = 0;
idHorarioSabadoSelected = 0;
idHorarioDomingoSelected = 0;


enableLunes = true;
enableMartes = true;
enableMiercoles = true;
enableJueves = true;
enableViernes = true;
enableSabado = true;
enableDomingo = true;


arrayDisciplinasHorarios : Array<{nombreDisciplina:string,horarioId: number}>= [];
 
 arrayPlanesDisciplina : Array<{planId:number,
                                planDescripcion: string,
                                planRecurrencia: number,
                                planImporte: number,
                                }>= [];
 
arrayCamaras: Array<any>= [];
                     
  disciplina= 
  { nombreDisciplina: "",
    horarioId: 0
  }                     
                                                  
  
  
  constructor(private sanitizer: DomSanitizer,
              config: NgbModalConfig, 
              private modalService: NgbModal,
              private httpServices: ServicesService,
              private _formBuilder: FormBuilder,
              private route: ActivatedRoute) { 
                
                
                  this.getAlumnos();
                  this.getMediaDevices();
                  this.getDisciplinas();
                  config.backdrop = 'static';
                  config.keyboard = false;
                  
                  this.alumno.alumnoPeso = this.permisos.value!;
                  this.alumno.alumnoEstatura = this.permisos.value!;
                  this.alumno.alumnoCodigoPostal = this.permisos.value!;
                  
                  //  this.todaysdate= new Date().getTime()+"";
                  this.todaysdate= new Date();
                  //this.openPDF();
                  this.todaysdate2= new Date();
  }

  ngOnInit(){
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.baseImg);
  }

  validaCorreoT()
  {
    if(this.tutor.tutorCorreoElectronico !="")
    {
      if(!this.isEmail(this.tutor.tutorCorreoElectronico))
      {
        alert("El correo no tiene un formato válido!");
      }
    }
    else
    {
      alert("El correo es un campo obligatorio");
    }
    
  } 

  validaCorreo()
  {
    if(this.alumno.alumnoCorreoElectronico !="")
    {
      if(!this.isEmail(this.alumno.alumnoCorreoElectronico))
      {
        alert("El correo no tiene un formato válido!");
      }
    }
    else
    {
      alert("El correo es un campo obligatorio");
    }
    
  } 

  isEmail(email:string)
  {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);  

  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  
  getQR(){
    this.value = crypto.AES.encrypt("A-"+this.alumno.alumnoCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();
  }
  
  calcularTotal(){
    console.log("hare el calculo ");
    for(var i = 0 ; i < this.arrayPlanesHorariosAlumno.length; i++){
      console.log("for i = "+i+" "+this.arrayPlanesHorariosAlumno[i].planImporte);
      this.importeDisciplinas = this.importeDisciplinas+this.arrayPlanesHorariosAlumno[i].planImporte;
    }
    
    this.subTotal = this.importeDisciplinas+this.importeInscripcion;
    this.pagoTotal = Number(((this.subTotal/100) * this.porcentajeDePago).toFixed(2));
    
    this.importeInscripcionAux = this.importeInscripcion;
    this.importeDisciplinasAux = this.importeDisciplinas;

    console.log(this.subTotal+" "+this.pagoTotal);
  }
  
goForward(stepper: MatStepper, form: number){

   
    
    console.log(stepper.steps);
    console.log("esto es validae");
    
    switch(form){
      case 1:
        if(this.validaFomr1()){
          console.log("formulario completado "+this.step1Complete);
          this.step1Complete = true;
          console.log("formulario completado "+this.step1Complete);
          console.log(stepper.selected?.completed);
          if(stepper.selected?.completed != null){
            stepper.selected.completed     = true;
          }
          stepper.next();
        }else{
          console.log("falta un dato");
          this.step1Complete = false;
        }
        break;
      case 2:
        if(this.validaFomr2()){
          console.log("formulario completado "+this.step2Complete);
          this.step2Complete = true;
          console.log("formulario completado "+this.step2Complete);
          console.log(stepper.selected?.completed);
          if(stepper.selected?.completed != null){
            stepper.selected.completed     = true;
          }
          
     
          stepper.next();
        }else{
          
          console.log("falta un dato");
          this.step2Complete = false;
        }  
        break;
        case 3:
          if(this.validaFomr3()){
            console.log("formulario completado "+this.step3Complete);
            this.step3Complete = true;
            console.log("formulario completado "+this.step3Complete);
            console.log(stepper.selected?.completed);
            if(stepper.selected?.completed != null){
              stepper.selected.completed     = true;
            }
            
       
            stepper.next();
          }else{
            
            console.log("falta un dato");
            this.step3Complete = false;
          }  
          break;
          case 4:
            if(this.validaFomr4()){
              console.log("formulario completado "+this.step4Complete);
              this.step4Complete = true;
              console.log("formulario completado "+this.step4Complete);
              console.log(stepper.selected?.completed);
              if(stepper.selected?.completed != null){
                stepper.selected.completed     = true;
              }
              
              this.planSelected = 0
              this.calcularFdad();
              stepper.next();
            }else{
              
              console.log("falta un dato");
              this.step4Complete = false;
            }  
            break;
            case 5:
              if(this.validaFomr5()){
                this.step5Complete = true;
                console.log(stepper.selected?.completed);
                if(stepper.selected?.completed != null){
                  stepper.selected.completed     = true;
                }
                this.getQR();
                this.importeDisciplinas = 0;
                this.calcularTotal();
                stepper.next();
              }else{
                console.log("falta un dato");
                this.step5Complete = false;
              }  
            break;
    }
}

validaFomr1(){
  if(this.alumno.alumnoBase64Image === ""){
    alert("Toma una foto al alumno");
    return false;
  }else if(this.alumno.alumnoNombre === ""){
    alert("Ingresa el nombre del alumno");
    return false;
  }else if(this.alumno.alumnoApellidoPaterno === ""){
    alert("Ingresa el apellido paterno del alumno");
    return false;
  }else if(this.alumno.alumnoApellidoMaterno === ""){
    alert("Ingresa el apellido materno del alumno");
    return false;
  }else if(this.alumno.alumnoFechaDeNacimiento === ""){
    alert("Ingresa la fecha de nacimiento del alumno");
    return false;
  }else if(this.alumno.alumnoCURP.length != 18 || !this.curpVerificado){
    alert("Ingresa una curp valida");
    return false;
  }else if(this.alumno.alumnoSexo === ""){
    alert("Ingresa el sexo del alumno");
    return false;
  }else if(this.alumno.alumnoFolio === ""){
    alert("Ingresa el folio del alumno");
    return false;
  }else {
    this.alumno.alumnoCURP = this.alumno.alumnoCURP.toUpperCase();
    return true;
  }
}  

validaFomr2(){
  if(this.alumno.alumnoAfiliacionMedica === ""){
    alert("Ingresa la afiliación medica del alumno");
    return false;
  }else if(this.alumno.alumnoTipoDeSangre === "" ) {
    alert("Ingresa el tipo de sangre del alumno "); 
    return false;
  }else if(this.alumno.alumnoEstatura === 0 || this.alumno.alumnoEstatura === undefined ){
    alert("Ingresa la estatura del alumno ");
    return false; 
  }else if(this.alumno.alumnoPeso === 0 || this.alumno.alumnoEstatura === undefined){
    alert("Ingresa el peso del alumno ");
    return false; 
  }else{
    return true;
  }
}  

validaFomr3(){
  
  if((this.alumno.alumnoTelefonoCelular+"").length != 10){
    alert("Ingresa un número celular a 10 digitos");
    return false; 
  }else if(this.alumno.alumnoCorreoElectronico === ""){
    alert("Ingresa un correo electrónico");
    return false; 
  }else if(this.alumno.alumnoCodigoPostal == 0){
    alert("Ingresa el codigo postal");
    return false; 
  }else if(this.alumno.alumnoEstado === ""){
    alert("Ingresa el estado");
    return false; 
  }else if(this.alumno.alumnoMunicipio === ""){
    alert("Ingresa el municipio");
    return false; 
  }else if(this.alumno.alumnoColonia === ""){
    alert("Ingresa la colonia");
    return false; 
  }else if(this.alumno.alumnoNumero === ""){
    alert("Ingresa el número");
    return false; 
  }else if(this.alumno.alumnoCalle === ""){
    alert("Ingresa la calle");
    return false; 
  }else{
    this.alumno.alumnoTelefonoCelular = this.alumno.alumnoTelefonoCelular+"";
    return true;
  }
  
}  

validaFomr4(){
    if( this.arrayTutores.length > 0){
      return true;
    }
  
  
    return false;
}  

validaFomr5(){
  if(this.arrayPlanesHorariosAlumno.length > 0){
    console.log(this.alumno);
    return true;
  }


  return false;
}  





  validarDatos(){
   // console.log(new Date(this.alumno.alumnoFechaNacimiento).toISOString());
    return true;
  }


  validarParcialidadDePago()
  {
    let result :Boolean = true;

    if(this.parcialidad.TipoDePago=="Selecciona una opción..." || this.parcialidad.TipoDePago=="")
    {
      result=false;
      alert("Selecciona una opcion de pago");
    }else if(this.parcialidad.folioDePago=="")
    {
      result=false;
      alert("La referencia no puede ir en blanco");
    }else if(this.parcialidad.cuentaBeneficiaria=="")
    {
      result=false;
      alert("La cuenta beneficiaria no puede ir en blanco");
    }else if(this.parcialidad.fechaDeReciboDePago=="")
    {
      result=false;
      alert("Selecciona la fecha del recibo de pago");
    }
    
    return result

  }
  
  setAlumno(steper : any){

    console.log(" ***************** GUARDAR ALUMNO ********************")
    console.log("**** Importe disciplina con descuento ****")
    for(let item of this.arrayPlanesHorariosAlumno)
    {
      console.log("disciplina:")
      console.log(item.planDescripcion)
      item.planImporte= ((this.porcentajeDisciplina * item.planImporte)/100)
      console.log(item.planImporte)
    }

    if(this.validarParcialidadDePago()){
      var jsonSend = {
        "tutoralumnoId": 0,
        "tutorIdFk": 0,
        "alumnoIdFk": 0,
        "usuarioIdFk": 0,
        "alumno" : this.alumno,
        "tutor" : this.arrayTutores,
        "planHorario" : this.arrayPlanesHorariosAlumno,
        //"inscripcionImporteAnual": this.importeInscripcion,
        "inscripcionImporteAnual": this.importeInscripcion,
        "porcentajeDePago" : this.porcentajeInscripcion,
        "inscripcionFechaDeVigencia": new Date().toISOString(),
        "parcialidad": this.parcialidad
      }  

      console.log("Solo JSON");
      //console.log(jsonSend);
      console.log("Stringify Json InscripcionAlumno")
      console.log(JSON.stringify(jsonSend));
          
      this.httpServices.setInscripcion(jsonSend).subscribe(
        datos => {
          console.log(datos);
          if(datos == 1){
            alert("Alumno guardado");
            this.openPDF();
            this.resetAll();
            
            steper.reset()
            
          }else if(datos=="El folio ya existe"){
            alert(datos);
          }
          else
          {
            alert("La CURP ya está siendo usada por otra persona");
          }           
        },
        error =>
        {
          alert("Hubo un error al guardar los datos del alumno. Intenta de nuevo!")
        }); 
    }    
  }
  
  
 
  getInfoCp(){
    //console.log("hola desde busqueda de cp con "+this.codePostal+" "+this.codePostal.toString().length);
    var strCP = this.alumno.alumnoCodigoPostal;
    if(strCP.toString().length >= 4){
      this.httpServices.getInfoCodePostal(strCP.toString()).subscribe(
        datos => {
          var json = JSON.parse(JSON.stringify(datos));
          this.arrayAsentamientos = [];
            console.log(json+" esto en json "+JSON.stringify(json));
            for(var i = 0 ; i < json.length; i++){
                let jsonAsentamiento = json[i];
                this.alumno.alumnoMunicipio = jsonAsentamiento.Municipio;
                this.alumno.alumnoEstado = jsonAsentamiento.Estado;
                //console.log(i+" "+jsonAsentamiento);
                this.arrayAsentamientos.push({nombre: jsonAsentamiento.Colonia,tipo: jsonAsentamiento.Tipo_Asentamiento, cadena: jsonAsentamiento.Colonia +" - "+jsonAsentamiento.Tipo_Asentamiento });
            }
        },
        error => {
         
            this.arrayAsentamientos = [];
          
        }
        
        
        );
    }else{
      this.arrayAsentamientos = [];
   
    }
 }
  
 calcularFdad(){
   console.log(this.alumno.alumnoFechaDeNacimiento);
   var nacimeinto = new Date(this.alumno.alumnoFechaDeNacimiento+" 00:00");
   var hoy = new Date();
   console.log(hoy+" "+nacimeinto);
   var edad = hoy.getTime()- (nacimeinto.getTime() + 1000*3600);
   edad = edad/ (1000*60*60*24*365.25);
   console.log("la edad es "+edad );
   return edad
  /* if(edad < 18){
      this.tutores = true;
   }else{
    this.tutores = false;
   }*/
   
 } 
 
 
 
 btnguardartutor(){
   if(this.tutor.tutorNombre == ""){
    alert("Ingresa el nombre del tutor");
   }else if(this.tutor.tutorApellidoPaterno == ""){
    alert("Ingresa el apellido paterno");
   }else if(this.tutor.tutorApellidoMaterno == ""){
    alert("Ingresa el apellido materno");
   }else if(this.tutor.tutorParentesco == ""){
    alert("Ingresa parentesco");
   }else if((this.tutor.tutorTelefonoCelular+"").length != 10){
    alert("Ingresa el telefono celular"); 
   }else if(this.tutor.tutorCorreoElectronico == ""){
    alert("Ingresa el correo electronico");
   }else if(this.arrayTutores.length == 3){
    alert("El máximo de tutores es 3");
   }else{
     
     this.tutor.tutorTelefonoCelular = this.tutor.tutorTelefonoCelular +""; 
       this.arrayTutores.push(
        {tutorNombre: this.tutor.tutorNombre, 
          tutorApellidoPaterno: this.tutor.tutorApellidoPaterno, 
          tutorApellidoMaterno: this.tutor.tutorApellidoMaterno,
          tutorParentesco: this.tutor.tutorParentesco,
          tutorTelefonoCasa: this.tutor.tutorTelefonoCasa,
          tutorTelefonoCelular: this.tutor.tutorTelefonoCelular,
          tutorCorreoElectronico: this.tutor.tutorCorreoElectronico
          } 
       );
       
       this.tutor.tutorNombre = "";
       this.tutor.tutorApellidoMaterno = "";
       this.tutor.tutorApellidoPaterno = "";
       this.tutor.tutorParentesco = "";
       this.tutor.tutorTelefonoCasa = "";
       this.tutor.tutorCorreoElectronico = "";
       this.tutor.tutorTelefonoCelular = "";
   }
 }
 
 quitarTutor(index : any){
   console.log("quitare el "+index);
   this.arrayTutores.splice(index,1);
 }
 
 
 
 
////////inicio funciones camara 
open(content:any) {
  console.log(content);
 this.modalService.open(content, { size: 'xl' });
 
}
getMediaDevices(){
  this.arrayCamaras = [];
  var contexto = this;
  navigator.mediaDevices.enumerateDevices().then(function(devices) {
    devices.forEach(function(device) {
      console.log(device.kind + ": " + device.label +
                  " id = " + device.deviceId);
                  if(device.kind === "videoinput"){
                    if(contexto.idCamara === ""){
                      contexto.idCamara = device.deviceId;
                    }
                    contexto.arrayCamaras.push(device);
                  }
    });
  })
  .catch(function(err) {
    console.log(err.name + ": " + err.message);
  });
}

async iniciarCamara(video: any) {
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    try {
      this.stream = await navigator.mediaDevices.getUserMedia({
        video: {
          deviceId: { exact: this.idCamara },
          width: { ideal: 500 },
        height: { ideal: 500 } 
        }
      });
      
      if (this.stream) {
        video.srcObject = this.stream;
        video.play();
        //this.error = null;
        
      } else {
        //this.error = "You have no output video device";
      console.log("error desde ek id stream");
      } 
    } catch (e) {
      console.log("error catch "+e);
      //this.error = e;
    }
  }
}

public captures!: Array<any>;

public capture(canvas: any, video: any) {
  this.isCaptured = true;
  this.captures = [];
  var context = canvas.getContext("2d").drawImage(video, 0, 0, 500,500 );
  
  console.log(canvas.toDataURL("image/png"));
  this.captures.push(canvas.toDataURL("image/png"));
  this.auxImg = canvas.toDataURL("image/png");
  
}

tomarOtra(){
  this.isCaptured = false;
  this.auxImg = "";
}

closeModal(){
  this.modalService.dismissAll();
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });

  this.isCaptured = false;
}


btnGuardarfoto(){
  navigator.mediaDevices.getUserMedia().finally();
  if(this.auxImg != ""){
    this.alumno.alumnoBase64Image = this.auxImg.split(",")[1];
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.alumno.alumnoBase64Image);
    this.modalService.dismissAll()
    
    
  }else{
    alert("no se guardo ninguna foto");
    this.modalService.dismissAll()
  }
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.isCaptured = false;
}

cambioCamara(video:any){
  this.stream.getTracks().forEach(function(track: any) {
    track.stop();
  });
  this.iniciarCamara(video);
}
////////fin funcines camara

  

  
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      });
  }
  
  
  getHorarioBydisciplina(){
    this.arrayhorariosSelected = [];
    this.getPlanByIdJson();
    
    this.idHorarioDomingoSelected = 0 ;
    this.idHorarioLunesSelected = 0 ;
    this.idHorarioMartesSelected = 0 ;
    this.idHorarioMiercolesSelected = 0 ;
    this.idHorarioJuevesSelected = 0 ;
    this.idHorarioViernesSelected = 0 ;
    this.idHorarioSabadoSelected = 0 ;
    
    this.enableDomingo = true;
    this.enableLunes = true;
    this.enableMartes = true;
    this.enableMiercoles = true;
    this.enableJueves = true;
    this.enableViernes = true;
    this.enableSabado = true;
    
    this.arrayHorariosLunes = [];
    this.arrayHorariosMartes = [];
    this.arrayHorariosMiercoles = [];
    this.arrayHorariosJueves = [];
    this.arrayHorariosViernes = [];
    this.arrayHorariosSabado = [];
    this.arrayHorariosDomingo = [];
    
    console.log(this.planSelected);
    
    if(this.planSelected != 0){
      
      var json = {
        "disciplinaId" : this.disciplinaSelected,
        "edad" :  parseInt(this.calcularFdad()+"", 10) ,
        "sexo" : this.alumno.alumnoSexo
      }
      // var json = {
      //   "disciplinaId" : this.disciplinaSelected,
      //   "edad" :  12,
      //   "sexo" : "Masculino"
      // } 
      
      
      this.httpServices.getHorarioAforoById(json).subscribe(
        datos => {
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
      
           
           
           for(var i = 0 ; i < jsonDatos.length; i++ ){
             
            var json = { horarioId: jsonDatos[i].horarioId,
                      horarioNombre: jsonDatos[i].horarioNombre,
                      horarioInicio: jsonDatos[i].horarioInicio,
                      horarioFin: jsonDatos[i].horarioFin,
                      horarioDias: jsonDatos[i].horarioDias,
                      horarioDisponibilidad: jsonDatos[i].Disponibilidad  
            }
            switch(json.horarioDias){
              case "Lunes": 
              this.arrayHorariosLunes.push(json);
              break;
              case "Martes": 
              this.arrayHorariosMartes.push(json);
              break;
              case "Miercoles": 
              this.arrayHorariosMiercoles.push(json);
              break;
              case "Jueves": 
              this.arrayHorariosJueves.push(json);
              break;
              case "Viernes": 
              this.arrayHorariosViernes.push(json);
              break;
              case "Sabado": 
              this.arrayHorariosSabado.push(json);
              break;
              case "Domingo": 
              this.arrayHorariosDomingo.push(json);
              break;
            }
  
            
            /*this.arrayHorarios.push({ 
            horarioId: jsonDatos[i].horarioId, 
            horarioNombre : jsonDatos[i].horarioNombre+" "+jsonDatos[i].horarioInicio+" - "+jsonDatos[i].horarioFin
          });   */
          }
        });
    }
    
  }
  
  
 
  btnAgregarDisciplina(){
    if(this.planHorario.horario.length !=  this.planHorario.planRecurrencia){
      alert("solo has agregado "+this.planHorario.horario.length+" clases a tu lpan")
    }else{
      console.log("Agregar disciplina ********************************")
      console.log(this.planHorario);

      this.arrayPlanesHorariosAlumno.push({
        planId: this.planHorario.planId ,
        planDescripcion : this.planHorario.planDescripcion,
        planRecurrencia: this.planHorario.planRecurrencia,
        planImporte : this.planHorario.planImporte,
        disciplinaId:this.planHorario.disciplinaId,
        horario: this.planHorario.horario        
      });
      console.log(this.arrayPlanesHorariosAlumno);
     this.resetDisciplinas();
      
    }
    //this.resetDisciplinas();
  }
  
  quitarDis(index : any){
    this.arrayPlanesHorariosAlumno.splice(index,1);
  }
  
  
  getNombreDIsHor(){
    var str = "";
    for(var i = 0 ; i < this.arrayDisciplinas.length ; i++){
      if(this.arrayDisciplinas[i].disciplinaId == this.disciplinaSelected){
        str = this.arrayDisciplinas[i].disciplinaNombre;
        break;
      }
    }
    
    for(var i = 0 ; i < this.arrayHorarios.length ; i++){
      if(this.arrayHorarios[i].horarioId == this.disciplina.horarioId){
        str = str+" "+ this.arrayHorarios[i].horarioNombre;
        break;
      }
    }
    
    this.disciplina.nombreDisciplina = str;
  }
  
   
  
  getPlanesByDisciplina(){
    this.planSelected = 0;
    
    this.arrayHorarios = [];
    this.arrayHorariosLunes = [];
    this.arrayHorariosMartes = [];
    this.arrayHorariosMiercoles = [];
    this.arrayHorariosJueves = [];
    this.arrayHorariosViernes = [];
    this.arrayHorariosSabado = [];
    this.arrayHorariosDomingo = [];
  
    this.enableLunes = true;
    this.enableMartes = true;
    this.enableMiercoles = true;
    this.enableJueves = true;
    this.enableViernes = true;
    this.enableSabado = true;
    this.enableDomingo = true;

    
    
    
    this.httpServices.getPlanesByDisciplina(this.disciplinaSelected).subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayPlanesDisciplina = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayPlanesDisciplina.push({ 
          planId: jsonDatos[i].planId, 
          planDescripcion : jsonDatos[i].planDescripcion,
          planRecurrencia: jsonDatos[i].planRecurrencia,
          planImporte : jsonDatos[i].planImporte 
        });
        }
      });
  }
  
  
  
  
  
  getAlumnos(){
    this.httpServices.getAlumnos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayAlumnos = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayAlumnos.push({ 
          alumnoNombre: jsonDatos[i].alumnoNombre, 
          alumnoApellidoPaterno : jsonDatos[i].alumnoApellidoPaterno,
          alumnoApellidoMaterno : jsonDatos[i].alumnoApellidoMaterno,
          alumnoDisciplina :  jsonDatos[i].disciplinaNombre,
        alumnoHorario : jsonDatos[i].horarioNombre});   
        }
      });
  }
  
  resetAll(){
    this.alumno.alumnoBase64Image =""
    this.alumno.alumnoNombre =  "";
    this.alumno.alumnoApellidoPaterno = "";
    this.alumno.alumnoApellidoMaterno = "";
    this.alumno.alumnoCURP = "";
    this.alumno.alumnoFechaDeNacimiento = "";
    this.alumno.alumnoSexo = "";
    this.alumno.alumnoRutaFoto = "";
    
    this.alumno.alumnoTelefonoCasa= "";
    this.alumno.alumnoTelefonoCelular = "";
    this.alumno.alumnoCorreoElectronico = "";
    this.alumno.alumnoRedesSociales = "";
    this.alumno.alumnoPassword = "";
    this.alumno.alumnoCodigoPostal = 0;
    this.alumno.alumnoEstado = "";
    this.alumno.alumnoMunicipio = "";
    this.alumno.alumnoColonia = "";
    this.alumno.alumnoNumero = "";
    this.alumno.alumnoCalle = "";
    
    this.alumno.alumnoAfiliacionMedica = "";
    this.alumno.alumnoAlergias = "";
    this.alumno.alumnoPadecimientos = "";
    this.alumno.alumnoTipoDeSangre = "";
    this.alumno.alumnoEstatura = 0;
    this.alumno.alumnoPeso = 0;

    this.alumno.alumnoFolio="";
    
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    + this.baseImg);
    
    this.tutor.tutorNombre = "";
    this.tutor.tutorApellidoMaterno = "";
    this.tutor.tutorApellidoPaterno = "";
    this.tutor.tutorParentesco = "";
    this.tutor.tutorTelefonoCasa = "";
    this.tutor.tutorCorreoElectronico = "";
    this.tutor.tutorTelefonoCelular = "";
    
    this.arrayTutores = [];
    
    this.tutores = true;
    this.arrayTutores = [];
    this.arrayPlanesHorariosAlumno = [];
    //this.getAlumnos();

    this.parcialidad.TipoDePago="";
    this.parcialidad.cuentaBeneficiaria="";
    this.parcialidad.fechaDeReciboDePago="";
    this.parcialidad.folioDePago="";
    this.parcialidad.observaciones="";

    this.importeInscripcionAux=0;
    this.importeDisciplinasAux=0;
    this.porcentajeInscripcion=0;
    this.porcentajeInscripcion=0;
  }
  
  getPlanByIdJson(){
    if(this.planSelected ==0){
      this.planHorario.planId = 0;
      this.planHorario.planImporte = 0;
      this.planHorario.planRecurrencia = 0;
      this.planHorario.disciplinaId = 0 ,
      this.planHorario.horario = [];
      this.planHorario.planDescripcion = "";
      
       
      
    }else{
      for(var i = 0 ; i < this.arrayPlanesDisciplina.length ; i++){
        if(this.arrayPlanesDisciplina[i].planId == this.planSelected){
          console.log(this.arrayPlanesDisciplina[i]);
          this.planHorario.planId = this.arrayPlanesDisciplina[i].planId;
          this.planHorario.planImporte = this.arrayPlanesDisciplina[i].planImporte;
          console.log(this.planHorario.planImporte+" "+this.arrayPlanesDisciplina[i].planImporte);
          this.planHorario.planRecurrencia = this.arrayPlanesDisciplina[i].planRecurrencia;          
          this.planHorario.planDescripcion = this.arrayPlanesDisciplina[i].planDescripcion;
          this.planHorario.disciplinaId = this.disciplinaSelected;
          
          this.planHorario.horario = [];
      }
    }
  }
  }
  
  insertDomingo(){
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioDomingoSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosDomingo.length ; i++){
          if(this.arrayHorariosDomingo[i].horarioId == this.idHorarioDomingoSelected){
   
        //   this.arrayhorariosSelected.push(this.arrayHorariosDomingo[i]);
           
            this.planHorario.horario.push(this.arrayHorariosDomingo[i]);
            
          }
        }
      }
    }else{
      //aqui inhabilitar el select
    
      this.idHorarioDomingoSelected = 0;
    }
    
  }
  
  insertLunes(){
    console.log("");
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioLunesSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosLunes.length ; i++){
          if(this.arrayHorariosLunes[i].horarioId == this.idHorarioLunesSelected){
         
               
            this.planHorario.horario.push(this.arrayHorariosLunes[i]);
          }
        }
      }
    }else{
 
      
      this.idHorarioLunesSelected = 0;
    }
   
  }
  
  insertMartes(){
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioMartesSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosMartes.length ; i++){
          if(this.arrayHorariosMartes[i].horarioId == this.idHorarioMartesSelected){
          
            this.planHorario.horario.push(this.arrayHorariosMartes[i]);
          }
        }
      }
    }else{

      
      this.idHorarioMartesSelected = 0;
    }
  }
  
  insertMiercoles(){
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioMiercolesSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosMiercoles.length ; i++){
          if(this.arrayHorariosMiercoles[i].horarioId == this.idHorarioMiercolesSelected){
         
            this.planHorario.horario.push(this.arrayHorariosMiercoles[i]);
          }
        }
      }
    }else{

      
      this.idHorarioMiercolesSelected = 0;
    }
   
  }
  
  insertJueves(){
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioJuevesSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosJueves.length ; i++){
          if(this.arrayHorariosJueves[i].horarioId == this.idHorarioJuevesSelected){
            this.planHorario.horario.push(this.arrayHorariosJueves[i]);
  
          }
        }
      } 
    }else{
     
      this.idHorarioJuevesSelected = 0;
    }
    
  }
  
  insertViernes(){

    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioViernesSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosViernes.length ; i++){
          if(this.arrayHorariosViernes[i].horarioId == this.idHorarioViernesSelected){
            this.planHorario.horario.push(this.arrayHorariosViernes[i]);
          }
        }
      }
    }else{
  
      
      this.idHorarioViernesSelected = 0;
    }
   
  }
  insertSabado(){
    if(this.planHorario.horario.length < this.planHorario.planRecurrencia){
      if(this.idHorarioSabadoSelected != 0){
        for(var i = 0 ; i < this.arrayHorariosSabado.length ; i++){
          if(this.arrayHorariosSabado[i].horarioId == this.idHorarioSabadoSelected){
            this.planHorario.horario.push(this.arrayHorariosSabado[i]);
            
          } 
        }
      }
    }else{

      this.idHorarioSabadoSelected = 0;
    }
  }
  
  
  
  
  validarClasesPlan()
  {
    this.enableDomingo = true;
    this.enableLunes = true;
    this.enableMartes = true;
    this.enableMiercoles = true;
    this.enableJueves = true;
    this.enableViernes = true;
    this.enableSabado = true;
    
   this.planHorario.horario = [];
   this.insertLunes();
   this.insertMartes();
   this.insertMiercoles();
   this.insertJueves();
   this.insertViernes();
   this.insertSabado();
   this.insertDomingo();
   
   console.log(this.planHorario.horario);
   if(this.planHorario.horario.length == this.planHorario.planRecurrencia){
     if(this.idHorarioLunesSelected == 0){
      this.enableLunes = false;
     }
     if(this.idHorarioMartesSelected == 0){
      this.enableMartes = false;
     }
     if(this.idHorarioMiercolesSelected == 0){
      this.enableMiercoles = false;
     }
     if(this.idHorarioJuevesSelected == 0){
      this.enableJueves = false;
     }
     if(this.idHorarioViernesSelected == 0){
      this.enableViernes = false;
     }
     if(this.idHorarioSabadoSelected == 0){
      this.enableSabado = false;
     }
     if(this.idHorarioDomingoSelected == 0){
      this.enableDomingo = false;
     }
   }
  }
  
  public openPDF():void 
  {
    var Planes="";
    var qrcode = document.getElementById("qrcode")?.innerHTML;
    var parser = new DOMParser();
    var divContenedor;
    console.log("entre al pdf");

    if(qrcode != undefined)
    {
     divContenedor = parser.parseFromString(qrcode,"text/html");  
    }
    
    var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 
    
    var nombre = this.alumno.alumnoNombre+" "+this.alumno.alumnoApellidoPaterno+" "+this.alumno.alumnoApellidoMaterno;
    var folio = this.alumno.alumnoFolio;
    var fechaAlta = new Date();

    console.log("Alumno: " + nombre + " Folio: " + folio + " Fecha de Alta: " + fechaAlta.toLocaleString().substring(0,10))

    let PDF = new jsPDF('p', 'mm', 'letter');

    var w = PDF.internal.pageSize.getWidth();
    console.log("entre al pdf");

    PDF.addImage(images.data,"jpg", 0, 0, 216,278,"idQR", "NONE",0);
    PDF.setFontSize(10);

    //FOLIO
    PDF.setFontSize(10);
    PDF.text(folio, 146, 32);

    PDF.setFontSize(10);
    //FECHA DE ALTA
    PDF.text(fechaAlta.toLocaleString().substring(0,10), 160, 53)
    
    //NOMBRE
    PDF.text(this.alumno.alumnoNombre,37, 60);
    PDF.text(this.alumno.alumnoApellidoPaterno,85, 60);
    PDF.text(this.alumno.alumnoApellidoMaterno,152, 60);
    PDF.text(this.alumno.alumnoCURP,28, 71);
    var date = this.alumno.alumnoFechaDeNacimiento.split("-");


    PDF.text(date[2],150, 71);
    PDF.text(date[1],170, 71);
    PDF.text(date[0],196, 71);    

    for(var i = 0 ; i < this.arrayPlanesHorariosAlumno.length; i++)
    {
      var altura = 0;
      
      if(i == 0)
      {
        altura = 87;
      }
      else if(i == 1)
      {
        altura = 93;
      }
      else if(i == 2)
      {
         altura = 99; 
      }
      else
      {
        break;
      }
      
      var SubPlanDesc = this.arrayPlanesHorariosAlumno[i].planDescripcion

      Planes =  this.arrayPlanesHorariosAlumno.map(
                            function (plan, index, array){
                              return plan.planDescripcion.split('-')[0];
                            }).join(',');


      //PDF.text(this.arrayPlanesHorariosAlumno[i].planDescripcion,42, altura);

      console.log("Descripcion del plan:")      
      PDF.text(SubPlanDesc.substring(0,35),42, altura);
      console.log(SubPlanDesc);
      
      console.log("Horario del plan");
      var horario = this.arrayPlanesHorariosAlumno[i].horario[0].horarioInicio + " - " + this.arrayPlanesHorariosAlumno[i].horario[0].horarioFin
      console.log(horario);
      PDF.text(horario,170,altura);
      

      
      for(var j = 0 ; j < this.arrayPlanesHorariosAlumno[i].horario.length; j++)
      {
        
        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Lunes")
        {
          PDF.text("X",102, altura);
        }
        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Martes")
        {
          PDF.text("X",112, altura);
        }
        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Miercoles")
        {    
          PDF.text("X",122, altura);
        }
        
        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Jueves")
        {    
          PDF.text("X",132, altura);
        }

        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Viernes")
        {
          PDF.text("X",142, altura);
        }        

        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Sabado")
        {
          PDF.text("X",152, altura);
        }
        
        if(this.arrayPlanesHorariosAlumno[i].horario[j].horarioDias === "Domingo")
        {
          PDF.text("X",162, altura);
        }
          
      }
      
    }

    PDF.text(this.alumno.alumnoCalle,15, 110);
    PDF.text(this.alumno.alumnoNumero,120, 110);
    PDF.text(this.alumno.alumnoColonia,135, 110);

    PDF.text(this.alumno.alumnoMunicipio,15, 119);
    PDF.text(this.alumno.alumnoEstado,80, 119);
    PDF.text(this.alumno.alumnoCodigoPostal+"",145, 119);

    PDF.text(this.alumno.alumnoTelefonoCelular+"",15, 128);
    PDF.text(this.alumno.alumnoTelefonoCasa+"",60, 128);
    PDF.text(this.alumno.alumnoCorreoElectronico,112, 128);


    PDF.text(this.alumno.alumnoSexo,15, 138);
    PDF.text(parseInt(this.calcularFdad()+"")+"",62, 138);
    PDF.text(this.alumno.alumnoEstatura+"",112, 138);
    PDF.text(this.alumno.alumnoPeso+"",162, 138);

    PDF.text(this.alumno.alumnoAfiliacionMedica,60, 147);
    PDF.text(this.alumno.alumnoAlergias,135, 147);

    PDF.text(this.arrayTutores[0].tutorNombre+" "+this.arrayTutores[0].tutorApellidoPaterno+" "+this.arrayTutores[0].tutorApellidoMaterno,15, 157);
    PDF.text(this.arrayTutores[0].tutorParentesco,120, 157);
    PDF.text(this.arrayTutores[0].tutorTelefonoCelular,157, 157);

    PDF.addPage();
    PDF.addImage(images.data1,"jpg", 0, 0, 216,278,"idQR1", "NONE",0);

    PDF.addPage();
    PDF.addImage(images.data2,"jpg", 0, 0, 216,278,"idQR2", "NONE",0);

    PDF.addPage();
    PDF.addImage(images.data3,"jpg", 0, 0, 216,278,"idQR3", "NONE",0);


    if(this.calcularFdad() >= 60)
    {
      PDF.addPage();
      PDF.addImage(carta60.data60,"jpg", 0, 0, 216,278,"idQR20", "NONE",0);
      PDF.text(nombre,40, 20);
      PDF.text(parseInt(this.calcularFdad()+"")+"",160,20);
      PDF.text(this.alumno.alumnoTelefonoCelular+"",15,28);
      PDF.text(this.alumno.alumnoCorreoElectronico,80,28);
      PDF.text(this.arrayTutores[0].tutorNombre+" "+this.arrayTutores[0].tutorApellidoPaterno+" "+this.arrayTutores[0].tutorApellidoMaterno,50,168);
      PDF.text(this.arrayTutores[0].tutorTelefonoCelular+"",22,172);
    }
    else
    {
      if(this.calcularFdad() >= 18)
      {
        PDF.addPage();
        //PDF.addImage(cartaGeneral.dataGeneral,"jpg", 0, 0, 216,278,"idQR30", "NONE",0);
        PDF.addImage(cartaEexoneracion22.cartaexoneracion22,"jpg", 0, 0, 216,278,"idQR30", "NONE",0);
        PDF.text(nombre,40, 20);
        PDF.text(this.alumno.alumnoTelefonoCelular+"",15,28);
        PDF.text(this.alumno.alumnoCorreoElectronico,80,28);
        PDF.text("",147,39); 
        PDF.text(this.arrayTutores[0].tutorNombre+" "+this.arrayTutores[0].tutorApellidoPaterno+" "+this.arrayTutores[0].tutorApellidoMaterno,50,172);
        PDF.text(this.arrayTutores[0].tutorTelefonoCelular,22,176);
      }
      else
      {
        PDF.addPage();
        //PDF.addImage(cartaGeneral.dataGeneral,"jpg", 0, 0, 216,278,"idQR30", "NONE",0);
        PDF.addImage(cartaEexoneracion22.cartaexoneracion22,"jpg", 0, 0, 216,278,"idQR30", "NONE",0);
        PDF.text(this.arrayTutores[0].tutorNombre+" "+this.arrayTutores[0].tutorApellidoPaterno+" "+this.arrayTutores[0].tutorApellidoMaterno,40, 20);
        PDF.text(this.arrayTutores[0].tutorTelefonoCelular+"",15,28);
        PDF.text(this.arrayTutores[0].tutorCorreoElectronico,80,28);
        //Datos del Menor
        PDF.text(nombre.substring(0,25),147,39);
        //Disciplina
        // PDF.text(SubPlanDesc.split('-')[0],70,43); 
        PDF.text(Planes.substring(0,25),70,43);
        //Folio
        PDF.text(folio,152,43);

        PDF.text(this.arrayTutores[0].tutorNombre+" "+this.arrayTutores[0].tutorApellidoPaterno+" "+this.arrayTutores[0].tutorApellidoMaterno,50,172);
        PDF.text(this.arrayTutores[0].tutorCorreoElectronico,22,176);
      }
    }

    PDF.addPage();
    PDF.setFontSize(15);

    if(codeBase64 != undefined)
    {
      PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
    }
    //PDF.setDrawColor(0.5);

    PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
    PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);


    PDF.setFillColor(150,150,150);
    PDF.rect(53,90,110,7,'F');

    PDF.rect(63,33,35,35);

    PDF.line(55,80,106,80);


    PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
    PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
    PDF.setFontSize(6)

    PDF.setTextColor(255,255,255);
    PDF.text("Asociación Monarca de Triatlon",72, 93);
    PDF.text("del Estado de México",72, 96);

    PDF.text("Centro de Desarrollo del Deporte",129, 93);
    PDF.text("'Gral. Agustín Millán Vivero'",132, 96);

    PDF.setTextColor(0,0,0);



    PDF.cell(53,12 , 55, 85," ",0, "center");
    PDF.cell(108,12 , 55, 85," ",0, "center");

    PDF.setFontSize(10);

    if(nombre.length >= 24 )
    {
      var char = nombre.split(""); //   MENOR TEST MENOR TEST MENOR TEST
      var espacio = 0;
      for(var i = 23 ; i > 0; i--)
      {
        if(char[i] === " ")
        {
          espacio = i;
          break;
        }
      }
      
      var nom1 = nombre.substring(0,espacio);
      var nom2 = nombre.substring(espacio);
                  
      PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
      PDF.text(nom2,(w-54)/2, 79, { align : 'center'});

    }
    else
    {      
      PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
    }

    console.log("entre al pdf");
    PDF.save(nombre+'_qr.pdf');

    console.log("entre al pdf");
      
  }
    

    
    calcularPago(){      
      //console.log(this.porcentajeDePago);
      console.log("Porcentaje Disciplina: ");
      console.log(this.porcentajeDisciplina);
      console.log("***********************************");
      console.log("Porcentaje Inscripción");
      console.log(this.porcentajeInscripcion);
      console.log("***********************************");

      this.importeDisciplinasAux=((this.porcentajeDisciplina * this.importeDisciplinas)/100);
      this.importeInscripcionAux=((this.porcentajeInscripcion * this.importeInscripcion)/100);
      
      this.subTotal = Number((this.importeDisciplinasAux + this.importeInscripcionAux).toFixed(2));
      this.pagoTotal = this.subTotal;

      //this.pagoTotal = Number(((this.subTotal/100) * this.porcentajeDePago).toFixed(2));      
    }
  
  
  
  //reset para disicplina
  
  resetDisciplinas(){
    this.planSelected = 0;
    this.disciplinaSelected = 0;
    
    this.arrayHorarios = [];
    this.arrayHorariosLunes = [];
    this.arrayHorariosMartes = [];
    this.arrayHorariosMiercoles = [];
    this.arrayHorariosJueves = [];
    this.arrayHorariosViernes = [];
    this.arrayHorariosSabado = [];
    this.arrayHorariosDomingo = [];
  
    this.enableLunes = true;
    this.enableMartes = true;
    this.enableMiercoles = true;
    this.enableJueves = true;
    this.enableViernes = true;
    this.enableSabado = true;
    this.enableDomingo = true;

    this.planHorario.planId = 0;
    this.planHorario.planImporte = 0;
   
    this.planHorario.planRecurrencia = 0;
    this.planHorario.disciplinaId = 0 ;
    this.planHorario.planDescripcion = "";
    this.planHorario.horario = [];
  }
  
  
  
  
  
  
  
  
  curpVerificado = false;  
  verificarCurp(){
    this.httpServices.verificarCURP(this.alumno.alumnoCURP).subscribe(
      datos => {
        console.log(datos);
        if(datos === 1){
          alert("la CURP ya ha sido registrada");
        }else{
          this.curpVerificado = true;
        }
      });
  }
  
  
  
  
  
  //array estaticos
  arrayAlergiaMedica = [
    "NINGUNA",
    "ÁCIDO ACETILSALICÍLICO(ASPIRINA)",
    "IBUPROFENO",
    "METAMIZOL SÓDICO",
    "PARACETAMOL",
    "BUPRENORFINA",
    "CAPSAICINA",
    "CLONIXINATO DE LISINA",
    "DEXMEDETOMIDINA",
    "DEXTROPROPOXIFENO",
    "ETOFENAMATO",
    "FENTANILO",
    "HIDROMORFONA",
    "KETOROLACO",
    "METADONA",
    "MORFINA",
    "NALBUFINA",
    "OXICODONA",
    "PARACETAMOL",
    "TAPENTADOL",
    "TRAMADOL",
    "PENICILINA",
    "DICLOFENACO",
    "SULFAS SULFONAMIDAS",
    "AMOXICILINA",
    "NAPROXENO",
    "ERITROMICINA", 
    "BETAMETASONA",
    "NEOMELUBRINA",
    "SALBUTAMOL",
    "AINES",
    "METFORMINA",
    "DEXAMETASONA",
    "CAPTOPRIL",
    "OMEPRAZOL",
    "SUBSALICILATO DE BISMUTO (PEPTOBISMOL)",
    "YODO",
    "CEFALOSPORINAS",
    "BICARBONATO DE SODIO" 
  ];
  arrayAfiliacionesMedicas = [
      "NINGUNA",
      "IMSS",
      "ISSEMYM",
      "ISSFAM",
      "ISSSTE",
      "SEGURO POPULAR",
      "PEMEX",
      "PRIVADA",
      "OTRO"
  ];
  arrayParentesco = [
        "ABUELO(A)",
        "CUÑADO(A)",
        "ESPOSO(A)",
        "HERMANO(A)",
        "HIJO(A)",
        "MAMÁ",
        "NIETO(A)",
        "NUERA",
        "PAPÁ",
        "PRIMO(A)",
        "SOBRINO(A)",
        "SUEGRO(A)",
        "TÍO (A)",
        "YERNO",
        "OTRO"
  ];
  arrayTipoSanguineo = [
    "O negativo",
    "O positivo",
    "A negativo",
    "A positivo",
    "B negativo",
    "B positivo",
    "AB negativo",
    "AB positivo"
  ];
  
  //base64 de foto por default
  baseImg ="/9j/4AAQSkZJRgABAQEBLAEsAAD/4QBWRXhpZgAATU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAAITAAMAAAABAAEAAAAAAAAAAAEsAAAAAQAAASwAAAAB/+0ALFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAPHAFaAAMbJUccAQAAAgAEAP/hDIFodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0n77u/JyBpZD0nVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkJz8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0nYWRvYmU6bnM6bWV0YS8nIHg6eG1wdGs9J0ltYWdlOjpFeGlmVG9vbCAxMS44OCc+CjxyZGY6UkRGIHhtbG5zOnJkZj0naHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyc+CgogPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9JycKICB4bWxuczp0aWZmPSdodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyc+CiAgPHRpZmY6UmVzb2x1dGlvblVuaXQ+MjwvdGlmZjpSZXNvbHV0aW9uVW5pdD4KICA8dGlmZjpYUmVzb2x1dGlvbj4zMDAvMTwvdGlmZjpYUmVzb2x1dGlvbj4KICA8dGlmZjpZUmVzb2x1dGlvbj4zMDAvMTwvdGlmZjpZUmVzb2x1dGlvbj4KIDwvcmRmOkRlc2NyaXB0aW9uPgoKIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PScnCiAgeG1sbnM6eG1wTU09J2h0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8nPgogIDx4bXBNTTpEb2N1bWVudElEPmFkb2JlOmRvY2lkOnN0b2NrOjllYzdmMDA5LTM4MTUtNGUzNy1iNTkwLTA4NDk5NGVmNGVlNDwveG1wTU06RG9jdW1lbnRJRD4KICA8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOmVjMzk5OWY2LTdmZjEtNDU5NC05MzkxLWM0OWRjNzg3YzZjNzwveG1wTU06SW5zdGFuY2VJRD4KIDwvcmRmOkRlc2NyaXB0aW9uPgo8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSd3Jz8+/9sAQwACAQEBAQECAQEBAgICAgIEAwICAgIFBAQDBAYFBgYGBQYGBgcJCAYHCQcGBggLCAkKCgoKCgYICwwLCgwJCgoK/9sAQwECAgICAgIFAwMFCgcGBwoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoK/8AAEQgB9AH0AwERAAIRAQMRAf/EAB4AAQACAgMBAQEAAAAAAAAAAAACAwEEBQkKCAYH/8QAThAAAQMCBAQEAwMJBQUGBQUAAQACAwQRBQYSIQcIMUEJE1FhFCJxCjKBFRkjQlJZkZahFmLB0eEkM3Kx8CVDU3SCkhc0RIOiY5OjstL/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIFBAMG/8QAMREBAAEDAQYGAQMEAwEAAAAAAAECAxEEEhQhMVHwBUFSkaHREyJxgTJCYfGxweEj/9oADAMBAAIRAxEAPwDv8QEBBh5sLINV87Y5A97bgdQgtNPTzvbUg3Fr2HQoKYcUpKuU0jorA7DUNj7IMuqZKOb4VzNbLXYb7genugy+aSoIbps30QXQx2G6CxAQEBAQEBAQEBAQEBAQEGH9PxQa8LmfEWeN/wBX6oE/wU8xppjok7G9r/T1QVw09Xh0hLB5sTuob94e9kE2z0LXmaGEaz1+SxQYY10z/Mf3QbMbA0IJICAgICAgICAgICAgIMEgBBB01u6Cmao2sCgxRODxJKG6i0bBBJ9UYqUGuY3W/pG1BUIpGkNkbY2QWth1WLje3S5QXRx6R0QTQEBAQEBAQEBAQEBBW+W3dBTJUDoEEW0vxDPMlk0tKDAgnwz9NDIZIju9h6geoQTNFRzzsxBjrfrG3Q+6CMlqicvHToEGxFEAEFgAHRAQEBAQEBAQEBAQEBAQEBBhwuEGpUMLXa27EdCgu/2eRra58d3Mad7bhBRHW1TpS8gaT0Zbogm2N8r/ADJOpQXxxho3CCaAgICAgICAgICAgICAgrmdZBWyOOojIDvm/wCSCHk09NF5lfIASel/8kGWsipWmto/mjI+dgPUeoQPOw6SUVQGp9tiQdkBrnzyeY4fQeiDYY2wugkgICAgICAgICAgICDDzYWQazp2Ml1SC4H9EEcQp3uj+Ipt9rlo7+4QYqInV+GMNM65ABt62FrIM4VHUQU721QIaPuh39UFVPE5rfLa46b3tdBtwxadkFwFtggICAgICAgICAgICAgICAgICCqaPUOiCEJMQc0i4PQIMQwAG5CC9rA0dEEkBAQEBAQEBAQEBAQEGCQAgg6YDugollLzpbvf0QQdSV7XCaDSCOjS7qglJHDisBjkbolZ1B6tP+SBSU8lDQPjmcLuJsAfwQYp6cW6bINqOINHRBNAQEBAQEBAQEBAQEEJJNPdBQ+pHZBXEWz1LWO33uQgVGKvpsQMMrP0YA7b/VBOaJ8H+10UgDXbuYeh9wgGWeoGl1gO4CC6GENCC0C2wQEBAQEBAQEBAQEBAQEBAQEBAQCAeqCJjB7/ANEGWtAQZQEBAQEBAQEBAQEBAQEFcz7BBWyJk8R0P+f37IKKFzvjzDMLOa02BQVVNdX01eQSS2+zSNiEGzXtLJo6mLZ+4PuEAebUO1SdugCDYijDR0QTQEBAQEBAQEBAQEBAQa07gCNQ2vuglUPpGRtEwAY7ZruwQa78PljkbVUEofbcNcev4oLXy0VQQayDS9v6r27j/NAklNRZjG6WjsgthiDRZBaBbYICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICASALlBW+YDugommDhYIMQuZTU766TfbYf9e6A10OJxippn6JozsfQ+h9kGW15voqKUh7SgfPUv1vG3YeiDYjjDR0QTQEBAQEBAQEBAQEBAQEFFQwkIIQNjqIHUkwuOw9kEZ6t8Ugp6RgaGdbj+iCT3SVVg5oAHp6oLYYQ0dEFoFhYICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgjKSAgphjZM8l5vb9VBXPRPe3XSSh390n/FBGklYYnYdXM0E3sHbaggnT0NLhzzN5riSLAEoMBrp5TI7ug2Y4w0dEE0BAQEBAQEBAQEBAQEBAQRkbqCCgMdFKHt/FAkYJZi8N/1QXRxADogmgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgjILhBqh5gnD+3f6ILBSx01S+tMxa0i5bfa/qgiK2CpDmTU509ri9/8AJBXFTtLiWssPRBtRRADogsQEBAQEBAQEBAQEBAQEBAQEGCwFBgRgIJICAgICAgICAgICAgICDDnBqCt04HdBgT3NgSgmyUHugmgICAgICAgICAgICAgICAgICAgIBFxZBr1EWrsgx8slKYperdh/ggjDT26hBsMjDR0QTQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQUzvsCUEWRxVENmO+Ydb9igrodYfLHKyzmj0QQhqD2KDbikuLoLEBAQEBAQEBAQEBAQEBAQEBAQEBBhzQ5BAw79EEms09UEkBAQEBAQEBAQEBAQEBBgvAQRMwv2QBKCgmCCLhAQEBAQEBAQEBAQEBAQEBAQRe8NCDWqH6jpAQQqWtw9gmZMfNJ+72d7INilqYquPz4mgPAsQe3sgh8aS4xy0xa4dr3QSi1OOo90F6AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMONhdBSSHyCPVZBRV66Z13XLT0cgsqHNgDA3uOvqgk2e4QbCAgICAgICAgICAgICAgXA6lBF8gA2KDXklc52hguUFT46mGRsz4Toa4FxBvYIJYjQSVb2zwOB+W1ie3qgwyj+AjbK2X9JfcdiPRBNjHzP8yQ7lBssZpF0EkBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQQldYdUGqY5KiTTGbe/ogmyZsrnYfWWLrbH9of5oJ1ApI2sbUg2As1xB/wQRtQW2O3sSg2UBAQEBAQEBAQEBAQEGHODUFMs4b3QVSmbS12g2d0sglQl2iVwZ87TYAoNfD8UqZaryKgXDjYbWLSgtlM9NUOZBIQw7hvUBBNkb5DrkcSfdBfHGG9kE0BAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAJA6oISShvdBrTT6ht0QSoX+ZBJ5ZAfcge22yDSioqmafS6NzSDu49vdBuySyCI00zQ53TV6j1+qDDKclu4QbaAgICAgICAgICAgIB2F0GvPKQghLA1kBlnm0E9LoM0NdA+FjJKhusm1j1QUyfHUVcZmsdKyU72CC4VVLrMraYiTvdoBCAxrpXmR/dBsMYAOiCSAgICAgICAgICAgICASB1QQMoCB53uEEmuDkGUBAQEBAQEBAQEBAQEBAQEBAQYc8NQUvn3sNyg1pajU7Tfcm3VBbV1seGBsbYtRd13sgyZmzU/x9Hs4D5gR19QUBtdLI2zYg0+t7oMxQknU7clBeGC24QSQEBAQEBAQEBAQEBBh5sLINaRwEzXHoDugzVUDK2Rsj5zpA+6EFbcPwqW8ERGoC92vuQgzT1k0QME7dZYSNd+qDLGOmkMr+pQbLIw0IJICAgICAgICAgICAgICCmaTTvdBXURyMi81rtQtcgII072yUz577g2+iCUNRfYlBsMffZBJAQEBAQEBAQEBAQEBAQEBBh7tIQa73ve7y4xugiwS09Wxj2gh9wDdBLEYSKR3w8ALr326j3CCpk9FitOGVJDXjrvax9kFgFPTU/w1MdV+puglBDbchBe1ob0QZQEBAQEBAQEBAQEBBh7w0INd8xLtLASfRBXMyVvzSMIHqgrdHr+UusD1QbEEEFAwvB1PcOvqgjHEZHF7u5uUGyxgaOiCSAgICAgICAgICDXxLFcNwagmxTFa+GmpqeMvnqKiVrI42jqXOcQGj3JTjMj5f46+Nx4UnLjic2CcUeeXIrK+BpM2H4DiD8XqGEfqujoGTFrvY2K96dNfr5UvGq/apjOXzjmf7Wt4ReX62WlwvH+I2OMjdZlRhWQZWsk92/ESRH+IC9o0Gol573axnP8Ax9uJwz7Xx4UddIY6zL/FyiAdYPqMkwPBHr+iq3H+imfDtRHREay1M/6+39V4V/aZPBs4otZDLzW/2YqpCAKbOGV8QoLX9ZTC6If+9Uq0WppjOF6dTaqqxEvrjhNzIcAuYfBG5j4Dcacq5zoHRh/xWV8wU9cwA9NXkvcWn2NiuaqiuicVRh7U10V8py/ZtmloWtdUEGNxsSDu0qqy5sNNDFI4f7uTcgbj+iCtrcP0aonbDsHFBZCSf4oLkBAQEBAQEBAQEBAQEBAQEFcxFjughTA+W9zAC++1/wCiDV/JVXUSGapqNJv23P8Aogmxs+FvGuUywvO5PVpQTnNI+PTDE0lxvcNsgzDBbchBssbpCDKAgICAgICAgICAgw52kIKn1AHUoKZKnbZBmheXxyyRAF42aCgS1L6aIQTOEsz/ANUD/rZBHyHxuDJLdOyC1kA62QXMjDQgkgICAgICAgICD81xc4xcK+AvD/EuKvGjiFg+V8t4RAZsSxvHcQjpqanYP2nvIFz0DRcuJAAJNlNNNVc4p4omqKYzLpU8RP7YJl/B6jEeG3hu8LGYvKxz4W8Sc70r46Q9R5lJh4LZJR3a+odGPWJwWnZ8OmeNyXBd11NPCnvvuHTlzW+Ihzt87+LPxPml5ls1ZtiMpkiwiqxAwYbASb/oqKAMp2fUMv7rSt2LVr+mGdXqLtznL+MMtG3RGA1v7LRYfwC9njMzVOZEQIMhzm/dcR9CiYiX6HhPnTNvDnPdHnLh/mzE8AxikeZKPFcExCSkqYngbFssTmuB/FUqppmMTD1iuqnGJdkHJ99qE8RLllzDS5f5gamDjPlBuhhgzC4U2MQs2uYq+Jl5Hddqhkt/2m9Vw3dBarj9PCXZb1tdM4q4999Xeh4cvjBcm3iM5dii4MZ7dh2ao6fzcT4fZk0U2MUdh8zmRhxbUxD/AMSFz2jbVoJssu9p7tif1Rw6tG3eouxwfUscQe8va0AE3AC8Hq2o2aQgmgICAgICAgICAgICAgICCEkgaNigoknB2CCtsj2OLo3WJQYcyurDp806e9th/RBdUtayBtKHFxFr3QIIO5CDYa0NCDKAgICAgICAgICAgIKp32BKCuEQVEZiJs/r7oIPNJh8bTVnU53tdBIGFkRr6EC1vnaNgR/gUBlbSvPnMpzrI6lov/FAjD5XmR/dBssbYXKDKAgICAgICAgIPmLxQfFX5bPCz4JO4mcZsTOI5gxJkkeTsjYdUNGIY7UNG4be/kwMJBkqHDSwEABzy1jvexYrv1Yh5Xb1NqnMvLR4j3in82fifcUf7dcwucfKwWhne7LGRsIkfHhOCRnp5cZP6WYj71RJeR3QFrbMG7Y09uxHDmxb2pruz/h839eq6HMICAgy5rm21NIuLi47ItFLmsDpaWiwx2NVMXmOFywW6AG234qszxW5NyixGix5pikhMcrPmbvcj3BUckRMS15szz0Ez6OqpA98bra2vsHe9rbKcZSpwXiBnHK+a6DPGUMyV2DYvhNUypwnE8JrH09RRTNN2yRSsIcx4IHzAgpNEVRiSK5onMO/TwOPtOVPxexnBuUjxHseoMOzJVGOjyvxTk0U9Li0xs1lPiLRZlPUO2DahumKQ2DhG4gvyNVoZp/Xb9mrp9XFc7NXPvvvLu3BDhcLMd4gICAgICAgICAgICAgi54agg+cDqUGtPPqNr9Tsgv1GCVlNDAXXF3vOwAQU1DonzltOL6R+k09AgzCHjdjiLoLGQfNc7lBe1ukIMoCAgICAgICAgICDDnaQgqfOB1KCmSR0rgxguT0CCL8MqriWKoDXjcbIJWjxKM0lZHomZ/1ceyDMNP8BQuge8OLyf67IFPAOtkGyyMNG4QSQEBAQEBAQEBB87+J34jfBnwxOVvFeYjiu746sL/gspZXgqAypx3E3tJjpoyfuMABfJJYiONrnWJ0tPrZs1X7kUw87tym1RmXkT5xucHjrz2cwWO8yfMPmt2KZgxuazIo9TabDaVpPlUVLGSfKgjBs1vUm7nFz3Ocfo7Vqi1RFNLAvXqr1WZfy9ejyEBAQ5t38h4jDSDEX04LG/MWE729SPRRmF4iH6OKnpauOKsko2axH8oIBsCOiolx2GV9PTmXCMShELS92lrjsAe1/wDFTPFCyGlwbBJHV/xmo6SGM1g/wt1QjEOBral1ZVyVThYvcTb0V4RMqkVCA4FrgCCLEEbEImJmJzDv0+zNePDi+ca7A/DW5xM1yVWI+UKPhNnPEqnVJVNY35MGqnuN3SBoIp5CbvDfJJLhHqx9dpNn/wClH8tfR6rb/RVzd6AIIuFltAQEBAQEBAQEBAQEGHGwugokcC8ML7XPVBCakm80CI/KepPZAfh9LKTE2pOsdbOFx+CCYnY5ppKmXRJaxINr+4QRaKakhMFLuT1N7oJwMsAgvAA6ICAgICAgICAgICAgIKp32BKCEbIaiItBs8dT3CCihL4sQdBOLO0HTfv9EFNQcRp8QL2lxu75bdCPRBtYiz9NFKw2e2+4QGMfK7XIST7oNmNmkdEEkBAQEBAQEBAQaeYcwYLlTAa3M+ZMVp6HDsOpJKqvrquUMip4Y2F8kj3HZrWtBcSegBSImZxCJmIji8hfjb+KDmbxQeczFeIWGYvVDhzleWbCuGWESAsbHQB/z1rmdpqpzRK4ncM8qO9o19FpNPFi3jz82Jq783K8RyfHa6nGICAg3cvQwz4tE2axAuQD3IGyieS1LlMXw/GcUrDA0tZTNtpJfsfcgbqsTELMx4NiWGNbUUdeZTGP9y5pAI7gbqMyjEwm7F8Aq4BWVLYy9rfuPZdw9vdTiUvzkrmPlc+OPS0uJa297D0V1ZlFFRAQX4XimJYHidNjWDYjUUdZR1DJ6SrpJjHLBKxwcyRj27se1wDg4bggEKJiJjErU1TTOYetTwBvFKg8TLktpMTz7jMEnFDIjosG4hU7QGuqpNJNPiQaNgypjaXG2wljmaAAAvntXY/Bd4cpb2nuxdt5fdK5XQICAgICAgICAgw54agrkmAHVBrSCWckRMJPsgzTVc9K8U9fGWgmzHnp9EFL8Jnp8RZPTOJaX3JJ3b63QXV0bZaoG24bZBZDABvZBsNbpCDKAgICAgICAgICAgE2F0FT5w3YlBRNOHBAje2koX1obdx6fxsgRyQYtAHsOiVhuCOrT/iEBtbVRkwzwt1D9YHYoJNa+Z/mSFBsMjDR0QSQEBAQEBAQEBAQdU/2sTn7n5bORyi5VsjYsYMzcaaiahrnwyWfTYDT6HVp23HnOfDT+7ZJfRd/h9rbu7U+Tj1l2bdvEebzJk3N1usMQEBAQxlmOR8TxLG8tc03BHYovEYhyLMfx2rIpYH3e7YaGC5UYhLk6Fs+CUEtZi1U5z3kHQX339PqVXhMj825xc4uPc3V1ZlhFRAQEBB9q+ARz9VHID4jmUc249iroMmZ3mZlTPDHPtGylqpWiCqd2vBU+VJfqGGUD7y5NZZ/LZnrDs0V2aLmPJ67mm4uV883GUBAQEBAQEBBhxsEFLna5BGXWugormvpvn3LT3QSmnkpcNbPALkgEm3S6DFBVDE4HwVUYJA3IHVBilqaiKLyHfMWmzXH0QWwxEnU7cnqg2Gt0hBlAQEBAQEBAQEBAQEEZTYIKqdscj3a9yOgPogqmo4qoF1JOAR1af8Aq4QRppDTxmixGKzTfS47tI+qCcEWH0RMlOdTnDazroDI3SvMj+pKDZYwNHRBJAQEBAQEBAQEBBhxs0kC9h0QeTn7S/zU4hzNeLLnvBYMRM2CcMooMnYJEH3ax1M3zKx222o1c07SfSNo7Lf0NuKNPE9eLE11e1dx33zfAS7XEICAiYjLmsDpaWkwx2MTwea+9mNte29hb3uqzPFZsY4yjlw5r6qmEdQ8DymM3dq9PcKIS/P/AKamm/WY9jvoQVdEzhZWYhWV7w+rnL7dB0A/BIjCuZUogQEBAQEC1/l1lt9tTTuPce6TxhMTicvZP4NfNVXc5/hn8I+PWP4iypxutytHh+Y5Q8lzsRoXuo6h7r9HPfAZP/uD1XzWpt/ivVUvorNe3biX06vB6iAgICAgIMOcGoK3zDpdBqyAzSCNp+8bBBsSVFKJBh877lzbHV3/ANUFQc7DR5EzC+An5HWvb2KCTZ6eNhbRxW1dwLIMwQW3IQbDWBqDKAgICAgICAgICAgICCMguEGq5xgnEnvv9EFjoKWlmfiDju4dv8PqghHiDpQRLALHoLoMRQXdq0gD0CDZjj09UE0BAQEBAQEBAQEBBo5nzBhmVMu12Z8ZnEdJh1HLVVUhP3Y42F7j+AaVMRmcImcRl4aOKvELF+LfFDMvFbH6l81dmfMNdi9ZNI7U58tTUSTuJPfd6+popimmIh85dq2q84cArPMQcsctGTC2VdHP5khGpzR0I9B7hVzxXiIW0mVaZsLX4lVOY9/RjXAW9t+pSakt2kYzAWCmqJrwOd+jldtpPof81HMRnGD0lS7Fqms8x/8A3bS8G3s0BOMj8/iNa7EKx9W5obqOwHYK8cIUlSiBAQEBAQEBB6PfsanF+szRyQcS+DFZVmX+yHEsVlIxx3hgxCiifpHo0y08zvq5yxPEaYi7EtvQzM2uM98v+ncOs52iAgICAgIKJ3kbeqDD6Mlm0vze/RBqOfPQ1DJaiM6Qd3DcWQXVuGMr3tqYZgLjc9QfdBbO4RwCm163WAcSgxBBbchBsNaGhBlAQEBAQEBAQEBAQEBAQOqCioiugiHNdTGGQbjYIEMFtygvawNQSQEBAQEBAQEBAQEBB/I+f7HRlfkU40Zl80sOH8J8xVDXg/dLMMqCCvSzGb1Mf5h53eFqr9peJSnbopomfsxNH8AF9PHJ89cnNyZ/ympUZa1z3BjGkkmwAHVFohy2EuxjBvnmopDTu3e233fceirOFm5jWHDFoG4hQyayG7NB2cPb3VUTGUMSdLDldsVcT5rtIAd1+9cf0UxzH59XRMiKiAgICAgICAg7yvsU+YXQZ75hcqmU6ajCctVYZf8AYlxCO/8A/IsnxTlTP7tXw6Zmanf0slpiAgICAgINepabIIFr6ukLGPIljPyuBtv2QWPqG0kLY6uTW8jew6oKZWU4saRzm6tyGEgfwQThgtuQg2WN0hBlAQEBAQEBAQEBAQEBAQEBBhzQ5BHye9ggk1oagygICAgICAgICAgICAg/k3PpgJzVyPcZMsNhMhxHhVmKmbG0XLi/DahoAH4r0tTi7T+8PO7GbVX7S8SNO7VTRP8A2omH+LQvp45PnrkYuTH+XLVGW5WYcytpphK7Tqe1vp7eqjPFERC/KFPG581UW3ewAM9r3SqVlmH5kqZK4U9dE0B79IAFi036KMIzxK+udl/FCIWaoZmanRXtY9Lj06JEZhLisTxSoxSfzZtmj7jAdmq0RhWZaylUQEBAQEBAQEBB3nfYpstST5x5hs3GE6IMOy1RCS213yYjIW3/APQD/BZPik8KY/dqeHR/U79lktQQEBAQEBBCVlwg14iYJ79jsUGKmBpqS8G5PX2QWQwAdkF7WhoQZQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAkDcoNHMGF4dmHBavAMVgEtLW00lPUxOH345Gljh/AlI4TlE8YeHDjVw0xjgvxjzbwezBTPhrsqZnxDBquGQWcySlqZISD/7F9TRVt0xPV87XTs3JhXg5Zg2Dtqa6d2mVwLWddN+lv+aTzQslg+ClOL4WwPjkF5omfrD9oe6IUvxjLZlFf5d5huB5Zvf/AJJiUuGxPEJMTqzUyCwtZrfQK0RhWZa6lUQEBAQEBAQEBAQekf7G9wbq8o8hnEDjNXUpiOdOJkkFG5w/3tNQUkMIcPbzpZ2/VpWH4jVm7EdG5oqaotce/P8A7dvqz3YICAgICAgEXFkFb4r9Qgw2Gx6fxQWAABBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEEJnaR1QVNgNQwudIW3uBbsg8q32nLk/xzlw8U7Nme6HCyMB4p0UGbMLmiFx572iCtYbfrCohdIfadpPXfe0F2K7ER0Y+tomm5teXf/r4Qhjp8fwaOASaZIwAf7rgLfwK63FwmEqKE5foZJK6sDm3u1g6X9B7lRzIjD85UzmpqH1BaG63E2A2C9ETKCKiAgICAgICAgICCUbJZJGsggdK8kBkTG3c93ZoHck7D6pK1NO1OHs28JXlTrOSjw6eE3LjjVK2HF8EynDPmFgj0kYnVOdV1YPqWzTvZf+4F8zfuflvVVPorNGxbiH0YvF6CAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCudtwgppnhshgePleOhQda/2nDkKq+ZzkPm4+ZVoXVOaODM0+NU7Io9UlRgsjWtxGLYXOhjI6gf+WcB95dmivfjvYnlLl1VqLlv9nmNqK/AKqF9bFUOilaNnRHS8/wD+luxliuCnqaipdrqJ3vPYvddXVmUEVEBAQEBAQEBAQEBB93/Z2PD+l58fEcy2/M+EOnyRwzdFmzOD3t/RzGCUfBUZvsTNUhhLe8cMvouPW3vxWeHOXborM3LmfKHrUAsLL59tsoCAgICAgICAgICAgICAgICAgICAgICCLXhyCSAgICAgICAgICAgICAgICAgICAgw8XCDVnjIdqb1G4KCvG8Nocewl9BiVLFPTzxujnpp4w+OVjgWuY5p2cCCQQeoJQeSzx4vCsx3wy+b+thyhgE44VZ6qJ8S4d4iGkx0zbh0+Fud2kp3Ps0E3dC6J25DrfQaPURet8ecMXV2Jt1Zjl33/t8OrscIgICAgICAgICAgIN/KuVsyZ4zPh2S8m4DV4ri+L10VFhWGUEJknq6mV4ZHDGwbue5zg0AdSVFVUUxmVqKJrqxD11+Bz4YeFeF9yVYZw3zBR0snEPND2YzxJxKneHh1e5lmUbHj70NNGRE3exd5rx/vCvnNTfm/dmfLyfQWLUWreH2Uud7CAgICAgICAgICAgICAgICAgICAgICAg04aj13QbTH6u6CSAgICAgICAgICAgICAgICAgICAgg+MO7IICGx6H+CD+K+ILyD8EfEd5Y8b5ZuOVC9tHiAbUYPjNIxpqsFxCMHya2Au21sJILTs9jnsds4r1s3a7Ne1S87lum5TiXkW5/eQfj/4cfMZi3LlzBYAYqykcZsFxumid8Dj1AXER1tK933mO6Ob96N4cx4BG/0Nm9Rfo2qWFfsVWav8P4ovZ4CAgICAgICAgIMsY6R4YwEkkAAC9z6JyTETM4h6Jfs13gMV3LzTYX4g/ORlB9PnutpDJw8ydiUGl+XKaVlvjqljhdtbIxxDIzvBG83/AEjyI8XW6vb/APnRPDzbOk0/442qubueAAFgFmu5guAQYMgCAHg9UEkBAQEBAQEBAQEBAQEBAQEBAQEBAQarZaFzbxxb9hayCyG5A+qC5AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQfwTxEPDk5bPEw4C1PA3mHyyX+UXz5czJQNa3EcBrC2wqaaQg2J2D43XZI0aXg7Eetm9XZq2qXnct03KcS8t/ik+DNzbeFjnqSLifgTsxZBrKksy9xKwWjf+T6sE/LFUDc0VTbrFIbON/LfIBcbtjVW78cOfRjX9LXanvvv+XyMQQbELqcggICAgICAg/Q8K+E/Ezjjn/DOFXB3IWLZnzLjNQIcKwPA6F9TU1Lz+yxgJsOpcbNaLlxAF1WuuminMr0W6rk4h6KPA8+zQ5W5QMTwnmt56qfDMzcTabRU5dyhEW1OGZVl2LZnv3bWVrezx+ihdcs1uDZBi6rWzd/TRwhsafS02+NXN29gBosAs92sPNggp+eVxaw2t1KCkyPbMIX7OJAQWSvEMugeiC2KUOGyCwG+4QEBAQEBAQEBAQEBAQEBAQEBAQEGpHGXvMjha5ug2WM0hBJAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEHGZxyZlLiFliuyVnvLOH4zg+J0zqfEsKxWiZUU1XC4WdHJFIC17T3DgQpiZicwiYiqMS6fPES+yIcv/FyoxDiPyA8QRw2xydz5v7FY75lVgEzzvohkF56EE32HnRjoGNC0bPiNdHCuMw4r2ipr408++/N02c3ng5eJFyP1E9Rx35WMxNweF1hmnLVP+V8KeOzjUUuvyrgXtK2N3stK3qrFzlLOuaW7R33D5kaWueY2OBc02c0G5B9COy6Mw55pqjnAQR1BCIEGS1zW63Cw9T0Qfr+C3L5x15j8zxZM5f+DmZ864pNIGMosrYHPXOBP7RiaWsHqXEAWNzsqVXKKIzVL2psXKpxEd/s7QuRP7IzzmcasQpM1c6eccP4TZbOmSXBqCaLFMeqGHfQGxuNNSkj9Z75HNP/AHZXBe8Rt08KOLstaCrnU7zuQnwu+TDw28kPylyvcJ4MPrauIMxnNeJv+KxjFbG/+0VTgHFtwCImBkTT0YFlXb929OapaVu1RajEQ+hOnReT0EFczgB1QarHVBl1U4uR1v0sgttBiDQ8fLJG4XB6g+hQSmgppZ7uns+1tIcEGNDIXaWPue/sgvZ0QZQEBAQEBAQEBAQEBAQEBAQEBAQQjjACCaAgICAgICAgICAgICAgICAgICAgICAgIBIAuSgICDBa072QfxDjt4anIFzNVMmIceOTvhzmatlv5mJ4hlSmFW6/X/aGNbL/APkvWm9eo4RVOHnNq3M5xxfOWb/sxXgv5rq5K2DlRqMIfIbluC53xeBjT/dYalzW/QCy9qddqY8/h5VaSxVOZj5lwuD/AGVzwbcMmMtXwKzLXjVcR1vEPEy0e3yStNlO/anr8I3OxPOH9c4V+Av4QnB2aGqyjyF5FqpoLFs+ZaWbGHkjuTXSSg/wXlVqtRV/c9KbFqmMY4PqXJuQ8k8OsCiyvkDKGF4HhkH+4w7B8PipYI/+GOJrWj+C8ZmZnMy9YiKYxDlgABYBQkQLgGxKCD5Q0bFBq1FQCOuyCQe9mGOmgHzEE7fVBo0Iq3VPm09y7q4k7H6oN2pfDUsafLIeDvfqPZBOniI3QbAFhZBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQCQOpQY1tQY8wdwgkCDuEBAQEBAQEAkAXJQfLXP94yHIT4b+GS0/MDxip5sziHzKPIWWg2uxqp2uLwNcBTtPaSd0bD2J6L3tae7en9McOrxuX7dqOMuk7nh+10863GuapytydZHwrhFgTnFseMVLY8WxyZvS+uVnw1Pf9lkT3DtItO14dbp418Wfd19XKnvv+H9z8Of7YHDFS0PDbxK+GcplbpiHEvItBqa8dNdZhwN2nu59MSD2hC8r3h0xxtz/D1s66mYxW7mOWrnH5XOcTJ7c98sPHbLOdsNLA6Z+A4oyWWmv0bPDtLA7+7Ixp9lm127lucVRh30101xwl/SwQ4XBuqLCAgICAgqqqymoqeSrqp2RxRMLpZXuAaxoFySTsAB3KD4I59vtIPhtckEFZlvCuJbeKGc6cOY3KvDuojrGxSj9Wprb/DU4vsRqfIP/DK67OjvXfLEOe5qbVvzdN/F37WH4o+dePcHFPhjieVMnZXoXOZR8O2YGyvoqiEkH/bKiUCeaWwA1xuhDd9LRc30qfDrEUYnjPVn1a+5NeaeXff07BOQT7W/yxcc6+g4e86/DufhXjlQWx/2mw6aTEMBmk9XkN+Iowf77ZGDvIFxXvD7lHGji67Wst18J77/AJdsuSeJXDXiRlehzpw8zdheYMFxKAS0GMYNWx1dLUsPRzJYyWuH0K4JiaZxLsiYqjMOae5tA0SQt1ROO4B+7fuPZQk+LZpIp4rE9yAEGIYTfU7qg2WN0hBlAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEFUjzew6nogpqHSQkayLHoUGQ7TTibVe7tkE4ptXdBcCCEGUBAQEH885nea3l75NeE1fxv5luKmFZTy3hwtJXYlPZ00hBLYYIm3kqJnWOmKNrnnsOpV6KK7lWzTGVa66aIzLz5+KH9q35jeYyTEuEnIVQV3C3JchfBLm6dzTmPFIrkao3NJZhrHDtGXzf/AKjN2rXseH0Uca+M/DMv66eVHffcupLFcWxTHcUqcbxvEqisrayZ01ZWVk7pZqiRxu58j3kue4ncucSStGIiIwzqqqqpzLXUqiDmcgcReIHCjNVPnvhdnrGctY5SOvS4zl/FJqKriP8Adlhc14+l7KtVFNUYmF6bldPKX3py0fagfFr5eIKXCMxcXcH4mYXTHak4jYG2pnc3uDWU7oZ3H3e55XHXoNPX5Y/Z10a67Tz77/d9s8Hvtp1B8Oyk4+8h1SyXbza/JedWSNPraCrgYR/+6Vy1eGTH9NTqp8QomeMd/P8Ay/vmUftiXhk4xRtfmvhZxhwWo/XjdlmhqWD6OirST/ALznw6/HnC9OutVdx9uarfteXhO0sQkp8M4sVTiL6IcixtI9ryVLR/VVjw/UT07/hedXaj/cfb+ZcRftnfJxhIkHCrlJ4nY89oPlflqsw7DGOPuWyzuA/9JPsvSPDbvnMPPf7Xf/mXyrx8+2Rc6udaafDeXvluyDkSOUkR1+M1NTjlXEOxbf4eEH6scPZe9HhluP6py8KvEZnlHf7/APjr55rfFM8QbnadUU3MpzW5tx/DKl5c/LsFd8DhQ9vgqURwkf8AE1x912W9NZtf0w5K9Tduc5fwOmpZqh7aakguf1WMFgP8l78njxqnMuXy7RRxVc9BX0RMhZuXC4Df9VWUrcRpaPAn0tTS0z7sk3eD1HcH39FGTk/vvJT4iXNxyE52Zn7lX4wVOF01RK12MZYr2mqwbFmj9WopHODddrjzWFkrb7PC8bti1ejFUPe1qLlucxL0E+Fr9or5UefaXDeB/FV9Lw04rVAZFBgGKV4OHY1Lb/6CqfYF5PSml0y72b5ti5ZF/R3LPGOMNWzqaLnDzdi1PE0i4XG6Ww1oagygICAgICAgICAgICAgEgdSgxqb6oMgg9CgICAgICAgICAgICAgICAgICDDzYINSd7i6zb37WQTZL5v+x1rLOcPlJ/W/wBUGfhY2UoppZrAO2de197oMGmZC3UJj7bdUFsR6ILEBAQfEPi8eOPy0eFZk92XcTezN3FTE6EzZd4e4fVBr2tdcMqq6UA/CU1xsSDJJYiNps5zenT6W5qJz5dXhev0WY483mA54uf7mk8RDjDPxn5oOJFRjFYHPbg+EQF0WG4JA4//AC9HTXLYWWsC7eR5F3vcd1u2bNFmnFMMW7qK7s5l/GF7PAQEBAQEBAQEBAQEH6HB4zh+BGupaYyzSC9gLk72H4BUnjL0UUuHZknnNe+fynltgZHdvS1jZTMwictuCqcz/sfHg27m/JITs8fX1UH7tDHY8MoGsiw2QiUm7yyQkW91MZOTimvex4ka4hwcHAg7gg3B+t97q0xlEVTE8HcN4Lf2n3iFy3T4Vy2eIdjmJ5t4ft0U2D8QX66rF8vM2DWVXV9dSt/a3njHTzWgNbl6rQRV+q3z6NLT63+2vvvvo9EnDziJkbizknC+JPDTNmH47gGN0UdZhGMYVVtnpqyB4uySORhIc0juP+ayJiaZxLTiYmMw5lQkQEBAQEBAQEBAQEGHO0hBS+U76QTbqggyd0j9DepQTEul5Y47hBa14cgkgICAgICAgICAgICAgICDDnBqCiaew6oIULmyTuJ/VCDSrZaiarIeSC11mj0QbzJHOgMGIR76dj+1/qghTw+g2QbcbdIQSQCQBclB1lePJ4/mT/Dey9Py78udRhuYON2K0Yc6OYCakyjTyNuyqq2dJKhwIdFTHqLSSWZpbJ26TSTfnaq5OXUaim1GI5vMZxM4mcQeMuf8X4qcV854jmHMmP1z6zGsbxeqdNU1k7ur3vPU9AALBoAAAAAG7TTTRTiOTErrquVZlwasoICAgICAgICAgICAiYiZb2GY/V4ZGYGta9l7hru30UTESu24cUx/GZwyk/RMvu5rdh9Sev0UYiBnN9RE50NKCC9l3O9r2SkcKrKTOREAJBuCg++fBV8dPjV4Wme4OHmcH12a+C2MV4fj+UTLqnwh7z89dhpcbRy/rPguI5rG+l9nri1WkovxmObu0uqm3OzVy77/AOf8ep7gVx14Tcy3CbA+OPA3PNBmTKuY6FtXg+MYdLqjnjOxBB3Y9rgWvY4BzHNc1wBBCwqqKqKtmqOLZpqiqMw/WqqRAQEBAQEBAQEBBXO6wQa8VY2B9pB8p6n0QTNKWVbKmAgsJ+YDtt1CDEtJUOmdI0t0k7b7oJsD4yGP6/VBc03CDKAgICAgICAgICAgIBIAuUFckwb3QUSVO2yAPh4KcVNVvq6BBmJtPM34ugtqGxA2B9igMrqWUiXyDrHctFx+KDB11D9bht2CDYijAHRBNAQdcvj7+N9lnwy+EZ4P8GMSo8R425uw5xwGjcGyx5cpHXacUqWHYm4Igid/vHtLiNDHX7NJpZv1Zn+mHNqNRTapx5vLPnLOWbOIebMSz5nvMldjON4zXS1uLYtidU6apramRxdJNLI4kve5xJJK36aYpjEMKuua5zLjVKogICAgICAgICAgILaKmdWVUdKw2L3Wv6e6LRD9DBQYJHUfkplD5j2svJI5t7fU+v0VMys4PGKSmpK58VHJqYDv/dP7N+6tAlRY7X0FM6lge3SfulwuW/RMRKsy1JJHyvMkjy5xNySdypRM5YRAgICD738DDxqs/wDhZ8am5Sz9W4hjPBfNVc3+1+XYnGR+FTOs38q0bO0rAB5sYsJ422++1hHHq9LF+nMc3dpdTNudmrl333w9WXDjiNkbi7kTCOJ3DPNVDjmX8ew6KuwbGMNnEsFZTStDmSscNi0gg/0NiCFgTE0ziWzExVGYc2oSICAgICAgICCLngdCg1qiYG4ugTSsoqIShgc6T173QauG174HeVJcxn2+7/og26nzIpQ+GpdZ25be6CULXOOt5JJQbAFhZAQEBAQEBAQEBAQEAmwugomm090FL3mN7Xzxu0k9LdUF1TS05Y+ZzHH5ejev4IKKZ0GKUIpnus9oH1HugnRUgw1j3SS31dAEEYIdRuQg2o4wAgmgIPmbxXvEr4V+F1yo4nx7zwIMRx6qc7D8jZVdPpkxrFHMJZGbbthYB5ksn6rGm3zOYD7WLFV+5FMPK7ci1RmXkF5guPnFfmi40Zj5gOOGbJsbzVmrE312MYjNsHSOsGsY3pHExobGyMbMYxrR0X0dFFNuiKaeTAu3Ju17Uvxyu8xAQEBAQEBAQEBBs4Vhr8Uqvh2yBgAu5x9PYd1EzhMRla3L2IOrzQ6ALbmQ/dt6pmMLYhyEOWajD5mVlNVCR0br6CzTcdxe6jOUuQkAxCld8BUiJzzZ7w35h6g+hVRxeLuw7CsNOEU4Ekjzd5O5B9T7q0ZmcolwqsoICAgICB06IO3v7MZ40z+VriRR8gHMpmoR8OM44rpyRjNfNaPLWMTvt8O5ztmUlS8/8MUxDtmyvIzNfpduPyU84aei1H9lT0kg3FwsZqiAgICAgIMFwHVBB84A6oKHSPleI2dSgy+hhf8Ao3VB126Aj/kgyfJ8sUNcRcD5XHYOHqPQoMMZR0cZbT2cXbdb3QYggt1CDZYzSEEkBAQEBAQEBAQEBAQRkNggogYJJyXC+noEFFWcQrJTFFE5rGnYnb8UEmS4lRvElbZ8ZsCW/q+6CySjoDqqW7FwuC13f2QQhic8/M4n3JQbUcYaOiCaAg47N+bct5CyriWd8441T4bhGD0E1diuI1cgZFS00THSSyvcejWsa5xPoFMRMziETMRGZeQfxpPFCzX4pfOFiHFSnqKqlyDl0SYVw1wOf5fh8OD7uqpGdBUVLgJX92t8uO5Ea+h0tiLFvHn5sPV35u148ofIi6nIICAgICAgICAgICCykNUycS0YfrYbgsFyEXjg/S4diYxSndTvJhqA2zhbce4uqSlp4TFilDjPwcznOY4EvJJIIt97+NlCsZiWhj8gZjMxp3lu41aTbewurxyTLR69VKszkRAgICAgICB7f4omJxL1GfZo/FvqOfnlnk5deNWYXVPFbhbh8ENbV1L7y4/g1xFTYgT1dKwhsE57v8uQn9NYYGt0/wCGvajlLd0t+LtGJ5uzhcTqEBAQEGHu0hBrzT22CCMMQqYnESfMDt7IIsDaOujZJODraRb0KCmuw6sNZ59Pchzrgg7tKDZr2B4ja+xcNygQwDqQg2GMDUEkBAQEBAQEBAQEBAQYc4NCCmWYdCUFLZXRyeYz+vdAmxCpI0wxAE9+qC0ec2hcKx13OBHRBVBALdEG1HGGjogmgICDpX+1weJzPww4WYX4b/CTHvLxrPFGzFeI1RTSjXS4KJCIKIlpuDUyxuc8bfooLbiVaXh9jbqm5PlycOsvbFOI5vPGSSblbTFEBAQEBAQEBAQEG1hWFzYrOYo3hoaLuce34KJnCYjKuqoaijqTSzss6+3ofcKcrRGHPVdTHlulhp6WmDi6+pxNrkWufqqTxJnDM0sOL4ccTpQY54QS13cEb29whnLWkzdeiAjgInIsSfuj3/0U7PFLhHuc9xe9xJJuSe5VlJnLCIEBAQEBAQEBB/ZvD+50+JPh9c2mUOarhi98tTl3EAMVwoSFrMWw2SzKqif2tJFexP3Xtjd1aF5XrVN63NMvfT3ZtXMvZnwN4z8PuYrg9lnjrwoxxmJZbzbgdNiuC1rCLyU88YezUATpeAdLm9Wua4HcL5qqmaKppnyfQUzFUZfq1VIgICCiofYFBEvio4PiZhdx6BBp09dUskklgodQf+y07INiWGmxmASsOmRvfu0+hQZJrKQNidUh+3Ut3CCUcbnu1yEk+6DYYwNCCSAgICAgICAgICAgEgdUETKAgonn2IBQRHkQQiepuS4/KB3QKuKKIB7DYuP3UEYHPY/Uz8R6oLHNkmdqkP0HogujjDR0QTQEBB+S49caci8ufBbNXHjidijaPL+UMAq8XxeoLgC2CCJ0jg25F3O06Wju5wHdWppmuqKY80VVRTGXi05xuabiLzsczudOaXipO44xnPHJa59MXlzaKDZlPSMv+pDC2OIezL9SV9NatxatxTD56/cm5czL+Zr0eIgICAgICAgICAg28Djq5MSjbRSaHdS49AO9/VROMLxwh+hxCkocYa+kMrfNi6EdWH/JV5J5qH1NHUxfk3HdMcrO7jYO/vApiRrV2I4ZheHvw/C5NbpL6nA3tfqb/T0UxEo5ODVlZnIiBAQEBAQEBAQEBB6AvsffiH1Oacj5q8OLiLjeuoyyyXMnDw1Eu5w+WUCuo23PSKeRk7QN7VMnZqxvEbOKouR/LZ0V3ap2Z8u+/wCXeEsx3iAgi94AQa9Q8EWCAKmnMQZUsvb1bdBhuJGSdsEFMSCdyTawQRq2mOs8yA6SWjVbugnFE57tchJPug2GMDQgkgICAgICAgICAgICCqV5HTv0QU1LJomh25B9PVBg0FRIz5pGtJ7dUFtMflbT1DQJIxt79rhBGOm8iR1XVza3n7thYD6IEDdRLrWBN7IL2sA6oJICAgIOmv7YNzy1PC/lnyhyNZNxcxYjxMrzi2amRSEObgtDI0xxOH7M1WWfUUrx3Wj4da27k1z5OHXXZot4jz77/Z5z+vVbbFEBAQEBAQEBAQTp4JaqZtPAwue42ACciIy5GqypXwxh8D2y7fM0bEH29VG1C8RBlmQUeKuhqWFjnsLBqFiDcGyiUtybBcThxU1tBK2z3lxc49L9QR3CqjE5QzdVUxjjpNLXSg6i79kf6q1KXBKykzkRAgICAgICAgICAgIP7FyAc3WauRHnGyBzW5TklLso4/HPilLE8j43DZLxVtMfUSU75Wj+9pPZeV+1F21NMvfT3Jt3YmHtQydmzL+fMqYZnbKeJx1uFYxh8FdhlbCbsqKeaNskcjT3DmOafxXzMxMTiX0ETExlyRIHUqEovkAGyDXe98jxGzqUEvhYyTGJwXgbhBruaWuLHjcdUF0EscUfyQ/N6+qAyNz3F8m5KDYY0NHRBJAQEBAQEBAQEBAQCQOpQQfILINaolB2ugxSYjaQU1TcX+449/ZBTirK+CpFXFM4s7Afq+yC/EGGWKJzxZ/U+226DEUDnbuJPuUG1HGGjogmgICAgw42F/4IPIB49HN/Pzn+KRxOz9Q4l8RgOWcVOUsq6HlzBQ4c50Dnsv2lqPiZv/uhfRaO1+KxEfywtZc27r47XU5BAQEBAQEBAQEEoZpKeVs8TrOYbgomnm/TTVNbVU0GI4S65dYPiduD9foVTktxWYh+S5AyDFHRiR4+W5sQfY9k4pcZjGIVmFvFJSYs6QEbhzQXMH1UxGRw73vkeZJHlzibkk3JVlJnLCIEBAQEBAQEBAQEBAQAbG9v4oPVL9lx5wZ+Zzwt8DyDmLEvPx7hLisuUqwyPJkfRRtbPQPN+wp5Wwj/AMuV8/rrX478z1b+lr27UOxhz3uJEbSSuN0qDNJI7QwEn0CBCJ6eqa6oiLWu21XuAUFs0MtOHGih1SSuJc8kbIIPpmwRNa595CbuPqguijuOiC1rA1BlAQEBAQEBAQEBAQEFUrzew6nogoqzJANT+h7j1QKV8baZ9a5urTewQYpq6nxTVTTwgEjYE3uglBUvpnupagl+j7ju5Ha6AS+ok1uFh2CDYjYAEE0BAQEBB/E/Ed5lm8nnIrxW5lo5Syqylkiuq8LNxvXOj8qlbv61EkQ/Felmj8l2KVLlWzRMvFVUT1dVO+pr6h008jy+eZ7rukeTdzie5JJP4r6eOEPnKqtqrKClUQEBAQEBAQEBAQc1lKv0vfh8jtnfNH9e4/69FWqF4cfjTZ2YnMyol1uDtne3b+imORnDVUqzORECAgICAgICAgICAgICAg7d/se3M9U8OOeHO3LFiVY8YdxKyWa6igDtjiWGPMjbD1NNPU3t/wCGPRZviVuJtxV0afh9fGae++b0fit+GfqcLtPULFarYe60D6mhY17nC490Gvhte+uL6apYCdN726j0KBHNVRPdCJbta4huob2QWRxPe7XISSg2WN0hBlAQEBAQEBAQEBAQCQNygg6QWsg1qmQHoguaW+Q2Ktc279gD39kFEX/Zj3U9RvC8/I+3T2KCUFNh1LIamGS57AOvb6IMNYZ5DIR1KDZjiAHRBNAQEBAQEHVL9r35gDwz8NPCOC9BWPZV8SuINDRzxNdYPoqJr66W/qPNiph/6l3+H0bV7PRya2rFnGO/94eZVbrCEBAQEBAQEBAQEBBKKWSCRs0Ly1zTdpHZBhznPcXvcSSbkk9UGEBAQEBAQEBAQEBAQEBAQTgpqipLhTwufpbqdpHQIPobwk+PcnLN4lfBTjGat0NNQ8QKGjxSRrrWoq1xoqi/t5VQ8/gvDU0bdmqHVpZmi9E994ey9/5Opg2OqOpx2JcL+11803mHM/JpFRTEugcfnZe9vcILTJSx3ngDS+QdR3QRgiJOpwQbLWhqDKAgICAgICAgICAgIK5n2QVeTPKzWDa/QHug1KmSSB4E0ZG/fuguxSjfiETJqZwdYbC/UFBbEJY6HRiADnWtb19PxQVU9OOwQbccYaOiCaAgICAgICDz5/bTeIWJ13GrgTwpdIRSYdlbG8XDA7Yyz1VNBcj2bT/1K1vC4jZqlmeIco75/wCnSOtZlCAgICAgICAgICAgICAgICAgICAgICAgICAgICDlMr11RDWfCRw62Sn5rdW+6iYXiODfxCjqMFrWZowXUyajeKgNjNiHMOsOb6G4CpM8F6I2q4jq9tfBnM0nFrgpk/iVJI0TY7lfD8Qk9CZ6aOU/1eV8xVGzXMPoKZzTEv1j4hT0LaNz9Rt/jdQsxBT7XIQbLGaUEkBAQEBAQEBAQEBAQEFNS24KCEL3ywOha8te0fKUGYXSSUhdiUTGjuD6IIGEU0fm0VTZh6MO4P09EEWiacgyOJQbMUYaEFiAgICAgICAg6C/tonALM4zZwU5oKOiklwc4diWVcRqGsOmmqfMZWUzXHoPMYam3r5RWr4ZXEbVLO8QpmaYnvvi6L1rsgQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEGWMfI8RxtLnONgAOqCyqoauhfoqoHMJ6X6H8UzlaKXLZNbHqnd+vZoH03ValnK8NMpZ74h8Q8L4UZUwWpxLGMy4rBhmGUUDC581VUSNijDbdbueP+fZUqmIpmZWtbX5Iw9sXCTh5Fwm4V5X4a0lWXjL+XaHDC+O4DxT08cOr8dF18zVO1VM9X0FMbNMQ/UQwlx1OULNhjNI6IJICAgICAgICAgICAgICCMjbhBq7wTCQDa+/0QSxGN8hYdfyenugjDT7C42QbMcQaOiCaAgICAgICAgIP5nze8pHA7nj5f8AH+WrmGyoMWy1mGnDJ2xv8uopJmHVFVU8liYp43gOY8A2IsQWlzTe3cqtV7VKtVMV04l53+c/7JT4gvBbM9biPKlX4Lxdyt5jn0DWYjDhWMxRX2bNT1DmwyOAsNUUvzWuGN+6ti14haqj9fBlXdDXHGjj331fJeL+CT4t2CVLqWt8PPig9zHFpdSYAKhpI9HRPcCPe66I1enn+6HjOjvR3P01PzMniw/u7uLf8oSqd6seqPdXdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/wCUJU3qx6o9zdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/AChKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv8AlCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o9zdL3T4n6PzMniw/u7uLf8oSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/wAoSpvVj1R7m6XunxP0fmZPFh/d3cW/5QlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/AJQlTerHqj3N0vdPifo/MyeLD+7u4t/yhKm9WPVHubpe6fE/R+Zk8WH93dxb/lCVN6seqPc3S90+J+j8zJ4sP7u7i3/KEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o9zdL3T4n6PzMniw/u7uLf8AKEqb1Y9Ue5ul7p8T9H5mTxYf3d3Fv+UJU3qx6o907pe7ifps4N4Ovis4diDJ5/Dx4saT8ri7KEvyg91E6qxP90e6Y0t2PL4n6c3WeEN4p0dW2iqPDy4qyxTbNcMpSmx9CD/zVd5seqE7rd6f8/T9pwq+zweLhxPxyOHLfJ/jOW2l+maszliVJh1NG09S7zJTIR/wscfQKtWtsRHNaNHdme/+8O4nwbPs7uVOQPOtFzLcz2ZsMznxQooScBo8KjccIy697C180RlaH1VTpLmiZzWNYHO0s1fOs3U6yb0bNPCHfp9NFnjPN2fxRue7W9cTrbLG6R0QSQEBAQEBAQEBAQEBAQEAkNBc42A6lBoDM+X3VPwQxSLzb20XKDakiDhcbg90EfLJtqN7dEFrGW3IQSQEBAQEBAQEBAQEAgHYhBgMY3ZrAPoEGbD0CDB0DrZBjUz9n+iDILD2QZsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BAsPQIFh6BBgtaRaw/gg1qqIEdEE6VwfDYsBczYXQarPPdOZydLj2btb2QbDIXOdrebkoL2Rho6IJICAgICAgICAgICAgICAgoxMkYbUEG36B+/4FB1YcqXG/OtRxawnH6rnPo8Tr8Sx7DcKr8uYrmiqxWgzL5ktQysraVppQMNe58lH8NTxlrG+RIyUkSBwDtKy8ZjgtMaj7/l/NvdBuICAgICAgICAgICAgICAgw91ggoL5HAljb2QVxSullEbXC5QTEwY8sJ6GyC9kgI/wAUEkBAQEBAQEBAQEBAQEBAQEBAQEBBGRmoIKWMfE8uYOoQZji+bU7ckoLmtDQgygICAgICAgICAgICAgICBceoQa2NNLsGq2tO5ppAP/aUHXxyhcUs65G4bZXwml5ouHGc/wAnVWD4dxDytWzUkdfgOJ11TFTTNpqrC2uimBrJ3NYyWE+Y+wNSLlyDsByzVCtwKnqQPvNI/g4j/BBvoCAgICAgICAgICAgICAgqndYHdBrxVT4XE6C5nV1h933QW/CtdVMradwsb6gO9x1QYkoZHSukEos43sR0QZY0xO0OcD9EF7TcIMoCAgICAgICAgICAgICAgICAgICDGlvogyAB0CAgICAgICAgICAgICAgIISSae6Cl1RbayDLZJHi7GkhBHHHFmC1jx1FLIf/xKDrn4EZdyrxN4K8D6XjRymY9g9Dh82AYrkDN2Tq38qQ0R86nqqelrSyNlTTxGQQiUSQvpxoD3TBzGyAOxXLkEdLglPBELNazYD6koN1AQEBAQEBAQEBAQEBAQQfIAOqDVqJwb+iCVXM6iomuhb8zjYm3sg1cLqpoCW6HOj6usPu+6DZqGxiUSU0hBdu4NOyCyCMjc9Sg2ALCyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMONhdBrVEmk/TsUEnCKvg/ROAcP6IJUAkbThkrbOa4gj8UFeONLsFrGgbmlkA/wDaUHXPyz50wfhNl3hPlfJfNNnLEJsVy/lmfNcOL4HWY7l+V+IsY2H/AGqoDKjC56t4kEDDII2uezVAAQCHYplqY1GB00xtuw9PqQg3kBAQEBAQEBAQEBAQYc4NQVSVAHUoKQ51RII2nqgk6HD3v+GdJ8//ABboMukigaKSs3aR8jiNiPf3QYElNDF5dGy1+4H+aBBBbchBssbpCDKAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgE23KCuWUAINeNoqJ9BOwFz9EFFbWNhqwaRga6PZx7O9kG7T11PPEJDI1p7tc7oUE6mMTU8kThs5hBv7hB1j8PMg8cMuccqfgzlHhDgFHguDZ1wypxuXJGEYSMBxySlr/Omr6zTJ59BPTUL8PfFTRtafiZASHxvYUHZZlenfS4BTQSdQw3/EkoN9AQEBAQEBAQEBAQRe/SEGvLMXHSwXPsgxTsiqonN1kPB6+iCGunw/EGRmU/OyztXb0P8ARBCrwiaSq8+CQWc65udwUF9cGvLIybkblBmGADqEGw1gagygICAgICAgICAgICAgIFx6oCAgICAgICAgICAgICAgICAgICAgw54agqlnA7oKmRSVNzqsPVAZTy0U3nl+thFn2G490CXC6aol+JbKQ125DTsUGJhR67Np2uAFr3sgxmTG6PLWX63MOIuc2noaWSeocyF0jgxjS5xDWAucbA7NBJ6AEoOvjDsq8mOWOaGo5msp8Uc50VdVYhV1lbhn9hMQmZPJOKEOYZ5KIziIfBl2jXsZjpLW6mvD744dZpos55Socx4W5zqSspIqike+B8TnRvaHNLmSAOYbEXa4AjoQCg5xAQEBAQEBAQEBAQUVDyBdBGWVtBTebp1Pd0+qDVgkxcF8sNMCH7m4tv7ILy2mxeHRKwskYdx3aUGHRyUpbDDVPItuCeiCyGE31O3KDYa0NCDKAgICAgICAgICAgICDDnaQgqfOB1KCLJjI8NYEEo5ge6C0EEIMoCAgICAgICAgICAgICAgICCEkoA6oNd0xe8Ma61za6DE1HUaj5bha1wSf6IMObJVYSG0zvmtuAevqEGMG+LDXxztdpHTUOh9EEIqcB5DOlzZBssgAG6DUzrjdXlrKGKZiocDrcTmoKCWoiw3DYmPqKtzGFwiia9zWukdbS0Oc0EkXIG6D4WxXnJ5lc1cehlyh4a5hy7kx2csIpMdxbGsGw1lZgU0rqVowqRja5zLVRmhf5zv0zI6ktjikL4XND7rynIZsv00pFi5hv8tu5QcigICAgICAgICAgg+QAINeeQOFgUGWVwjYGyRk26EIIxV9VUVIjihAZf5r77IMVjGmr8xhsQ0XIQWQwm+p3VBsNaGhBlAQEBAQEBAQEBAQEBAQVTvsCgrglhkBp5BYn+qDFNFLT1jmPJLSw6XfiEEGx1LHm8TuuxQXxOcNnAg+6C7qgICAgICAgICAgICAgICAgw82ag1iHTy+WDt3QUYnPSRN8hjLvb1IPRBZBilJWH4V7C0Obb5j19kEaegraGZ3wsjXRnfS8oJ/FTzt0FobfrZBbBCB2QXgACwQCQBc9kHXJzAUuI1/iQ1E8GCVRoXZxy/I7J0dNjJZmdzI4B/aEyRPFEwUFgC2RhLvyeNRDvJQdhOUCTlukuCP0Z6j3KDkkBAQEBAQEBBguA7oIvlACCgudPII2H6lBJsFLI50LJCXt+9Y9EGu4aXFhINjbZBfHM9kWhkYB9UCKE31O3JPVBsNaGhBlAQEBAQEBAQEBAQEBAQRc8NQa08uo2CCNcIKan8ssvI77rh1B9UEsOxDzx5M+zx0P7SDL56uCUxvc1w7G26CcIcfmcbkoLxsLICAgICAgICAgICAgICAgE23KCuV4sgoilbFOXOGx2QZe/DIHGfS0ucb9Lm/8AggOdBiVI97oiNN9JcNx7oK4p6mSPy3vvt1tugvhht2QXAABBlBVXmQUMxh+/5TtNvWxsg6uuF1SzMvEyh4mUfCXiNUUmLZ+y7XcQ8MzPxGx1sFDmWqqKamjpKWke1sdd+TjS09RK+fU1jWNYwhkTGsDs7yuJG4DTNlaQ4NIIJ3+8UG+gICAgICAgw91ggpJkkv5Y6IKA6ad5jjaSR19AgzHHUUlQJpmgsIsS030oLZ6eYR+XQ6Wa3EveTvughJBDAxkDN3DqfVBbFHtZBaGgIMoCAgICAgICAgICAgIBIHVBW+YAdUFLpHyv0R7oIy0FQRqZK243sQgnPTxYlGCXFj2Hcd2n0KCLqampYfL+88m9+4KDMURe7W83Pug2WMDQgygICAgICAgICAgICAgIBIHUoKppQAgqZHLUfMDYeqCM1PJD8x3B7hBWxkLnjzgbd7IL5ZA5vkQMs3vsglBCB2QXgACwQEBBh7mMaXyOAA6knZB8K8Zeanj3l/xD3cu2XqzCJ8NxdlHJgeGsmoQWULanDn1dZKXTio8zyBjMbYwzSDFSuAfqfpD7cyvI+bAaaV7rlzSSfxKDfQEBAQEBBguA6lBXNIAEGo6tdTP1jcd2+qDYklfPRumw5zdbhsbd/wDNBr4PWVNS59PVDVpHUjp2sUCLz43OijmdpDiGi/ZBfDAb6nblBsNbpCDKAgICAgICAgICAgICDDnhqCiWe3QoImCWWISMIJJ6X7IFE10VTJDKRq0gtt3CDUh/KNPiIY7Ubu+b0cPVBs10QNU2Rux072KCUMHcoNhjA3qgkgICAgICAgICAgICAgIISSae6CiSoLeyCqSSWRpLI3O/4W3QWwxtraFjPMIGr5wO/sgwPOq5fJbG6OFmxJFi5AjjGsgG4vsUF7Ih1AQTAA6IMoCAg4vO+GYjjeTsUwfCMySYPV1dBLDSYvDTslfRSuaQyZrJAWPLHEODXAtJFiCLhB1g5ny9juS+c7EsnZq4y1GZqT/4pZUmzDXVpwSixfEMULaI0clJRsoRUPomRshjmfFNEA34s6HBsuoOz/Jotlmk3/7s/wD9ig5NAQEBAQYe7SEFJMj7+WOiDWMplkEQdu42QW1FVQ0LxDJFqJFydIKDErG0lq6itodbzGDoR6j0QWOqodJdTtGp/U2QYp4bbkINgNAQZQEBAQEBAQEBAQEBAQOiDXnkIGyCMraelg11W5cOiDXo8Vp6ePy5Guvr69dkFlVQySTNr8Pl+Y7nfYoLDV1UZ8uWJuq3Vp2QI43Pf5khuSg2WMDQgygICAgICAgICAgICAgIMFwAQa1RJbYFBmOWnr2eS/ZzewP/ACQVVmKOoagU7ab5QOvS/wBEE6mV0bWVtKRZ9tQI2cLbH6oHxM8w02Db9bILYYtIsgu6ICAgICDjc54BT5ryjimWKvE66iixDD5qeSswyqdBUwNewtMkUjfmjkbe7XDdpAI6IOtTPOQqfhHztYRlvBePddjdXWZtwh2AYhjXE+or8VwzDtcDa3DZ8NbHJNKyd7ZgJ3FsLG1ZMjmiLcOyzKcL6fLtLDJa4jN7H3KDkUBAQEGC4DqUFczxYoKaaWXz9DBcHd1+yBW0ji8VtJYvY67mg/et/igxNTUuLNbNHJZw2O249iEEpGRwUwomO1W6/wAboMwQdyEGw1gagygICAgICAgICAgICAgIIvcALXQa07vmBHYoJy/A1IbJUEbdiUGIamhlk+EihuCD+psgrZ5lHM+GA/JfZp7ILI4nPfrkNyUF7WBqCSAgICAgICAgICAgICAghJJp7oKJKgt2sgjDH8US5zrNHUoMChpJ/wBJQ1JbI07Oa6+/ugk3ysRjNNWR6ZYz8wB/qEGZ3MfpghHys6WQWQxWAQXAAdEBAQEBAQRkjbLG6J/RzSD9EHy3k/wtuH3BLidgnEXlp4j4plSlwptRTT5YraaLEaGehqp6aWria+QCpbJIaSG0rpn6C24YQXNcH1Bh9KaOijpXEEsba46ILkBAQYcbC6CiR7i7QwXJQVVPmwi8jTb1HRBnC5WyGRoNnbIKMPhxKkrTEY3OY4/MT0+t0F1bBD8QHRts4j57ILIIO5CDYa0NCDKAgICAgICAgICAgICBcDqUEXyADYoNdz3yP8uMXKBJSTBuoEO9QEFBAcN0GxEaamjvAwlzvVBiKNz3a39Sg2WMDQgygICAgICAgICAgICAgIMOcAEGvUP09EEi1lfTdC09AbdCgpoAQyXDqkaX7/iCLXCCugwyqpKzzC8aADuD95BOeNstW6Rv0ugvhg09kFwFhZBlAQEBAQEBAQEBAQEGH9PxQa0bnNqQAeuxQWNme6sfTOsWhoI2QU1tLFTg1tOCx7f2eh/BBFmI1D4wSGgnuAglANTruNz6lBtsAAugygICAgICAgICAgICAgw42Fwgqe4goKpZH9L90GaAkvlJ7Wt/VBCN76fDBUMcS94Bc5xvuUEWtADDcnUwON/VBsMY30QWsaLXQSQEBAQEBAQEBAQEBAQEGHkjogpe9wvZBrzuJBPoEGcWqZaGKIUx0i52t1sgurIWTU3nOuHsbqa5psQUGtDVVErLSTEoNiBjR2QbDRYBBlAQEBAQEBB//9k="  
  
}
