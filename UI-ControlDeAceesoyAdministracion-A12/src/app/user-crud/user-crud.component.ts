import { Component, OnInit } from '@angular/core';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { isThisTypeNode } from 'typescript';

import { ServicesService } from '../services/services.service';
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import tarjeta from '../../assets/images/pdfimg/card.json';
import {
  startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours,
} from 'date-fns';



@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.css']
})
export class UserCRUDComponent implements OnInit {
  
  //permisos:any = this.route.snapshot.paramMap.get('usr');
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  visibleCancelar = 1;
  strBusqueda = "";
  value = '';
  
  usuario = {
    usuarioId : 0,
    usuarioNombre:  "",
    usuarioApellidoPaterno : "",
    usuarioApellidoMaterno : "",
    usuarioCorreoElectronico : "",
    usuarioPassword : "",
    rolRolIdFk : 0
  }
  
  
  pass = "";
  pass2 = "";
  
  arrayUsuarios:Array<{ 
                        usuarioId : number,
                        permisosNombre: string, 
                        rolesNombre : string , 
                        usuarioApellidoPaterno: string, 
                        usuarioApellidoMaterno: string, 
                        usuarioCorreoElectronico : string, 
                        usuarioNombre: string,
                        usuarioActivo: string,
                        usuarioPassword : string
                        ver: number,
                      isActive: boolean}>=[];                      
                        
  arrayRoles:Array<{ 
  rolesId: number, 
  rolesNombre : string}>=[];                        

  
  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
   
    this.getUsuarios();
    this.getRolesActivos();

  }


  validaCorreo()
  {
    if(this.usuario.usuarioCorreoElectronico !="")
    {
      if(!this.isEmail(this.usuario.usuarioCorreoElectronico))
      {
        alert("El correo no tiene un formato válido!");
      }
    }
    else
    {
      alert("El correo es un campo obligatorio");
    }
    
  } 

  isEmail(email:string)
  {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);  

  }

  getUsuarios(){
    this.httpServices.getUsuarios().subscribe(
      datos => {
        console.log(datos);        
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayUsuarios = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayUsuarios.push({
          usuarioId: jsonDatos[i].usuarioId, 
          permisosNombre: jsonDatos[i].permisosNombre, 
          rolesNombre : jsonDatos[i].rolesNombre , 
          usuarioApellidoPaterno: jsonDatos[i].usuarioApellidoPaterno, 
          usuarioApellidoMaterno: jsonDatos[i].usuarioApellidoMaterno, 
          usuarioCorreoElectronico : jsonDatos[i].usuarioCorreoElectronico, 
          usuarioNombre: jsonDatos[i].usuarioNombre,
          usuarioActivo: jsonDatos[i].usuarioActivo,
          usuarioPassword: jsonDatos[i].usuarioPassword,
          ver: 1,
        isActive: this.calcularBooleano(jsonDatos[i].usuarioActivo)});   
        }
      });
  }
  
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
  
  getRolesActivos(){
    this.httpServices.getRolesActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayRoles = [];
         this.arrayRoles.push({ 
          rolesId: -1, 
          rolesNombre : "Selecciona..." });   
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayRoles.push({ 
          rolesId: jsonDatos[i].rolesId, 
          rolesNombre : jsonDatos[i].rolesNombre });   
        }
      });
  }
  
  
  setNuevoUsuario(){
    
    if(this.usuario.usuarioNombre === ""){
      alert("Ingresa el nombre del usuario");
    }else if(this.usuario.usuarioApellidoPaterno === ""){
      alert("Ingresa el apellido paterno del usuario");
    }else if(this.usuario.usuarioCorreoElectronico === ""){
      alert("Ingresa el correo electrónico del usuario");
    }else if(this.usuario.rolRolIdFk === 0){
      alert("Selecciona un rol para el usuario");
    }else if(this.pass === "" ){
      alert("Ingresa una contraseña para el usuario");
    }else if(this.pass != this.pass2 ){
      alert("las contraseñas no son iguales")
    }else{
      this.usuario.usuarioPassword = this.pass;
      
      this.httpServices.setUsuario(this.usuario).subscribe(
        datos => {
          console.log(datos);
           /*var jsonDatos = JSON.parse(JSON.stringify(datos));
          console.log(JSON.stringify(datos));*/
          if(datos == 1){
            alert("Registro guardado éxito")
            this.getUsuarios();
            this.resetUser();
          }else{
            alert("El correo electrónico ya está siendo usado por otro usuario");
          }
          
           
        });
    }
    
    
   
  }
  
  btnEditar(user: any){
    
    this.visibleCancelar = 0;
    this.usuario.usuarioApellidoMaterno = user.usuarioApellidoMaterno;  
    this.usuario.usuarioApellidoPaterno = user.usuarioApellidoPaterno;  
    this.usuario.usuarioId = user.usuarioId;
    this.usuario.usuarioNombre = user.usuarioNombre;
    this.usuario.usuarioPassword = user.usuarioPassword;
    this.pass = user.usuarioPassword;
    this.pass2 = user.usuarioPassword;
    this.usuario.usuarioCorreoElectronico = user.usuarioCorreoElectronico;
    
    for(var i = 0; i < this.arrayRoles.length ; i++){
      if(this.arrayRoles[i].rolesNombre === user.rolesNombre) {
        this.usuario.rolRolIdFk = this.arrayRoles[i].rolesId;
        break;
      }
    }
     
  }
  
  
  btnCancelar(){
    this.visibleCancelar = 1;
    
this.resetUser();
    
  }
  
  resetUser(){
    this.usuario.usuarioApellidoMaterno = "";  
    this.usuario.usuarioApellidoPaterno = "";  
    this.usuario.usuarioId = 0;
    this.usuario.usuarioNombre = "";
    this.usuario.usuarioPassword = "",
    this.usuario.usuarioCorreoElectronico = "";
    this.usuario.rolRolIdFk = 0;
    this.usuario.usuarioPassword = "";
    this.pass = "";
    this.pass2 = "";
  }
  
  
  
  buscar(event: any){
    console.log("Busco: "+this.strBusqueda);
    if(this.strBusqueda.length >= 2){
      
      
      for(var i = 0; i < this.arrayUsuarios.length ; i++ ){
        
        var nombre = this.arrayUsuarios[i].usuarioNombre +" "
                      +this.arrayUsuarios[i].usuarioApellidoPaterno+" "+
                      this.arrayUsuarios[i].usuarioApellidoMaterno;
        
        if(!nombre.toUpperCase().includes(this.strBusqueda.toUpperCase()) &&
           !this.arrayUsuarios[i].usuarioCorreoElectronico.toUpperCase().includes(this.strBusqueda.toUpperCase())){
            this.arrayUsuarios[i].ver = 0;
          }
      }
      
    }else{
       
      for(var i = 0; i < this.arrayUsuarios.length ; i++ ){

            this.arrayUsuarios[i].ver = 1;
         
      }
    }
  }



  desactivarActivarUsuario(json: any){
    console.log(json);
    var send = {
      "usuarioId" : json.usuarioId,
      "usuarioActivo": "SI" 
    };
    
    if(json.isActive){
      send.usuarioActivo = "SI";
    }else{
      send.usuarioActivo = "NO";
    }
    json.usuarioActivo = send.usuarioActivo;

    console.log(send);    
    this.httpServices.activarDesactivarUsuarios(send).subscribe(
      datos => {
        console.log(datos);
   
      },
      error => {
        console.log("hola desde el error"); 
        console.log(error); 
        if(json.isActive){
          json.usuarioActivo = "NO";
          json.isActive = false;
          
        }else{
          json.usuarioActivo = "SI";
          json.isActive = true;
        }
        
      });
}



public openPDF(json: any):void {
  
  this.value = crypto.AES.encrypt("M-"+json.instructorCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();
  var nombre = json.instructorNombre+" "+json.instructorApellidoPaterno+" "+json.instructorApellidoMaterno;
  var contexto = this;
  
  
  
  setTimeout(function()
  { 
              
        console.log("entre al pdf "+contexto.value);
  
        
       

      let PDF = new jsPDF('p', 'mm', 'letter');
      console.log("entre al pdf");
      
      
      var qrcode = document.getElementById("qrcode")?.innerHTML;
      var parser = new DOMParser();
      var divContenedor;
      console.log("entre al pdf");
      console.log(qrcode);

      if(qrcode != undefined){
        divContenedor = parser.parseFromString(qrcode,"text/html");  
      }
      var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 

      if(codeBase64 != undefined){
        PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
      }
      
      
      //PDF.setDrawColor(0.5);
      
      PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
      PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
      
      
      PDF.setFillColor(150,150,150);
      PDF.rect(53,90,110,7,'F');
      
      PDF.rect(63,33,35,35);
      
      PDF.line(55,80,106,80);
      
      
      PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
      PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
      PDF.setFontSize(6)
      
      PDF.setTextColor(255,255,255);
      PDF.text("Asociación Monarca de Triatlon",72, 93);
      PDF.text("del Estado de México",72, 96);
      
      PDF.text("Centro de Desarrollo del Deporte",129, 93);
      PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
      
      PDF.setTextColor(0,0,0);
      
      
      
      PDF.cell(53,12 , 55, 85," ",0, "center");
      PDF.cell(108,12 , 55, 85," ",0, "center");
      
      
      
      
      
      PDF.setFontSize(8);
      var w = PDF.internal.pageSize.getWidth();
      
      if(nombre.length > 30 ){
        var char = nombre.split("");
        var espacio = 0;
        for(var i = 29 ; i > 0; i--){
          if(char[i] === " "){
            espacio = i;
            break;
          }
        }
        
        var nom1 = nombre.substring(0,espacio);
        var nom2 = nombre.substring(espacio);
                    
        PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
        PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
      
      }else{
        
        PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
      }
      PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
      console.log("entre al pdf");
      PDF.save(nombre+'_qr.pdf');
      
      contexto.value = "";
      console.log("entre al pdf");
      
  
  }, 500);
  }
  


generarQRMaster(){
    var json ={
      instructorCURP : "999999999999999999",
      instructorNombre : "Master",
      instructorApellidoPaterno : "",
      instructorApellidoMaterno : ""
    }
    this.openPDF(json);
    
}

}

