import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporte-alumno-pagos-alumno',
  templateUrl: './reporte-alumno-pagos-alumno.component.html',
  styleUrls: ['./reporte-alumno-pagos-alumno.component.css']
})
export class ReporteAlumnoPagosAlumnoComponent implements OnInit {

  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";
  
  fecha1 = "";
  fecha2 = "";
  
  arrayResultadosReporte: Array<{
    Nombre : string,
    ApellidoPaterno: string,
    ApellidoMaterno: string,
    Importe: string,
    TipoDePago: number,
    FolioDePago: number,
    CuentaBeneficiaria: number,
    ConceptoDePago: string,
    Observaciones: string ,
    FechaDeReciboDePago : number,
    //total: number,
    
  }> =[];
  
  
  
  arrayAlumnosResultados:Array<{
    alumnoId: number,
    alumnoNombre: string, 
    alumnoFechaDeNacimiento: string,
    alumnoCURP: string
    }>=[];   
  
    alumno = {
      alumnoId :0,
      alumnoBase64Image : "",
      alumnoNombre :  "",
      alumnoApellidoPaterno : "",
      alumnoApellidoMaterno : "",
      alumnoCURP : "",
      alumnoFechaDeNacimiento : "",
      alumnoSexo : "",
      alumnoRutaFoto : "",
      
      alumnoTelefonoCasa: "",
      alumnoTelefonoCelular : "",
      alumnoCorreoElectronico: "",
      alumnoRedesSociales: "",
      alumnoPassword: "",
      alumnoCodigoPostal : 0,
      alumnoEstado: "",
      alumnoMunicipio: "",
      alumnoColonia: "",
      alumnoNumero: "",
      alumnoCalle: "",
      
      alumnoAfiliacionMedica : "",
      alumnoAlergias : "",
      alumnoPadecimientos : "",
      alumnoTipoDeSangre : "",
      alumnoEstatura : 0,
      alumnoPeso : 0,
      alumnoActivo : "Si",
      isActive : false,
      alumnoFolio : ""
     }
  
  
  constructor(private route: ActivatedRoute,private httpServices: ServicesService) { }

  ngOnInit(): void {
  }

  
  
  
  
  
  selectedAlumno(){
    console.log("select");
    console.log(this.strBusqueda);
    
    for(var i = 0 ; i < this.arrayAlumnosResultados.length; i++){
      if(this.strBusqueda === this.arrayAlumnosResultados[i].alumnoCURP){
        this.alumno.alumnoId = this.arrayAlumnosResultados[i].alumnoId;
        this.alumno.alumnoCURP = this.strBusqueda;
        this.alumno.alumnoFechaDeNacimiento = this.arrayAlumnosResultados[i].alumnoFechaDeNacimiento;
        
        this.strBusqueda = this.arrayAlumnosResultados[i].alumnoNombre;
        this.arrayAlumnosResultados = [];
        break;
      }
    }
    
    //this.getMensualidadesAlumno();

    this.arrayAlumnosResultados = [];
  
  }
  
  
  
 
  busquedaAlumno(newObj: any){
    console.log(newObj);
  //console.log(this.strBusqueda);
    if(this.strBusqueda.length >= 3){
      this.httpServices.searchAlumnos(this.strBusqueda).subscribe(
        datos => {
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
           this.arrayAlumnosResultados = [];
           for(var i = 0 ; i < jsonDatos.length; i++ ){
              this.arrayAlumnosResultados.push({ 
              alumnoId : jsonDatos[i].alumnoId,
              alumnoNombre: jsonDatos[i].nombre, 
              alumnoFechaDeNacimiento :  jsonDatos[i].alumnoFechaDeNacimiento.toString().split("T")[0],
              alumnoCURP: jsonDatos[i].alumnoCURP});   
            } 
        });
    }else{
      this.arrayAlumnosResultados = [];
      this.alumno.alumnoId = 0;
      this.alumno.alumnoCURP = "";
      this.alumno.alumnoFechaDeNacimiento = "";
      this.arrayResultadosReporte = [];
   
    }
   
  }
 
  
  
  
  getPagosAlumno(){
    
    if(this.alumno.alumnoId == 0){
      alert("Busca un alumno para generar un reporte");  
      return;
    }
    
    
    if((this.fecha1 == "" && this.fecha2 != "") || (this.fecha1 != "" && this.fecha2 == "")){
      alert("Ingresa ambas fechas para establecer un rango");
    return;
    }  
    
    
    
    if(this.fecha1 != "" && this.fecha2 != ""){
   
      
      var f1 = new Date(this.fecha1);
      var f2 = new Date(this.fecha2);
      if(f1.getTime() > f2.getTime()){
        alert("la fecha de inicio no puede ser mayor a la de fin");
        return;
      }
    }
  
    var json = {
      "alumnoIdFk": this.alumno.alumnoId,
      "fechaEntrada": this.fecha1,
      "fechaSalida": this.fecha2 
    }
    
    
    console.log(json);
    this.httpServices.getPagosAlumno(json).subscribe(
      datos => {
        console.log("ya te traje el alumno");
        console.log(datos);
        this.arrayResultadosReporte = [];
        var json = JSON.parse(JSON.stringify(datos));
        for(var i = 0; i < json.length ; i++){
          this.arrayResultadosReporte.push({
              Nombre : json[i].alumnoNombre,
              ApellidoPaterno: json[i].alumnoApellidoPaterno,
              ApellidoMaterno: json[i].alumnoApellidoMaterno,
              Importe: json[i].parcialidadImporte,
              TipoDePago: json[i].TipoDePago,
              FolioDePago: json[i].folioDePago,
              CuentaBeneficiaria: json[i].cuentaBeneficiaria,
              ConceptoDePago: json[i].conceptoDePago,
              Observaciones: json[i].observaciones,
              FechaDeReciboDePago : json[i].fechaDeReciboDePago              
            }
          );
        }

      });
  }

  
  
  
  
  formatDate(date: string){
    if(date == null || date == undefined){
      return "";
    }
    if(date.split("T").length != 2){
      return ""
    }else{
      return date.split("T")[0];
    }
  }
  
  
  
  downloadReporte(){
    if(this.arrayResultadosReporte.length > 0){
      this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_pagos_alumno");
    }else{
      console.log("El reporte esta vacio");
    }
  }
  
}
