import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAlumnoPagosAlumnoComponent } from './reporte-alumno-pagos-alumno.component';

describe('ReporteAlumnoPagosAlumnoComponent', () => {
  let component: ReporteAlumnoPagosAlumnoComponent;
  let fixture: ComponentFixture<ReporteAlumnoPagosAlumnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAlumnoPagosAlumnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAlumnoPagosAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
