import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporte-financiero',
  templateUrl: './reporte-financiero.component.html',
  styleUrls: ['./reporte-financiero.component.css']
})
export class ReporteFinancieroComponent implements OnInit {

  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";
  todaysdate:any;

  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];

  ReporteFinancieroInf : any =[];

  
  arrayResultadosReporte: Array<{
    nombre : string,
    disciplinaNombre: string,
    plan: string,      
  }> =[];
    
  disciplinaId = 0;
  fechaInicio: any;
  fechaFin:any

  constructor(private route: ActivatedRoute,private httpServices: ServicesService) 
  { 
    this.getDisciplinas();

  }

  ngOnInit(): void {
  }

  Buscar()
  {
    if(this.disciplinaId==0)
    {
      return alert("Debes seleccionar una disciplina");
    }else
    if(this.fechaInicio==undefined || this.fechaInicio=="")
    {
      return alert("Debes seleccionar una fecha de inicio");
    }else
    if(this.fechaFin==undefined || this.fechaFin=="")
    {
      alert("Debes seleccionar una fecha de fin");
    }else
    if(this.fechaInicio>this.fechaFin)
    {
      alert("La fecha de inicio no debe ser mayor a la de fin")      ;
    }
    else
    {
      this.getReporteFinanciero()
    }
    
  }

  downloadReporte(){
    if(this.ReporteFinancieroInf.length > 0){
      this.httpServices.exportAsExcelFile(this.ReporteFinancieroInf, "reporte_disciplinas_alumno");
    }else{
      console.log("El reporte esta vacio");
    }
    // if(this.arrayResultadosReporte.length > 0){
    //   this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_disciplinas_alumno");
    // }else{
    //   console.log("El reporte esta vacio");
    // }
  }
  
  getReporteFinanciero(){
    var json = {
      "dId": Number(this.disciplinaId),
      "fi": this.fechaInicio,
      "ff": this.fechaFin
    }
    console.log(json);
    this.httpServices.getReporteFinanciero(json).subscribe(
      datos => {        
        console.log(datos);
        this.arrayResultadosReporte = [];
        this.ReporteFinancieroInf = datos;
        console.log("ReporteFinancieroInfo");
        console.log(this.ReporteFinancieroInf);
        // var json = JSON.parse(JSON.stringify(datos));
        // for(var i = 0; i < json.length ; i++){
        //   this.arrayResultadosReporte.push({
        //         nombre : json[i].alumnoNombre+" "+json[i].alumnoApellidoPaterno+" "+json[i].alumnoApellidoMaterno,
        //         disciplinaNombre : json[i].disciplinaNombre,
        //         plan: json[i].planDescripcion
        //     }
        //   );
        // }

      });
    // this.httpServices.getAlumnosByDisciplina(json).subscribe(
    //   datos => {        
    //     console.log(datos);
    //     this.arrayResultadosReporte = [];
    //     var json = JSON.parse(JSON.stringify(datos));
    //     for(var i = 0; i < json.length ; i++){
    //       this.arrayResultadosReporte.push({
    //             nombre : json[i].alumnoNombre+" "+json[i].alumnoApellidoPaterno+" "+json[i].alumnoApellidoMaterno,
    //             disciplinaNombre : json[i].disciplinaNombre,
    //             plan: json[i].planDescripcion
    //         }
    //       );
    //     }

    //   });
  }
    
  getDisciplinas(){
    this.httpServices.getDisciplinasAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });
        }
        
      });
  }

}
