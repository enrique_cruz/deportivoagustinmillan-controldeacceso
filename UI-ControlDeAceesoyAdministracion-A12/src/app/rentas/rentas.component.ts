import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ServicesService } from '../services/services.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';



@Component({
  selector: 'app-rentas',
  templateUrl: './rentas.component.html',
  styleUrls: ['./rentas.component.css']
})
export class RentasComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');  
  usr=this.route.snapshot.queryParamMap.get('usr');

  message: string = ""; // <--- Nuevo atributo
  parentMessage = "0";
  parentMessage1 = "0";  

  isVisibleRentas : boolean = true;
  
  //  EquipoInfo: any = [];
  
  EquipoInfo: Array<{
    equipoId: number;    
    equipoRepresentante: string;
    equipoNombre: string;    
    equipoRepresentanteTelefono: string;
    equipoActivo: string;
    isActive:boolean
  }>= [];


  @ViewChild('dialogRef')
  dialogRef!: TemplateRef<any>;

  myFooList = ['Some Item', 'Item Second', 'Other In Row', 'What to write', 'Blah To Do'];  

  constructor(private route: ActivatedRoute, private httpServices: ServicesService, public dialog: MatDialog, private router: Router) 
  { 
    this.cargarListadeEquipos();
  }

               
  ngOnInit(): void {
    

    //this.openTempDialog();
  }

buscarEquipo(e:any){
  console.log(e.target.value);

  if(e.target.value.length>=1)
  {

    this.httpServices.getEquipoSearchBy(e.target.value).toPromise()
    .then
    (
      data => {
        
        this.EquipoInfo=[];
        var json = JSON.parse(JSON.stringify(data));

        for(var i = 0 ; i < json.length; i++)
        {
          this.EquipoInfo.push(
            {
              equipoId: json[i].equipoId,              
              equipoRepresentante: json[i].equipoRepresentante,
              equipoNombre: json[i].equipoNombre,    
              equipoRepresentanteTelefono: json[i].equipoRepresentanteTelefono,
              equipoActivo: json[i].equipoActivo,
              isActive: this.getBooleando(json[i].equipoActivo)
            }
          );
        }

      }
    )
    .catch
    (
      error => {
        alert("Hubo un error al consultar la base de datos. Intenta mas tarde o contacta a tu administrador de sistemas: " + JSON.stringify(error))
      }
    );
  }
  else if (e.target.value.length ==0)
  {
    this.cargarListadeEquipos();
  }
}


getBooleando(activo: string){
  if(activo == "SI"){
    return true;
  }else{
    return false;
  }
}

cargarListadeEquipos(){
  this.httpServices.getEquiposAll().subscribe(
    datos=>{
      console.log("Se cargan los equipos inicialmente");
      console.log(datos);
      this.EquipoInfo = [];
      var json = JSON.parse(JSON.stringify(datos));
      for(var i = 0 ; i < json.length; i++){
        this.EquipoInfo.push(
          {
            equipoId: json[i].equipoId,              
            equipoRepresentante: json[i].equipoRepresentante,
            equipoNombre: json[i].equipoNombre,    
            equipoRepresentanteTelefono: json[i].equipoRepresentanteTelefono,
            equipoActivo: json[i].equipoActivo,
            isActive: this.getBooleando(json[i].equipoActivo)
          }
        );
      }
    },
    error =>{
      alert("Hubo un error al cargar los datos de los quipos. Intenta de nuevo")
    }
  );
}



  openTempDialog() {
    const myTempDialog = this.dialog.open(this.dialogRef, { data: this.myFooList });
    myTempDialog.afterClosed().subscribe((res) => {

      // Data back from dialog
      console.log({ res });
    });
  }

  close(){}
  save(){}

  btnEditar(e:any)
  {

    console.log("Desde editar: "+e.equipoId)
    console.log(e);

    this.isVisibleRentas = false;

    this.parentMessage1 = e.equipoId+"";
  }


  activarDesactivar(e:any)
  {
    if(e.isActive){
      console.log("desactivo");
      this.activarDesactivar2(e);
      
    }else{
      if(confirm("¿Está seguro que desea desactivar a "+ e.equipoNombre + "?")) {
        console.log("Desactivar aqui");
        this.activarDesactivar2(e);
      }
    }
  }


  pagar(e:any){

    console.log("pago");
    console.log(e);

    this.isVisibleRentas=false;    

    //this.router.navigate(['/RentasPago', e.equipoId]);
    this.router.navigate(['RentasPago'], {queryParams: {usr:this.usr, id:e.equipoId}});
  }



receiveMessage(event: string) {
  console.log("recibi el mensaje "+event);
  this.message = event;
  this.cargarListadeEquipos();
  if(event === "up"){
    this.parentMessage1 = "0";
    this.parentMessage="0";
    this.isVisibleRentas=true;
  }
  if(event === "add"){
    this.parentMessage = "0";
  }
}

nuevoEquipo(){
  this.parentMessage = "1";
  this.isVisibleRentas = false;
}

activarDesactivar2(equipo : any){
  var send = {
    "equipoId" : equipo.equipoId,
    "equipoActivo": "SI" 
  };
  if(equipo.isActive){
    send.equipoActivo = "SI";
  }else{
    send.equipoActivo = "NO";
  }
  equipo.equipoActivo = send.equipoActivo;
  
  console.log(send);
  this.httpServices.activarDesactivarEquipo(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){
        if(equipo.isActive){
          equipo.equipoActivo = "NO";
          equipo.isActive = false;
          
        }else{
          equipo.equipoActivo = "SI";
          equipo.isActive = true;
        }
        alert("error");
      }
      
      
      
    },
    error => {
      console.log("hola desde el error"); 
      console.log(error); 
      if(equipo.isActive){
        equipo.equipoActivo = "NO";
        equipo.isActive = false;
        
      }else{
        equipo.equipoActivo = "SI";
        equipo.isActive = true;
      }
      
    });
  }


}
