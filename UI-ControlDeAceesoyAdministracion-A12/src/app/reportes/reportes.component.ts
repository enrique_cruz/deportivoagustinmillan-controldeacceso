import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
  
  idIntructorFiltro=0;
  idDisciplinaFiltro=0;
  
  showTD = 0;
  
  permisos : any = this.route.snapshot.queryParamMap.get('usr');

  arrayInstructores:Array<{ 
    instructorId: 0,
    instructorNombre: string,
    instructorApellidoPaterno: string,
    instructorApellidoMaterno: string,
    instructorCURP: string,
    disciplinaId: 0,
    disciplinaNombre: string,
    horarioId: 0,
    horarioNombre: string,
    horarioInicio: string,
    horarioFin: string,
    horarioDias: string,
    horarioFechaDeRegistro: string,
    HorasPorClaseDia: number,
    TotalHorasPorClase: number,
    TotalHoras: number}>=[];
    
    
    arrayInstructoresShow:Array<{ 
      instructorId: 0,
      instructorNombre: string,
      instructorApellidoPaterno: string,
      instructorApellidoMaterno: string,
      instructorCURP: string,
      disciplinaId: 0,
      disciplinaNombre: string,
      horarioId: 0,
      horarioNombre: string,
      horarioInicio: string,
      horarioFin: string,
      horarioDias: string,
      horarioFechaDeRegistro: string,
      HorasPorClaseDia: number,
      TotalHorasPorClase: number,
      TotalHoras: number}>=[];

  
      arrayInstructoresActivos: Array<{
        instructorId: number;    
        instructorNombre: string;
        instructorApellidoPaterno: string;
        instructorApellidoMaterno: string;
        }>= [];
        

      arrayDisciplinasActivas: Array<{
          disciplinaId: number;    
          disciplinaNombre: string;
        }>= [];
  
  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllinstructores();
    this.getDisciplinas();
    this.getInstructores();
  }

  getAllinstructores(){
    this.httpServices.getInstructoresAllData().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayInstructores = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
           this.arrayInstructores.push({ 
            instructorId: jsonDatos[i].instructorId,
            instructorNombre: jsonDatos[i].instructorNombre,
            instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno,
            instructorApellidoMaterno: jsonDatos[i].instructorApellidoMaterno,
            instructorCURP: jsonDatos[i].instructorCURP,
            disciplinaId: jsonDatos[i].disciplinaId,
            disciplinaNombre: jsonDatos[i].disciplinaNombre,
            horarioId: jsonDatos[i].horarioId,
            horarioNombre: jsonDatos[i].horarioNombre,
            horarioInicio: jsonDatos[i].horarioInicio,
            horarioFin: jsonDatos[i].horarioFin,
            horarioDias: jsonDatos[i].horarioDias,
            horarioFechaDeRegistro: jsonDatos[i].horarioFechaDeRegistro.split("T")[0],
            HorasPorClaseDia: jsonDatos[i].HorasPorClaseDia,
            TotalHorasPorClase: jsonDatos[i].TotalHorasPorClase,
            TotalHoras: jsonDatos[i].TotalHoras});   
         
            if(!this.insertShowAll(jsonDatos[i])){
              this.arrayInstructoresShow.push({ 
                instructorId: jsonDatos[i].instructorId,
                instructorNombre: jsonDatos[i].instructorNombre,
                instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno,
                instructorApellidoMaterno: jsonDatos[i].instructorApellidoMaterno,
                instructorCURP: jsonDatos[i].instructorCURP,
                disciplinaId: jsonDatos[i].disciplinaId,
                disciplinaNombre: jsonDatos[i].disciplinaNombre,
                horarioId: jsonDatos[i].horarioId,
                horarioNombre: jsonDatos[i].horarioNombre,
                horarioInicio: jsonDatos[i].horarioInicio,
                horarioFin: jsonDatos[i].horarioFin,
                horarioDias: jsonDatos[i].horarioDias,
                horarioFechaDeRegistro: jsonDatos[i].horarioFechaDeRegistro.split("T")[0],
                HorasPorClaseDia: jsonDatos[i].HorasPorClaseDia,
                TotalHorasPorClase: jsonDatos[i].TotalHorasPorClase,
                TotalHoras: jsonDatos[i].TotalHoras});
            }
          }
      });
  }
  
  
  insertShowAll(json: any){
    for(var i = 0 ; i < this.arrayInstructoresShow.length; i++){
      if(json.instructorId == this.arrayInstructoresShow[i].instructorId){
        return true;
      }
    }
    return false;
  }
  
  
 
 
  
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinasActivas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinasActivas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      });
  }
  
  getInstructores(){
    this.httpServices.getInstructoresActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayInstructoresActivos = [];

         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayInstructoresActivos.push({ 
          instructorId: jsonDatos[i].instructorId, 
          instructorNombre : jsonDatos[i].instructorNombre,
          instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno,
          instructorApellidoMaterno : jsonDatos[i].instructorApellidoMaterno });   
        }
      });
  }
  
  
  downloadReporte(){
    if(this.idDisciplinaFiltro != 0 && this.idIntructorFiltro != 0){
      var array = [];
      for(var i = 0 ; i < this.arrayInstructoresShow.length; i++){
      array.push(
            {
             "Nombre" : this.arrayInstructoresShow[i].instructorNombre+" "+this.arrayInstructoresShow[i].instructorApellidoPaterno+" "+this.arrayInstructoresShow[i].instructorApellidoMaterno,
            "Disciplina" : this.arrayInstructoresShow[i].disciplinaNombre,
            "Horario" : this.arrayInstructoresShow[i].horarioNombre,
            "Día" : this.arrayInstructoresShow[i].horarioDias,
            "Hora_Inicio" : this.arrayInstructoresShow[i].horarioInicio,
            "Hora_Fin" : this.arrayInstructoresShow[i].horarioFin,
            "Horas_clase_dia" :  this.arrayInstructoresShow[i].HorasPorClaseDia,
            "Horas_clase_semanal" :  this.arrayInstructoresShow[i].TotalHorasPorClase,
             "Horas_Semanales" : this.arrayInstructoresShow[i].TotalHoras 
            }
          );
      
      }
      this.httpServices.exportAsExcelFile(array, "reporte");
    
    }else{
      if(this.idDisciplinaFiltro != 0){
        var array = [];
        for(var i = 0 ; i < this.arrayInstructoresShow.length; i++){
        
          array.push(
                {
                 "Nombre" : this.arrayInstructoresShow[i].instructorNombre+" "+this.arrayInstructoresShow[i].instructorApellidoPaterno+" "+this.arrayInstructoresShow[i].instructorApellidoMaterno,
                "Disciplina" : this.arrayInstructoresShow[i].disciplinaNombre,
                "Horario" : this.arrayInstructoresShow[i].horarioNombre,
                "Día" : this.arrayInstructoresShow[i].horarioDias,
                "Hora_Inicio" : this.arrayInstructoresShow[i].horarioInicio,
                "Hora_Fin" : this.arrayInstructoresShow[i].horarioFin,
                "Horas_clase_dia" :  this.arrayInstructoresShow[i].HorasPorClaseDia,
                "Horas_clase_semanal" :  this.arrayInstructoresShow[i].TotalHorasPorClase,
                 "Horas_Semanales" : this.arrayInstructoresShow[i].TotalHoras 
                }
              );
          
          }
          this.httpServices.exportAsExcelFile(array, "reporte");
      }else if(this.idIntructorFiltro != 0){
        var array = [];
        for(var i = 0 ; i < this.arrayInstructoresShow.length; i++){
        
          array.push(
                {
                 "Nombre" : this.arrayInstructoresShow[i].instructorNombre+" "+this.arrayInstructoresShow[i].instructorApellidoPaterno+" "+this.arrayInstructoresShow[i].instructorApellidoMaterno,
                "Disciplina" : this.arrayInstructoresShow[i].disciplinaNombre,
                "Horario" : this.arrayInstructoresShow[i].horarioNombre,
                "Día" : this.arrayInstructoresShow[i].horarioDias,
                "Hora_Inicio" : this.arrayInstructoresShow[i].horarioInicio,
                "Hora_Fin" : this.arrayInstructoresShow[i].horarioFin,
                "Horas_clase_dia" :  this.arrayInstructoresShow[i].HorasPorClaseDia,
                "Horas_clase_semanal" :  this.arrayInstructoresShow[i].TotalHorasPorClase,
                 "Horas_Semanales" : this.arrayInstructoresShow[i].TotalHoras 
                }
              );
          
          }
          this.httpServices.exportAsExcelFile(array, "reporte");
      }else{
        var array = [];
        for(var i = 0 ; i < this.arrayInstructoresShow.length; i++){
          array.push(
            {
             "Nombre" : this.arrayInstructoresShow[i].instructorNombre+" "+this.arrayInstructoresShow[i].instructorApellidoPaterno+" "+this.arrayInstructoresShow[i].instructorApellidoMaterno,
             "Horas_Semanales" : this.arrayInstructoresShow[i].TotalHoras 
            }
          );
        }
        this.httpServices.exportAsExcelFile(array, "reporte");
      }
    }
    
    

  }
 
  

cambioFiltros(){
  this.arrayInstructoresShow = [];
  
 //console.log(this.arrayInstructores.map(instructor=> instructor.horarioNombre == "Hugo"));
  if(this.idDisciplinaFiltro != 0 && this.idIntructorFiltro != 0){
    this.showTD = 1;
    for(var i = 0 ; i < this.arrayInstructores.length; i++){
      if( this.arrayInstructores[i].disciplinaId == this.idDisciplinaFiltro && this.arrayInstructores[i].instructorId == this.idIntructorFiltro ){
        this.arrayInstructoresShow.push({ 
          instructorId: this.arrayInstructores[i].instructorId,
          instructorNombre: this.arrayInstructores[i].instructorNombre,
          instructorApellidoPaterno: this.arrayInstructores[i].instructorApellidoPaterno,
          instructorApellidoMaterno: this.arrayInstructores[i].instructorApellidoMaterno,
          instructorCURP: this.arrayInstructores[i].instructorCURP,
          disciplinaId: this.arrayInstructores[i].disciplinaId,
          disciplinaNombre: this.arrayInstructores[i].disciplinaNombre,
          horarioId: this.arrayInstructores[i].horarioId,
          horarioNombre: this.arrayInstructores[i].horarioNombre,
          horarioInicio: this.arrayInstructores[i].horarioInicio,
          horarioFin: this.arrayInstructores[i].horarioFin,
          horarioDias: this.arrayInstructores[i].horarioDias,
          horarioFechaDeRegistro: this.arrayInstructores[i].horarioFechaDeRegistro.split("T")[0],
          HorasPorClaseDia: this.arrayInstructores[i].HorasPorClaseDia,
          TotalHorasPorClase: this.arrayInstructores[i].TotalHorasPorClase,
          TotalHoras: this.arrayInstructores[i].TotalHoras});
      }
    } 
  }else{
    
    if(this.idDisciplinaFiltro != 0){
      this.showTD = 1;
      for(var i = 0 ; i < this.arrayInstructores.length; i++){
        if( this.arrayInstructores[i].disciplinaId == this.idDisciplinaFiltro ){
          this.arrayInstructoresShow.push({ 
            instructorId: this.arrayInstructores[i].instructorId,
            instructorNombre: this.arrayInstructores[i].instructorNombre,
            instructorApellidoPaterno: this.arrayInstructores[i].instructorApellidoPaterno,
            instructorApellidoMaterno: this.arrayInstructores[i].instructorApellidoMaterno,
            instructorCURP: this.arrayInstructores[i].instructorCURP,
            disciplinaId: this.arrayInstructores[i].disciplinaId,
            disciplinaNombre: this.arrayInstructores[i].disciplinaNombre,
            horarioId: this.arrayInstructores[i].horarioId,
            horarioNombre: this.arrayInstructores[i].horarioNombre,
            horarioInicio: this.arrayInstructores[i].horarioInicio,
            horarioFin: this.arrayInstructores[i].horarioFin,
            horarioDias: this.arrayInstructores[i].horarioDias,
            horarioFechaDeRegistro: this.arrayInstructores[i].horarioFechaDeRegistro.split("T")[0],
            HorasPorClaseDia: this.arrayInstructores[i].HorasPorClaseDia,
            TotalHorasPorClase: this.arrayInstructores[i].TotalHorasPorClase,
            TotalHoras: this.arrayInstructores[i].TotalHoras});
        }
      } 
      
      
    }else if(this.idIntructorFiltro != 0){
      this.showTD = 1;
      for(var i = 0 ; i < this.arrayInstructores.length; i++){
        if( this.arrayInstructores[i].instructorId == this.idIntructorFiltro ){
          this.arrayInstructoresShow.push({ 
            instructorId: this.arrayInstructores[i].instructorId,
            instructorNombre: this.arrayInstructores[i].instructorNombre,
            instructorApellidoPaterno: this.arrayInstructores[i].instructorApellidoPaterno,
            instructorApellidoMaterno: this.arrayInstructores[i].instructorApellidoMaterno,
            instructorCURP: this.arrayInstructores[i].instructorCURP,
            disciplinaId: this.arrayInstructores[i].disciplinaId,
            disciplinaNombre: this.arrayInstructores[i].disciplinaNombre,
            horarioId: this.arrayInstructores[i].horarioId,
            horarioNombre: this.arrayInstructores[i].horarioNombre,
            horarioInicio: this.arrayInstructores[i].horarioInicio,
            horarioFin: this.arrayInstructores[i].horarioFin,
            horarioDias: this.arrayInstructores[i].horarioDias,
            horarioFechaDeRegistro: this.arrayInstructores[i].horarioFechaDeRegistro.split("T")[0],
            HorasPorClaseDia: this.arrayInstructores[i].HorasPorClaseDia,
            TotalHorasPorClase: this.arrayInstructores[i].TotalHorasPorClase,
            TotalHoras: this.arrayInstructores[i].TotalHoras});
        }
      } 
    }else{
      this.showTD = 0;
      this.getAllinstructores();
    }
  }
}
  
  
}
