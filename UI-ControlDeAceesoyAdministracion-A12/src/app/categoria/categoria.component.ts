import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  //permisos:any = this.route.snapshot.paramMap.get('usr');
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
strBusqueda = "";
  
  
  
  visibleCancelar = 1;
  
  categoria = {
    categoriaId : 0,
    categoriaNombre : "",
    categoriaEdadRango1 : 0,
    categoriaEdadRango2 : 0,
    isActive : true,
    categoriaActivo : "",
    categoriaTipo : ""
  }

  
  arrayCategorias:Array<{ 
 
    categoriaId: number, 
    categoriaNombre : "",
    categoriaEdadRango1 : number,
    categoriaEdadRango2 : number,
    categoriaActivo : string,
    categoriaTipo: string,
    isActive: boolean }>=[];  
    
    arrayCategoriasShow:Array<{ 
 
      categoriaId: number, 
      categoriaNombre : "",
      categoriaEdadRango1 : number,
      categoriaEdadRango2 : number,
      categoriaActivo : string,
      categoriaTipo: string,
      isActive: boolean }>=[];   
    

  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getCategorias();
  }

  getCategorias(){
    this.httpServices.getCategoriasAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayCategorias = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayCategorias.push({ 
          categoriaId: jsonDatos[i].categoriaId, 
          categoriaNombre : jsonDatos[i].categoriaNombre ,
          categoriaActivo : jsonDatos[i].categoriaActivo,
          categoriaEdadRango1 : jsonDatos[i].categoriaEdadRango1,
          categoriaEdadRango2 : jsonDatos[i].categoriaEdadRango2,
          categoriaTipo: jsonDatos[i].categoriaTipo,
          isActive : this.calcularBooleano(jsonDatos[i].categoriaActivo)});
        }
        this.arrayCategoriasShow = this.arrayCategorias;
        
      });
  }
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
 
  setCategoria(){
    if(this.categoria.categoriaNombre == "")
    {
      alert("Ingresa un nombre de categoria.");
    }
    else if(this.categoria.categoriaEdadRango1 == 0 && this.categoria.categoriaEdadRango2 == 0 )
    {
      alert("Ingresa un rango de edad.");
    }
    else if(this.categoria.categoriaTipo == "")
    {
      alert("Selecciona un tipo.");
    }else if(this.categoria.categoriaEdadRango1 > this.categoria.categoriaEdadRango2)
    {
      alert("El rango de edad 1 no puede ser mayor al rango de edad 2.")
    }
    else {
     if(this.visibleCancelar == 1){
      this.httpServices.setCategoria(this.categoria).subscribe(
        datos => {
          console.log(datos);
     
          if(datos == 1){
            alert("Registro guardado con exito!");
            this.reset();
            this.getCategorias();
          }else{
            alert("El nombre de la categoría ya está en uso.");
          }
        });
     } else{
       console.log("actualizo categoria");
       this.updateCategoria()
     }
     
    }
  }

  reset()
  {
    this.categoria.isActive = false;
    this.categoria.categoriaActivo = "";
    this.categoria.categoriaEdadRango1 = 0;
    this.categoria.categoriaEdadRango2 = 0;
    this.categoria.categoriaNombre = "";
    this.categoria.categoriaId =0;
    this.categoria.categoriaTipo = "";     
  }
  
  
  updateCategoria(){
        this.httpServices.updateCategoria(this.categoria).subscribe(
      datos => {
        console.log(datos);
        if(datos == 1){
          alert("Datos actualizados con éxito.")
          this.getCategorias();
          this.btnCancelar();
        }else{
          alert("El nombre de la categoría ya está en uso.");
        }
      });
  }
  
  
  btnEditar(json : any){
    this.categoria.isActive = json.isActive;
    this.categoria.categoriaActivo = json.categoriaActivo;
    this.categoria.categoriaEdadRango1 = json.categoriaEdadRango1;
    this.categoria.categoriaEdadRango2 = json.categoriaEdadRango2;
    this.categoria.categoriaNombre = json.categoriaNombre;
    this.categoria.categoriaId = json.categoriaId;
    this.categoria.categoriaTipo = json.categoriaTipo;
    this.visibleCancelar = 0; 
  }
  
    
  btnCancelar(){
    this.categoria.isActive = true;
    this.categoria.categoriaActivo = "";
    this.categoria.categoriaEdadRango1 = 0;
    this.categoria.categoriaEdadRango2 = 0;
    this.categoria.categoriaNombre = "";
    this.categoria.categoriaId = 0;
    this.categoria.categoriaTipo = "";
    this.visibleCancelar = 1; 
  }
  
  
  
  activarDesactivar(categoria : any){
    var send = {
      "categoriaId" : categoria.categoriaId,
      "categoriaActivo": "SI" 
    };
    if(categoria.isActive){
      send.categoriaActivo = "SI";
    }else{
      send.categoriaActivo = "NO";
    }
    categoria.categoriaActivo = send.categoriaActivo;
    
    console.log(send);
    this.httpServices.activeInactiveCategoria(send).subscribe(
      datos => {
        console.log(datos);
        if(datos==0){
          if(categoria.isActive){
            categoria.categoriaActivo = "NO";
            categoria.isActive = false;
            
          }else{
            categoria.categoriaActivo = "SI";
            categoria.isActive = true;
          }
          alert("error");
        }        
      },
      error => {
        console.log("hola desde el error"); 
        console.log(error); 
        if(categoria.isActive){
          categoria.categoriaActivo = "NO";
          categoria.isActive = false;
          
        }else{
          categoria.categoriaActivo = "SI";
          categoria.isActive = true;
        }
        
      });
    
  }
  
  
  buscarCategoria(){
    this.arrayCategoriasShow = [];
    if(this.strBusqueda.length >= 2){
      for(var i = 0 ; i < this.arrayCategorias.length ; i++){
       if(this.arrayCategorias[i].categoriaNombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
        this.arrayCategoriasShow.push({
          categoriaId: this.arrayCategorias[i].categoriaId, 
          categoriaNombre : this.arrayCategorias[i].categoriaNombre ,
          categoriaActivo : this.arrayCategorias[i].categoriaActivo,
          categoriaEdadRango1 : this.arrayCategorias[i].categoriaEdadRango1,
          categoriaEdadRango2 : this.arrayCategorias[i].categoriaEdadRango2,
          categoriaTipo: this.arrayCategorias[i].categoriaTipo,
          isActive : this.calcularBooleano(this.arrayCategorias[i].categoriaActivo)
        }
        );
       }
      }
    }else{
      this.arrayCategoriasShow = this.arrayCategorias;
    }
    
  }
}
