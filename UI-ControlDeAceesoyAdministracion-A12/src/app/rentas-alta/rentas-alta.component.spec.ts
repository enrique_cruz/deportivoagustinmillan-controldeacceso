import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentasAltaComponent } from './rentas-alta.component';

describe('RentasAltaComponent', () => {
  let component: RentasAltaComponent;
  let fixture: ComponentFixture<RentasAltaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentasAltaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentasAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
