import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
//import { CalendarEvent, CalendarView } from 'angular-calendar';
import { result } from 'lodash';
import { Subject } from 'rxjs';
import { ServicesService } from '../services/services.service';
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import tarjeta from '../../assets/images/pdfimg/card.json';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours,} from 'date-fns';
import { CalendarEvent, CalendarEventAction,CalendarEventTimesChangedEvent, CalendarView,} from 'angular-calendar';

import { Router } from '@angular/router';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  }
};



@Component({
  selector: 'app-rentas-alta',
  templateUrl: './rentas-alta.component.html',
  styleUrls: ['./rentas-alta.component.css']
})
export class RentasAltaComponent implements OnInit {
 
 value = "";
  @Input() childMessage: string = "";
  
  
  @Output() messageEvent = new EventEmitter<string>();
  message: string = "up"; // <--- Mensaje a enviar
  
  fechaDomingo = "";
  fechaLunes = "";
  fechaMartes = "";
  fechaMiercoles = "";
  fechaJueves = "";
  fechaViernes = "";
  fechaSabado = "";


  selectedTab = 0;


  /*  calendario */
  dayStartHour = 5;
  dayEndHour = 23;

  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  locale =  'es';
  
  events: CalendarEvent[] = [ ];
  
  eventsShow: CalendarEvent[] = [ ];
  /* fin calendario */

  lunes = false;
  martes = false;
  miercoles = false;
  jueves = false;
  viernes = false;
  sabado = false;
  domingo = false;
  
  horaInicioLunes = "";
  horaFinLunes = "";
  horaInicioMartes = "";
  horaFinMartes = "";
  horaInicioMiercoles = "";
  horaFinMiercoles = "";
  horaInicioJueves = "";
  horaFinJueves = "";
  horaInicioViernes = "";
  horaFinViernes = "";
  horaInicioSabado = "";
  horaFinSabado = "";
  horaInicioDomingo = "";
  horaFinDomingo = "";

  EquipoInfo: any = [];
  
  equipo = {
    equipoNombre : "",
    equipoImporte:0,
    disciplinaId: 0,
    gimnasioId :0,
    integrantes : [],
    horario: [],
    importe:0.0
  }
  
  integrante = {
    integranteNombre : "",
    integranteApellidoPaterno: "",
    integranteApellidoMaterno: "",
    integranteCURP: "",
    integranteTelefono : "",
    integranteValueQR: ""
  }
  
  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];
  
  arrayGimnasios:Array<{ 
    gimnasioId: number, 
    gimnasioNombre : string}>=[];  
  
  arrayIntegrantes: Array<{
    integranteId : number,
    integranteNombre : string,
    integranteApellidoPaterno: string,
    integranteApellidoMaterno: string,
    integranteCURP: string,
    integranteTelefono : string ,
    integranteValueQR: string   
  }>=[];
  
  
  arrayHorariosDias: Array<{
    dia : string,
    horarioInicio : string,
    horaFin: string
  }>=[];
  

  constructor( private httpServices: ServicesService, private router:Router) { 
    this.getDisciplinas();
    this.getGimnacios();
    this.ultimoDomingo();
  }

  ngOnInit(): void {
  }

 

  editarHorario(horario: any)
  {
    console.log("botonEditar clickeado")
    console.log(horario);
  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  
  sendMessage() {
    console.log("si envio el mensaje" + this.message);
    this.messageEvent.emit(this.message);
  }
  
  btnCancelar(){
    this.sendMessage();
  }

 
 
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      });
  }
  
  getGimnacios(){
    this.httpServices.getGimnaciosActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayGimnasios = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayGimnasios.push({ 
          gimnasioId: jsonDatos[i].gimnasioId, 
          gimnasioNombre : jsonDatos[i].gimnasioNombre });   
        }
      });
  }
  
  aceptarPrimerTab(){
    console.log("seleccione tab");
    // if(this.equipo.equipoNombre === ""){
    //   alert("Ingresa un nombre al equipo.");
    // }else if(this.equipo.disciplinaId == 0){
      
    // }else if(this.equipo.gimnasioId == 0){
      
    // }else{
      
    // }
    this.selectedTab = 1;
  
  }
  
  
  
  changedTab(event: any){
    console.log("seleccione tab evento "+event); 
    this.selectedTab = event;

  }
  
  
  
  guardarEquipo(){
    console.log("si detecto el click");
    console.log(this.equipo.importe)
            if(this.equipo.equipoNombre === ""){
             alert("Ingresa un nombre al equipo.");
           }else if(this.equipo.disciplinaId == 0){
             alert("Selecciona una disciplina.");
           }else if(this.equipo.gimnasioId == 0){
             alert("Selecciona una gimnasio.");
           }else if(this.equipo.importe===null || this.equipo.importe == 0 ){
            alert("Debes ingresar un importe!");
          }else if(!this.buildArrayHorarios()){

           }else if(this.arrayIntegrantes.length == 0){
             alert("Ingresa al menos un integrante del equipo.");
           }else{
            
           var json = {
             "disciplina": {
               "disciplinaId" : this.equipo.disciplinaId
             },
             "equipo": {
               "equipoNombre" : this.equipo.equipoNombre,
               "integrantes": this.arrayIntegrantes,
               "equipoRepresentante" : this.arrayIntegrantes[0].integranteNombre+" "+this.arrayIntegrantes[0].integranteApellidoPaterno+" "+this.arrayIntegrantes[0].integranteApellidoMaterno,
               "equipoRepresentanteTelefono" : this.arrayIntegrantes[0].integranteTelefono,
               "equipoImporte":this.equipo.importe                
             },
            
            
             "gimnasio" : {
               "gimnasioId" : this.equipo.gimnasioId
             },
             "horario": this.arrayHorariosDias
          }  
          
           console.log(json);
      
      //this.generarQREquipo();
        
     this.httpServices.insertRentaEquipo(json).subscribe(
       datos => {
         console.log(datos);
         if(datos>0)
         {
            alert("Equipo registrado existosamente");
            this.cargarListadeEquipos()
            this.sendMessage();
            // this.router.navigate(['Rentas'], {queryParams: {usr:'webmaster@dam.com'}});                         
            
         }

         if(datos==0)
         {
            alert("El nombre de equipo ya existe")
         }
     
       });
    }
    
  }


  cargarListadeEquipos(){
    this.httpServices.getEquiposAll().subscribe(
      datos=>{
        console.log("Se cargan los equipos inicialmente");
        console.log(datos);
        this.EquipoInfo = [];
        var json = JSON.parse(JSON.stringify(datos));
        for(var i = 0 ; i < json.length; i++){
          this.EquipoInfo.push(
            {
              equipoId: json[i].equipoId,              
            equipoRepresentante: json[i].equipoRepresentante,
            equipoNombre: json[i].equipoNombre,    
            equipoRepresentanteTelefono: json[i].equipoRepresentanteTelefono,
            equipoActivo: json[i].equipoActivo,
            isActive: this.getBooleando(json[i].equipoActivo)
            }
          );
        }
      },
      error =>{
        alert("Hubo un error al cargar los datos de los quipos. Intenta de nuevo")
      }
    );
  }

  getBooleando(activo: string){
    if(activo == "SI"){
      return true;
    }else{
      return false;
    }
  }
  
  getHorariosByGimnacio(){
    
  }

  getTodosLosEquipos(){
    this.httpServices.getEquiposAll().subscribe(
      datos=>{
        console.log(datos);
        this.EquipoInfo=datos;
        console.log("Equipos en el array");
        console.log(this.EquipoInfo)
      },
      error=>{
        alert("Hubo un error al cargar los datos de los equipos");
      }
      
    );

  }
  
  buildArrayHorarios(){
    this.arrayHorariosDias = [];
    
    if(this.lunes){
      if(this.horaInicioLunes === ""){
        alert("Ingresa un hora de inicio para el dia lunes");
        return false;
      }
      if(this.horaFinLunes === ""){
        alert("Ingresa un hora de fin para el dia lunes");
        return false;
      }
      if(this.horaInicioLunes>this.horaFinLunes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Lunes",
        horarioInicio : this.horaInicioLunes,
        horaFin: this.horaFinLunes
      });
    
    }
    
    if(this.martes){
      if(this.horaInicioMartes === ""){
        alert("Ingresa un hora de inicio para el dia martes");
        return false;
      } 
      if(this.horaFinMartes === ""){
        alert("Ingresa un hora de fin para el dia martes");
        return false;
      }
      if(this.horaInicioMartes>this.horaFinMartes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Martes",
        horarioInicio : this.horaInicioMartes,
        horaFin: this.horaFinMartes
      });
      
      
      
    }
    
    if(this.miercoles){
      if(this.horaInicioMiercoles === ""){
        alert("Ingresa un hora de inicio para el dia miercoles");
        return false;
      }
      if(this.horaFinMiercoles === ""){
        alert("Ingresa un hora de fin para el dia miercoles");
        return false;
      }
      if(this.horaInicioMiercoles>this.horaFinMiercoles)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      
      this.arrayHorariosDias.push({
        dia : "Miercoles",
        horarioInicio : this.horaInicioMiercoles,
        horaFin: this.horaFinMiercoles
      });
    }
    
    if(this.jueves){
      if(this.horaInicioJueves === ""){
        alert("Ingresa un hora de inicio para el dia jueves");
        return false;
      }
      if(this.horaFinJueves === ""){
        alert("Ingresa un hora de fin para el dia jueves");
        return false;
      }
      if(this.horaInicioJueves>this.horaFinJueves)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      
      this.arrayHorariosDias.push({
        dia : "Jueves",
        horarioInicio : this.horaInicioJueves,
        horaFin: this.horaFinJueves
      });
    }
    
    if(this.viernes){
      if(this.horaInicioViernes === ""){
        alert("Ingresa un hora de inicio para el dia viernes");
        return false;
      }
      if(this.horaFinViernes === ""){
        alert("Ingresa un hora de fin para el dia viernes");
        return false;
      }
      if(this.horaInicioViernes>this.horaFinViernes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      
      this.arrayHorariosDias.push({
        dia : "Jueves",
        horarioInicio : this.horaInicioViernes,
        horaFin: this.horaFinViernes
      });
    }
    
    if(this.sabado){
      if(this.horaInicioSabado === ""){
        alert("Ingresa un hora de inicio para el dia sabado");
        return false;
      }
      if(this.horaFinSabado === ""){
        alert("Ingresa un hora de fin para el dia sabado");
        return false;
      }
      if(this.horaInicioSabado>this.horaFinSabado)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      
      this.arrayHorariosDias.push({
        dia : "Sabado",
        horarioInicio : this.horaInicioSabado,
        horaFin: this.horaFinSabado
      });
    }
    
    if(this.domingo){
      if(this.horaInicioDomingo === ""){
        alert("Ingresa un hora de inicio para el dia domingo");
        return false;
      }
      if(this.horaFinDomingo === ""){
        alert("Ingresa un hora de fin para el dia domingo");
        return false;
      }
      if(this.horaInicioDomingo>this.horaFinDomingo)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Domingo",
        horarioInicio : this.horaInicioDomingo,
        horaFin: this.horaFinDomingo
      });
    }
        
    if(this.arrayHorariosDias.length == 0){
      alert("Selecciona dias y horas para el equipo");
      return false;
    }
    
    return true;
    
  }
  
  
  agregarIntegrantes(){
    if(this.integrante.integranteNombre === ""){
      alert("Agrega el nombre del integrante");
    }else if(this.integrante.integranteApellidoPaterno === ""){
      alert("Agrega el apellido paterno del integrante");
    }else if(this.integrante.integranteApellidoMaterno === ""){
      alert("Agrega el apellido materno del integrante");
    }else if(this.integrante.integranteCURP === ""){
      alert("Agrega la CURP del integrante");
    }else if(this.arrayIntegrantes.length === 0 && this.integrante.integranteTelefono.length != 10){
      alert("Ingresa el teléfono del integrante.");
    }else{
      this.arrayIntegrantes.push({
        integranteId : 0,
        integranteNombre : this.integrante.integranteNombre,
        integranteApellidoPaterno: this.integrante.integranteApellidoPaterno,
        integranteApellidoMaterno: this.integrante.integranteApellidoMaterno,
        integranteCURP: this.integrante.integranteCURP,
        integranteTelefono : this.integrante.integranteTelefono,
        integranteValueQR : crypto.AES.encrypt("A-"+this.integrante.integranteCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString()
      });
      this.resetIntegrante();
    }
  }
  
 
  generarQr(CURP: string){
    this.value = crypto.AES.encrypt("A-"+CURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();

  }
  
  
  
  public openPDF(CURP: string, nombre: string):void {
    this.value = crypto.AES.encrypt("A-"+CURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();

    
      var contexto = this;
      setTimeout(function()
      {                 
           
          console.log("entre al pdf "+contexto.value);
        
          let PDF = new jsPDF('p', 'mm', 'letter');
          console.log("entre al pdf");
                    
          var qrcode = document.getElementById("qrcode")?.innerHTML;
          var parser = new DOMParser();
          var divContenedor;
          console.log("entre al pdf");
          console.log(qrcode);
    
          if(qrcode != undefined){
            divContenedor = parser.parseFromString(qrcode,"text/html");  
          }
          var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 
    
          if(codeBase64 != undefined){
            PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
          }
          
          
          //PDF.setDrawColor(0.5);
          
          PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
          PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
          
          
          PDF.setFillColor(150,150,150);
          PDF.rect(53,90,110,7,'F');
          
          PDF.rect(63,33,35,35);
          
          PDF.line(55,80,106,80);
          
          
          PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
          PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
          PDF.setFontSize(6)
          
          PDF.setTextColor(255,255,255);
          PDF.text("Asociación Monarca de Triatlon",72, 93);
          PDF.text("del Estado de México",72, 96);
          
          PDF.text("Centro de Desarrollo del Deporte",129, 93);
          PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
          
          PDF.setTextColor(0,0,0);
          
          PDF.cell(53,12 , 55, 85," ",0, "center");
          PDF.cell(108,12 , 55, 85," ",0, "center");
          
          PDF.setFontSize(8);
          var w = PDF.internal.pageSize.getWidth();

          if(nombre.length > 30 ){
            var char = nombre.split("");
            var espacio = 0;
            for(var i = 29 ; i > 0; i--){
              if(char[i] === " "){
                espacio = i;
                break;
              }
            }
            
            var nom1 = nombre.substring(0,espacio);
            var nom2 = nombre.substring(espacio);
                        
            PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
            PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
          
          }else{
            
          PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
          }
                    
          console.log("entre al pdf");
          
          PDF.save(nombre+'_qr.pdf');
                    
          contexto.value = "";
          console.log("entre al pdf");
      }, 300);
}



generarQREquipo(){
    
  let PDF = new jsPDF('p', 'mm', 'letter');
  var w = PDF.internal.pageSize.getWidth();
 for(var i  = 0 ; i < this.arrayIntegrantes.length ; i++){

  console.log("entre al pdf");
    
  var qrcode = document.getElementById(this.arrayIntegrantes[i].integranteCURP)?.innerHTML;
  var parser = new DOMParser();
  var divContenedor;
  console.log("entre al pdf");
  console.log(qrcode);

  if(qrcode != undefined){
    divContenedor = parser.parseFromString(qrcode,"text/html");  
  }
  var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 

  if(codeBase64 != undefined){
    PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
  }
  
  
  //PDF.setDrawColor(0.5);
  
  PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
  PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
  
  
  PDF.setFillColor(150,150,150);
  PDF.rect(53,90,110,7,'F');
  
  PDF.rect(63,33,35,35);
  
  PDF.line(55,80,106,80);
  
  
  PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
  PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
  PDF.setFontSize(6)
  
  PDF.setTextColor(255,255,255);
  PDF.text("Asociación Monarca de Triatlon",72, 93);
  PDF.text("del Estado de México",72, 96);
  
  PDF.text("Centro de Desarrollo del Deporte",129, 93);
  PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
  
  PDF.setTextColor(0,0,0);
  
  PDF.cell(53,12 , 55, 85," ",0, "center");
  PDF.cell(108,12 , 55, 85," ",0, "center");
  
  PDF.setFontSize(8);
  
  PDF.text(this.arrayIntegrantes[i].integranteNombre+" "+this.arrayIntegrantes[i].integranteApellidoPaterno+" "+this.arrayIntegrantes[i].integranteApellidoMaterno,(w-54)/2, 79, { align : 'center'});
  
  console.log("entre al pdf");
  
  if(i != this.arrayIntegrantes.length-1){
  PDF.addPage();    
  }else{
    PDF.save(this.arrayIntegrantes[i].integranteNombre+" "+this.arrayIntegrantes[i].integranteApellidoPaterno+" "+this.arrayIntegrantes[i].integranteApellidoMaterno+'_qr.pdf');
  }
 }
}

descargarPDF(integrante : any){
  var nombre = this.integrante.integranteNombre+" "+this.integrante.integranteApellidoPaterno+" "+this.integrante.integranteApellidoMaterno;
  var contexto = this;
  setTimeout(function()
  { 
              
      console.log("entre al pdf "+contexto.value);                 

      let PDF = new jsPDF('p', 'mm', 'letter');
      console.log("entre al pdf");
            
      var qrcode = document.getElementById(integrante.integranteCURP)?.innerHTML;
      var parser = new DOMParser();
      var divContenedor;
      console.log("entre al pdf");
      console.log(qrcode);

      if(qrcode != undefined){
        divContenedor = parser.parseFromString(qrcode,"text/html");  
      }
      var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 

      if(codeBase64 != undefined){
        PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
      }
      
      
      //PDF.setDrawColor(0.5);
      
      PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
      PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);
      
      
      PDF.setFillColor(150,150,150);
      PDF.rect(53,90,110,7,'F');
      
      PDF.rect(63,33,35,35);
      
      PDF.line(55,80,106,80);
      
      
      PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
      PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
      PDF.setFontSize(6)
      
      PDF.setTextColor(255,255,255);
      PDF.text("Asociación Monarca de Triatlon",72, 93);
      PDF.text("del Estado de México",72, 96);
      
      PDF.text("Centro de Desarrollo del Deporte",129, 93);
      PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
      
      PDF.setTextColor(0,0,0);
              
      PDF.cell(53,12 , 55, 85," ",0, "center");
      PDF.cell(108,12 , 55, 85," ",0, "center");                        
      
      PDF.setFontSize(8);
      var w = PDF.internal.pageSize.getWidth();
      PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
      
      console.log("entre al pdf");
      
      PDF.save(nombre+'_qr.pdf');
      
      
      contexto.value = "";
      console.log("entre al pdf");
      
  
  }, 500);
}



  
getHorariosGim(){
  
  console.log("Hola mundo desde la busqueda de horarios"+this.equipo.gimnasioId);
  
  this.httpServices.getHorariosByGinmnasio(this.equipo.gimnasioId).subscribe(
    datos => {
      console.log("estos son los horarios");
      console.log(datos);
      var jsonDatos = JSON.parse(JSON.stringify(datos));
      this.events = [];
      for(var i = 0 ; i < jsonDatos.length; i++ ){
        /* this.arrayHorarios.push({ 
          horarioId: jsonDatos[i].horarioId, 
          horarioNombre : jsonDatos[i].horarioNombre, 
          disciplinaIdFk : jsonDatos[i].disciplinaIdFk,
          categoriaIdFk : jsonDatos[i].categoriaIdFk,
          gimnasioIdFk: jsonDatos[i].gimnasioIdFk,
          instructorIdFk: jsonDatos[i].instructorIdFk,
          horarioInicio: jsonDatos[i].horarioInicio,
          horarioFin: jsonDatos[i].horarioFin,
          horarioDias : jsonDatos[i].horarioDias,
          horarioTurno: jsonDatos[i].horarioTurno,
          horarioActivo: jsonDatos[i].horarioActivo,
          isActive: this.calcularBooleano(jsonDatos[i].horarioActivo)
            });   */
          // this.events.push(this.crearJsonEvento(jsonDatos[i]));
          /* this.events = [
              ...this.events
              , 
              this.crearJsonEvento(jsonDatos[i])
          ]*/
        this.events.push(this.crearJsonEvento(jsonDatos[i]));
          
      }
      
      console.log(this.events);
    });
}


crearJsonEvento(json: any){
  var fecha = "";
  switch(json.horarioDias){
    case "Lunes":
      fecha = this.fechaLunes;
    break;
    case "Martes":
      fecha = this.fechaMartes;
    break;
    case "Miercoles":
      fecha = this.fechaMiercoles;
    break;
    case "Jueves":
      fecha = this.fechaJueves;
    break;
    case "Viernes":
      fecha = this.fechaViernes;
    break;
    case "Sabado":
      fecha = this.fechaSabado;
    break;
    case "Domingo":
      fecha = this.fechaDomingo;
    break;
  }
  
  var fechaInicio = new Date(fecha+"T"+json.horarioInicio);
  var fechaFin = new Date(fecha+"T"+json.horarioFin);

  var json2 = {
    objeto: json,
    start: fechaInicio,
    end: fechaFin,
    title: json.horarioNombre,
    color: colors.blue,
    actions: [],
    resizable: {
      beforeStart: true,
      afterEnd: true,
    },
    draggable: false,
  }
  
  return json2;
  
}



ultimoDomingo(){
  var fecha = new Date(new Date().toISOString().split("T")[0]);
  // for(var i = 0 ; i < 7; i++){
  //   console.log(fecha.getDay() + " "+fecha);
  //   fecha.setDate(fecha.getDate()-1 );
  //   if(fecha.getDay){      
  //   }
  // }
  
  // var fecha = new Date("2021-10-31");
  while(fecha.getDay() != 0){
    console.log(fecha.getDay());
    fecha.setDate(fecha.getDate()-1 );
  }

  this.fechaDomingo = fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha domingo" +this.fechaDomingo+" "+fecha);

  fecha.setDate(fecha.getDate()+1);
  this.fechaLunes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha lunes" +this.fechaLunes);  

  fecha.setDate(fecha.getDate()+1);
  this.fechaMartes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha martes" +this.fechaMartes);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaMiercoles =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha miercoles" +this.fechaMiercoles);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaJueves =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha jueves" +this.fechaJueves);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaViernes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha viernes" +this.fechaViernes);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaSabado =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha sabado "+this.fechaSabado);  
  
  
  console.log("ultimo domingo");
  console.log(addHours(startOfDay(new Date()), 6));
}

cancelarOcultar(){
  console.log("hola desde ocultar");
  
  this.sendMessage();
}

resetIntegrante(){
  this.integrante.integranteNombre = "",
  this.integrante.integranteApellidoPaterno = "",
  this.integrante.integranteApellidoMaterno = "",
  this.integrante.integranteCURP = "",
  this.integrante.integranteTelefono = "",
  this.integrante.integranteValueQR = ""
}

resetAll(){
  
  this.lunes = false;
  this.martes = false;
  this.miercoles = false;
  this.jueves = false;
  this.viernes = false;
  this.sabado = false;
  this.domingo = false;
  
  this.horaInicioLunes = "";
  this.horaFinLunes = "";
  this.horaInicioMartes = "";
  this.horaFinMartes = "";
  this.horaInicioMiercoles = "";
  this.horaFinMiercoles = "";
  this.horaInicioJueves = "";
  this.horaFinJueves = "";
  this.horaInicioViernes = "";
  this.horaFinViernes = "";
  this.horaInicioSabado = "";
  this.horaFinSabado = "";
  this.horaInicioDomingo = "";
  this.horaFinDomingo = "";

  this.equipo.equipoNombre = "";
  this.equipo.disciplinaId = 0;
  this.equipo.gimnasioId = 0;
  this.equipo.integrantes = [];
  this.equipo.horario = [];
  this.events = [];
}


}
