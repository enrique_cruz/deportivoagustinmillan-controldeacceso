import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteDisciplinasComponent } from './reporte-disciplinas.component';

describe('ReporteDisciplinasComponent', () => {
  let component: ReporteDisciplinasComponent;
  let fixture: ComponentFixture<ReporteDisciplinasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteDisciplinasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteDisciplinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
