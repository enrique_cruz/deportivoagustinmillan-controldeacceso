import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadEnd } from '@angular/router';
import { createModuleResolutionCache } from 'typescript';
import { ServicesService } from '../services/services.service';


@Component({
  selector: 'app-roles-crud',
  templateUrl: './roles-crud.component.html',
  styleUrls: ['./roles-crud.component.css']
})
export class RolesCrudComponent implements OnInit {

  visibleCancelar = 1;
  
  permisos : any = this.route.snapshot.queryParamMap.get('usr');

  rol={
    rolIdFk :  0,
    rolNombre : "",
    permiso : []
  };
  strBusqueda = "";
  
  permiArray: any;
  

  arrayPermisos:Array<{ 
    permisosId: number, 
    permisosNombre : string}>=[];   
  
  
  arrayRoles:Array<{ 
    rolesId: number, 
    rolesNombre : string
    rolesActivo : string,
    isActive: boolean }>=[];   
    
    arrayRolesShow:Array<{ 
      rolesId: number, 
      rolesNombre : string
      rolesActivo : string,
      isActive: boolean }>=[];   

  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getRoles();
    this.getPermisos();
  }
  
  
  getRoles(){
    this.httpServices.getRoles().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayRoles = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayRoles.push({ 
          rolesId: jsonDatos[i].rolesId, 
          rolesNombre : jsonDatos[i].rolesNombre ,
          rolesActivo : jsonDatos[i].rolesActivo,
          isActive : this.calcularBooleano(jsonDatos[i].rolesActivo)
                         });
             
        }
        this.arrayRolesShow = this.arrayRoles;
      });
  }
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
  
    
  getPermisos(){
    this.httpServices.getPermisos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayPermisos = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayPermisos.push({ 
          permisosId: jsonDatos[i].permisosId, 
          permisosNombre : jsonDatos[i].permisosNombre });   
        }
      });
  }
  

  btnEditar(rol: any){
    console.log("editar");
    this.getPermisosRol(rol.rolesId);
    this.rol.rolIdFk = rol.rolesId;
    this.rol.rolNombre = rol.rolesNombre;
    //this.rol.permisos = rol.permisos;
    
    this.visibleCancelar = 0;
    
    
  }
  
  
  guardar(){
    console.log();
  }
  
  
  btnCancelar(){
    this.visibleCancelar = 1;
    this.rol.rolIdFk = 0;
    this.rol.rolNombre = "";
    this.rol.permiso = [];
    this.permiArray = [];
  }
  
  getPermisosRol(id: number){
    this.httpServices.getPermisosRol(id).subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         //this.arrayPermisos = [];
         var array = [];
         for(var i = 0 ; i < jsonDatos.length ; i++){
            array.push(jsonDatos[i].permisosId);
         }
         console.log("array permisos");
         console.log(array);
         this.rol.permiso = array as any;
         this.permiArray = array;
      
         
         
      });
  }
  
  setUpdateRol(){
    if(this.rol.rolNombre == ""){
      alert("Agrega un nombre de rol");
    }else if(this.permiArray.length == 0 ){
      alert("Agrega permisos al rol");
    }else{
      var arrayPermisos = [];
      for(var i = 0; i < this.permiArray.length; i++ ){
        var json = {
          "permisosId" : this.permiArray[i]
        };
        arrayPermisos.push(json);
      }

      this.rol.permiso = arrayPermisos as any;
    
    
      console.log(this.rol);
      console.log(JSON.stringify(this.rol));
      this.httpServices.setUpdateRol(this.rol).subscribe(
        datos => {
          console.log(datos);
     
          if(datos == 1){
            this.getRoles();
            this.reset();
          }else{
            alert("Error, intenta de nuevo");
          }
          
         
          
        });
      }
    }
    
    reset(){
      this.rol.rolNombre = "";
      this.rol.rolIdFk = 0;
      this.rol.permiso = [];
      this.permiArray = [];
      this.visibleCancelar = 1;
      
    }
    
    desactivarActivar(json: any){
      
        console.log(json);
        var send = {
          "rolesId" : json.rolesId,
          "rolesActivo": "SI" 
        };
        
        if(json.isActive){
          send.rolesActivo = "SI";
        }else{
          send.rolesActivo = "NO";
        }
        json.rolesActivo = send.rolesActivo;
        
        console.log(send);
        this.httpServices.activeInactiveRol(send).subscribe(
          datos => {
            console.log(datos);
       
          },
          error => {
            console.log("hola desde el error"); 
            console.log(error); 
            if(json.isActive){
              json.rolesActivo = "NO";
              json.isActive = false;
              
            }else{
              json.rolesActivo = "SI";
              json.isActive = true;
            }
            
          });
        
    }
    
    
buscarRol(){
  this.arrayRolesShow = [];
  if(this.strBusqueda.length >= 2){
    for(var i = 0 ; i < this.arrayRoles.length ; i++){
      
      var nombre = this.arrayRoles[i].rolesNombre;
     if(nombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
      this.arrayRolesShow.push({
        rolesId: this.arrayRoles[i].rolesId, 
        rolesNombre : this.arrayRoles[i].rolesNombre ,
        rolesActivo : this.arrayRoles[i].rolesActivo,
        isActive : this.calcularBooleano(this.arrayRoles[i].rolesActivo)
      }
      );
     }
    }
  }else{
    this.arrayRolesShow = this.arrayRoles;
  }
  
}    
    
    
    
    
  }
    
    
    
 
