import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAlumnosDisciplinaComponent } from './reporte-alumnos-disciplina.component';

describe('ReporteAlumnosDisciplinaComponent', () => {
  let component: ReporteAlumnosDisciplinaComponent;
  let fixture: ComponentFixture<ReporteAlumnosDisciplinaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAlumnosDisciplinaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAlumnosDisciplinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
