import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformativaAccesoComponent } from './informativa-acceso.component';

describe('InformativaAccesoComponent', () => {
  let component: InformativaAccesoComponent;
  let fixture: ComponentFixture<InformativaAccesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformativaAccesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformativaAccesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
