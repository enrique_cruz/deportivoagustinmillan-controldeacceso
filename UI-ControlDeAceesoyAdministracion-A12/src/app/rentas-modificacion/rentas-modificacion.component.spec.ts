import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentasModificacionComponent } from './rentas-modificacion.component';

describe('RentasModificacionComponent', () => {
  let component: RentasModificacionComponent;
  let fixture: ComponentFixture<RentasModificacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentasModificacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentasModificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
