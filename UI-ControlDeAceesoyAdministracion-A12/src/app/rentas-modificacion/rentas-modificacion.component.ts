import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
//import { CalendarEvent, CalendarView } from 'angular-calendar';
import { result } from 'lodash';
import { Subject } from 'rxjs';
import { ServicesService } from '../services/services.service';
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';
import tarjeta from '../../assets/images/pdfimg/card.json';
import {
  startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours, isThisISOWeek, secondsToMilliseconds,
} from 'date-fns';
import StringEncoding from '@zxing/library/esm/core/util/StringEncoding';


import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { analyzeAndValidateNgModules, ConditionalExpr } from '@angular/compiler';
import { EventListenerFocusTrapInertStrategy } from '@angular/cdk/a11y';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { id } from 'date-fns/locale';
//import { Console } from 'console';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  }
};

@Component({
  selector: 'app-rentas-modificacion',
  templateUrl: './rentas-modificacion.component.html',
  styleUrls: ['./rentas-modificacion.component.css']
})
export class RentasModificacionComponent implements OnInit {

  @Input() childMessage1: string = "";
  @Output() messageEvent = new EventEmitter<string>();
  message: string = "up"; // <--- Mensaje a enviar

  isDisabled: boolean = true;
  isDisabledEquipo:boolean = true;
  isDisabledGimnasio: boolean = true;
  isDisabledDisciplina: boolean = true;
  isDisabledImporte : boolean = true;
  
  //CHECKS - Variables de desactivacion
  isDisabledCheckLunes: boolean = true;
  isDisabledCheckMartes: boolean = true;
  isDisabledCheckMiercoles: boolean = true;
  isDisabledCheckJueves: boolean = true;
  isDisabledCheckViernes: boolean = true;
  isDisabledCheckSábado: boolean = true;
  isDisabledCheckDomingo: boolean = true;

  //HORA INICIO - variables de desctivación
  isDisabledHoraInicioLunes: boolean = true;
  isDisabledHoraInicioMartes: boolean = true;
  isDisabledHoraInicioMiercoles: boolean = true;
  isDisabledHoraInicioJueves: boolean = true;
  isDisabledHoraInicioViernes: boolean = true;
  isDisabledHoraInicioSabado: boolean = true;
  isDisabledHoraInicioDomngo: boolean = true;


  //HORA FIN - variables de desactivación
  isDisabledHoraFinLunes: boolean = true;
  isDisabledHoraFinMartes: boolean = true;
  isDisabledHoraFinMiercoles: boolean = true;
  isDisabledHoraFinJueves: boolean = true;
  isDisabledHoraFinViernes: boolean = true;
  isDisabledHoraFinSabado: boolean = true;
  isDisabledHoraFinDomingo: boolean = true;

  //###############################################    BOTONES DE EDICIÓN     ########################################

  //ACTUALIZAR HORARIO
  isDisabledBotonGuardarCambios:boolean = false;
  isDisabledBotonActualizarHorario :boolean = true;
  

  //NUEVO HORARIO
  isDisabledNuevoHorario : boolean = false;
  isVisibleGuardarHorario : boolean = false;

  //###############################################    BOTONES DE EDICIÓN     ########################################


  horarioViejo:any;
  horarioNuevo:any;

  //******************************************************** CALENDARIO */
  
  view2: CalendarView = CalendarView.Week;
  CalendarView2 = CalendarView;
  viewDate2: Date = new Date();
  refresh2: Subject<any> = new Subject();
  locale2 =  'es';

  actions2: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        //this.handleEvent('Edited', event);
        console.log("editar", JSON.parse(JSON.stringify(event)).objeto);
        var  objeto = JSON.parse(JSON.stringify(event)).objeto;
        //console.log("Aqui es editar a "+JSON.stringify(event));
        console.log("Aqui es editar a "+ objeto);
        //objeto.horarioDias = this.buscarEventosDias(objeto);
        this.horarioViejo = objeto;
        this.editarHorario(this.horarioViejo);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        var  objeto = JSON.parse(JSON.stringify(event)).objeto;
        //console.log("Aqui es eliminar a "+JSON.stringify(event));
        console.log("Aqui es eliminar a ");
        console.log(objeto);
        this.EliminarHorario(objeto);


      },
    },
  ];

  events2: CalendarEvent[] = [ ];
  
  eventsShow2: CalendarEvent[] = [ ];


  //******************************************************** CALENDARIO */
  visibleCancelar = 1;
  


  btnCancelar(){
    this.isDisabledBotonGuardarCambios = false;
    this.isDisabledBotonActualizarHorario  = false;
    //this.esguardar = false;
    this.sendMessage();
  }

  value = "";

  HEInfo:any = [] ;
  DInfo:any = [] ;
  HInfo: any = [];

  equipoIdGlobal=0;
  disciplinaId=0;
  gimnasioId=0;
  disciplinaNombre="";
  gimnasioNombre="";

  selectedTab = 0;

  dayStartHour = 5;
  dayEndHour = 23;

  fechaDomingo = "";
  fechaLunes = "";
  fechaMartes = "";
  fechaMiercoles = "";
  fechaJueves = "";
  fechaViernes = "";
  fechaSabado = "";

  lunes = false;
  martes = false;
  miercoles = false;
  jueves = false;
  viernes = false;
  sabado = false;
  domingo = false;

  horaInicioLunes = "";
  horaFinLunes = "";
  horaInicioMartes = "";
  horaFinMartes = "";
  horaInicioMiercoles = "";
  horaFinMiercoles = "";
  horaInicioJueves = "";
  horaFinJueves = "";
  horaInicioViernes = "";
  horaFinViernes = "";
  horaInicioSabado = "";
  horaFinSabado = "";
  horaInicioDomingo = "";
  horaFinDomingo = "";

  EquipoInfo: any = [];

  equipo = {
    equipoId:0,
    equipoNombre : "",
    equipoImporte:0,
    disciplinaId: 0,
    disciplinaNombre:"",
    gimnasioId :0,
    gimnasioNombre:"",
    integrantes : [],
    horario: []
  }

  integrante = {
    integranteId:0,
    integranteNombre : "",
    integranteApellidoPaterno: "",
    integranteApellidoMaterno: "",
    integranteCURP: "",
    integranteTelefono : "",
    integranteValueQR: "",
    equipoId:0
  }

  arrayDisciplinas: Array<{
    disciplinaId: number;
    disciplinaNombre: string;
  }>= [];

  arrayGimnasios:Array<{
    gimnasioId: number,
    gimnasioNombre : string}>=[];

  arrayIntegrantes: Array<{
    integranteId : number,
    integranteNombre : string,
    integranteApellidoPaterno: string,
    integranteApellidoMaterno: string,
    integranteCURP: string,
    integranteTelefono : string ,
    integranteValueQR: string
    integranteActivo: string,
    interganteEquipoId:number,
    isActive: boolean

  }>=[];


  arrayHorariosDias: Array<{
    dia : string,
    horaInicio : string,
    horaFin: string
  }>=[];

  HorarioEquipoInfo : Array<{
    equipoId: number;
    equipoNombre: string;
    gimnasioId:number;
    gimnasioNombre:string;
    horarioEquipoDia:string;
    horarioEquipoInicio:string;
    horarioEquipoFin:string;
    horarioEquipoActivo:string;
    disciplinaId:number;
    disciplinaNobre:string;
  }>= [];

  constructor( private httpServices: ServicesService) {
    console.log("el id es "+this.childMessage1);
    this.getDisciplinas();
    this.getGimnacios();    
    //this.getHorarioByGimIdEquipoId();
    this.ultimoDomingo();


  }

  ngOnInit(): void {

    console.log("Inicializamos Gimnasio y disciplina " + this.childMessage1);
    //EL SIMBOLO DE + ES PARA COVNERTIR DE STRING A NUMBER
    this.initGimnasio();
    this.initDisciplina();
    this.intiHorarios();
    //this.getHorarioByGimIdEquipoId();

  }

  EliminarHorario(horario:any){
    console.log("Eliminar Horario:")
    console.log(horario)
    if(confirm("Desea eliminar este horario?")== true)
    {
      this.httpServices.deleteHorarioEquipo(horario.horarioId).toPromise()
        .then(
          (data)=>{
            if(data==1){
              alert("El horario se eliminó con éxito");
              //this.updtaeHorarioList();
              this.getHorariosGim();
              this.reset();
              this.intiHorarios();
            }
            else if(data==2){
              alert("Hubo un error al eliminar el horario");
            }
          }
        )
        .catch(
          (error)=>{
            console.log("Error al actualizar el horario: " + JSON.stringify(error));
          }          
        );
    }
    else
    {
      console.log("Eliminacion cancelada")
    }
  }

  guardarHorario(){

    if(this.buildArrayHorarios())
    {
      if(this.arrayHorariosDias.length>1){
        alert("No puedes configurar mas de 1 dia para este horario.")
      }
      else
      {   
        if(this.horarioViejo ===undefined){
          alert("Tienes que elegir un horario del calendario antes de guardar cambios.")
        } 
        else if(this.equipo.equipoImporte == null)
        {
          alert("Debes ingresar un importe.");
        }
        else
        {
          console.log("Actualizando horario viejo :")
          console.log(this.horarioViejo);
          console.log("con los datos del horario nuevo");
          console.log(this.arrayHorariosDias);
          console.log("Nuevo Gimansio");
          console.log(this.equipo.gimnasioId)

          this.horarioNuevo=
          {
            equipo:{
              equipoId:this.equipo.equipoId,
              equipoNombre:this.equipo.equipoNombre,
              equipoImporte:this.equipo.equipoImporte,
            },        
            horarioId:this.horarioViejo.horarioId,
            gimnasioId:this.equipo.gimnasioId,
            horarioEquipoDia: this.arrayHorariosDias[0].dia,
            horarioEquipoInicio:this.arrayHorariosDias[0].horaInicio,
            horarioEquipoFin:this.arrayHorariosDias[0].horaFin,

          }

          console.log("QUEDARÍA DE LA SIGUIENTE MANERA")
          console.log(this.horarioNuevo);

          this.httpServices.updateHorarioEquipo(this.horarioNuevo).toPromise()
            .then(
              (data)=>{
                console.log(data);
                if(data==1)
                {
                  alert("Horario actualizado correctamente");
                  this.isDisabledBotonActualizarHorario=true;
                  this.getHorariosGim();
                  this.sendMessage();
                }
                else if(data == 2)
                {
                  alert("El horaio ya existe. Elige otro:");
                }
                else if(data==3)
                {
                  alert("El nombre ya esta asignado a otro equipo. Elige otro")
                }
              }
            )
            .catch(
              (error)=>{
                console.log("Error al actualizar el horario: " + JSON.stringify(error));
              }
            );
        }                
      }
    }

  }

  agregarHorarioNuevo(){
    //this.resetAll();
    this.limpiaCampos();

    this.isDisabledBotonGuardarCambios=true;
    this.isVisibleGuardarHorario = true;      
    this.isDisabledGimnasio = false;
    this.isDisabled = false;
  
  }


  limpiaCampos()
  {
    this.lunes = false;
    this.martes = false;
    this.miercoles = false;
    this.jueves = false;
    this.viernes = false;
    this.sabado = false;
    this.domingo = false;

    this.horaInicioLunes = "";
    this.horaFinLunes = "";
    this.horaInicioMartes = "";
    this.horaFinMartes = "";
    this.horaInicioMiercoles = "";
    this.horaFinMiercoles = "";
    this.horaInicioJueves = "";
    this.horaFinJueves = "";
    this.horaInicioViernes = "";
    this.horaFinViernes = "";
    this.horaInicioSabado = "";
    this.horaFinSabado = "";
    this.horaInicioDomingo = "";
    this.horaFinDomingo = "";

    // this.equipo.equipoNombre = "";
    // this.equipo.disciplinaId = 0;
    this.equipo.gimnasioId = 0;
    // this.equipo.integrantes = [];
    // this.equipo.horario = [];
    // this.events2 = [];

  }

  editarHorario(horario:any)
  {
    console.log("Horario a editar: ");
    console.log(horario);

    this.isDisabledBotonActualizarHorario = false;
    this.isDisabledNuevoHorario= true;
    this.isDisabledImporte = false;
    this.isDisabledEquipo = false

    //Obetenemos el gimnasio del horario y lo seteamos en el dropdown
    this.httpServices.getGimnasioIdByHorarioId(this.horarioViejo.horarioId).toPromise()
      .then(
        (data)=>{
          if(data>0)
          {
            console.log("Gimnasio Id encontrado: " +data.toString());
            this.equipo.gimnasioId = +data;
            console.log(this.equipo);            
            
          }
          else
          {
            console.log("No se encontró gimnasio para el horario");
          }
        }
      )
      .catch(
        (error)=>{
          console.log("Ocurrio un error al obtener el gimasio del horario.")
        }
      );
    
    this.reset();

    this.mantenerHorarioSeleccionado(horario);

    this.isDisabledGimnasio=false;
  }

  mantenerHorarioSeleccionado(h:any){
            if(h.horarioDias=="Lunes")
            {
              this.lunes=true;
              this.horaInicioLunes = h.horarioInicio;
              this.horaFinLunes = h.horarioFin;

            }else
            if(h.horarioDias=="Martes")
            {
              this.martes=true;
              this.horaInicioMartes = h.horarioInicio;
              this.horaFinMartes = h.horarioFin;

            }else
            if(h.horarioDias=="Miercoles")
            {
              this.miercoles=true;
              this.horaInicioMiercoles = h.horarioInicio;
              this.horaFinMiercoles = h.horarioFin;

            }else
            if(h.horarioDias=="Jueves")
            {
              this.jueves = true;
              this.horaInicioJueves = h.horarioInicio;
              this.horaFinJueves = h.horarioFin;

            }else
            if(h.horarioDias=="Viernes")
            {
              this.viernes=true;
              this.horaInicioViernes = h.horarioInicio;
              this.horaFinViernes = h.horarioFin;

            }else
            if(h.horarioDias=="Sabado")
            {
              this.sabado=true;
              this.horaInicioSabado = h.horarioInicio;
              this.horaFinSabado = h.horarioFin;

            }else
            if(h.horarioDias=="Domingo")
            {
              this.domingo=true;
              this.horaInicioDomingo = h.horarioInicio;
              this.horaFinDomingo = h.horarioFin;
            }

  }

  reset(){    

    this.lunes = false;
    this.martes = false;
    this.miercoles = false;
    this.jueves = false;
    this.viernes = false;
    this.sabado = false;
    this.domingo = false;

    this.horaInicioLunes = "";
    this.horaFinLunes = "";
    this.horaInicioMartes = "";
    this.horaFinMartes = "";
    this.horaInicioMiercoles = "";
    this.horaFinMiercoles = "";
    this.horaInicioJueves = "";
    this.horaFinJueves = "";
    this.horaInicioViernes = "";
    this.horaFinViernes = "";
    this.horaInicioSabado = "";
    this.horaFinSabado = "";
    this.horaInicioDomingo = "";
    this.horaFinDomingo = "";

    this.isDisabled=false;

  }

  intiHorarios()
  {
    console.log("INIT HORARIO RENTA DATA PRINT: " + this.childMessage1);

    this.httpServices.getHorariosByEquipoId(+this.childMessage1)
      .toPromise()
      .then(
        data =>{
          this.HInfo = data;
          console.log("HORARIOS de equipo: " + this.childMessage1)
          console.log(this.HInfo)

          //LLENAMOS LOS DATOS DE LOS HORARIOS DE LUNES A VIERNES
          for(let hi of this.HInfo)
          {

            if(hi.horarioEquipoDia=="Lunes")
            {
              this.lunes=true;
              this.horaInicioLunes = hi.horarioEquipoInicio;
              this.horaFinLunes = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Martes")
            {
              this.martes=true;
              this.horaInicioMartes = hi.horarioEquipoInicio;
              this.horaFinMartes = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Miercoles")
            {
              this.miercoles=true;
              this.horaInicioMiercoles = hi.horarioEquipoInicio;
              this.horaFinMiercoles = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Jueves")
            {
              this.jueves = true;
              this.horaInicioJueves = hi.horarioEquipoInicio;
              this.horaFinJueves = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Viernes")
            {
              this.viernes=true;
              this.horaInicioViernes = hi.horarioEquipoInicio;
              this.horaFinViernes = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Sabado")
            {
              this.sabado=true;
              this.horaInicioSabado = hi.horarioEquipoInicio;
              this.horaFinSabado = hi.horarioEquipoFin;

            }else
            if(hi.horarioEquipoDia=="Domingo")
            {
              this.domingo=true;
              this.horaInicioDomingo = hi.horarioEquipoInicio;
              this.horaFinDomingo = hi.horarioEquipoFin;

            }
          }

          //OBETENEMOS TODOS LOS HORARIOS DEL GIMNACIO PARA LLENAR EL CALENDARIO
          //this.getHorariosGim();
          this.getHorarioByGimIdEquipoId();

        }
      )
      .catch(
        error=>{
          console.log("Promesa rechazada con : " + JSON.stringify(error));
        }
      );

  }

  initGimnasio(){
    console.log("INIT Gimnasio Data print:" + this.childMessage1);

    this.httpServices.getGimnasiosByEquipoId(+this.childMessage1).toPromise()
      .then(
        (data)=>{
          this.HEInfo = data;
          console.log("HEInfo");
          console.log(this.HEInfo);
          console.log(this.HEInfo[0].gimnasioNombre);

          this.equipo.equipoId=this.HEInfo[0].equipoId;
          this.equipo.equipoNombre= this.HEInfo[0].equipoNombre;
          this.equipo.equipoImporte = this.HEInfo[0].equipoImporte;          
          this.equipo.disciplinaId= this.HEInfo[0].disciplinaIdFk;
          this.equipo.disciplinaNombre= this.HEInfo[0].disciplinaNombre;
          this.equipo.gimnasioId= this.HEInfo[0].gimnasioId;
          this.equipo.gimnasioNombre= this.HEInfo[0].gimnasioNombre;

          console.log("EQUIPO: ");
          console.log(this.equipo);
        }
      )
      .catch(
        (error) =>{
          console.log("Promesa rechazada con : " + JSON.stringify(error));
        }
      );
  }

  initDisciplina(){
    this.httpServices.getDisciplinaByEquipoId(+this.childMessage1).subscribe(
      data=>{
        console.log("INIT Disciplina Data print:" + this.childMessage1);
        this.DInfo = data;
        console.log(this.DInfo);
      },
      error=>{
        console.log("Hubo un error al obtener la disciplina del equipo: " + this.childMessage1);
      }

    );
  }

  
  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  sendMessage() {
    console.log("si envio el mensaje");
    this.messageEvent.emit(this.message);
  }

  async getDisciplinas(){
    // this.httpServices.getDisciplinasActive().toPromise()
    //   .then(
    //     (datos) =>{
    //       console.log(datos);
    //       var jsonDatos = JSON.parse(JSON.stringify(datos));
    //       this.arrayDisciplinas = [];
    //       for(var i = 0 ; i < jsonDatos.length; i++ )
    //       {
    //         this.arrayDisciplinas.push({
    //           disciplinaId: jsonDatos[i].disciplinaId,
    //           disciplinaNombre : jsonDatos[i].disciplinaNombre 
    //         });
    //       }
    //     }
    //   )
    //   .catch(
    //     (error)=>{
    //       alert("Algo salió mal. Intenta de nuevo!")
    //     }
    //   );
    // let d = await this.httpServices.getDisciplinasActive().toPromise()
    // console.log("GETDISCIPLINAS");
    // console.log(d);


    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({
          disciplinaId: jsonDatos[i].disciplinaId,
          disciplinaNombre : jsonDatos[i].disciplinaNombre });
        }
      });
  }

  getGimnacios(){
    




    this.httpServices.getGimnaciosActivos().subscribe(
      datos => {

        console.log(datos);
        var jsonDatos = JSON.parse(JSON.stringify(datos));

        this.arrayGimnasios = [];

        for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayGimnasios.push({
          gimnasioId: jsonDatos[i].gimnasioId,
          gimnasioNombre : jsonDatos[i].gimnasioNombre });
        }

      });
  }


  aceptarPrimerTab(){
    console.log("seleccione tab");
    // if(this.equipo.equipoNombre === ""){
    //   alert("Ingresa un nombre al equipo.");
    // }else if(this.equipo.disciplinaId == 0){

    // }else if(this.equipo.gimnasioId == 0){

    // }else{

    // }
    this.selectedTab = 1;


  }



  changedTab(event: any){
    console.log("seleccione tab evento "+event);
    this.selectedTab = event;

  }



  guardarEquipo(){

    console.log("Guardar Nuevo Horario");
    
    if(this.equipo.equipoNombre === "")
    {
      alert("Ingresa un nombre al equipo.");
    }
    else if(this.equipo.disciplinaId == 0)
    {
      alert("Selecciona una disciplina.");
    }
    else if(this.equipo.gimnasioId == 0)
    {
      alert("Selecciona una gimnasio.");
    }
    else if(!this.buildArrayHorarios())
    {

    }
    else if(this.arrayHorariosDias.length>1){
      alert("No puedes configurar mas de 1 dia para este horario.")
    }else
    {
  
      var json = {
        "disciplina": {
          "disciplinaId" : this.equipo.disciplinaId
        },
        "equipo": {
          "equipoId":this.equipo.equipoId,
          "equipoNombre" : this.equipo.equipoNombre,          
          // "integrantes": this.arrayIntegrantes,
          // "equipoRepresentante" : this.arrayIntegrantes[0].integranteNombre+" "+this.arrayIntegrantes[0].integranteApellidoPaterno+" "+this.arrayIntegrantes[0].integranteApellidoMaterno,
          // "equipoRepresentanteTelefono" : this.arrayIntegrantes[0].integranteTelefono
        },
        "gimnasio" : {
          "gimnasioId" : this.equipo.gimnasioId
        },
        "horario": this.arrayHorariosDias
      }

      console.log(json);


      this.httpServices.addNewHorario(json).toPromise()
      .then(
        (data) =>{
          console.log(data);

          if(data>0)    
          {
            alert("Horario agregado satisfactoriamente");
            //this.getHorariosGim();
            this.getHorarioByGimIdEquipoId();
            //this.intiHorarios();            
            this.sendMessage();
          }
          else
          {
            alert("El horario ya existe para este equipo.");
          }
        }
      )
      .catch(
        (err) =>{
          alert("Hubo un error al guardar el horario: " + JSON.stringify(err))
        }
      );

      //this.generarQREquipo();

      // this.httpServices.insertRentaEquipo(json).subscribe(
      //   datos => {
      //     console.log(datos);
      //     if(datos>0)
      //     {
      //       alert("Equipo registrado existosamente");
      //       this.getTodosLosEquipos();
      //       this.sendMessage();
      //     }

      //     if(datos==0)
      //     {
      //       alert("El nombre de equipo ya existe")
      //     }
      //   }
      // );
    }

  }

  getHorariosByGimnacio(){

  }

  getTodosLosEquipos(){
    this.httpServices.getEquiposAll().subscribe(
      datos=>{
        console.log(datos);
        this.EquipoInfo=datos;
        console.log("Equipos en el array");
        console.log(this.EquipoInfo)
      },
      error=>{
        alert("Hubo un erorr al cargar los datos de los equipos");
      }

    );

  }

  buildArrayHorarios(){
    this.arrayHorariosDias = [];

    if(this.lunes){
      if(this.horaInicioLunes === ""){
        alert("Ingresa un hora de inicio para el dia lunes");
        return false;
      }
      if(this.horaFinLunes === ""){
        alert("Ingresa un hora de fin para el dia lunes");
        return false;
      }
      if(this.horaInicioLunes>this.horaFinLunes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;        
      }
      this.arrayHorariosDias.push({
        dia : "Lunes",
        horaInicio : this.horaInicioLunes,
        horaFin: this.horaFinLunes
      });
    }

    if(this.martes){
      if(this.horaInicioMartes === ""){
        alert("Ingresa un hora de inicio para el dia martes");
        return false;
      }
      if(this.horaFinMartes === ""){
        alert("Ingresa un hora de fin para el dia martes");
        return false;
      }
      if(this.horaInicioMartes>this.horaFinMartes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Martes",
        horaInicio : this.horaInicioMartes,
        horaFin: this.horaFinMartes
      });
    }

    if(this.miercoles){
      if(this.horaInicioMiercoles === ""){
        alert("Ingresa un hora de inicio para el dia miercoles");
        return false;
      }
      if(this.horaFinMiercoles === ""){
        alert("Ingresa un hora de fin para el dia miercoles");
        return false;
      }
      if(this.horaInicioMiercoles>this.horaFinMiercoles)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Miercoles",
        horaInicio : this.horaInicioMiercoles,
        horaFin: this.horaFinMiercoles
      });
    }

    if(this.jueves){
      if(this.horaInicioJueves === ""){
        alert("Ingresa un hora de inicio para el dia jueves");
        return false;
      }
      if(this.horaFinJueves === ""){
        alert("Ingresa un hora de fin para el dia jueves");
        return false;
      }
      if(this.horaInicioJueves>this.horaFinJueves)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Jueves",
        horaInicio : this.horaInicioJueves,
        horaFin: this.horaFinJueves
      });
    }

    if(this.viernes){
      if(this.horaInicioViernes === ""){
        alert("Ingresa un hora de inicio para el dia viernes");
        return false;
      }
      if(this.horaFinViernes === ""){
        alert("Ingresa un hora de fin para el dia viernes");
        return false;
      }
      if(this.horaInicioViernes>this.horaFinViernes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Viernes",
        horaInicio : this.horaInicioViernes,
        horaFin: this.horaFinViernes
      });
    }

    if(this.sabado){
      if(this.horaInicioSabado === ""){
        alert("Ingresa un hora de inicio para el dia sabado");
        return false;
      }
      if(this.horaFinSabado === ""){
        alert("Ingresa un hora de fin para el dia sabado");
        return false;
      }
      if(this.horaInicioSabado>this.horaFinSabado)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Sabado",
        horaInicio : this.horaInicioSabado,
        horaFin: this.horaFinSabado
      });
    }

    if(this.domingo){
      if(this.horaInicioDomingo === ""){
        alert("Ingresa un hora de inicio para el dia domingo");
        return false;
      }
      if(this.horaFinDomingo === ""){
        alert("Ingresa un hora de fin para el dia domingo");
        return false;
      }
      if(this.horaInicioDomingo>this.horaFinDomingo)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Domingo",
        horaInicio : this.horaInicioDomingo,
        horaFin: this.horaFinDomingo
      });
    }

    if(this.arrayHorariosDias.length == 0){
      alert("Selecciona dias y horas para el equipo");
      return false;
    }

    return true;

  }


  updtaeHorarioList(){
    if(this.lunes){
      if(this.horaInicioLunes === ""){
        alert("Ingresa un hora de inicio para el dia lunes");
        return false;
      }
      if(this.horaFinLunes === ""){
        alert("Ingresa un hora de fin para el dia lunes");
        return false;
      }
      if(this.horaInicioLunes>this.horaFinLunes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;        
      }
      this.arrayHorariosDias.push({
        dia : "Lunes",
        horaInicio : this.horaInicioLunes,
        horaFin: this.horaFinLunes
      });
    }

    if(this.martes){
      if(this.horaInicioMartes === ""){
        alert("Ingresa un hora de inicio para el dia martes");
        return false;
      }
      if(this.horaFinMartes === ""){
        alert("Ingresa un hora de fin para el dia martes");
        return false;
      }
      if(this.horaInicioMartes>this.horaFinMartes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Martes",
        horaInicio : this.horaInicioMartes,
        horaFin: this.horaFinMartes
      });
    }

    if(this.miercoles){
      if(this.horaInicioMiercoles === ""){
        alert("Ingresa un hora de inicio para el dia miercoles");
        return false;
      }
      if(this.horaFinMiercoles === ""){
        alert("Ingresa un hora de fin para el dia miercoles");
        return false;
      }
      if(this.horaInicioMiercoles>this.horaFinMiercoles)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Miercoles",
        horaInicio : this.horaInicioMiercoles,
        horaFin: this.horaFinMiercoles
      });
    }

    if(this.jueves){
      if(this.horaInicioJueves === ""){
        alert("Ingresa un hora de inicio para el dia jueves");
        return false;
      }
      if(this.horaFinJueves === ""){
        alert("Ingresa un hora de fin para el dia jueves");
        return false;
      }
      if(this.horaInicioJueves>this.horaFinJueves)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Jueves",
        horaInicio : this.horaInicioJueves,
        horaFin: this.horaFinJueves
      });
    }

    if(this.viernes){
      if(this.horaInicioViernes === ""){
        alert("Ingresa un hora de inicio para el dia viernes");
        return false;
      }
      if(this.horaFinViernes === ""){
        alert("Ingresa un hora de fin para el dia viernes");
        return false;
      }
      if(this.horaInicioViernes>this.horaFinViernes)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Jueves",
        horaInicio : this.horaInicioViernes,
        horaFin: this.horaFinViernes
      });
    }

    if(this.sabado){
      if(this.horaInicioSabado === ""){
        alert("Ingresa un hora de inicio para el dia sabado");
        return false;
      }
      if(this.horaFinSabado === ""){
        alert("Ingresa un hora de fin para el dia sabado");
        return false;
      }
      if(this.horaInicioSabado>this.horaFinSabado)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Sabado",
        horaInicio : this.horaInicioSabado,
        horaFin: this.horaFinSabado
      });
    }

    if(this.domingo){
      if(this.horaInicioDomingo === ""){
        alert("Ingresa un hora de inicio para el dia domingo");
        return false;
      }
      if(this.horaFinDomingo === ""){
        alert("Ingresa un hora de fin para el dia domingo");
        return false;
      }
      if(this.horaInicioDomingo>this.horaFinDomingo)
      {
        alert("La hora de inicio no puede ser mayor a la de fin");
        return false;
      }
      this.arrayHorariosDias.push({
        dia : "Domingo",
        horaInicio : this.horaInicioDomingo,
        horaFin: this.horaFinDomingo
      });
    }
  
    return true;
  }


  agregarIntegrantes(){
    if(this.integrante.integranteNombre === ""){
      alert("Agrega el noombre del integrante");
    }else if(this.integrante.integranteApellidoPaterno === ""){
      alert("Agrega el apellido paterno del integrante");
    }else if(this.integrante.integranteApellidoMaterno === ""){
      alert("Agrega el apellido materno del integrante");
    }else if(this.integrante.integranteCURP === ""){
      alert("Agrega la CURP del integrante");
    }else if(this.integrante.integranteTelefono.length === 0 || this.integrante.integranteTelefono.length != 10){
      alert("El telefono debe ser numerico de 10 digitos.");
    }else{
      /*this.arrayIntegrantes.push({
        integranteId : 0,
        integranteNombre : this.integrante.integranteNombre,
        integranteApellidoPaterno: this.integrante.integranteApellidoPaterno,
        integranteApellidoMaterno: this.integrante.integranteApellidoMaterno,
        integranteCURP: this.integrante.integranteCURP,
        integranteTelefono : this.integrante.integranteTelefono,
        integranteValueQR : crypto.AES.encrypt("A-"+this.integrante.integranteCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString()
      });*/
      this.integrante.equipoId=this.equipoIdGlobal;

      console.log("Agregando nuevo integrante****************");
      console.log(this.integrante);
      this.httpServices.insertIntegranteEquipo(this.integrante).subscribe(
        data=>{
          console.log(data);
          if(data===0)
          {
            alert("El integrante ya existe!");   
            this.getIntegrantes(this.equipoIdGlobal);
            this.resetIntegrante();         
            
          }
          else if(data===-1)
          {
            alert("Integrante actualizado con éxito")
            this.getIntegrantes(this.equipoIdGlobal);
            this.resetIntegrante();
          }
          else
          {
            alert("Integrante agregado con exito!");
            this.getIntegrantes(this.equipoIdGlobal);
            this.resetIntegrante();

          }
        },
        error=>{
          alert("Error al guardar el integrante. Intenta de nuevo!");
        }
      );

    }
  }


  EditarIntegrante(e:any){
    console.log("Editar integrante ");
    console.log(e);

    this.integrante.integranteId = e.integranteId;
    this.integrante.integranteNombre= e.integranteNombre;
    this.integrante.integranteApellidoPaterno= e.integranteApellidoPaterno;
    this.integrante.integranteApellidoMaterno= e.integranteApellidoMaterno;
    this.integrante.integranteCURP= e.integranteCURP;
    this.integrante.integranteTelefono = e.integranteTelefono;


  }

  generarQr(CURP: string){
    this.value = crypto.AES.encrypt("A-"+CURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();

  }



  public openPDF(CURP: string, nombre: string):void {
    this.value = crypto.AES.encrypt("A-"+CURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();


      var contexto = this;
      setTimeout(function()
      {


          console.log("entre al pdf "+contexto.value);




          let PDF = new jsPDF('p', 'mm', 'letter');
          console.log("entre al pdf");


          var qrcode = document.getElementById("qrcode")?.innerHTML;
          var parser = new DOMParser();
          var divContenedor;
          console.log("entre al pdf");
          console.log(qrcode);

          if(qrcode != undefined){
            divContenedor = parser.parseFromString(qrcode,"text/html");
          }
          var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src;

          if(codeBase64 != undefined){
            PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
          }


          //PDF.setDrawColor(0.5);

          PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
          PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);


          PDF.setFillColor(150,150,150);
          PDF.rect(53,90,110,7,'F');

          PDF.rect(63,33,35,35);

          PDF.line(55,80,106,80);


          PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
          PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
          PDF.setFontSize(6)

          PDF.setTextColor(255,255,255);
          PDF.text("Asociación Monarca de Triatlon",72, 93);
          PDF.text("del Estado de México",72, 96);

          PDF.text("Centro de Desarrollo del Deporte",129, 93);
          PDF.text("'Gral. Agustín Millán Vivero'",132, 96);

          PDF.setTextColor(0,0,0);

          PDF.cell(53,12 , 55, 85," ",0, "center");
          PDF.cell(108,12 , 55, 85," ",0, "center");

          PDF.setFontSize(8);
          var w = PDF.internal.pageSize.getWidth();



          if(nombre.length > 30 ){
            var char = nombre.split("");
            var espacio = 0;
            for(var i = 29 ; i > 0; i--){
              if(char[i] === " "){
                espacio = i;
                break;
              }
            }

            var nom1 = nombre.substring(0,espacio);
            var nom2 = nombre.substring(espacio);

            PDF.text(nom1,(w-54)/2, 75, { align : 'center'});
            PDF.text(nom2,(w-54)/2, 79, { align : 'center'});

          }else{

          PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
          }


          console.log("entre al pdf");

          PDF.save(nombre+'_qr.pdf');


          contexto.value = "";
          console.log("entre al pdf");
      }, 300);
}




generarQREquipo(){

  let PDF = new jsPDF('p', 'mm', 'letter');
  var w = PDF.internal.pageSize.getWidth();
 for(var i  = 0 ; i < this.arrayIntegrantes.length ; i++){

  console.log("entre al pdf");


  var qrcode = document.getElementById(this.arrayIntegrantes[i].integranteCURP)?.innerHTML;
  var parser = new DOMParser();
  var divContenedor;
  console.log("entre al pdf");
  console.log(qrcode);

  if(qrcode != undefined){
    divContenedor = parser.parseFromString(qrcode,"text/html");
  }
  var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src;

  if(codeBase64 != undefined){
    PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
  }


  //PDF.setDrawColor(0.5);

  PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
  PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);


  PDF.setFillColor(150,150,150);
  PDF.rect(53,90,110,7,'F');

  PDF.rect(63,33,35,35);

  PDF.line(55,80,106,80);


  PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
  PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
  PDF.setFontSize(6)

  PDF.setTextColor(255,255,255);
  PDF.text("Asociación Monarca de Triatlon",72, 93);
  PDF.text("del Estado de México",72, 96);

  PDF.text("Centro de Desarrollo del Deporte",129, 93);
  PDF.text("'Gral. Agustín Millán Vivero'",132, 96);

  PDF.setTextColor(0,0,0);

  PDF.cell(53,12 , 55, 85," ",0, "center");
  PDF.cell(108,12 , 55, 85," ",0, "center");

  PDF.setFontSize(8);

  PDF.text(this.arrayIntegrantes[i].integranteNombre+" "+this.arrayIntegrantes[i].integranteApellidoPaterno+" "+this.arrayIntegrantes[i].integranteApellidoMaterno,(w-54)/2, 79, { align : 'center'});

  console.log("entre al pdf");

  if(i != this.arrayIntegrantes.length-1){
  PDF.addPage();
  }else{
    PDF.save(this.arrayIntegrantes[i].integranteNombre+" "+this.arrayIntegrantes[i].integranteApellidoPaterno+" "+this.arrayIntegrantes[i].integranteApellidoMaterno+'_qr.pdf');
  }
 }
}

descargarPDF(integrante : any){
  console.log("Imprimiendo integrante");
  console.log(integrante);
   
  var nombre = integrante.integranteNombre+" "+integrante.integranteApellidoPaterno +" "+this.integrante.integranteApellidoMaterno;
  var contexto = this;
  setTimeout(function()
  {
      console.log("entre al pdf "+contexto.value);

      let PDF = new jsPDF('p', 'mm', 'letter');
      console.log("entre al pdf");

      var qrcode = document.getElementById(integrante.integranteCURP)?.innerHTML;
      var parser = new DOMParser();
      var divContenedor;
      console.log("entre al pdf");
      console.log(qrcode);

      if(qrcode != undefined){
        divContenedor = parser.parseFromString(qrcode,"text/html");
      }
      var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src;

      if(codeBase64 != undefined){
        PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
      }

      //PDF.setDrawColor(0.5);

      PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
      PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);

      PDF.setFillColor(150,150,150);
      PDF.rect(53,90,110,7,'F');

      PDF.rect(63,33,35,35);

      PDF.line(55,80,106,80);

      PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
      PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
      PDF.setFontSize(6)

      PDF.setTextColor(255,255,255);
      PDF.text("Asociación Monarca de Triatlon",72, 93);
      PDF.text("del Estado de México",72, 96);

      PDF.text("Centro de Desarrollo del Deporte",129, 93);
      PDF.text("'Gral. Agustín Millán Vivero'",132, 96);

      PDF.setTextColor(0,0,0);

      PDF.cell(53,12 , 55, 85," ",0, "center");
      PDF.cell(108,12 , 55, 85," ",0, "center");

      PDF.setFontSize(8);

      var w = PDF.internal.pageSize.getWidth();
      
      if(nombre.length > 30 )
      {
        var char = nombre.split("");
        var espacio = 0;
        for(var i = 29 ; i > 0; i--)
        {
          if(char[i] === " ")
          {
            espacio = i;
            break;
          }
        }
        
        var nom1 = nombre.substring(0,espacio);
        var nom2 = nombre.substring(espacio);
                    
        PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
        PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
      
      }
      else
      {       
        PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
      }
      //PDF.text(nombre,(w-54)/2, 79, { align : 'center'});

      console.log("entre al pdf");

      PDF.save(nombre+'_qr.pdf');

      contexto.value = "";
      console.log("entre al pdf");


  }, 500);
}

getHorariosGim(){

  console.log("Hola mundo desde la busqueda de horarios"+this.equipo.gimnasioId);

  this.httpServices.getHorariosByGinmnasio(this.equipo.gimnasioId).subscribe(
    datos => {
      console.log("estos son los horarios");
      console.log(datos);
       var jsonDatos = JSON.parse(JSON.stringify(datos));
       this.events2 = [];
       for(var i = 0 ; i < jsonDatos.length; i++ )
       {
          console.log(datos);
          var jsonDatos = JSON.parse(JSON.stringify(datos));          

          this.events2.push(this.crearJsonEvento(jsonDatos[i]));
        }

      console.log(this.events2);
    });
}

getHorarioByGimIdEquipoId()
{
  console.log("Obteniendo el horario del equipo y los horarios del gimnasio");
  console.log(this.equipo.gimnasioId);
  console.log(this.equipo.equipoId),
  
  
  this.httpServices.getHorarioByGimnasioEquipoId(this.equipo.gimnasioId, this.equipo.equipoId).toPromise()
    .then(
      (data)=>{
        if(data)
        {
          console.log("Estos son los horarios");
          console.log(data);

          var jsonDatos = JSON.parse(JSON.stringify(data));
          this.events2 = [];

          for(var i = 0 ; i < jsonDatos.length; i++ )
          {
            console.log(data);
            var jsonDatos = JSON.parse(JSON.stringify(data));          

            this.events2.push(this.crearJsonEvento(jsonDatos[i]));  
          }
        }
        else
        {
          console.log("No hay horarios");
        }
      }
    )
    .catch();
}




crearJsonEvento(json: any){
  
  var fecha = "";
  var color: any;
  var actions:any;

  
  switch(json.horarioDias){
    case "Lunes":
      fecha = this.fechaLunes;
    break;
    case "Martes":
      fecha = this.fechaMartes;
    break;
    case "Miercoles":
      fecha = this.fechaMiercoles;
    break;
    case "Jueves":
      fecha = this.fechaJueves;
    break;
    case "Viernes":
      fecha = this.fechaViernes;
    break;
    case "Sabado":
      fecha = this.fechaSabado;
    break;
    case "Domingo":
      fecha = this.fechaDomingo;
    break;
  }

  var fechaInicio = new Date(fecha+"T"+json.horarioInicio);
  var fechaFin = new Date(fecha+"T"+json.horarioFin);

  console.log("ObjetoJson: ");
  console.log(json);
  console.log("Objeto equipo: ");
  console.log(this.equipo);

  if(json.horarioNombre == this.equipo.equipoNombre)
  {
    color=colors.blue;
    actions=this.actions2
  }
  else 
  {
    color=colors.red;
    actions= [];
  }

  var json2 = {
    objeto: json,
    start: fechaInicio,
    end: fechaFin,
    title: json.horarioNombre,
    // color: colors.blue,
    // actions: this.actions2,
    color:color,
    actions:actions,
    resizable: {
    beforeStart: true,
     afterEnd: true,
  },
    draggable: false,
  }

  return json2;

}



ultimoDomingo(){
  var fecha = new Date(new Date().toISOString().split("T")[0]);
  // for(var i = 0 ; i < 7; i++){
  //   console.log(fecha.getDay() + " "+fecha);
  //   fecha.setDate(fecha.getDate()-1 );
  //   if(fecha.getDay){

  //   }
  // }

// var fecha = new Date("2021-10-31");
  while(fecha.getDay() != 0){
    console.log(fecha.getDay());
    fecha.setDate(fecha.getDate()-1 );
  }

  this.fechaDomingo = fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha domingo" +this.fechaDomingo+" "+fecha);

  fecha.setDate(fecha.getDate()+1);
  this.fechaLunes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha lunes" +this.fechaLunes);

  fecha.setDate(fecha.getDate()+1);
  this.fechaMartes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha martes" +this.fechaMartes);

  fecha.setDate(fecha.getDate()+1);
  this.fechaMiercoles =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha miercoles" +this.fechaMiercoles);

  fecha.setDate(fecha.getDate()+1);
  this.fechaJueves =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha jueves" +this.fechaJueves);

  fecha.setDate(fecha.getDate()+1);
  this.fechaViernes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha viernes" +this.fechaViernes);

  fecha.setDate(fecha.getDate()+1);
  this.fechaSabado =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha sabado "+this.fechaSabado);


  console.log("ultimo domingo");
  console.log(addHours(startOfDay(new Date()), 6));
}

cancelarOcultar(){
  console.log("hola desde ocultar");
  this.sendMessage();
}

resetIntegrante(){
  this.integrante.integranteId=0;
  this.integrante.integranteNombre = "",
  this.integrante.integranteApellidoPaterno = "",
  this.integrante.integranteApellidoMaterno = "",
  this.integrante.integranteCURP = "",
  this.integrante.integranteTelefono = "",
  this.integrante.integranteValueQR = ""
}

resetAll(){

  this.lunes = false;
  this.martes = false;
  this.miercoles = false;
  this.jueves = false;
  this.viernes = false;
  this.sabado = false;
  this.domingo = false;

  this.horaInicioLunes = "";
  this.horaFinLunes = "";
  this.horaInicioMartes = "";
  this.horaFinMartes = "";
  this.horaInicioMiercoles = "";
  this.horaFinMiercoles = "";
  this.horaInicioJueves = "";
  this.horaFinJueves = "";
  this.horaInicioViernes = "";
  this.horaFinViernes = "";
  this.horaInicioSabado = "";
  this.horaFinSabado = "";
  this.horaInicioDomingo = "";
  this.horaFinDomingo = "";

  this.equipo.equipoNombre = "";
  this.equipo.disciplinaId = 0;
  this.equipo.gimnasioId = 0;
  this.equipo.integrantes = [];
  this.equipo.horario = [];
  this.events2 = [];
}

cambio(){
  console.log("hola ya cambie");
}

ngOnChanges(){

  console.log("hola ya cambie "+this.childMessage1);
  this.getIntegrantes(Number(this.childMessage1));
}

buscarIntegrante(e:any)
{
  console.log(e.target.value);
  console.log(this.equipoIdGlobal);

  if(e.target.value.length>=1)
  {
    this.httpServices.getIntegranteEquipoSearchBy(this.equipoIdGlobal, e.target.value).toPromise()
    .then(
      data => {
        var jsonDatos = JSON.parse(JSON.stringify(data));
        this.arrayIntegrantes = [];

        console.log(data);
      
        for(var i = 0 ; i < jsonDatos.length; i++){
          this.equipo.equipoNombre = jsonDatos[i].equipoNombre;
  
          this.arrayIntegrantes.push({
            integranteId : jsonDatos[i].integranteId,
            integranteNombre : jsonDatos[i].integranteNombre,
            integranteApellidoPaterno: jsonDatos[i].integranteApellidoPaterno,
            integranteApellidoMaterno: jsonDatos[i].integranteApellidoMaterno,
            integranteCURP: jsonDatos[i].integranteCURP,
            integranteTelefono : jsonDatos[i].integranteTelefono,
            integranteValueQR : crypto.AES.encrypt("I-"+jsonDatos[i].integranteCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString(),
            interganteEquipoId:this.equipo.equipoId,
            integranteActivo: jsonDatos[i].integranteActivo,
            isActive : this.getBooleando(jsonDatos[i].integranteActivo)
          });
        }

      }
    )
    .catch(
      error => {
        alert ("Hubo un error al consultar la base de datos. Intenta mas tarde o contacta tu administrador de sistemas: " + JSON.stringify(error))
      }
    );

  }
  else
  {
    console.log("EquipoId: " + this.equipo.equipoId);
    this.getIntegrantes(this.equipo.equipoId);
  }

  

}

getIntegrantes(id: number){
  this.equipoIdGlobal=id;
  this.httpServices.getIntegrantesByEquipos(id).subscribe(
    datos => {
      console.log("***************** "+id)
      console.log(datos);

      var jsonDatos = JSON.parse(JSON.stringify(datos));
      this.arrayIntegrantes = [];

      for(var i = 0 ; i < jsonDatos.length; i++){
        this.equipo.equipoNombre = jsonDatos[i].equipoNombre;

        this.arrayIntegrantes.push({
          integranteId : jsonDatos[i].integranteId,
          integranteNombre : jsonDatos[i].integranteNombre,
          integranteApellidoPaterno: jsonDatos[i].integranteApellidoPaterno,
          integranteApellidoMaterno: jsonDatos[i].integranteApellidoMaterno,
          integranteCURP: jsonDatos[i].integranteCURP,
          integranteTelefono : jsonDatos[i].integranteTelefono,
          integranteValueQR : crypto.AES.encrypt("I-"+jsonDatos[i].integranteCURP+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString(),
          interganteEquipoId:id,
          integranteActivo: jsonDatos[i].integranteActivo,
          isActive : this.getBooleando(jsonDatos[i].integranteActivo)
        });
      }
      console.log("Integrantes de Equipo*************");
      console.log(this.arrayIntegrantes);
    });
}



getBooleando(activo: string){
  if(activo == "SI"){
    return true;
  }else{
    return false;
  }

}

activarDesactivar(integrante : any){
  var send = {
    "integranteId" : integrante.integranteId,
    "integranteActivo": "SI" ,
    "equipoId": integrante.interganteEquipoId
  };
  if(integrante.isActive){
    send.integranteActivo = "SI";
  }else{
    send.integranteActivo = "NO";
  }
  integrante.integranteActivo = send.integranteActivo;

  console.log(send);
  this.httpServices.activeInactiveIntegrantes(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){

        if(integrante.isActive)
        {
          integrante.integranteActivo = "NO";
          integrante.isActive = false;

        }
        else
        {
          integrante.integranteActivo = "SI";
          integrante.isActive = true;
        }

        alert("error");
      }
    },
    error => {
      console.log("hola desde el error");
      console.log(error);
      if(integrante.isActive){
        integrante.integranteActivo = "NO";
        integrante.isActive = false;
      }else{
        integrante.integranteActivo = "SI";
        integrante.isActive = true;
      }

    });
  }

}
