import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentasPagoComponent } from './rentas-pago.component';

describe('RentasPagoComponent', () => {
  let component: RentasPagoComponent;
  let fixture: ComponentFixture<RentasPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentasPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentasPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
