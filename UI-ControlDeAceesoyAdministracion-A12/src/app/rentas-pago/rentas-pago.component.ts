import { Component, OnInit } from '@angular/core';
import { CommonModule, CurrencyPipe} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { result } from 'lodash';
import { ServicesService } from '../services/services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rentas-pago',
  templateUrl: './rentas-pago.component.html',
  styleUrls: ['./rentas-pago.component.css']
})
export class RentasPagoComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  
  EquipoId:any=this.route.snapshot.queryParamMap.get('id');
  todaysdate:any;
  
  importeSinSigno:any;
  eInfo:any=[];

  equipoHorarioInfo:any=[];

  ParcialidadRenta : any = 
  {
    equipoId:this.EquipoId,
    parcialidadImporte:"",
    TipoDePago:"Tipo de pago...",
    folioDePago:"",
    cuentaBeneficiaria:"",
    observaciones:"",
    fechaDeReciboDePago:"",
    conceptoDePago:"Renta",    
  };


  constructor(private route: ActivatedRoute, 
              private httpServices: ServicesService, 
              private currencyPipe : CurrencyPipe,
              private router: Router) 
  {
    this.todaysdate= new Date();
    this.importeSinSigno ="";
    this.getEquipoInfo(this.EquipoId);
  }

  ngOnInit(): void {
    
  }

  getEquipoInfo(id:number){
    this.httpServices.getEquipoById(this.EquipoId).toPromise()
      .then(
        (data) =>{
          console.log("Datos del equipo: ");
          this.eInfo=data;
          console.log(this.eInfo);
        }  
      )
      .catch(
        (error) => {
          alert("Hubo un error al cargar la información del equipo!!!.")
        }
      );
  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) ||(charCode == 46)) {
      return false;
    }
    return true;
  }  

  transformAmount(element:any){
    this.ParcialidadRenta.parcialidadImporte = this.currencyPipe.transform(this.ParcialidadRenta.parcialidadImporte, '$');
    element.target.value = this.ParcialidadRenta.parcialidadImporte;
  }

  pagar()
  {
    if(this.validarCampos())
    {
      // this.ParcialidadRenta.parcialidadImporte = this.importeSinSigno.replace("$","");
      this.importeSinSigno =this.ParcialidadRenta.parcialidadImporte.replace("$","");
      this.ParcialidadRenta.parcialidadImporte = this.importeSinSigno;
      console.log(this.ParcialidadRenta);      

      this.httpServices.insertParcialidadEquipo(this.ParcialidadRenta).subscribe(
        data=>{
          console.log(data);
          this.limpiarDatos();
          alert("Pago registrado con éxito");     
          this.router.navigate(['Rentas'], {queryParams: {usr:'webmaster@dam.com'}});                         
        },
        error=>{
          alert("El pago no fue registrado. Favor de contactar a su administrador!")
        }
      );
    }
    
  }

  regresar(){
    this.router.navigate(['Rentas'], {queryParams: {usr:'webmaster@dam.com'}});                         
  }

  limpiarDatos()
  {
    this.ParcialidadRenta.equipoId=0;
    this.ParcialidadRenta.parcialidadImporte="";
    this.ParcialidadRenta.TipoDePago="Tipo de pago...";
    this.ParcialidadRenta.folioDePago="";
    this.ParcialidadRenta.cuentaBeneficiaria="";
    this.ParcialidadRenta.observaciones="";
    this.ParcialidadRenta.fechaDeReciboDePago="";
    this.ParcialidadRenta.conceptoDePago="Renta";

    //this.importeSinSigno="";
  }


  validarCampos()
  {
    var result:Boolean;
    result=true;

    this.importeSinSigno = this.ParcialidadRenta.parcialidadImporte.replace("$","").replace(",","");

    if(this.ParcialidadRenta.parcialidadImporte=="")
    {
      alert("El importe no debe ir vacio!")
       result=false;
    }else if(this.ParcialidadRenta.TipoDePago=="Tipo de pago...")
    {
      alert("Seleccione un tipo de pago!")
       result=false;
    }else

    if(this.ParcialidadRenta.folioDePago=="")
    {
      alert("El folio de pago no debe ir vacio!")
       result=false;
    }else
    
    if(this.ParcialidadRenta.cuentaBeneficiaria=="")
    {
      alert("La cuenta beneficiaria no debe ir vacia!")
       result=false;
    }else

    if(this.ParcialidadRenta.observaciones=="")
    {
      alert("Las observaciones no deben ir vacias!")
       result=false;
    }else

    if(this.ParcialidadRenta.fechaDeReciboDePago=="")
    {
      alert("La fecha de recibo de pago no debe ir vacia!")
       result=false;
    }else

    if(this.ParcialidadRenta.conceptoDePago=="")
    {
      alert("El concepto de pago no debe ir vacio!")
       result=false;
    }
    else

    if(  +this.importeSinSigno != this.eInfo[0].equipoImporte)
    {
      alert("El importe depositado debe ser igual al importe de mensualidad.")
       result=false;
    }

    return result;

  }


}
