import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

import tarjeta from '../../assets/images/pdfimg/card.json';
import * as crypto from 'crypto-js';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-qr-maestro',
  templateUrl: './qr-maestro.component.html',
  styleUrls: ['./qr-maestro.component.css']
})
export class QrMaestroComponent implements OnInit {

  constructor(private httpServices: ServicesService,private route:ActivatedRoute) { }

  permisos : any = this.route.snapshot.queryParamMap.get('usr');

  hoy: any;
  qrString: string ="";
  newQr: string ="";

  ngOnInit(): void {

  }

  generarQr(){

    this.hoy = new Date().toLocaleString();
    this.qrString ="M-" + crypto.AES.encrypt(this.hoy+"-"+(new Date().toISOString()),"!A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS").toString();
    this.newQr = this.qrString.split('/').join('').split('+').join('');

    console.log("Generar QR");
    console.log(this.newQr);
    console.log(this.newQr.length);

    this.httpServices.createQrMaestro(this.newQr).toPromise()
    .then(
      (data)=>{
        if(data>0)
        {
          console.log("Data:");
          console.log(this.newQr);
          alert("Generando QR MAESTRO");
          this.openPDF();
        }
        else
        {
          alert("Error al crear el PDF ");
        }
      }
    )
    .catch(
      (error)=>{
        alert("Hubo un error al crear el nuevo QR. INtenta mas tarde!. " + JSON.stringify(error));
      }
    );

  }

  public openPDF():void {
        
    setTimeout(function()
    { 
                
      console.log(" " );
               
      let PDF = new jsPDF('p', 'mm', 'letter');
      console.log("entre al pdf");
                
      var qrcode = document.getElementById("qrcode")?.innerHTML;
      var parser = new DOMParser();
      var divContenedor;
      console.log("entre al pdf");
      console.log(qrcode);

      if(qrcode != undefined){
        divContenedor = parser.parseFromString(qrcode,"text/html");  
      }

      var codeBase64 = divContenedor?.getElementsByTagName("img")[0].src; 

      console.log("Qr base 64");
      console.log(codeBase64);
      if(codeBase64 != undefined){
        PDF.addImage(codeBase64,"png", 106, 27, 58,58,"idQR6", "NONE",0);
      }        
        
      //PDF.setDrawColor(0.5);
        
      PDF.addImage(tarjeta.mexico,"png", 115, 8, 40,20,"idQR10", "NONE",0);
      PDF.addImage(tarjeta.gobierno,"png", 55, 5, 53,34,"idQR11", "NONE",0);       
        
      PDF.setFillColor(150,150,150);
      PDF.rect(53,90,110,7,'F');
        
      PDF.rect(63,33,35,35);
      
      PDF.line(55,80,106,80);
                
      PDF.addImage(tarjeta.asociacion,"png", 57, 90, 10,8,"idQR12", "NONE",0);
      PDF.addImage(tarjeta.m,"png", 108, 86, 20,11,"idQR14", "NONE",0);
      PDF.setFontSize(6)
        
      PDF.setTextColor(255,255,255);
      PDF.text("Asociación Monarca de Triatlon",72, 93);
      PDF.text("del Estado de México",72, 96);
        
      PDF.text("Centro de Desarrollo del Deporte",129, 93);
      PDF.text("'Gral. Agustín Millán Vivero'",132, 96);
        
      PDF.setTextColor(0,0,0);              
        
      PDF.cell(53,12 , 55, 85," ",0, "center");
      PDF.cell(108,12 , 55, 85," ",0, "center");
                
      PDF.setFontSize(8);
      var w = PDF.internal.pageSize.getWidth();
        
      // if(nombre.length > 30 )
      // {
      //   var char = nombre.split("");
      //   var espacio = 0;
      //   for(var i = 29 ; i > 0; i--)
      //   {
      //     if(char[i] === " ")
      //     {
      //       espacio = i;
      //       break;
      //     }
      //   }
          
      //   var nom1 = nombre.substring(0,espacio);
      //   var nom2 = nombre.substring(espacio);
                      
      //   PDF.text(nom1,(w-54)/2, 75, { align : 'center'});     
      //   PDF.text(nom2,(w-54)/2, 79, { align : 'center'});
        
      // }
      // else
      // {
          
      //   PDF.text(nombre,(w-54)/2, 79, { align : 'center'});
      // }
      
      PDF.text("QR MAESTRO",(w-54)/2, 79, { align : 'center'});
      console.log("entre al pdf");
      PDF.save('QrMaestro_qr.pdf');
    
    }, 500);
  }

}
