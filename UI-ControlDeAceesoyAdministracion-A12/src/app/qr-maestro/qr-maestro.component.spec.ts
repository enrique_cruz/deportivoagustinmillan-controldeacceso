import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QrMaestroComponent } from './qr-maestro.component';

describe('QrMaestroComponent', () => {
  let component: QrMaestroComponent;
  let fixture: ComponentFixture<QrMaestroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QrMaestroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QrMaestroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
