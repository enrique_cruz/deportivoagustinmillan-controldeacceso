import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAlumnosComponent } from './reporte-alumnos.component';

describe('ReporteAlumnosComponent', () => {
  let component: ReporteAlumnosComponent;
  let fixture: ComponentFixture<ReporteAlumnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAlumnosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
