import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesAlumnosDisciplinainscripcionComponent } from './reportes-alumnos-disciplinainscripcion.component';

describe('ReportesAlumnosDisciplinainscripcionComponent', () => {
  let component: ReportesAlumnosDisciplinainscripcionComponent;
  let fixture: ComponentFixture<ReportesAlumnosDisciplinainscripcionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportesAlumnosDisciplinainscripcionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesAlumnosDisciplinainscripcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
