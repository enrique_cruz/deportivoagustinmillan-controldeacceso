import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reportes-alumnos-disciplinainscripcion',
  templateUrl: './reportes-alumnos-disciplinainscripcion.component.html',
  styleUrls: ['./reportes-alumnos-disciplinainscripcion.component.css']
})
export class ReportesAlumnosDisciplinainscripcionComponent implements OnInit {


  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];
  
  
  fecha1 = "";
  fecha2 = "";
  
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";

  
  arrayResultadosReporte: Array<{
    nombre : string,
    disciplinaNombre: string,
    plan: string,
    fecha: string
    
  }> =[];
  
  
  disciplinaId = 0;
  
  

  
  constructor(private route: ActivatedRoute,private httpServices: ServicesService) {
    this.getDisciplinas();
    
   }

  ngOnInit(): void {
  }
  
  
  getAlumnosDisciplinas(){
    var json = {
      "alumnoIdFk": Number(this.disciplinaId),
      "fechaEntrada": this.fecha1,
      "fechaSalida": this.fecha2
    }
    console.log(json);
    this.httpServices.getInscritosDisciplinaFechas(json).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
        this.arrayResultadosReporte = [];
        var json = JSON.parse(JSON.stringify(datos));
        for(var i = 0; i < json.length ; i++){
          this.arrayResultadosReporte.push({
                nombre : json[i].alumnoNombre+" "+json[i].alumnoApellidoPaterno+" "+json[i].alumnoApellidoMaterno,
                disciplinaNombre : json[i].disciplinaNombre,
                plan: json[i].planDescripcion,
                fecha: json[i].FechaInscripcion
            }
          );
        }

      });
  }
  
  
  
  
  formatDate(date: string){
    if(date == null || date == undefined){
      return "";
    }
    if(date.split("T").length != 2){
      return ""
    }else{
      return date.split("T")[0];
    }
  }
  
  
  
  downloadReporte(){
    if(this.arrayResultadosReporte.length > 0){
      this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_inscripciones");
    }else{
      console.log("El reporte esta vacio");
    }
  }
  
  
    
  getDisciplinas(){
    this.httpServices.getDisciplinasAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });
        }
        
      });
  }
  
  
  
  

}
