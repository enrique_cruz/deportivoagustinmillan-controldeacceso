import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  
  @Input() usrData:any;  

  permisosLst : any = [];
  correoElectronico : string = "";

  constructor(private router: Router,private httpServices: ServicesService) { }
  
  ngOnInit(): void {
    console.log("NG ON INIT")
    this.httpServices.getRolPermisoByCorreoElectronico(this.usrData).subscribe(
      data=>{
        console.log("Peticion de permisos Correcta")
        this.permisosLst = data;
        this.correoElectronico = this.permisosLst[0].usuarioCorreoElectronico;
        console.log(this.permisosLst);
        console.log(this.correoElectronico)
      },
      error=>{
        console.log("Error en la petición http.: " + error)
      }
    );
  }

  ngAfterViewInit()
  {
    console.log("Hola desde el menú AfterViewInit()!: ");
    console.log(this.usrData);
  }

  

  getRolesyPermisos(){
    

  }


  cerrarSesion(){
    this.httpServices.deleteSesion();
    this.router.navigate(['']);
  }
  
  
}
