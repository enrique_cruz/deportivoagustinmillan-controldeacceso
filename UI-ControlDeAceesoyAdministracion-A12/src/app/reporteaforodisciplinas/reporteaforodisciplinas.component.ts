import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporteaforodisciplinas',
  templateUrl: './reporteaforodisciplinas.component.html',
  styleUrls: ['./reporteaforodisciplinas.component.css']
})
export class ReporteaforodisciplinasComponent implements OnInit {

  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];
  
  
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";

  
  arrayResultadosReporte: Array<{
    categoriaNombre : string,
    disciplinaNombre: string,
    dia: string,
    horarioInicio : string,
    horarioFin : string,
    instructor : string,
    aforo : number,
    disponibilidad: number    
  }> =[];
  
  
  disciplinaId = 0;
  
  

  
  constructor(private route: ActivatedRoute,private httpServices: ServicesService) {
    this.getDisciplinas();
    
   }

  ngOnInit(): void {
  }
  
  
  getAlumnosDisciplinas(){
    var json = {
      "alumnoIdFk": Number(this.disciplinaId)
    }
    console.log(json);
    this.httpServices.getAforoByDisciplina(json).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
        this.arrayResultadosReporte = [];
        var json = JSON.parse(JSON.stringify(datos));
        
        for(var i = 0; i < json.length ; i++){
          this.arrayResultadosReporte.push({
              categoriaNombre : json[i].categoriaNombre,
              disciplinaNombre: json[i].disciplinaNombre,
              dia: json[i].horarioDias,
              horarioInicio : json[i].horarioInicio,
              horarioFin : json[i].horarioFin,
              instructor : json[i].instructorNombre,
              aforo : json[i].horarioAforo,
              disponibilidad: json[i].Disponibilidad 
            }
          );
        }

      });
  }
  
  
  
  
  formatDate(date: string){
    if(date == null || date == undefined){
      return "";
    }
    if(date.split("T").length != 2){
      return ""
    }else{
      return date.split("T")[0];
    }
  }
  
  
  
  downloadReporte(){
    if(this.arrayResultadosReporte.length > 0){
      this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_disciplinas_aforo");
    }else{
      console.log("El reporte esta vacio");
    }
  }
  
  
    
  getDisciplinas(){
    this.httpServices.getDisciplinasAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });
        }
        
      });
  }
  
  
  
}
