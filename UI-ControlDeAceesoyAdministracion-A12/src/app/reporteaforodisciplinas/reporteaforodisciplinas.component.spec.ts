import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteaforodisciplinasComponent } from './reporteaforodisciplinas.component';

describe('ReporteaforodisciplinasComponent', () => {
  let component: ReporteaforodisciplinasComponent;
  let fixture: ComponentFixture<ReporteaforodisciplinasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteaforodisciplinasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteaforodisciplinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
