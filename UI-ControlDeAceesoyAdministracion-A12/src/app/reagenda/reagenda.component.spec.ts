import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReagendaComponent } from './reagenda.component';

describe('ReagendaComponent', () => {
  let component: ReagendaComponent;
  let fixture: ComponentFixture<ReagendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReagendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReagendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
