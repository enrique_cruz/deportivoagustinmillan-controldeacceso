import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';
import iconpeople from '../../assets/images/pdfimg/iconpeople.json';
import { DomSanitizer } from '@angular/platform-browser';
import { textChangeRangeIsUnchanged } from 'typescript';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reagenda',
  templateUrl: './reagenda.component.html',
  styleUrls: ['./reagenda.component.css']
})

export class ReagendaComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";
  showImg : any;
  disciplinaSelected =0;
  arrayDisciplinas:Array<{
    disciplinaId : number,
    disciplinaNombre: string
    }>=[]; 
    
contador = 0;
fechaReposicion: string = "AAAA-MM-DD";
  
  
  alumno = {
    alumnoId :0,
   alumnoBase64Image : "",
   alumnoNombre :  "",
   alumnoApellidoPaterno : "",
   alumnoApellidoMaterno : "",
   alumnoCURP : "",
   alumnoFechaDeNacimiento : "",
   alumnoSexo : ""
  
  }
  
  
  arrayHorariosLunes: Array<{
    horarioId: number,
    horarioNombre: string,
    horarioInicio: string,
    horarioFin: string,
    horarioDias: string,
    horarioDisponibilidad: number   
 }>=[];   
 arrayHorariosMartes: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosMiercoles: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[];
 arrayHorariosJueves: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosViernes: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[] ;
 arrayHorariosSabado: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[]  ;
 
 arrayHorariosDomingo: Array<{
   horarioId: number,
   horarioNombre: string,
   horarioInicio: string,
   horarioFin: string,
   horarioDias: string,
   horarioDisponibilidad: number   
 }>=[]   
 
 
 idHorarioLunesSelected = 0;
idHorarioMartesSelected = 0;
idHorarioMiercolesSelected = 0;
idHorarioJuevesSelected = 0;
idHorarioViernesSelected = 0;
idHorarioSabadoSelected = 0;
idHorarioDomingoSelected = 0;


enableLunes = true;
enableMartes = true;
enableMiercoles = true;
enableJueves = true;
enableViernes = true;
enableSabado = true;
enableDomingo = true;



  arrayAlumnosResultados:Array<{
    alumnoId: number,
    alumnoNombre: string, 
    alumnoFechaDeNacimiento: string,
    alumnoCURP: string
    }>=[];   
  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private httpServices: ServicesService) { 
    this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    +iconpeople.people);
    
  }

  ngOnInit(): void {
  }

  busquedaAlumno(newObj: any){
    console.log(newObj);
  //console.log(this.strBusqueda);
    if(this.strBusqueda.length >= 3){
      this.httpServices.searchAlumnos(this.strBusqueda).subscribe(
        datos => {
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
           this.arrayAlumnosResultados = [];
           for(var i = 0 ; i < jsonDatos.length; i++ ){
              this.arrayAlumnosResultados.push({ 
              alumnoId : jsonDatos[i].alumnoId,
              alumnoNombre: jsonDatos[i].nombre, 
              alumnoFechaDeNacimiento :  jsonDatos[i].alumnoFechaDeNacimiento.toString().split("T")[0],
              alumnoCURP: jsonDatos[i].alumnoCURP});   
            } 
          
        });
    }else{
      this.arrayAlumnosResultados = [];
      
    }
   
  }
  
  
  
  validarClasesPlan(){
    this.contador = 0;
   

    if(Number(this.idHorarioLunesSelected) === 0){
      console.log("entro al if en dero de lunes");
      this.enableLunes = false;
    }else{
      
      console.log("no entro al if cero del lunes");
      this.contador = 1;
    }

    
    if(Number(this.idHorarioMartesSelected) === 0 ){
      this.enableMartes = false;
    } else{
      this.contador = 2;
    }
    
    if(Number(this.idHorarioMiercolesSelected) === 0){
      this.enableMiercoles = false;
    } else{
      this.contador = 3;
    }
    if(Number(this.idHorarioJuevesSelected) === 0 ){
      this.enableJueves = false;
    } else{
      this.contador = 4;
    }
    if(Number(this.idHorarioViernesSelected) === 0 ){
      this.enableViernes = false;
    } else{
      this.contador = 5;
    }
    if( Number(this.idHorarioSabadoSelected) === 0 ){
      this.enableSabado = false;
    } else{
      this.contador = 6;
    }
    if(Number(this.idHorarioDomingoSelected) === 0){
      this.enableDomingo = false;
    } else{
      this.contador = 7;
    }
    
    if(this.contador == 0 ){
      this.enableLunes = true;
      this.enableMartes = true;
      this.enableMiercoles = true;
      this.enableJueves = true;
      this.enableViernes = true;
      this.enableSabado = true;
      this.enableDomingo = true;
    }
    //this.fechaReposicion = "";
    this.getFechaReprogramada();
    
    
    
  }
  
  selectedAlumno(){
    console.log("select");
    console.log(this.strBusqueda);
    
    for(var i = 0 ; i < this.arrayAlumnosResultados.length; i++){
      if(this.strBusqueda === this.arrayAlumnosResultados[i].alumnoCURP){
        this.alumno.alumnoId = this.arrayAlumnosResultados[i].alumnoId;
        this.alumno.alumnoCURP = this.strBusqueda;
        this.alumno.alumnoFechaDeNacimiento = this.arrayAlumnosResultados[i].alumnoFechaDeNacimiento;
        this.arrayAlumnosResultados = [];
        break;
      }
    }
    

    this.getAlumnoById();
    // this.getTutoresAlumno();
    //this.getPlanAlumno();
    this.getDisciplinas(this.alumno.alumnoId);
    this.strBusqueda = "";
    this.arrayAlumnosResultados = [];
  
  }
  
  getAlumnoById(){
    this.httpServices.getAlumnoById( this.alumno.alumnoId).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
 
         this.alumno.alumnoNombre = jsonDatos[0].alumnoNombre;
         this.alumno.alumnoApellidoPaterno = jsonDatos[0].alumnoApellidoPaterno;
         this.alumno.alumnoApellidoMaterno = jsonDatos[0].alumnoApellidoMaterno;
         this.alumno.alumnoBase64Image = jsonDatos[0].alumnoBase64ImgaeString;
         this.alumno.alumnoSexo = jsonDatos[0].alumnoSexo;
                  
          if(this.alumno.alumnoBase64Image != "" && this.alumno.alumnoBase64Image != null){
            this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
            +this.alumno.alumnoBase64Image);
          }else{
            console.log("no asigne la foto por que es "+this.alumno.alumnoBase64Image);
          }
      });
  }
  getDisciplinas(id: number){
    this.httpServices.getPlanesByAlumno(id).subscribe(
    datos => {
      console.log("Horarios");
      console.log(datos);
       var jsonDatos = JSON.parse(JSON.stringify(datos));
       this.arrayDisciplinas = [];
       
       for(var i = 0 ; i < jsonDatos.length; i++){
         if(!this.validarDisciplina(jsonDatos[i].disciplinaNombre)){
           this.arrayDisciplinas.push({
             disciplinaId : jsonDatos[i].disciplinaId,
            disciplinaNombre : jsonDatos[i].disciplinaNombre
           });
         }
       }
       
  
    });
}


validarDisciplina(nombre: String){
  for(var i = 0 ; i < this.arrayDisciplinas.length ; i++){
    if(this.arrayDisciplinas[i].disciplinaNombre ==  nombre){
      return true;
    }
  }
  return false;
}

  
  
 
 getHorarioBydisciplina(){
  //this.arrayhorariosSelected = [];
  this.fechaReposicion = "AAAA-MM-DD";
  
  this.idHorarioDomingoSelected = 0 ;
  this.idHorarioLunesSelected = 0 ;
  this.idHorarioMartesSelected = 0 ;
  this.idHorarioMiercolesSelected = 0 ;
  this.idHorarioJuevesSelected = 0 ;
  this.idHorarioViernesSelected = 0 ;
  this.idHorarioSabadoSelected = 0 ;
  
  this.enableDomingo = true;
  this.enableLunes = true;
  this.enableMartes = true;
  this.enableMiercoles = true;
  this.enableJueves = true;
  this.enableViernes = true;
  this.enableSabado = true;
  
  this.arrayHorariosLunes = [];
  this.arrayHorariosMartes = [];
  this.arrayHorariosMiercoles = [];
  this.arrayHorariosJueves = [];
  this.arrayHorariosViernes = [];
  this.arrayHorariosSabado = [];
  this.arrayHorariosDomingo = [];
  

 
    var json = {
      "disciplinaId" : this.disciplinaSelected,
      "edad" :  parseInt(this.calcularFdad()+"", 10) ,
      "sexo" : this.alumno.alumnoSexo
    }
    // var json = {
    //   "disciplinaId" : this.disciplinaSelected,
    //   "edad" :  12,
    //   "sexo" : "Masculino"
    // } 
    
    
    this.httpServices.getHorarioAforoById(json).subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
    
         
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
           
          var json = { horarioId: jsonDatos[i].horarioId,
                    horarioNombre: jsonDatos[i].horarioNombre,
                    horarioInicio: jsonDatos[i].horarioInicio,
                    horarioFin: jsonDatos[i].horarioFin,
                    horarioDias: jsonDatos[i].horarioDias,
                    horarioDisponibilidad: jsonDatos[i].Disponibilidad  
          }
          switch(json.horarioDias){
            case "Lunes": 
            this.arrayHorariosLunes.push(json);
            break;
            case "Martes": 
            this.arrayHorariosMartes.push(json);
            break;
            case "Miercoles": 
            this.arrayHorariosMiercoles.push(json);
            break;
            case "Jueves": 
            this.arrayHorariosJueves.push(json);
            break;
            case "Viernes": 
            this.arrayHorariosViernes.push(json);
            break;
            case "Sabado": 
            this.arrayHorariosSabado.push(json);
            break;
            case "Domingo": 
            this.arrayHorariosDomingo.push(json);
            break;
          }

          
          /*this.arrayHorarios.push({ 
          horarioId: jsonDatos[i].horarioId, 
          horarioNombre : jsonDatos[i].horarioNombre+" "+jsonDatos[i].horarioInicio+" - "+jsonDatos[i].horarioFin
        });   */
        }
      });
  
  
}
  

calcularFdad(){
  console.log(this.alumno.alumnoFechaDeNacimiento);
  var nacimeinto = new Date(this.alumno.alumnoFechaDeNacimiento+" 00:00");
  var hoy = new Date();
  console.log(hoy+" "+nacimeinto);
  var edad = hoy.getTime()- (nacimeinto.getTime() + 1000*3600);
  edad = edad/ (1000*60*60*24*365.25);
  console.log("la edad es "+edad );
  return edad
 /* if(edad < 18){
     this.tutores = true;
  }else{
   this.tutores = false;
  }*/
} 


getFechaReprogramada(){
  if(this.contador == 0){
    this.fechaReposicion = "AAAA-MM-DD";
  }else{
    this.buscarFechaProxima(this.contador--);
  }
}
  

buscarFechaProxima(idDia: number){
  console.log("calculo de fecha");
  var fecha = new Date();
  console.log(fecha);
  fecha.setDate(fecha.getDate()+1);
  console.log(fecha)
  while(fecha.getDay() != idDia){
    console.log(fecha.getDay());
    fecha.setDate(fecha.getDate()+1 );
  }
  this.fechaReposicion = fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
}


guardarRepocision(){

  if(this.alumno.alumnoId == 0){
    alert("Busca y selecciona a un alumno");
  }else if(this.disciplinaSelected == 0){
    alert("Selecciona una disciplina");
  }else if(this.fechaReposicion === "AAAA-MM-DD"){
    alert("Selecciona un día donde se va a reagendar la clase");
  }else{
    var json = {
      alumnoIdFk : this.alumno.alumnoId,
      horarioIdFk: this.getIdHorarioByDia(this.contador),
      fechaDeReposicion: this.fechaReposicion
    }
    console.log("esto mando");
    console.log(json);
    this.httpServices.reagendarClase(json).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos)
          if(datos <= 0){
            alert("ocurrio un error")
          }else{
            alert("Clase reagendada exitosamente");
            this.reset();
          }
      });
  }
  
  
}



reset(){

   this.alumno.alumnoId  = 0;
   this.alumno.alumnoBase64Image = "";
   this.alumno.alumnoNombre =  "";
   this.alumno.alumnoApellidoPaterno = "";
   this.alumno.alumnoApellidoMaterno = "";
   this.alumno.alumnoCURP = "";
   this.alumno.alumnoFechaDeNacimiento = "";
   this.alumno.alumnoSexo = "";

   
   this.idHorarioDomingoSelected = 0 ;
   this.idHorarioLunesSelected = 0 ;
   this.idHorarioMartesSelected = 0 ;
   this.idHorarioMiercolesSelected = 0 ;
   this.idHorarioJuevesSelected = 0 ;
   this.idHorarioViernesSelected = 0 ;
   this.idHorarioSabadoSelected = 0 ;
   
   this.enableDomingo = true;
   this.enableLunes = true;
   this.enableMartes = true;
   this.enableMiercoles = true;
   this.enableJueves = true;
   this.enableViernes = true;
   this.enableSabado = true;
   
   this.arrayDisciplinas = [];
   this.arrayHorariosDomingo = [];
   this.arrayHorariosLunes = [];
   this.arrayHorariosMartes = [];
   this.arrayHorariosMiercoles = [];
   this.arrayHorariosJueves = [];
   this.arrayHorariosViernes = [];
   this.arrayHorariosJueves = [];
   
   this.fechaReposicion = "AAAA-MM-DD";
   
   this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
   +iconpeople.people);
   
}

getIdHorarioByDia(dia: number){
  console.log("esto es dia"+dia);
  dia++;
  switch(dia){
    case 1: return this.idHorarioLunesSelected;
    case 2: return this.idHorarioMartesSelected;
    
    case 3: return this.idHorarioMiercolesSelected;
    case 4: return this.idHorarioJuevesSelected;
    
    case 5: return this.idHorarioViernesSelected;
    case 6: return this.idHorarioSabadoSelected;
    
    case 7: return this.idHorarioDomingoSelected;
  }
  return 0;
}



  
}
