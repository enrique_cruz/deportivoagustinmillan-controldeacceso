import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteReposicionClasesAlumnosComponent } from './reporte-reposicion-clases-alumnos.component';

describe('ReporteReposicionClasesAlumnosComponent', () => {
  let component: ReporteReposicionClasesAlumnosComponent;
  let fixture: ComponentFixture<ReporteReposicionClasesAlumnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteReposicionClasesAlumnosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteReposicionClasesAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
