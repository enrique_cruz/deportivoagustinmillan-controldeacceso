import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporte-reposicion-clases-alumnos',
  templateUrl: './reporte-reposicion-clases-alumnos.component.html',
  styleUrls: ['./reporte-reposicion-clases-alumnos.component.css']
})
export class ReporteReposicionClasesAlumnosComponent implements OnInit {


  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";
  
  fecha1 = "";
  fecha2 = "";
  
  arrayResultadosReporte: Array<{
    nombre: string,
    horarioNombre : string,
    disciplina: string,
    fechaReposicion: string
    horaInicioReposicion : string,
    horaFinReposicion : string,
    fechaSolicitud : string
    dia: string    
  }> =[];
  
  
  
  arrayAlumnosResultados:Array<{
    alumnoId: number,
    alumnoNombre: string, 
    alumnoFechaDeNacimiento: string,
    alumnoCURP: string
    }>=[];   
  
    alumno = {
      alumnoId :0,
      alumnoBase64Image : "",
      alumnoNombre :  "",
      alumnoApellidoPaterno : "",
      alumnoApellidoMaterno : "",
      alumnoCURP : "",
      alumnoFechaDeNacimiento : "",
      alumnoSexo : "",
      alumnoRutaFoto : "",
      
      alumnoTelefonoCasa: "",
      alumnoTelefonoCelular : "",
      alumnoCorreoElectronico: "",
      alumnoRedesSociales: "",
      alumnoPassword: "",
      alumnoCodigoPostal : 0,
      alumnoEstado: "",
      alumnoMunicipio: "",
      alumnoColonia: "",
      alumnoNumero: "",
      alumnoCalle: "",
      
      alumnoAfiliacionMedica : "",
      alumnoAlergias : "",
      alumnoPadecimientos : "",
      alumnoTipoDeSangre : "",
      alumnoEstatura : 0,
      alumnoPeso : 0,
      alumnoActivo : "Si",
      isActive : false,
      alumnoFolio : ""
     }
  
  
  constructor(private route: ActivatedRoute,private httpServices: ServicesService) { }

  ngOnInit(): void {
  }

  
  
  
  
  
  selectedAlumno(){
    console.log("select");
    console.log(this.strBusqueda);
    
    for(var i = 0 ; i < this.arrayAlumnosResultados.length; i++){
      if(this.strBusqueda === this.arrayAlumnosResultados[i].alumnoCURP){
        this.alumno.alumnoId = this.arrayAlumnosResultados[i].alumnoId;
        this.alumno.alumnoCURP = this.strBusqueda;
        this.alumno.alumnoFechaDeNacimiento = this.arrayAlumnosResultados[i].alumnoFechaDeNacimiento;
        
        this.strBusqueda = this.arrayAlumnosResultados[i].alumnoNombre;
        this.arrayAlumnosResultados = [];
        break;
      }
    }
    
    //this.getMensualidadesAlumno();

    this.arrayAlumnosResultados = [];
  
  }
  
  
  
 
  busquedaAlumno(newObj: any){
    console.log(newObj);
  //console.log(this.strBusqueda);
    if(this.strBusqueda.length >= 3){
      this.httpServices.searchAlumnos(this.strBusqueda).subscribe(
        datos => {
          console.log(datos);
           var jsonDatos = JSON.parse(JSON.stringify(datos));
           this.arrayAlumnosResultados = [];
           for(var i = 0 ; i < jsonDatos.length; i++ ){
              this.arrayAlumnosResultados.push({ 
              alumnoId : jsonDatos[i].alumnoId,
              alumnoNombre: jsonDatos[i].nombre, 
              alumnoFechaDeNacimiento :  jsonDatos[i].alumnoFechaDeNacimiento.toString().split("T")[0],
              alumnoCURP: jsonDatos[i].alumnoCURP});   
            } 
        });
    }else{
      this.arrayAlumnosResultados = [];
      this.alumno.alumnoId = 0;
      this.alumno.alumnoCURP = "";
      this.alumno.alumnoFechaDeNacimiento = "";
      this.arrayResultadosReporte = [];
   
    }
   
  }
 
  
  
  
  getPagosAlumno(){
    
    if(this.alumno.alumnoId == 0){
      alert("Busca un alumno para generar un reporte");  
      return;
    }
    if((this.fecha1 == "" && this.fecha2 != "") || (this.fecha1 != "" && this.fecha2 == "")){
      alert("Ingresa ambas fechas para establecer un rango");
    return;
    }  
    if(this.fecha1 != "" && this.fecha2 != ""){
      var f1 = new Date(this.fecha1);
      var f2 = new Date(this.fecha2);
      if(f1.getTime() > f2.getTime()){
        alert("la fecha de inicio no puede ser mayor a la de fin");
        return;
      }
    }
  
    var json = {
      "alumnoIdFk": this.alumno.alumnoId,
      "fechaEntrada": this.fecha1,
      "fechaSalida": this.fecha2 
    }
    console.log(json);
    this.httpServices.getClasesReagendadasAlumno(json).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
        this.arrayResultadosReporte = [];
        var json = JSON.parse(JSON.stringify(datos));
        for(var i = 0; i < json.length ; i++){
          var fecha = "";
          var hEntrada = "";
          if(json[i].entradasalidaFechaEntrada != null){
            console.log(json[i].entradasalidaFechaEntrada);

            fecha = json[i].entradasalidaFechaEntrada.split("T")[0];
            console.log("fecha: " + fecha)
            hEntrada = (json[i].entradasalidaFechaEntrada.split("T")[1]).substring(0,8);
            console.log("hEntrada: " + hEntrada)
            
          }
          
          var hSalida = "";
          if(json[i].entradasalidaFechaSalida != null){
            hSalida = json[i].entradasalidaFechaSalida.split("T")[1].substring(0,8);
          }
          
          this.arrayResultadosReporte.push({
            nombre: json[i].alumnoNombre+" "+json[i].alumnoApellidoPaterno+" "+json[i].alumnoApellidoMaterno,
            horarioNombre : json[i].horarioNombre,
            disciplina: json[i].Disciplina,
            fechaReposicion: json[i].FechaDeReposicion.split("T")[0],
            horaInicioReposicion : json[i].horarioInicio,
            horaFinReposicion : json[i].horarioFin,
            fechaSolicitud : json[i].FechaSolicitud.split("T")[0],
            dia: json[i].dias    
            }
          );
        }
      });
  }
  
  formatDate(date: string){
    if(date == null || date == undefined){
      return "";
    }
    if(date.split("T").length != 2){
      return ""
    }else{
      return date.split("T")[0];
    }
  }
  
  
  
  downloadReporte(){
    if(this.arrayResultadosReporte.length > 0){
      this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_reposiciones");
    }else{
      console.log("El reporte esta vacio");
    }
  }
  

}
