import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { BehaviorSubject } from 'rxjs';
import { ServicesService } from '../services/services.service';
import { BroadcastChannel } from 'broadcast-channel';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DomSanitizer } from '@angular/platform-browser';
import iconpeople from '../../assets/images/pdfimg/iconpeople.json';
import { isThisSecond } from 'date-fns';

@Component({
  selector: 'app-home-guardia',
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({      
        marginLeft: "1%",
        marginRight: "1%",
        minWidth: "53%",
        maxWidth: "53%",
        marginTop: "2%",
        borderRadius: "20px",
    
        textAlign: "center"
      })),
      state('closed', style({
        marginLeft: "1%",
        marginRight: "1%",
        minWidth: "0%",
        maxWidth: "0%",
        marginTop: "2%",
        borderRadius: "20px",
 
        textAlign: "center",
        fontSize: "0px",
        visibility: "hidden"    
      })),
      state('open2', style({      
        textAlign: "center",
        borderRadius: "20px",
        minWidth: "45%",
        maxWidth: "4%",
        backgroundColor: "#ffffff",
        boxShadow: "rgba(0,0,0,0.30) 10px 10px 10px 2px",
        marginTop: "2%",
        color: "black",
      })),
      state('closed2', style({
        textAlign: "center",
        borderRadius: "20px",
        minWidth: "96%",
        maxWidth: "96%",
        backgroundColor: "#ffffff",
        boxShadow: "rgba(0,0,0,0.30) 10px 10px 10px 2px",
        marginTop: "2%",
        color: "black",
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
      transition('open2 => closed2', [
        animate('0.5s')
      ]),
      transition('closed2 => open2', [
        animate('0.5s')
      ]),
    ]),
  ],
  templateUrl: './home-guardia.component.html',
  styleUrls: ['./home-guardia.component.css']
})

export class HomeGuardiaComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  channel = new BroadcastChannel('foobar');
  
  strBusqueda: string = "";

  id:number = 0
  CURP:string = "";
  dId: number = 0;
  fi : Date = new Date();
  ff : Date = new Date();
  fechaInicio : string ="";
  //fechaFin : Date;

  objAbuscar ={
    id:0,
    CURP:"",
    disciplinaId:0,
    entradasalidaFechaEntrada:"",
    entradasalidaFechaSalida: ""
  }

  alumnoSeleccionado ={
    id: 0,
    nombre:"",
    CURP : "",
  }

  disciplinasInfo : any = [];
  alumnosResultInfo : any = [];

  disciplinaObj = {
    disciplinaId:0,
    disciplinaNombre:""
  }

  showImg : any;
  showImgModal : any;  

  isOpen = false;
  idDevice1 = "";
  idDevice2 = "";
  
  
  alumno = {
    alumnoNombre : "",
    foto : "",
    mensaje : "",
    acceso : false
  }

  instructorInfo : any = [];
  
  alumno2 = {
  alumnoId :0,
   alumnoBase64Image : "",
   alumnoNombre :  "",
   alumnoApellidoPaterno : "",
   alumnoApellidoMaterno : "",
   alumnoCURP : "",
   alumnoFechaDeNacimiento : "",
   alumnoSexo : "",
   alumnoRutaFoto : "",
   
   alumnoTelefonoCasa: "",
   alumnoTelefonoCelular : "",
   alumnoCorreoElectronico: "",
   alumnoRedesSociales: "",
   alumnoPassword: "",
   alumnoCodigoPostal : 0,
   alumnoEstado: "",
   alumnoMunicipio: "",
   alumnoColonia: "",
   alumnoNumero: "",
   alumnoCalle: "",
   
   alumnoAfiliacionMedica : "",
   alumnoAlergias : "",
   alumnoPadecimientos : "",
   alumnoTipoDeSangre : "",
   alumnoEstatura : 0,
   alumnoPeso : 0,
   alumnoActivo : "Si"
  }
  
  arrayDisciplinas:Array<{
    disciplinaNombre: string
    }>=[]; 
  
  
  
  
  @ViewChild('scanner',{static:true, read: false}) scanner!: ZXingScannerComponent;
  
  arrayCamaras: Array<any>= [];
  //dispositivos de entrada
  availableDevices!: MediaDeviceInfo[];
  deviceCurrent!: MediaDeviceInfo;
  deviceSelected: string = "7d224acafedb4f70f93fb370903364cf22957fa378f89c5898ddb1795984eafc";
  hasDevices!: boolean;
  hasPermission!: boolean;
  qrResultString!: string;
  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;
  //dispositivo de salida
  availableDevices2!: MediaDeviceInfo[];
  deviceCurrent2!: MediaDeviceInfo;
  deviceSelected2: string = "bbdb56f3147c24f0f4590d5b459947d330a743e7277081b1be89522ad12ba7d0";
  hasDevices2!: boolean;
  hasPermission2!: boolean;
  qrResultString2!: string;
  torchEnabled2 = false;
  torchAvailable$2 = new BehaviorSubject<boolean>(false);
  tryHarder2 = false;

  
  fecha = "";
  hora = "";
  blurApply = 0;
  
  identifier: string = '';
  
  currentDevice!: MediaDeviceInfo;
  
  
  arrayRegistros: Array<{
    alumnoApellidoMaterno: string,
    alumnoApellidoPaterno: string,
    alumnoIdFk: number,
    alumnoNombre: string,
    disciplinaNombre: string,
    entradasalidaFechaEntrada: string,
    entradasalidaFechaSalida: string,
    horarioDias: string,
    horarioFin: string,
    horarioId: number,
    horarioInicio: string,
    tutorApellidoMaterno: string,
    tutorApellidoPaterno: string,
    tutorNombre: string,
    isExit : false;
  }>= []
  
  
    
  arrayShow: Array<{
    alumnoApellidoMaterno: string,
    alumnoApellidoPaterno: string,
    alumnoIdFk: number,
    alumnoNombre: string,
    disciplinaNombre: string,
    entradasalidaFechaEntrada: string,
    entradasalidaFechaSalida: string,
    horarioDias: string,
    horarioFin: string,
    horarioId: number,
    horarioInicio: string,
    tutorApellidoMaterno: string,
    tutorApellidoPaterno: string,
    tutorNombre: string,
    isExit : false;
  }>= []
  
 
  todaysdate:any
  todaysdate2:any

  constructor(private sanitizer: DomSanitizer,private httpServices: ServicesService, private router: Router,config: NgbModalConfig, private modalService: NgbModal, private route: ActivatedRoute) {
    config.backdrop = 'static';
    config.keyboard = false;
    
     this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
     +iconpeople.people);
         
     this.showImgModal = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
     +iconpeople.people);
     
     
     this.todaysdate= new Date();
     this.todaysdate2= new Date();
    
   }

  ngOnInit(): void 
  {
    this.fecha = new Date().getUTCDate()+" "+this.getmonthSpanish(new Date().getMonth()+1)+" "+new Date().getFullYear();
       
    
    this.getRegisrosToday();
      setInterval(() => {
        var minutes = new Date().getMinutes()+"";
        if(minutes.length == 1){
          minutes = "0"+minutes;
        }
      
        var hours = new Date().getHours()+"";
        if(hours.length == 1){
          hours = "0"+hours;
        }
      
        this.hora= hours+":"+minutes;
 
      //this.getRegisrosToday();
      }, 1000);       
   
    this.listenerIngreso();
    this.GetTodasLasdisciplinas();
  
  }
  
  verMas(content: any, json: any)
  {

    console.log("img click");
    this.modalService.open(content,{ size: 'lg' });
    this.blurApply = 1;

    if(json.disciplinaNombre!="" && json.horarioDias!="" && json.horarioId!=0)
    {
      this.getAlumnoById(json);
      this.getDisciplinas(json.alumnoIdFk);
    }
    else
    {
      this.getInstructorDataById(json);
    }    
  }

  
  closeModal(){
    //this.alumno2=[];
    this.arrayDisciplinas=[];
    
    this.blurApply = 0;
    this.modalService.dismissAll()
  }

  getmonthSpanish(numberMonth : number){
    var month = "";
    switch(numberMonth){
      case 1: 
      month = "Enero"
      break;
      case 2: 
      month = "Febrero"
      break;
      case 3: 
      month = "Marzo"
      break;
      case 4: 
      month = "Abril"
      break;
      case 5: 
      month = "Mayo"
      break;
      case 6: 
      month = "Junio"
      break;
      case 7: 
      month = "Julio"
      break;
      case 8: 
      month = "Agosto"
      break;
      
      case 9: 
      month = "Septiembre"
      break;
      case 10: 
      month = "Octubre"
      break;
      case 11: 
      month = "Noviembre"
      break;
      case 12: 
      month = "Diciembre"
      break;
    }
    return month;
  }
  /*

  ngAfterViewInit(){

     var contexto = this;
     setTimeout(function()
     { 
          contexto.onDeviceSelectChange2("bbdb56f3147c24f0f4590d5b459947d330a743e7277081b1be89522ad12ba7d0");
      contexto.onDeviceSelectChange("7d224acafedb4f70f93fb370903364cf22957fa378f89c5898ddb1795984eafc");
    }, 5000);
    
  }*/
  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
   /* */
  }
  
  onCamerasFound2(devices: MediaDeviceInfo[]): void {
    this.availableDevices2 = devices;
    this.hasDevices2 = Boolean(devices && devices.length);
    console.log("Aqui empiezo");
    /*
*/
  }
  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    console.log(this.qrResultString);
  }

  onCodeResult2(resultString: string) {
    this.qrResultString = resultString;
    console.log("salida")
    console.log(this.qrResultString);
  }


  onDeviceChange(device: MediaDeviceInfo) {
    console.log("Aqui empiezo 1");
    const selectedStr = device?.deviceId || '';
    if (this.deviceSelected === selectedStr) { return; }
    this.deviceSelected = selectedStr;
    this.deviceCurrent = device || undefined;
    

    
  }
  onDeviceChange2(device: MediaDeviceInfo) {
    const selectedStr = device?.deviceId || '';
    if (this.deviceSelected2 === selectedStr) { return; }
    this.deviceSelected2 = selectedStr;
    this.deviceCurrent2 = device || undefined;

  }

  onHasPermission(has: boolean) {
    console.log("Aqui empiezo 2");
    this.hasPermission = has;
  }
  
  onHasPermission2(has: boolean) {
    this.hasPermission2 = has;
  }
  
  onDeviceSelectChange(selected: string) {
console.log("aqui estoy");
    const selectedStr = selected || '';
    if (this.deviceSelected === selectedStr) { return; }
    this.deviceSelected = selectedStr;
    const device = this.availableDevices.find(x => x.deviceId === selected);
    if(device != undefined){
      this.deviceCurrent = device ;
    }

    //this.currentDevice = device;
  }

  
  
  onDeviceSelectChange2(selected: string) {
    console.log("aqui estoy 2");
    const selectedStr = selected || '';
    if (this.deviceSelected2 === selectedStr) { return; }
    this.deviceSelected2 = selectedStr;
    const device = this.availableDevices2.find(x => x.deviceId === selected);
    if(device != undefined){
      this.deviceCurrent2 = device ;
    }
    //this.currentDevice = device;
  }

  
  btnBienvenida(){
    console.log(this.deviceSelected);
    console.log(this.deviceSelected2);
    if(this.deviceSelected != this.deviceSelected2)
    {
      //PRODUCCIÖN
      this.router.navigate([]).then(result => {                
        window.open("https://192.168.100.89/controlescolar/Bienvenida/"+ this.deviceSelected+"/"+ this.deviceSelected2, '_blank', 'location=yes,height=800px,width=500px,scrollbars=yes,status=yes'); 
      });
      
      this.router.navigate([]).then(result => {         
        window.open("https://192.168.100.89/controlescolar/Salida/"+ this.deviceSelected+"/"+ this.deviceSelected2, '_blank', 'location=yes,height=800px,width=500px,scrollbars=yes,status=yes'); 
      });

    }else
    {
      alert("Selecciona una camara para la salida y otra para la entrada");
    }

    //DESARROLLO
    // this.router.navigate([]).then(result => {       
    //   window.open("http://localhost:4200/Bienvenida/"+ this.deviceSelected+"/"+ this.deviceSelected2, '_blank', 'location=yes,height=800px,width=500px,scrollbars=yes,status=yes');      
    // });

    // this.router.navigate([]).then(result => { 
    //   window.open("http://localhost:4200/Salida/"+ this.deviceSelected+"/"+ this.deviceSelected2, '_blank', 'location=yes,height=800px,width=500px,scrollbars=yes,status=yes');       
    //  });   
  }

  
  /*
  alumnoApellidoMaterno: "Perales"
alumnoApellidoPaterno: "Cruz"
alumnoIdFk: 18
alumnoNombre: "Enrique"
disciplinaNombre: "Clavados"
entradasalidaFechaEntrada: "2021-10-11T13:03:34.11"
entradasalidaFechaSalida: "2021-10-11T13:06:47.427"
horarioDias: "Lunes"
horarioFin: "09:00:00"
horarioId: 29
horarioInicio: "08:00:00"
tutorApellidoMaterno: "Arellano"
tutorApellidoPaterno: "Perales"
tutorNombre: "Elodia"

  */ 
    
  getRegisrosToday(){

    this.httpServices.getRegistrosToday().subscribe(datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayRegistros = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          var json = {
            alumnoApellidoMaterno : jsonDatos[i].alumnoApellidoMaterno,
            alumnoApellidoPaterno : jsonDatos[i].alumnoApellidoPaterno,
            alumnoIdFk: jsonDatos[i].alumnoIdFk, 
            alumnoNombre: jsonDatos[i].alumnoNombre,
            disciplinaNombre : jsonDatos[i].disciplinaNombre,
            entradasalidaFechaEntrada : this.formatFechaInicio(jsonDatos[i].entradasalidaFechaEntrada),
            entradasalidaFechaSalida : this.formatFechaFin(jsonDatos[i].entradasalidaFechaSalida),
            horarioDias : jsonDatos[i].horarioDias,
            horarioFin: jsonDatos[i].horarioFin,
            horarioId : jsonDatos[i].horarioId,
            horarioInicio: jsonDatos[i].horarioInicio,
            tutorApellidoMaterno: jsonDatos[i].tutorApellidoMaterno,
            tutorApellidoPaterno: jsonDatos[i].tutorApellidoPaterno,
            tutorNombre: jsonDatos[i].tutorNombre,
            isExit: this.calcularSalida(jsonDatos[i].entradasalidaFechaSalida)
          } 
          this.insertJson(json);                  
         
         }

        this.arrayShow = this.arrayRegistros;
        console.log("arrayShow => ");
        console.log(this.arrayShow);
                  
      });
}


insertJson(json: any){
  for(var i=0 ; i < this.arrayRegistros.length;i++){
    if(this.arrayRegistros[i].alumnoIdFk === json.alumnoIdFk && this.arrayRegistros[i].entradasalidaFechaEntrada === json.entradasalidaFechaEntrada ){
      this.arrayRegistros[i].disciplinaNombre = (this.arrayRegistros[i].disciplinaNombre+" "+json.disciplinaNombre); 
      //var variable = this.arrayRegistros[i].disciplinaNombre.toString();
      //variable = variable ;
      //this.arrayRegistros[i].disciplinaNombre = variable;
      return "";
    }
  }
  this.arrayRegistros.push(json);
  
  return "";  
}


formatFechaInicio(fecha: string){
  if(fecha == null){
    return "";
  }else{
    var filtro = fecha.replace("T"," ");
    var filtro2 = filtro.split(":");
    return filtro2[0]+":"+filtro2[1];
  }
}

formatFechaFin(fecha: string){
  if(fecha == null){
    return "";
  }else{
    var filtro = fecha.split("T")[1];
    var filtro2 = filtro.split(":");
    return filtro2[0]+":"+filtro2[1];
  }
}

calcularSalida(fecha: string){
  if(fecha == null){
    return false;
  }else{
    return true;
  }
}


validarMensaje(json :any){
  
  console.log("######################################Validando mensaje y estatus:##########################################")
  console.log(json[0]);
  if(json[0].Estatus === "Salida")
  {
    //ES SALIDA
    this.getRegisrosToday();
  }
  else
  {
    //ES ENTRADA

    this.alumno.alumnoNombre = "";
    console.log("esto es alumno antes de la asignacion. Imprimiedo VariaBLE aLUMNO");
    console.log(this.alumno);

    if(json[0].tipo == "A")
    {
      console.log("ES UN ALUMNO:");
      console.log(json[0]);
      if(json[0].alumnoBase64ImageString != "")
      {
        this.alumno.alumnoNombre = json[0].alumnoNombre+" "+json[0].alumnoApellidoPaterno+" "+json[0].alumnoApellidoMaterno;

        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        +json[0].alumnoBase64ImageString);
      }
      else
      {
        this.alumno.alumnoNombre = json[0].alumnoNombre+" "+json[0].alumnoApellidoPaterno+" "+json[0].alumnoApellidoMaterno;  
      }

      
      //console.log("if correcto");
    }
    else if(json[0].tipo =="M")
    {
      console.log("ES UN QR MAESTRO:");
      console.log(json[0]);
      if(json[0].alumnoBase64ImageString != "")
      {
        this.alumno.alumnoNombre ="QR MAESTRO"
        
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        +json[0].alumnoBase64ImageString);
      }
      else
      {
        this.alumno.alumnoNombre ="QR MAESTRO"
      }

      
    }
    else if(json[0].tipo=="IE")
    {
      console.log("ES UN QR Integrante de Equipo:");
      console.log(json[0]);
      if(json[0].alumnoBase64ImageString != "")
      {
        this.alumno.alumnoNombre="IntegranteEquipo"
        
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        +json[0].alumnoBase64ImageString);
      }
      else
      {
        this.alumno.alumnoNombre="IntegranteEquipo"
      }

      
      console.log(this.alumno.alumnoNombre);
    }
    else
    {
      if(json[0].instructorBase64ImageString != "")
      {
        this.showImg = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
        +json[0].instructorBase64ImageString);
      }
      this.alumno.alumnoNombre = json[0].instructorNombre+" "+json[0].instructorApellidoPaterno+" "+json[0].instructorApellidoMaterno;
      console.log("ES Insctructor ");
      console.log(this.alumno.alumnoNombre);
    }
    
    console.log("esto es alumno despues de la asignacion");
    console.log(this.alumno);
  
    
    if(json[0].Estatus === "Success"){
      this.getRegisrosToday();
      this.alumno.mensaje = "Membresía vigente";
      this.alumno.acceso = true;         
    
    }else{
      this.alumno.mensaje = json[0].Estatus;
      this.alumno.acceso = false;
  
    }
       
  this.isOpen = true;
  }
  
  
  var contexto = this;
  setTimeout(function()
    { 
      contexto.alumno.alumnoNombre = "";
      contexto.alumno.mensaje = "";
      contexto.isOpen = false;
      contexto.showImg = contexto.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      +iconpeople.people);
   }, 7000);
    
  
}


listenerIngreso(){
  console.log("Recibi mensaje: ");
  this.channel.onmessage = msg => this.validarMensaje(JSON.parse(msg));  
}


getInstructorDataById(json:any)
{
  this.httpServices.getInstructorbyId(json.alumnoIdFk).toPromise()
  .then(
    (data)=>{
      if(data)
      {
        this.instructorInfo=data;

        this.alumno2.alumnoFechaDeNacimiento = this.instructorInfo[0].instructorFechaDeNacimiento;
        this.alumno2.alumnoCURP = this.instructorInfo[0].instructorCURP;
        this.alumno2.alumnoAlergias = "N/A";
        this.alumno2.alumnoNombre = this.instructorInfo[0].instructorNombre;
        this.alumno2.alumnoApellidoPaterno = this.instructorInfo[0].instructorApellidoPaterno;
        this.alumno2.alumnoApellidoMaterno = this.instructorInfo[0].instructorApellidoMaterno;
        //this.alumno2.alumnoBase64Image = "";
        this.alumno2.alumnoBase64Image = this.instructorInfo[0].instructorBase64ImageString;
                
        if(this.alumno2.alumnoBase64Image != "" && this.alumno2.alumnoBase64Image != null){
          this.showImgModal = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +this.alumno2.alumnoBase64Image);
        }else{
          console.log("no asigne la foto por que es "+this.alumno2.alumnoBase64Image);
        }


        this.alumno2.alumnoAfiliacionMedica = "";
        this.alumno2.alumnoCalle =  this.instructorInfo[0].instructorCalle;
        this.alumno2.alumnoCodigoPostal = this.instructorInfo[0].instructorCodigoPostal;
        this.alumno2.alumnoColonia = this.instructorInfo[0].instructorColonia;
        this.alumno2.alumnoCorreoElectronico = this.instructorInfo[0].instructorCorreoElectronico;
        this.alumno2.alumnoEstado = this.instructorInfo[0].instructorEstado;
        this.alumno2.alumnoEstatura = 0;
        this.alumno2.alumnoMunicipio = this.instructorInfo[0].instructorMunicipio;
        this.alumno2.alumnoNumero = this.instructorInfo[0].instructorNumero;
        this.alumno2.alumnoPadecimientos = "N/A";
        this.alumno2.alumnoPeso = 0;
        this.alumno2.alumnoRedesSociales = "N/A";
        this.alumno2.alumnoSexo = this.instructorInfo[0].instructorSexo;
        this.alumno2.alumnoTelefonoCasa = this.instructorInfo[0].instructorTelefono;
        this.alumno2.alumnoTelefonoCelular = "N/A";
        this.alumno2.alumnoTipoDeSangre = "N/A";
        this.alumno2.alumnoActivo = "Si";

        this.arrayDisciplinas.push({
            disciplinaNombre : "INSTRUCTOR"
          });

      } 
      else
      {
        alert("No se encontró información del Instructor")
      }   
    }
  )
  .catch(
    (error) => {
      alert("Hubo un error al consultar la base de datos: " + JSON.stringify(error))
    }
  )

}


getAlumnoById(json: any){
  this.httpServices.getAlumnoById( json.alumnoIdFk).subscribe(
    datos => {
      console.log("ya te traje el al");
      console.log(datos);
       var jsonDatos = JSON.parse(JSON.stringify(datos));
       this.alumno2.alumnoFechaDeNacimiento = jsonDatos[0].alumnoFechaDeNacimiento.split("T")[0];
       this.alumno2.alumnoCURP = jsonDatos[0].alumnoCURP; 
       this.alumno2.alumnoAlergias = jsonDatos[0].alumnoAlergias;
       this.alumno2.alumnoNombre = jsonDatos[0].alumnoNombre;
       this.alumno2.alumnoApellidoPaterno = jsonDatos[0].alumnoApellidoPaterno;
       this.alumno2.alumnoApellidoMaterno = jsonDatos[0].alumnoApellidoMaterno;
       this.alumno2.alumnoBase64Image = jsonDatos[0].alumnoBase64ImgaeString;
                
        if(this.alumno2.alumnoBase64Image != "" && this.alumno2.alumnoBase64Image != null){
          this.showImgModal = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          +this.alumno2.alumnoBase64Image);
        }else{
          console.log("no asigne la foto por que es "+this.alumno2.alumnoBase64Image);
        }
       this.alumno2.alumnoAfiliacionMedica = jsonDatos[0].alumnoAfiliacionMedica;
       this.alumno2.alumnoCalle = jsonDatos[0].alumnoCalle;   
       this.alumno2.alumnoCodigoPostal = jsonDatos[0].alumnoCodigoPostal;
       this.alumno2.alumnoColonia = jsonDatos[0].alumnoColonia;
       this.alumno2.alumnoCorreoElectronico = jsonDatos[0].alumnoCorreoElectronico;
       this.alumno2.alumnoEstado = jsonDatos[0].alumnoEstado;
       this.alumno2.alumnoEstatura = jsonDatos[0].alumnoEstatura;
       this.alumno2.alumnoMunicipio = jsonDatos[0].alumnoMunicipio;
       this.alumno2.alumnoNumero = jsonDatos[0].alumnoNumero;
       this.alumno2.alumnoPadecimientos = jsonDatos[0].alumnoPadecimientos;
       this.alumno2.alumnoPeso = jsonDatos[0].alumnoPeso;
       this.alumno2.alumnoRedesSociales = jsonDatos[0].alumnoRedesSociales;
       this.alumno2.alumnoSexo = jsonDatos[0].alumnoSexo;
       this.alumno2.alumnoTelefonoCasa = jsonDatos[0].alumnoTelefonoCasa;
       this.alumno2.alumnoTelefonoCelular = jsonDatos[0].alumnoTelefonoCelular;
       this.alumno2.alumnoTipoDeSangre = jsonDatos[0].alumnoTipoDeSangre; 
       this.alumno2.alumnoActivo = "Si";
               
    });
}


  getDisciplinas(id: number){
    this.httpServices.getPlanesByAlumno(id).subscribe(
    datos => {
      console.log("Horarios");
      console.log(datos);
      var jsonDatos = JSON.parse(JSON.stringify(datos));
      this.arrayDisciplinas = [];
      
      for(var i = 0 ; i < jsonDatos.length; i++){
        if(!this.validarDisciplina(jsonDatos[i].disciplinaNombre)){
          this.arrayDisciplinas.push({
            disciplinaNombre : jsonDatos[i].disciplinaNombre
          });
        }
      }
      
      
      console.log(this.arrayDisciplinas);
      //  for(var i = 0 ; i < jsonDatos.length; i++ ){
      //     this.arrayTutores.push({ 
      //     tutorId : jsonDatos[i].tutorId,
      //     tutorNombre: jsonDatos[i].tutorNombre, 
      //     tutorApellidoMaterno :  jsonDatos[i].tutorApellidoMaterno,
      //     tutorParentesco :  jsonDatos[i].tutorParentesco,
      //     tutorApellidoPaterno :  jsonDatos[i].tutorApellidoPaterno});   
      //   } 
    });
  }

  validarDisciplina(nombre: String)
  {
    for(var i = 0 ; i < this.arrayDisciplinas.length ; i++) 
    {
      if(this.arrayDisciplinas[i].disciplinaNombre ==  nombre)
      {
        return true;
      }
    } 
    return false;
  }


  GetTodasLasdisciplinas()  
  {
    console.log("Obteniendo todas las disciplinas");

    this.httpServices.getDisciplinasActive().toPromise()
    .then
    (
      (data) => {
        if(data)
        {
          console.log("#*#*#Esta es la lista de disciplinas activas#*#*#");
          console.log(data);
          this.disciplinasInfo = data;
        }
        else
        {
          alert("No hay disciplinas para mostrar");
        }        
      }
    )
    .catch(
      (error) => {
        alert("Hubo un error al obtener la lista de disciplinas de la base de datos");
      }
    );
  }

  busquedaAlumno(newObj: any){    

    if(this.strBusqueda.length >= 3)
    {
      this.httpServices.searchAlumnos(this.strBusqueda).subscribe(
        datos => 
        {
          console.log("#*#*#*#*# BÚSQUEDA DE ALUMNOS #*#*#*#*#");
          console.log(datos);
          this.alumnosResultInfo = datos;  
          console.log("EN ALUMNOS RESULT INFO:")
          console.log(this.alumnosResultInfo)              
        },
        error =>
        {
          alert("Hubo un error al realizar la búsqeuda de alumnos. Intente más tarde: " + JSON.stringify(error));
        }
        );
    }
    else
    {
      this.alumnosResultInfo = [];      
    }
   
  }

  selectedAlumno(a:any){
    console.log("#*#*#**#*#*# ALUMNO SELECCIONADO #*#*#**#*#*#");
    console.log(a);
    
    this.id= a.alumnoId; 
    this.CURP = a .alumnoCURP;    
  
  }

  asignarDisciplina(id: number)
  {
    console.log("#*#*#*#*#*#*# Asignando disciplinaID #*#*#*#*#*#*#")
    this.dId = id;
    console.log(this.dId);
  }

  buscarEntradaSalida()
  {

    console.log("Buscando información de la búsqueda solicitada...");
     
    this.objAbuscar.id = this.id;
    this.objAbuscar.CURP = this.CURP;
    this.objAbuscar.disciplinaId= this.dId
    
    console.log(this.objAbuscar);
    console.log(this.id);
    console.log(this.CURP);
   
    if(this.id===0)
    {
      //ENTONCES VALIDAMOS QUE HAYA UNA DISCIPLINA SELECIONADA, FECHA DE INICIO Y FIN
      if(this.dId===0)
      {
        return alert("Debes seleccionar una disciplina.");
      }
      else if(this.objAbuscar.entradasalidaFechaEntrada ==="")
      {
        return alert("Debes seleecionar una fecha de inicio.");
      }
      else if (this.objAbuscar.entradasalidaFechaSalida === "")
      {
        return alert("Debes seleccionar una fecha de fin.")
      }
      else if(this.objAbuscar.entradasalidaFechaEntrada > this.objAbuscar.entradasalidaFechaSalida)
      {
        return alert("la fecha de inicio no debe ser mayor a la fecha de fin");
      }
      else
      {                          
        this.httpServices.getEntradaSalidaByDisciplina(this.objAbuscar).toPromise()
        .then(
          data =>
          {
            console.log("Limpiamos la lista de ingresos D")
            this.alumnosResultInfo = [];
            this.alumnosResultInfo = data;

            this.arrayShow = this.alumnosResultInfo;
            console.log("arrayShow: ");
            console.log(this.arrayShow);
            
          }
        )
        .catch(
          error => 
          {
            console.log("Hubo un error al traer la lista de entradas y salidas por disciplina");
          }
        )
        
      }

    }
    else
    {
      //AQUI SOLO BUSCAMOS EL ALUMNO CON LAS FECHAS INDICADAS        
      if(this.objAbuscar.entradasalidaFechaEntrada ==="")
      {
        return alert("Debes seleecionar una fecha de inicio.");
      }
      else if (this.objAbuscar.entradasalidaFechaSalida === "")
      {
        return alert("Debes seleccionar una fecha de fin.")
      }else if(this.objAbuscar.entradasalidaFechaEntrada > this.objAbuscar.entradasalidaFechaSalida)
      {
        return alert("la fecha de inicio no debe ser mayor a la fecha de fin");
      }
      else
      {
        //BUSCAMOS todas las entradas y salidas del alumno seleccionado en el rango de fechas estabecidas
         this.httpServices.getEntradaSalidaByCURP(this.objAbuscar).toPromise()
        .then(
          (data) => {
            console.log("#*#*#*#**# RESET DE LA LISTA DE ENTRADA #*#*#*#**#");
            this.alumnosResultInfo=[];
            this.arrayShow=[]

            this.alumnosResultInfo = data;
            this.arrayShow = this.alumnosResultInfo;
            console.log("arrayShow: ");
            console.log(this.arrayShow);
          }
        )
        .catch(
          (error) => {
            alert("Hubo un error al obtener los datos de las entradas y salidas por CURP.")
          }
        )        
      }
    }
  }

  btnLimpiar()
  {
    console.log("Limpiando controles de búsqueda");

    this.strBusqueda="";
    this.disciplinaObj.disciplinaId=0;
    
    this.id=0;
    this.CURP="";
    this.dId=0;
    // this.fi = "";
    // this.ff = "";

    this.alumnosResultInfo=[];

    this.objAbuscar.id = 0;
    this.objAbuscar.CURP ="";
    this.objAbuscar.disciplinaId = 0;
    this.objAbuscar.entradasalidaFechaEntrada ="";
    this.objAbuscar.entradasalidaFechaSalida ="";
 
    this.getRegisrosToday();

  }

}
