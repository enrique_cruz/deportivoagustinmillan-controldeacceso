import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAsistenciaProfesoresComponent } from './reporte-asistencia-profesores.component';

describe('ReporteAsistenciaProfesoresComponent', () => {
  let component: ReporteAsistenciaProfesoresComponent;
  let fixture: ComponentFixture<ReporteAsistenciaProfesoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAsistenciaProfesoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAsistenciaProfesoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
