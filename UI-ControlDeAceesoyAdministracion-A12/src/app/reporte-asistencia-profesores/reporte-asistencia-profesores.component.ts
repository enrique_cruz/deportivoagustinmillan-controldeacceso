import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-reporte-asistencia-profesores',
  templateUrl: './reporte-asistencia-profesores.component.html',
  styleUrls: ['./reporte-asistencia-profesores.component.css']
})
export class ReporteAsistenciaProfesoresComponent implements OnInit {


  arrayDisciplinas: Array<{
    disciplinaId: number;    
    disciplinaNombre: string;
  }>= [];
  
  
  fecha1 = "";
  fecha2 = "";
  
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  strBusqueda = "";

  
  arrayResultadosReporte: Array<{
    nombre : string,
    entradah: string,
    salidah: string
    
  }> =[];
  
  
  instructorId = 0;
  
  
  arrayInstructores: Array<{
    instructorId: number;
    instructorNombre: string;
    instructorApellidoPaterno: string;
    instructorApellidoMaterno: string;
    instructorCURP: string;
    }>= [];
    
  

  
  constructor(private route: ActivatedRoute,private httpServices: ServicesService) {
    this.getInstructores();
    
   }

  ngOnInit(): void {
  }
  
  
  getAsistenciasInstructores(){
    var json = {
      "alumnoIdFk": Number(this.instructorId),
      "fechaEntrada": this.fecha1,
      "fechaSalida": this.fecha2
    }
    console.log(json);
    this.httpServices.getAsistenciaInstructores(json).subscribe(
      datos => {
        console.log("ya te traje el al");
        console.log(datos);
        this.arrayResultadosReporte = [];
        var json = JSON.parse(JSON.stringify(datos));
        for(var i = 0; i < json.length ; i++){
    
          
          this.arrayResultadosReporte.push({
                nombre : json[i].instructorNombre+" "+json[i].instructorApellidoPaterno+" "+json[i].instructorApellidoMaterno,
                entradah : this.formatDate(json[i].fechaentrada),
                salidah: this.formatDate(json[i].fechaSalida)
            }
          );
        }

      });
  }
  
  
  
  
  formatDate(date: string){
    if(date == null || date == undefined){
      return "";
    }
    if(date.split("T").length != 2){
      return ""
    }else{
      return date.split("T")[0] +" "+date.split("T")[1].substring(0,5);
    }
  }
  
  
  
  downloadReporte(){
    if(this.arrayResultadosReporte.length > 0){
      this.httpServices.exportAsExcelFile(this.arrayResultadosReporte, "reporte_asistencias_instructores");
    }else{
      console.log("El reporte esta vacio");
    }
  }
  
  
    
  getInstructores(){
    this.httpServices.getInstructoresAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayInstructores = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayInstructores.push({
          instructorId: jsonDatos[i].instructorId, 
          instructorNombre: jsonDatos[i].instructorNombre,
          instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno, 
          instructorApellidoMaterno: jsonDatos[i].instructorApellidoMaterno, 
          instructorCURP: jsonDatos[i].instructorCURP, 
       
        
        
        })  
    
        }
      });
  }
  

}
