import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeGuardiaComponent } from './home-guardia/home-guardia.component';
import { InformativaAccesoComponent } from './informativa-acceso/informativa-acceso.component';
import { FormsModule } from '@angular/forms';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { HomeAdminComponent } from './home-admin/home-admin.component';
import { MenuComponent } from './menu/menu.component';
import { UserCRUDComponent } from './user-crud/user-crud.component';
import { HttpClientModule } from '@angular/common/http';
import { RolesCrudComponent } from './roles-crud/roles-crud.component';
import { CookieService } from 'ngx-cookie-service';
import {MatSelectModule} from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { CategoriaComponent } from './categoria/categoria.component';
import {MatSliderModule} from '@angular/material/slider';
import { DisciplinaComponent } from './disciplina/disciplina.component';
import { GimnacioComponent } from './gimnacio/gimnacio.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import {MatStepperModule} from '@angular/material/stepper';
import {  ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { InstructoresComponent } from './instructores/instructores.component';
import { HoraiosComponent } from './horaios/horaios.component';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { CurrencyPipe, registerLocaleData } from '@angular/common';

import localeEs from '@angular/common/locales/es';
import { PlanesComponent } from './planes/planes.component';


import { NgxQRCodeModule } from 'ngx-qrcode2';

import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { IngresoAlumnoComponent } from './ingreso-alumno/ingreso-alumno.component';
import { InformesComponent } from './informes/informes.component';
import { SplashComponent } from './splash/splash.component';
import { AlumnosComponent } from './alumnos/alumnos.component';
import { LandingComponent } from './landing/landing.component';
import {MatTabsModule} from '@angular/material/tabs';
import { SalidaComponent } from './salida/salida.component';
import { ReportesComponent } from './reportes/reportes.component';
import { ReporteGimnaciosComponent } from './reporte-gimnacios/reporte-gimnacios.component';
import { ReporteDisciplinasComponent } from './reporte-disciplinas/reporte-disciplinas.component';
import { ReporteFinancieroComponent } from './reporte-financiero/reporte-financiero.component';
import { ReporteAlumnosComponent } from './reporte-alumnos/reporte-alumnos.component';
import { ReagendaComponent } from './reagenda/reagenda.component';
import { RentasComponent } from './rentas/rentas.component';
import { RentasAltaComponent } from './rentas-alta/rentas-alta.component';
import { RentasModificacionComponent } from './rentas-modificacion/rentas-modificacion.component';
import { ReporteAlumnoEntradaSalidaComponent } from './reporte-alumno-entrada-salida/reporte-alumno-entrada-salida.component';
import { ReporteAlumnoPagosAlumnoComponent } from './reporte-alumno-pagos-alumno/reporte-alumno-pagos-alumno.component';
import { ReporteAlumnosDisciplinaComponent } from './reporte-alumnos-disciplina/reporte-alumnos-disciplina.component';
import { ReporteaforodisciplinasComponent } from './reporteaforodisciplinas/reporteaforodisciplinas.component';
import { ReportesAlumnosDisciplinainscripcionComponent } from './reportes-alumnos-disciplinainscripcion/reportes-alumnos-disciplinainscripcion.component';
import { ReporteAsistenciaProfesoresComponent } from './reporte-asistencia-profesores/reporte-asistencia-profesores.component';
import { ReporteReposicionClasesAlumnosComponent } from './reporte-reposicion-clases-alumnos/reporte-reposicion-clases-alumnos.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RentasPagoComponent } from './rentas-pago/rentas-pago.component';
import { QrMaestroComponent } from './qr-maestro/qr-maestro.component';


registerLocaleData(localeEs);


@NgModule({
  declarations: [
    AppComponent,    
    LoginComponent,
    HomeGuardiaComponent,
    InformativaAccesoComponent,
    NuevoUsuarioComponent,
    HomeAdminComponent,
    MenuComponent,
    UserCRUDComponent,
    RolesCrudComponent,
    CategoriaComponent,
    DisciplinaComponent,
    GimnacioComponent,
    InstructoresComponent,
    HoraiosComponent,
    PlanesComponent,
    IngresoAlumnoComponent,
    InformesComponent,
    SplashComponent,
    AlumnosComponent,
    LandingComponent,
    SalidaComponent,
    ReportesComponent,
    ReporteGimnaciosComponent,
    ReporteDisciplinasComponent,
    ReporteFinancieroComponent,
    ReporteAlumnosComponent,
    ReagendaComponent,
    RentasComponent,
    RentasAltaComponent,
    RentasModificacionComponent,
    ReporteAlumnoEntradaSalidaComponent,
    ReporteAlumnoPagosAlumnoComponent,
    ReporteAlumnosDisciplinaComponent,
    ReporteaforodisciplinasComponent,
    ReportesAlumnosDisciplinainscripcionComponent,
    ReporteAsistenciaProfesoresComponent,
    ReporteReposicionClasesAlumnosComponent,
    RentasPagoComponent,
    QrMaestroComponent
  ],
  imports: [
    MatDialogModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatSliderModule,
    NgbModule,
    MatIconModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    NgxQRCodeModule,
    MatTabsModule,
    ZXingScannerModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    
  ],
  providers: [
    CookieService,
    CurrencyPipe
  
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }


/*
  
  !A%D*G-PISSAPeShVmYq3t6w9z5v8y/B?E(H+MbkXp2$CPISSANcQfTjWnZKbPeS
  
*/ 