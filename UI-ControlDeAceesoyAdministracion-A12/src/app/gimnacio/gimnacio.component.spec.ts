import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GimnacioComponent } from './gimnacio.component';

describe('GimnacioComponent', () => {
  let component: GimnacioComponent;
  let fixture: ComponentFixture<GimnacioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GimnacioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GimnacioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
