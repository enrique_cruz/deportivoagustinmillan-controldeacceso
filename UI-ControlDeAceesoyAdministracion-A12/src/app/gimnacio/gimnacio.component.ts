import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-gimnacio',
  templateUrl: './gimnacio.component.html',
  styleUrls: ['./gimnacio.component.css']
})
export class GimnacioComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  visibleCancelar = 1;
  strBusqueda="";
  gimnasio = {
    gimnasioId : 0,
    gimnasioNombre : "",
    isActive : true,
    gimnasioActivo : ""
  } 

  
  arrayGimnasio:Array<{ 
 
    gimnasioId: number, 
    gimnasioNombre : "",
    gimnasioActivo : string,
    isActive: boolean }>=[];        
      
  arrayGimnasioShow:Array<{ 
 
    gimnasioId: number, 
    gimnasioNombre : "",
    gimnasioActivo : string,
    isActive: boolean }>=[];                      

  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    //this.getgimnasios();
    this.getGimnasios();
  }


  
  getGimnasios(){
    this.httpServices.getGimnaciosAll().subscribe(
      datos => {
        console.log("gym");
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayGimnasio = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayGimnasio.push({ 
          gimnasioId: jsonDatos[i].gimnasioId, 
          gimnasioNombre : jsonDatos[i].gimnasioNombre ,
          gimnasioActivo : jsonDatos[i].gimnasioActivo,
          isActive : this.calcularBooleano(jsonDatos[i].gimnasioActivo)});
        }
        this.arrayGimnasioShow = this.arrayGimnasio;
      });
  }
  
    
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
  
  
  setGimnasio(){
    if(this.gimnasio.gimnasioNombre == ""){
      alert("ingresa un nombre de gimnasio.");
    }else{
     if(this.visibleCancelar == 1){
      this.httpServices.setGimnacio(this.gimnasio).subscribe(
        datos => {
          console.log(datos);
     
          if(datos == 1){
            this.getGimnasios();
            //this.btnCancelar();
          }else{
            alert("El nombre del gimnasio ya existe");
          }
        });
     } else{
       console.log("Actualizó gimnasio");
       this.updategimnasio()
     }
     
    }
  }
  
  btnCancelar(){
    this.gimnasio.isActive = true;
    this.gimnasio.gimnasioActivo = "";
    this.gimnasio.gimnasioNombre = "";
    this.gimnasio.gimnasioId = 0;
    this.visibleCancelar = 1; 
  }
  
  btnEditar(json : any){
    this.gimnasio.isActive = json.isActive;
    this.gimnasio.gimnasioActivo = json.gimnasioActivo;
    this.gimnasio.gimnasioNombre = json.gimnasioNombre;
    this.gimnasio.gimnasioId = json.gimnasioId;
    this.visibleCancelar = 0; 
  }
  
  updategimnasio(){
    this.httpServices.updateGimnacio(this.gimnasio).subscribe(
  datos => {
    console.log(datos);
    if(datos == 1){
      alert("Datos guardados correctamente")
      this.getGimnasios();
      this.btnCancelar();
    }else{
      alert("El gimnasio ya existe");
    }
  });
}
 



activarDesactivar(gimnasio : any){
  var send = {
    "gimnasioId" : gimnasio.gimnasioId,
    "gimnasioActivo": "SI" 
  };
  if(gimnasio.isActive){
    send.gimnasioActivo = "SI";
  }else{
    send.gimnasioActivo = "NO";
  }
  gimnasio.gimnasioActivo = send.gimnasioActivo;
  
  console.log(send);
  this.httpServices.activeInactiveGimnacio(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){
        if(gimnasio.isActive){
          gimnasio.gimnasioActivo = "NO";
          gimnasio.isActive = false;
          
        }else{
          gimnasio.gimnasioActivo = "SI";
          gimnasio.isActive = true;
        }
        alert("Error a desactivar el gimnasio");
      }
      
      
      
    },
    error => {
      console.log("hola desde el error"); 
      console.log(error); 
      if(gimnasio.isActive){
        gimnasio.gimnasioActivo = "NO";
        gimnasio.isActive = false;
        
      }else{
        gimnasio.gimnasioActivo = "SI";
        gimnasio.isActive = true;
      }
      
    });
  }

buscarGimnasio(){
  this.arrayGimnasioShow = [];
  if(this.strBusqueda.length >= 2){   
    for(var i = 0 ; i < this.arrayGimnasio.length ; i++){
      if(this.arrayGimnasio[i].gimnasioNombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
        this.arrayGimnasioShow.push({
          gimnasioId: this.arrayGimnasio[i].gimnasioId, 
          gimnasioNombre : this.arrayGimnasio[i].gimnasioNombre ,
          gimnasioActivo : this.arrayGimnasio[i].gimnasioActivo,
          isActive : this.calcularBooleano(this.arrayGimnasio[i].gimnasioActivo)});
       }
     }
  }else{
    this.arrayGimnasioShow = this.arrayGimnasio;
  }
}




  
}
