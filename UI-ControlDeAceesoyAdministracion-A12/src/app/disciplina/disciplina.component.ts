import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-disciplina',
  templateUrl: './disciplina.component.html',
  styleUrls: ['./disciplina.component.css']
})
export class DisciplinaComponent implements OnInit {
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  visibleCancelar = 1;
  strBusqueda="";
  
  disciplina = {
    disciplinaId : 0,
    disciplinaNombre : "",
    isActive : true,
    disciplinaActivo : ""
  }

  
  arrayDisciplinas:Array<{ 
 
    disciplinaId: number, 
    disciplinaNombre : "",
    disciplinaActivo : string,
    isActive: boolean }>=[];   
    
    arrayDisciplinasShow:Array<{ 
 
      disciplinaId: number, 
      disciplinaNombre : "",
      disciplinaActivo : string,
      isActive: boolean }>=[];   

  constructor(private httpServices: ServicesService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.getDisciplinas();
  }

  
  getDisciplinas(){
    this.httpServices.getDisciplinasAll().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
      
         
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre ,
          disciplinaActivo : jsonDatos[i].disciplinaActivo,
          isActive : this.calcularBooleano(jsonDatos[i].disciplinaActivo)});
        }
        
        this.arrayDisciplinasShow = this.arrayDisciplinas;
      });
  }
  
    
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }
  
  
   
  setDisciplina(){
    if(this.disciplina.disciplinaNombre == ""){
      alert("ingresa un nombre de disciplina.");
    }else{
     if(this.visibleCancelar == 1){
      this.httpServices.setDisciplina(this.disciplina).subscribe(
        datos => {
          console.log(datos);
     
          if(datos == 1){
            this.getDisciplinas();
            this.btnCancelar();
          }else{
            alert("La disciplina ya existe.");
          }
        });
     } else{
       console.log("actualizo disciplina");
       alert("Actualización correcta.")
       this.updateDisciplina()
     }
     
    }
  }
  
  updateDisciplina(){
    this.httpServices.updateDisciplina(this.disciplina).subscribe(
  datos => {
    console.log(datos);
    if(datos == 1){
      this.getDisciplinas();
      this.btnCancelar();
    }else{
      alert("La disciplina ya existe!");
    }
  });
}

  
btnCancelar(){
  this.disciplina.isActive = true;
  this.disciplina.disciplinaActivo = "";
  this.disciplina.disciplinaNombre = "";
  this.disciplina.disciplinaId = 0;
  this.visibleCancelar = 1; 
}

btnEditar(json : any){
  this.disciplina.isActive = json.isActive;
  this.disciplina.disciplinaActivo = json.disciplinaActivo;
  this.disciplina.disciplinaNombre = json.disciplinaNombre;
  this.disciplina.disciplinaId = json.disciplinaId;
  this.visibleCancelar = 0; 
}

activarDesactivar(disciplina : any){
  var send = {
    "disciplinaId" : disciplina.disciplinaId,
    "disciplinaActivo": "SI" 
  };
  if(disciplina.isActive){
    send.disciplinaActivo = "SI";
  }else{
    send.disciplinaActivo = "NO";
  }
  disciplina.disciplinaActivo = send.disciplinaActivo;
  
  console.log(send);
  this.httpServices.activeInactiveDisciplina(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){
        if(disciplina.isActive){
          disciplina.disciplinaActivo = "NO";
          disciplina.isActive = false;
          
        }else{
          disciplina.disciplinaActivo = "SI";
          disciplina.isActive = true;
        }
        alert("error");
      }
      
      
      
    },
    error => {
      console.log("hola desde el error"); 
      console.log(error); 
      if(disciplina.isActive){
        disciplina.disciplinaActivo = "NO";
        disciplina.isActive = false;
        
      }else{
        disciplina.disciplinaActivo = "SI";
        disciplina.isActive = true;
      }
      
    });
  
}  

buscarDisciplina(){
  this.arrayDisciplinasShow = [];
  if(this.strBusqueda.length >= 2){   
    for(var i = 0 ; i < this.arrayDisciplinas.length ; i++){
      if(this.arrayDisciplinas[i].disciplinaNombre.toUpperCase().includes(this.strBusqueda.toUpperCase())){
        this.arrayDisciplinasShow.push({
          disciplinaId: this.arrayDisciplinas[i].disciplinaId, 
          disciplinaNombre : this.arrayDisciplinas[i].disciplinaNombre ,
          disciplinaActivo : this.arrayDisciplinas[i].disciplinaActivo,
          isActive : this.calcularBooleano(this.arrayDisciplinas[i].disciplinaActivo)});
       }
     }
  }else{
    this.arrayDisciplinasShow = this.arrayDisciplinas;
  }
}








}
