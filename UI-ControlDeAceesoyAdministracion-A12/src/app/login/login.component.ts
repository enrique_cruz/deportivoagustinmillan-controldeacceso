import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
usuario= "";
pass="";

permisosLst : any = [];
ruta : string ="";

showButton = true;


  constructor( private router: Router,private httpServices: ServicesService) { }

  ngOnInit(): void {
    
  }
  
  
  
  login(){
    this.showButton = false;
    this.httpServices.getRolPermisoByCorreoElectronico(this.usuario).subscribe(
      data=>{
        this.permisosLst = data
        console.log(this.permisosLst.length);
        console.log(this.permisosLst);

      if(this.permisosLst.length == 0){
        alert("Error: Verifica tus credenciales");
        this.showButton = true;
      }else{
        console.log(data);
        this.ruta = this.permisosLst[0].Ruta;
        console.log("Ruta");
        console.log(this.ruta);
        
        var usuario = {
          usuarioCorreoElectronico : this.usuario, 
          usuarioPassword : this.pass
        }    
        
        this.httpServices.login(usuario).subscribe(
          datos => {                
             var jsonDatos = JSON.parse(JSON.stringify(datos));
             if(jsonDatos.length >= 1){
              //this.router.navigate(['/usuarios', {usr:this.usuario}]);
              //this.router.navigate([this.ruta, {queryParams: {usr:this.usuario}}]);
              this.router.navigate([this.ruta], {queryParams: {usr:this.usuario}});
             }else{
               alert("Error: Verifica tus credenciales");
             }
             this.showButton = true;
          });
      }
        
        
      },
      error =>{
        console.log("Hubo un error al trater los permisos del usuario.");
        this.showButton = true;
      }
    );
    
    
    
    
    
    
    //console.log(this.usuario+" "+this.pass);
    //this.router.navigate(['homeGuardia']);
  }

  
  
  
}
