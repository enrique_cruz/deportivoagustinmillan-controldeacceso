import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ServicesService } from '../services/services.service';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';

import {
  startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';



const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  }
};


@Component({
  selector: 'app-horaios',
  templateUrl: './horaios.component.html',
  styleUrls: ['./horaios.component.css']
})
export class HoraiosComponent implements OnInit {
  
  @ViewChild('modalContent', { static: true }) modalContent!: TemplateRef<any>;
  permisos : any = this.route.snapshot.queryParamMap.get('usr');
  
  fechaDomingo = "";
  fechaLunes = "";
  fechaMartes = "";
  fechaMiercoles = "";
  fechaJueves = "";
  fechaViernes = "";
  fechaSabado = "";
  eventDelete: any;
  deleteModalNombre = "";
  deleteModalHoraInicio = "";
  deleteModalHoraFin = "";
  
  
  //*************************** */
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  locale =  'es';

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        //this.handleEvent('Edited', event);
      console.log("editar", JSON.parse(JSON.stringify(event)).objeto);
      var  objeto = JSON.parse(JSON.stringify(event)).objeto;
        
       //objeto.horarioDias = this.buscarEventosDias(objeto);
        this.btnEditar(objeto);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
      //  this.events = this.events.filter((iEvent) => iEvent !== event);
        //this.handleEvent('Deleted', event);
        this.eventDelete = event;
        this.deleteModalHoraFin = JSON.parse(JSON.stringify(event)).objeto.horarioFin;
        this.deleteModalHoraInicio = JSON.parse(JSON.stringify(event)).objeto.horarioInicio;
        this.deleteModalNombre = JSON.parse(JSON.stringify(event)).objeto.horarioNombre;
        
        this.modal.open(this.modalContent, { size: 'lg' });
        console.log("eliminar");
      },
    },
  ];

  dayStartHour = 5;
  dayEndHour = 23;
  events: CalendarEvent[] = [
 
    
  ];
  
  eventsShow: CalendarEvent[] = [
 
    
  ];
    
  //*************************** */
  visibleCancelar = 1;
  horario = {
    horarioId: 0,
    horarioNombre : "",
    disciplinaIdFk: 0,
    categoriaIdFk : 0,
    gimnasioIdFk : 0,
    instructorIdFk: 0,
    horarioInicio: "",
    horarioFin : "",
    horarioDias: "",
    horarioTurno: "",
    horarioActivo: "",
    horarioAforo: 0,
    isActive: true
  }
  
  
  
  lunes = false;
  martes = false;
  miercoles = false;
  jueves = false;
  viernes = false;
  sabado = false;
  domingo = false;
  
  
  arrayInstructores: Array<{
    instructorId: number;    
    instructorNombre: string;
    instructorApellidoPaterno: string;
    instructorApellidoMaterno: string;
    }>= [];
    
  arrayGimnasios: Array<{
      gimnasioId: number;    
      gimnasioNombre: string;
    }>= [];
    
  arrayDisciplinas: Array<{
      disciplinaId: number;    
      disciplinaNombre: string;
    }>= [];
      
  arrayCategorias: Array<{
      categoriaId: number;    
      categoriaNombre: string;
      categoriaEdadRango1: number;
      categoriaEdadRango2: number;
    }>= [];

    arrayHorarios: Array<{
      horarioId: 0,
      horarioNombre : string;
      disciplinaIdFk: number;
      categoriaIdFk : number;
      gimnasioIdFk : number;
      instructorIdFk: number;
      horarioInicio: string;
      horarioFin : string;
      horarioDias: string;
      horarioTurno: string;
      horarioActivo: "";
      isActive: boolean;
    }>= []
    
    
    
  constructor(private modal: NgbModal,private httpServices: ServicesService, private route: ActivatedRoute) {
    this.ultimoDomingo();

    this.horario.horarioAforo = this.permisos.value!;
   }

  ngOnInit(): void {
    this.getDisciplinas();
    this.getGimnacios();
    this.getCategorias();
    this.getInstructores();
    this.getHorarios();
  }
  
  setHorario(){
    //console.log(this.horario);
    this.horario.horarioDias = this.stringDiasBuild();
    if(this.validarDatos()){
      if(this.visibleCancelar  == 1){
        console.log("inserto");
        this.httpServices.setHorario(this.horario).subscribe(
          datos => {
            console.log(datos);      
            if(datos == 1){
              alert("Insercion Correcta!")
              this.getHorarios();
              //this.btnCancelar();
              this.reset()
            }else{
              alert("El horario ya existe!");
            }
          });
        
        
      }else{
        
        this.updateHorario();
      }
    }
    
  }
  
  updateHorario(){
    //this.horario.horarioDias = this.stringDiasBuild();
      console.log("actualizo");
      console.log(this.horario);
      
      var splitted = this.horario.horarioDias.split(",");
      if(splitted.length>1 && this.validarDatos())
      {
        alert("Solo debes elegir un dia a la vez");
      }
      else
      {
        this.httpServices.updateHorario(this.horario).subscribe(
          datos => {
            console.log(datos);
       
            if(datos == 1){
              alert("Actualizacion correcta!")
              this.getHorarios();
              this.reset()
            }else{
              alert("El horario ya existe!.");
            }
          },
          error => {
            alert("Hubo un erorr al actualizar el horario. Contacta al administrador");
          });
      }
  }
  
  
  getDisciplinas(){
    this.httpServices.getDisciplinasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayDisciplinas = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayDisciplinas.push({ 
          disciplinaId: jsonDatos[i].disciplinaId, 
          disciplinaNombre : jsonDatos[i].disciplinaNombre });   
        }
      });
  }
  
  
  getGimnacios(){
    this.httpServices.getGimnaciosActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayGimnasios = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayGimnasios.push({ 
          gimnasioId: jsonDatos[i].gimnasioId, 
          gimnasioNombre : jsonDatos[i].gimnasioNombre });   
        }
      });
  }

  
  getCategorias(){
    this.httpServices.getCategoriasActive().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayCategorias = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayCategorias.push({ 
          categoriaId: jsonDatos[i].categoriaId, 
          categoriaNombre : jsonDatos[i].categoriaNombre,
          categoriaEdadRango1:jsonDatos[i].categoriaEdadRango1,
          categoriaEdadRango2:jsonDatos[i].categoriaEdadRango2  });   
        }
      });
  }
  
  
  getInstructores(){
    this.httpServices.getInstructoresActivos().subscribe(
      datos => {
        console.log(datos);
         var jsonDatos = JSON.parse(JSON.stringify(datos));
         this.arrayInstructores = [];
         for(var i = 0 ; i < jsonDatos.length; i++ ){
          this.arrayInstructores.push({ 
          instructorId: jsonDatos[i].instructorId, 
          instructorNombre : jsonDatos[i].instructorNombre,
          instructorApellidoPaterno: jsonDatos[i].instructorApellidoPaterno,
          instructorApellidoMaterno : jsonDatos[i].instructorApellidoMaterno });   
        }
      });
  }
  
  getHorarios(){
    this.httpServices.getHorariosActivos().subscribe(
      datos => {
        console.log(datos);
        var jsonDatos = JSON.parse(JSON.stringify(datos));
        this.events = [];

        for(var i = 0 ; i < jsonDatos.length; i++ ){         
         this.events.push(this.crearJsonEvento(jsonDatos[i]));             
        }

        this.showFiltros();
        console.log(this.events);
      });
  }
  
  calcularBooleano(activo : string){
    if(activo === "SI"){
      return true;
    }else{
      return false;
    }
  }

stringDiasBuild(){
  var str = "";
  var array = [];
  if(this.lunes){
    array.push("Lunes")
  }
  if(this.martes){
    array.push("Martes")
  }
  if(this.miercoles){
    array.push("Miercoles")
  }
  if(this.jueves){
    array.push("Jueves")
  }
  if(this.viernes){
    array.push("Viernes")
  }
  if(this.sabado){
    array.push("Sabado")
  }
  if(this.domingo){
    array.push("Domingo")
  }
  str = array.join(",");
  
  return str;
  
}


reset(){

  this.horario.horarioId = 0;
  this.horario.horarioNombre = "";
  this.horario.disciplinaIdFk = 0;
  this.horario.categoriaIdFk = 0;
  this.horario.gimnasioIdFk = 0;
  this.horario.instructorIdFk = 0;
  this.horario.horarioInicio = "";
  this.horario.horarioFin = "";
  this.horario.horarioDias = "";
  this.horario.horarioTurno = "";
  this.horario.horarioActivo =  "";
  this.horario.horarioAforo =0;
  this.horario.isActive = true;
  this.lunes = false;
  this.martes = false; 
  this.miercoles = false;
  this.jueves = false;
  this.viernes = false;
  this.sabado = false;
  this.domingo = false;
  this.visibleCancelar = 1;
  
}


btnEditar(json : any){
  this.reset();
 
  this.visibleCancelar = 0;
  this.horario.horarioId = json.horarioId;;
  this.horario.horarioNombre = json.horarioNombre;
  this.horario.disciplinaIdFk = json.disciplinaIdFk;
  this.horario.categoriaIdFk = json.categoriaIdFk;
  this.horario.gimnasioIdFk = json.gimnasioIdFk;
  this.horario.instructorIdFk = json.instructorIdFk;
  this.horario.horarioInicio = json.horarioInicio;
  this.horario.horarioFin = json.horarioFin;
  this.horario.horarioDias = json.horarioDias;
  this.horario.horarioTurno = json.horarioTurno;
  this.horario.horarioActivo =  json.horarioActivo;
  this.horario.isActive = json.isActive;
  this.horario.horarioAforo = json.horarioAforo;
  this.calcularDiasCheack(json.horarioDias);
  
}

calcularDiasCheack(dias: string){
  console.log("split");
  console.log(dias)
  var array = dias.split(",");
  for(var i = 0 ; i < array.length; i++){
    if(array[i] === "Lunes"){
      this.lunes = true;
    }
    if(array[i] === "Martes"){
      this.martes = true;
    }
    if(array[i] === "Miercoles"){
      this.miercoles = true;
    }
    if(array[i] === "Jueves"){
      this.jueves = true;
    }
    if(array[i] === "Viernes"){
      this.viernes = true;
    }
    if(array[i] === "Sabado"){
      this.sabado = true;
    } 
    if(array[i] === "Domingo"){
      this.sabado = true;
    }   
  }
}



activarDesactivar(){
  var horario = JSON.parse(JSON.stringify(this.eventDelete)).objeto;
  var send = {
    "horarioId" : horario.horarioId,
    "horarioActivo": "No" 
  };
  
  console.log(send);
  this.httpServices.activeInactiveHorarios(send).subscribe(
    datos => {
      console.log(datos);
      if(datos==0){
        
        alert("error");
      }else{
        this.events = this.events.filter((iEvent) => iEvent !== this.eventDelete);
        this.modal.dismissAll();
        this.getHorarios();
      
      }
      
      
      
    });
  }

validarDatos(){
  if(this.horario.horarioAforo === 0 || this.horario.horarioAforo === undefined){
    alert("Ingresa el aforo del horario");
    return false;
  }else if(this.horario.disciplinaIdFk == 0){
    alert("Seleciona una disciplina para el horario");
    return false;
  }else if(this.horario.categoriaIdFk == 0){
    alert("Seleciona una categoria para el horario");
    return false;
  }else if(this.horario.gimnasioIdFk == 0){
    alert("Seleciona un gimnacio para el horario");
    return false;
  }else if(this.horario.instructorIdFk == 0){
    alert("Seleciona un instructor para el horario");
    return false;
  }else if(this.horario.horarioInicio === ""){
    alert("Ingresa una hora de inicio para el horario");
    return false;
  }else if(this.horario.horarioFin === ""){
    alert("Ingresa una hora de fin para el horario");
    return false;
  }else if(this.horario.horarioDias === ""){
    alert("Selecciona al menos un día para el horario");
    return false;
  }else if(this.horario.horarioTurno === ""){
    alert("Selecciona el turno del horario");
    return false;
  }else if(this.horario.horarioInicio>this.horario.horarioFin){
    alert("El horario de inicio no debe ser mayor al horario de fin");
    return false;
  }else if(this.horario.horarioInicio==this.horario.horarioFin){
    alert("El horario de inicio no debe ser igual al horario de fin");
    return false;
  }else{
    this.construirNombre();
    return true;
  }
}


ultimoDomingo(){
  var fecha = new Date(new Date().toISOString().split("T")[0]);
  // for(var i = 0 ; i < 7; i++){
  //   console.log(fecha.getDay() + " "+fecha);
  //   fecha.setDate(fecha.getDate()-1 );
  //   if(fecha.getDay){
      
  //   }
  // }
  
// var fecha = new Date("2021-10-31");
  while(fecha.getDay() != 0){
    console.log(fecha.getDay());
    fecha.setDate(fecha.getDate()-1 );
  }

  this.fechaDomingo = fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha domingo" +this.fechaDomingo+" "+fecha);

  fecha.setDate(fecha.getDate()+1);
  this.fechaLunes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha lunes" +this.fechaLunes);  

  fecha.setDate(fecha.getDate()+1);
  this.fechaMartes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha martes" +this.fechaMartes);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaMiercoles =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha miercoles" +this.fechaMiercoles);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaJueves =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha jueves" +this.fechaJueves);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaViernes =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha viernes" +this.fechaViernes);
  
  fecha.setDate(fecha.getDate()+1);
  this.fechaSabado =   fecha.getFullYear()+"-"+("0"+(fecha.getMonth()+1)).slice(-2)+"-"+("0"+fecha.getDate()).slice(-2);
  console.log("fecha sabado "+this.fechaSabado);  
  
  
  console.log("ultimo domingo");
  console.log(addHours(startOfDay(new Date()), 6));
}


crearJsonEvento(json: any){
  var fecha = "";
  switch(json.horarioDias){
    case "Lunes":
      fecha = this.fechaLunes;
    break;
    case "Martes":
      fecha = this.fechaMartes;
    break;
    case "Miercoles":
      fecha = this.fechaMiercoles;
    break;
    case "Jueves":
      fecha = this.fechaJueves;
    break;
    case "Viernes":
      fecha = this.fechaViernes;
    break;
    case "Sabado":
      fecha = this.fechaSabado;
    break;
    case "Domingo":
      fecha = this.fechaDomingo;
    break;
  }
  
  var fechaInicio = new Date(fecha+"T"+json.horarioInicio);
  var fechaFin = new Date(fecha+"T"+json.horarioFin);
  var json2 = {
   objeto: json,
    start: fechaInicio,
    end: fechaFin,
    title: json.horarioNombre,
    color: colors.blue,
    actions: this.actions,
    resizable: {
      beforeStart: true,
      afterEnd: true,
    },
    draggable: false,
  }
  
  return json2;
  
}


construirNombre()
{
  var nombre = "";
  for(var i  = 0; i < this.arrayDisciplinas.length;i++){
    if(this.arrayDisciplinas[i].disciplinaId == this.horario.disciplinaIdFk){
      nombre = this.arrayDisciplinas[i].disciplinaNombre;
      break;
    }
  }
  
  for(var i  = 0; i < this.arrayCategorias.length;i++){
    if(this.arrayCategorias[i].categoriaId == this.horario.categoriaIdFk){
      nombre = nombre +", "+ this.arrayCategorias[i].categoriaNombre;
      break;
    }
  }
  
  for(var i  = 0; i < this.arrayInstructores.length;i++){
    if(this.arrayInstructores[i].instructorId == this.horario.instructorIdFk){
      nombre = nombre+", "+this.arrayInstructores[i].instructorNombre+" "+this.arrayInstructores[i].instructorApellidoPaterno;
      break;
    }
  }
  
  for(var i  = 0; i < this.arrayGimnasios.length;i++){
    if(this.arrayGimnasios[i].gimnasioId == this.horario.gimnasioIdFk){
      nombre =nombre+", "+ this.arrayGimnasios[i].gimnasioNombre;
      break;
    }
  }
  
  this.horario.horarioNombre = nombre;
  
  
}


buscarEventosDias(busqueda:any ){
  console.log("busqeuda");
  console.log(busqueda);
  var arrayDias = [];
  for(var i =  0 ; i < this.events.length; i++){
    var json = JSON.parse(JSON.stringify(this.events[i]));
    console.log("busqeuda2");
    console.log(json);
    if(json.objeto.horarioNombre === busqueda.horarioNombre &&
        json.objeto.disciplinaIdFk === busqueda.disciplinaIdFk &&
        json.objeto.categoriaIdFk === busqueda.categoriaIdFk &&
        json.objeto.gimnasioIdFk === busqueda.gimnasioIdFk &&
        json.objeto.instructorIdFk === busqueda.instructorIdFk &&
        json.objeto.horarioInicio === busqueda.horarioInicio &&
        json.objeto.horarioFin === busqueda.horarioFin){
          console.log("si");
      arrayDias.push(json.objeto.horarioDias);
     
    }
  }
  var str = arrayDias.join(",");
  console.log(str);

  console.log("dias");
return str;
}


  showFiltros(){

    console.log("si entro a los filtros");
    this.eventsShow = [];
                
    if(this.horario.disciplinaIdFk != 0 && this.horario.gimnasioIdFk != 0 ){
      for(var i = 0;i <  this.events.length; i++){
        var json = JSON.parse(JSON.stringify(this.events[i])) ;
        if(this.horario.disciplinaIdFk ==  json.objeto.disciplinaIdFk && this.horario.gimnasioIdFk == json.objeto.gimnasioIdFk ){
          this.eventsShow.push(this.events[i]);
        }
        
      }
    
    }
    else
    {
      if(this.horario.disciplinaIdFk != 0  ){
        for(var i = 0;i <  this.events.length; i++){
          var json = JSON.parse(JSON.stringify(this.events[i])) ;
          if(this.horario.disciplinaIdFk ==  json.objeto.disciplinaIdFk){
            this.eventsShow.push(this.events[i]);
          }
        }
      }
      
      if(this.horario.gimnasioIdFk != 0 ){
        for(var i = 0;i <  this.events.length; i++){
          var json = JSON.parse(JSON.stringify(this.events[i])) ;
          if(this.horario.gimnasioIdFk == json.objeto.gimnasioIdFk ){
            this.eventsShow.push(this.events[i]);
          }
        }
      }          
    }
  }



}
