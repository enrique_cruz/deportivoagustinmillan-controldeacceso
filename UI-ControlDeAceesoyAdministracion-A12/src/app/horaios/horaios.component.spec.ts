import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HoraiosComponent } from './horaios.component';

describe('HoraiosComponent', () => {
  let component: HoraiosComponent;
  let fixture: ComponentFixture<HoraiosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HoraiosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoraiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
