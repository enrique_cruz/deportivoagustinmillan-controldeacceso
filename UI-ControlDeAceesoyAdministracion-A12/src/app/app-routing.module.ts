import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnosComponent } from './alumnos/alumnos.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { DisciplinaComponent } from './disciplina/disciplina.component';
import { GimnacioComponent } from './gimnacio/gimnacio.component';
import { HomeAdminComponent } from './home-admin/home-admin.component';
import { HomeGuardiaComponent } from './home-guardia/home-guardia.component';
import { HoraiosComponent } from './horaios/horaios.component';
import { InformativaAccesoComponent } from './informativa-acceso/informativa-acceso.component';
import { InformesComponent } from './informes/informes.component';
import { IngresoAlumnoComponent } from './ingreso-alumno/ingreso-alumno.component';
import { InstructoresComponent } from './instructores/instructores.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { PlanesComponent } from './planes/planes.component';
import { ReporteGimnaciosComponent } from './reporte-gimnacios/reporte-gimnacios.component';
import { ReportesComponent } from './reportes/reportes.component';
import { RolesCrudComponent } from './roles-crud/roles-crud.component';
import { SalidaComponent } from './salida/salida.component';
import { SplashComponent } from './splash/splash.component';
import { UserCRUDComponent } from './user-crud/user-crud.component';
import { ReagendaComponent } from './reagenda/reagenda.component';
import { RentasComponent } from './rentas/rentas.component';
import { RentasPagoComponent } from './rentas-pago/rentas-pago.component';
import { ReporteAlumnoEntradaSalidaComponent } from './reporte-alumno-entrada-salida/reporte-alumno-entrada-salida.component';
import { ReporteAlumnoPagosAlumnoComponent } from './reporte-alumno-pagos-alumno/reporte-alumno-pagos-alumno.component';
import { ReporteAlumnosDisciplinaComponent } from './reporte-alumnos-disciplina/reporte-alumnos-disciplina.component';
import { ReporteaforodisciplinasComponent } from './reporteaforodisciplinas/reporteaforodisciplinas.component';
import { ReportesAlumnosDisciplinainscripcionComponent } from './reportes-alumnos-disciplinainscripcion/reportes-alumnos-disciplinainscripcion.component';
import { ReporteAsistenciaProfesoresComponent } from './reporte-asistencia-profesores/reporte-asistencia-profesores.component';
import { ReporteReposicionClasesAlumnosComponent } from './reporte-reposicion-clases-alumnos/reporte-reposicion-clases-alumnos.component';
import { ReporteFinancieroComponent } from './reporte-financiero/reporte-financiero.component';
import { QrMaestroComponent } from './qr-maestro/qr-maestro.component';



const routes: Routes = [
  { path: 'login', component: LoginComponent ,  pathMatch: 'full'},
  { path: 'nuevoAlumno', component: NuevoUsuarioComponent ,  pathMatch: 'full'},
  { path: 'reportesInstructores', component: ReportesComponent ,  pathMatch: 'full'},
  { path: 'reportesGimnacios', component: ReporteGimnaciosComponent ,  pathMatch: 'full'},
  { path: 'horarios', component: HoraiosComponent ,  pathMatch: 'full'},
  { path: 'homeGuardia', component: HomeGuardiaComponent ,  pathMatch: 'full'},
  { path: 'homeAdministrador', component: HomeAdminComponent ,  pathMatch: 'full'},
  { path: 'usuarios', component: UserCRUDComponent ,  pathMatch: 'full'},
  { path: 'roles', component: RolesCrudComponent ,  pathMatch: 'full'},
  { path: 'categorias', component: CategoriaComponent ,  pathMatch: 'full'},
  { path: 'disciplinas', component: DisciplinaComponent ,  pathMatch: 'full'},
  { path: 'gimnasios', component: GimnacioComponent ,  pathMatch: 'full'},
  { path: 'instructores', component: InstructoresComponent ,  pathMatch: 'full'},
  { path: 'ReporteEntradaSalidaAlumno', component: ReporteAlumnoEntradaSalidaComponent ,  pathMatch: 'full'},
  { path: 'ReportePagosAlumno', component: ReporteAlumnoPagosAlumnoComponent ,  pathMatch: 'full'},
  { path: 'ReporteAlumnosDisciplina', component: ReporteAlumnosDisciplinaComponent ,  pathMatch: 'full'},
  { path: 'ReporteAsistenciaProfesores', component: ReporteAsistenciaProfesoresComponent ,  pathMatch: 'full'},
  { path: 'ReporteAlumnosDisciplinaInscritos', component: ReportesAlumnosDisciplinainscripcionComponent ,  pathMatch: 'full'},
  { path: 'ReporteAforoDisciplina', component: ReporteaforodisciplinasComponent ,  pathMatch: 'full'},
  { path: 'ReporteReposicionClasesAlumno', component: ReporteReposicionClasesAlumnosComponent ,  pathMatch: 'full'},
  { path: 'ReporteFinanciero', component: ReporteFinancieroComponent ,  pathMatch: 'full'},
  { path: 'planes', component: PlanesComponent ,  pathMatch: 'full'},
  { path: 'Bienvenida/:idEntrada/:idSalida', component: IngresoAlumnoComponent ,  pathMatch: 'full'},
  { path: 'Salida/:idEntrada/:idSalida', component: SalidaComponent ,  pathMatch: 'full'},
  { path: 'Informes', component: InformesComponent ,  pathMatch: 'full'},
  { path: 'Alumnos', component: AlumnosComponent ,  pathMatch: 'full'},
  { path: 'Reagenda', component:  ReagendaComponent,  pathMatch: 'full'},
  { path: 'Rentas', component:  RentasComponent,  pathMatch: 'full'},
  { path: 'RentasPago', component:RentasPagoComponent, pathMatch:'full'},
  { path: 'QrMaestro', component:QrMaestroComponent, pathMatch:'full'},
  { path: '', component: LandingComponent ,  pathMatch: 'full'},
  { path: 'Splash', component: SplashComponent ,  pathMatch: 'full'},
  { path: 'informativa', component: InformativaAccesoComponent ,  pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


/*
ShVkYp3s6v9y$)H@7w!z%C*F-JdRgUkXp2s5v8y/A?D(G+McQfTjWmYq3t6w9z$CJQfTjWnZr4u7x!ASgVkYp3s6v9y$B&E)H@MbQeThWmZq4t7w!z%C*F-J5u8x/A?D

6v9y$)H@7w%C*FShVkYp3s-JdRgUkXp2s5v!z8y/A?D(G+Mu8x/A?cQfTjWmr4u7x!ASgv9y$B&E)H@MbQeThWmZq4YqVkYp3s63t6w9z$CJQfTjWnz%C*F-J5DZt7w!


*/ 