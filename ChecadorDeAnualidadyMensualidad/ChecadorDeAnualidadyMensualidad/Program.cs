﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ChecadorDeAnualidadyMensualidad
{
    class Program
    {

        static void Main(string[] args)
        {
            //private readonly IConfiguration _configuration;
            string SP_NAME = "DeshabilitarAnalidadMensualidadAtrasada";
            string CON_STR = ConfigurationManager.ConnectionStrings["CADAM_DB"].ConnectionString;
            int result = 0;

            SqlConnection con = new SqlConnection(CON_STR);

            try
            {
                Console.WriteLine("Proceso inciado...");
                SqlCommand cmd = new SqlCommand(SP_NAME, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                con.Open();
                result = (int)cmd.ExecuteScalar();

                if (result == 1)
                {
                    Console.WriteLine("Proceso terminado satisfactoriamente!");
                }
                else
                {
                    Console.WriteLine("Hubo un error en la actualización.");
                }

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message.ToString());
            }
            finally
            {
                con.Close();
            }
        }
    }
}
